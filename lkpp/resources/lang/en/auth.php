<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email/Username belum terdaftar, silakan klik Daftar.',
    'failed_username' => 'Email/Username belum terdaftar, silakan klik Daftar.',
    'failed_password' => 'Password yang Anda masukkan salah.',
    'notactivated' => 'Akun anda tidak aktif.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];

@extends('layout.app2')
@section('title')
	Kontak
@stop
@section('css')
<style>
	body{
		width: 100%;
	}

	.bg-top{
		width: 100%;
		background-image: url('{{ asset('assets/img/back.jpg') }}'); /* The image used */
		background-color: #cccccc; /* Used if the image is unavailable */
		height: 300px; /* You must set a specified height */
		background-position: center; /* Center the image */
		background-repeat: no-repeat; /* Do not repeat the image */
		background-size: cover; /* Resize the background image to cover the entire container */
	}	

	.info-txt{
		padding: 50px;
	}

	.info-txt hr{
		border-top: 1px solid rgba(0, 0, 0, 0.43);
	}

	.txt-tp{
		font-weight: 600;
	}

	.txt-btm{
		font-size: 22px;
	}

	.lbl-form{
		width: 100%;
		font-weight: 600;
		color: gray;
	}

	.card-head{
		color: white;
		padding: 10px 20px 10px 20px;
		background: #ff4141;
		margin: -1px;
	}

	.card-head h3{
		font-weight: 600;
	}

	.card-body{
		padding-bottom: 10px;
	}

	.card-bottom{
		flex: 1 1 auto;
		padding: 0px 20px 10px 20px;
		margin: 35px 0px;
	}

	.card{
		margin-top: -110px;
		-webkit-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		-moz-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
	}

	.card .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
	}

	.btn-log{
		background: #ff4141;
		font-weight: 600;
		color: white;
		width: 100px;
	}

	.btn-daf{
		background: slategray;
		font-weight: 600;
		color: white;
		width: 100px;
	}

	.p-daf{
		margin: 15px 0px;
	}

	@media(min-width: 320px) and (max-width: 768px) {
		.card{
			margin-top: 10px;
		} 
	}
	
	.running{
		height: 50px;
		background: #3a394e;
	}

	#type {
		margin-bottom: 15px;
		font-size: 18px;
		font-weight: 200;
		color: #ffffff;
	}
	
	@media screen and (min-width: 768px) {
		#type {
		font-size: 23px;
		}
	}

	.txt-running{
		line-height: 2;
	}

	span.badge{
		vertical-align: -webkit-baseline-middle;
		width: 100%;
		font-size: 16px;
		font-weight: 600;
		color: #3a394e;
	}

	.submit-btn{
		width: 100%;
		text-align: right;
	}

	.submit-btn button{
		margin: 10px;
	}

	ul {
		list-style-type: none;
	}
	.carousel-inner{
		max-height: 500px;
	}
</style>
@endsection
@section('content')
{{-- <div class="bg-top"></div> --}}
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="5000">
	<ol class="carousel-indicators">
                @foreach($data_banner as $key => $datas)
  				  @if($key == 0)
                  <li data-target="#carouselExampleSlidesOnly" data-slide-to="{{ $key}}" class="active"></li>
                  @else
                  <li data-target="#carouselExampleSlidesOnly" data-slide-to="{{$key}}" class=""></li>
                  @endif
                @endforeach
                </ol>
  <div class="carousel-inner">
  	@foreach($data_banner as $key => $datas)
  	@if($key == 0)
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('storage/data/banner/'.$datas->file)}}" alt="First slide">
    </div>
    @else
    <div class="carousel-item ">
      <img class="d-block w-100" src="{{ asset('storage/data/banner/'.$datas->file)}}" alt="First slide">
    </div>
    @endif
    @endforeach
  </div>
</div>
<div class="running">
	<div class="row txt-running container-fluid">
		<div class="col-md-1"  style="line-height: 2.5">
			<span class="badge badge-warning" style="margin-left: 15px;"><i class="fa fa-info"></i> Info</span>
		</div>
		<div class="col-md-11">
			<marquee direction="left" scrollamount="4" behavior="scroll"  onmouseover="this.stop()" width="100%"  onmouseout="this.start()" loop="infinite" >
				<div id="type">
				@foreach($text as $texts)
					{{ $texts->text }} <b style="color: #ff4141">||</b>  
				@endforeach
				</div>
			</marquee>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-md-12 info-txt">
			<h5 class="txt-tp">Kontak</h5><hr>
			<p class="txt-btm">
				{!! $kontak->text !!}
			</p>
		</div>
	</div>
</div>
@endsection
@section('js')
	<script>
		$('#refresh').click(function(){
			$.ajax({
				type:'GET',
				url:'refreshcaptcha',
				success:function(data){
					$(".captcha span").html(data.captcha);
				}
			});
		});
	</script>
@endsection
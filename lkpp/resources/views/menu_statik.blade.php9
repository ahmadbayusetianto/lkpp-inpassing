@extends('layout.app2')

@section('title')
Statistik Pelaksanaan Ujian
@stop

@section('css')
<style>
  body{
    width: 100%;
  }

  .bg-top{
    width: 100%;
    background-image: url('{{ asset('assets/img/back.jpg') }}'); /* The image used */
    background-color: #cccccc; /* Used if the image is unavailable */
    height: 300px; /* You must set a specified height */
    background-position: center; /* Center the image */
    background-repeat: no-repeat; /* Do not repeat the image */
    background-size: cover; /* Resize the background image to cover the entire container */
  }

  .info-txt{ 
    padding: 50px;
  }

  .info-txt hr{
    border-top: 1px solid rgba(0, 0, 0, 0.43);
  }

  .txt-tp{
    font-weight: 600;
  }

  .txt-btm{
    font-size: 22px;
  }

  .lbl-form{
    width: 100%;
    font-weight: 600;
    color: gray;
  }

  .card-head{
    color: white;
    padding: 10px 20px 10px 20px;
    background: #ff4141;
    margin: -1px;
  }

  .card-head h3{
    font-weight: 600;
  }

  .card-body{
    padding-bottom: 10px;
  }

  .card-bottom{
    flex: 1 1 auto;
    padding: 0px 20px 10px 20px;
    margin: 35px 0px;
  }

  .card{
    margin-top: -110px;
    -webkit-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
    -moz-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
    box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
  }

  .card .form-control{
    -webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    -moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
  }

  .btn-log{
    background: #ff4141;
    font-weight: 600;
    color: white;
    width: 100px;
  }

  .btn-daf{
    background: slategray;
    font-weight: 600;
    color: white;
    width: 100px;
  }

  .p-daf{
    margin: 15px 0px;
  }

  @media(min-width: 320px) and (max-width: 768px) {
    .card{
      margin-top: 10px;
    } 
  }
  .running{
    height: 50px;
    background: #3a394e;
  }

  #type {
    margin-bottom: 15px;
    font-size: 18px;
    font-weight: 200;
    color: #ffffff;
  }
  @media screen and (min-width: 768px) {
    #type {
      font-size: 23px;
    }
  }

  .txt-running{
    line-height: 2;
  }

  span.badge{
    vertical-align: -webkit-baseline-middle;
    width: 100%;
    font-size: 16px;
    font-weight: 600;
    color: #3a394e;
  }

  .submit-btn{
    width: 100%;
    text-align: right;
  }
  
  .submit-btn button{
    margin: 10px;
  }

  ul {
    list-style-type: none;
  }
  h4{
    color: #fff;
  }
</style>
@endsection

@section('content')
<div class="bg-top">

</div>
<div class="running">
  <div class="row txt-running container-fluid">
    <div class="col-md-1" style="line-height: 2.5;">
      <span class="badge badge-warning" style="margin-left: 15px;"><i class="fa fa-info"></i> Info</span>
    </div>
    <div class="col-md-11">
      <marquee direction="left" scrollamount="4" behavior="scroll"  onmouseover="this.stop()" width="100%"  onmouseout="this.start()" loop="infinite" >
        <div id="type">
          @foreach($text as $texts)
          {{$texts->text }} <b style="color: #ff4141">||</b>  
          @endforeach
        </div>
      </marquee>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 col-md-12 info-txt">
      <h5 class="txt-tp">Statistik Pelaksanaan Ujian</h5>
      <hr>
      <p class="txt-btm">
        @php
        echo $statistik->text;
        @endphp
      </p>
    </div>
      <div class="col-md-4 info-txt" style="margin-top: 3%;">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header bg-success">
            <h4>Jumlah Instansi Pengusul</h4>
          </div>
          <div class="card-body">
            <canvas id="CHbar"></canvas>
            <span class="badge" style="background: #29B0D0;color: #fff"><div id="jumlah_daerah">   </div></span>
            <span class="badge" style="background: #2A516E;color: #fff"> <div id="jumlah_menteri">  </div></span>
          </div>
        </div>
      </div>
    </div>    
  </div>
  <div class="col-md-4 info-txt" style="margin-top: 3%;">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header bg-info">
            <h4>Jumlah Peserta</h4>
          </div>
          <div class="card-body">

            <canvas id="CHbar2"></canvas>
            <span class="badge" style="background: #F07124;color: #fff"><div id="jumlah_madya">   </div></span>
            <span class="badge" style="background: #CBE0E3;color: #fff"> <div id="jumlah_muda">  </div></span>
            <span class="badge" style="background: #979193;color: #fff"><div id="jumlah_pertama">   </div></span>
            
          </div>
        </div>
      </div>
    </div>     
  </div>
  <div class="col-md-4 info-txt" style="margin-top: 3%;">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header bg-danger">
            <h4>Jumlah Peserta Terdaftar</h4>
          </div>
          <div class="card-body">
            <canvas id="CHbar3"></canvas>
            <span class="badge" style="background: #a991d3;color: #fff"><div id="jumlah_madya_jadwal">   </div></span>
            <span class="badge" style="background: #a0ccf4;color: #fff"> <div id="jumlah_muda_jadwal">  </div></span>
            <span class="badge" style="background: #f094cc;color: #fff"><div id="jumlah_pertama_jadwal">   </div></span>
          </div>
        </div>
      </div>
    </div>     

  </div>
   <div class="col-md-6 info-txt" style="margin-top: 3%;">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header bg-warning">
            <h4>Hasil Peserta Ujian (Regular LKPP)</h4>
          </div>
          <div class="card-body">
            <div class="panel-group">
              <div class="panel panel-default">
                @foreach($jadwal as $keys => $jadwals)
                <div class="panel-heading">
                  <h4 class="panel-title">
                    @if($jadwals->metode == 'tes_tulis')
                    <a href="{{ url('menu-statik-inpassing')."/".$jadwals->id }}" class="btn btn-primary col-md-12">
                      {{ Helper::tanggal_indo($jadwals->tanggal_ujian).' - Tes Tertulis' }}</a>
                      @else
                      <a href="{{ url('menu-statik-inpassing')."/".$jadwals->id }}" class="btn btn-success col-md-12">
                        {{ Helper::tanggal_indo($jadwals->tanggal_ujian).' - Verifikasi Portofolio' }}</a>
                        @endif
                      </h4>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>     
      </div>
      <div class="col-md-6 info-txt" style="margin-top: 3%;">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header bg-secondary">
            <h4>Hasil Peserta Ujian (Instansi)</h4>
          </div>
          <div class="card-body">
            <div class="panel-group">
              <div class="panel panel-default">
                @foreach($jadwal_instansi as $keys => $jadwals)
                <div class="panel-heading">
                  <h4 class="panel-title">
                    @if($jadwals->metode == 'tes_tulis')
                    <a href="{{ url('menu-statik-instansi')."/".$jadwals->id }}" class="btn btn-primary col-md-12">
                      {{ Helper::tanggal_indo($jadwals->tanggal_ujian).' - Tes Tertulis' }}</a>
                      @else
                      <a href="{{ url('menu-statik-instansi')."/".$jadwals->id }}" class="btn btn-success col-md-12">
                        {{ Helper::tanggal_indo($jadwals->tanggal_ujian).' - Verifikasi Portofolio' }}</a>
                        @endif
                      </h4>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>     
      </div> 
  </div>
</div>
@endsection

@section('js')
<!-- ChartJS -->



<!--<script>
  $.fn.typer = function(text, options){
    options = $.extend({}, {
      char: ' ',
      delay: 2000,
      duration: 1000,
      endless: true
    }, options || text);

    text = $.isPlainObject(text) ? options.text : text;

    var elem = $(this),
    isTag = false,
    c = 0;
    
    (function typetext(i) {
      var e = ({string:1, number:1}[typeof text] ? text : text[i]) + options.char,
      char = e.substr(c++, 1);

      if( char === '<' ){ isTag = true; }
      if( char === '>' ){ isTag = false; }
      elem.html(e.substr(0, c));
      if(c <= e.length){
        if( isTag ){
          typetext(i);
        } else {
          setTimeout(typetext, options.duration/10, i);
        }
      } else {
        c = 0;
        i++;
        
        if (i === text.length && !options.endless) {
          return;
        } else if (i === text.length) {
          i = 0;
        }
        setTimeout(typetext, options.delay, i);
      }
    })(0);
  };

  $('#type').typer([
    @foreach($text as $texts)
    '{{ $texts->text }}',
    @endforeach
    ]);
  
  </script>-->
  <script>
    $('#refresh').click(function(){
      alert('coba');
      $.ajax({
       type:'GET',
       url:'refreshcaptcha',
       success:function(data){
        $(".captcha span").html(data.captcha);
      }
    });
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      var jumlah_daerah = parseInt(@foreach($prov as $provs) {{ $provs->jumlah}} @endforeach)+parseInt(@foreach($kota as $kotas) {{ $kotas->jumlah}} @endforeach)+parseInt(@foreach($kab as $kabs) {{ $kabs->jumlah}} @endforeach);
      
      var lembaga_menteri = parseInt(@foreach($kementrian as $kementrians) {{ $kementrians->jumlah}} @endforeach)+parseInt(@foreach($lembaga as $lembagas) {{ $lembagas->jumlah}} @endforeach);

      $('#jumlah_daerah').html('Jumlah Pengusul Daerah : '+jumlah_daerah);
      $('#jumlah_menteri').html('Jumlah Pengusul Menteri : '+lembaga_menteri);

      @foreach($pesertaSistem as $pesertas)
@if($pesertas->jenjang == 'Madya')
var jumlah_madya = {{$pesertas->jumlah}};
$('#jumlah_madya').html('Jumlah Peserta Madya : '+jumlah_madya);
@endif
@if($pesertas->jenjang == 'Muda')
var jumlah_muda = {{$pesertas->jumlah}};
$('#jumlah_muda').html('Jumlah Peserta Muda : '+jumlah_muda);
@endif
@if($pesertas->jenjang == 'Pertama')
var jumlah_pertama = {{$pesertas->jumlah}};
$('#jumlah_pertama').html('Jumlah Peserta Pertama : '+jumlah_pertama);
@endif
@endforeach
   @foreach($pesertaSistemJadwal as $pesertas)
@if($pesertas->jenjang == 'Madya')
var jumlah_madya = {{$pesertas->jumlah}};
$('#jumlah_madya_jadwal').html('Jumlah Peserta Madya : '+jumlah_madya);
@endif
@if($pesertas->jenjang == 'Muda')
var jumlah_muda = {{$pesertas->jumlah}};
$('#jumlah_muda_jadwal').html('Jumlah Peserta Muda : '+jumlah_muda);
@endif
@if($pesertas->jenjang == 'Pertama')
var jumlah_pertama = {{$pesertas->jumlah}};
$('#jumlah_pertama_jadwal').html('Jumlah Peserta Pertama : '+jumlah_pertama);
@endif

      
      
@endforeach


    });
    $(function () {
      var jumlah_daerah = parseInt(@foreach($prov as $provs) {{ $provs->jumlah}} @endforeach)+parseInt(@foreach($kota as $kotas) {{ $kotas->jumlah}} @endforeach)+parseInt(@foreach($kab as $kabs) {{ $kabs->jumlah}} @endforeach);
      
      var lembaga_menteri = parseInt(@foreach($kementrian as $kementrians) {{ $kementrians->jumlah}} @endforeach)+parseInt(@foreach($lembaga as $lembagas) {{ $lembagas->jumlah}} @endforeach);
      

      var ctx = document.getElementById('CHbar').getContext('2d');
      new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ['Pemerintah Daerah','Lembaga/Kementrian'],
          datasets: [{
            label: '# of Total',
            data: [jumlah_daerah,lembaga_menteri],
            backgroundColor: ['#29B0D0',
            '#2A516E',
            '#F07124',
            '#CBE0E3',
            '#979193']
          }]
        },
        options: {
          legend: {
            display: false,
            labels: {
              display: false
            }
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true,
                fontSize: 10,
                max: 200
              }
            }],
            xAxes: [{
              ticks: {
                beginAtZero:true,
                fontSize: 11
              }
            }]
          }
        }
      });

    });
    $(function () {
// var jumlah_daerah = parseInt(@foreach($prov as $provs) {{ $provs->jumlah}} @endforeach)+parseInt(@foreach($kota as $kotas) {{ $kotas->jumlah}} @endforeach)+parseInt(@foreach($kab as $kabs) {{ $kabs->jumlah}} @endforeach);
// var lembaga_menteri = parseInt(@foreach($kementrian as $kementrians) {{ $kementrians->jumlah}} @endforeach)+parseInt(@foreach($lembaga as $lembagas) {{ $lembagas->jumlah}} @endforeach);


var ctx = document.getElementById('CHbar2').getContext('2d');
new Chart(ctx, {
  type: 'bar',
  data: {
    labels: [@foreach($pesertaSistem as $pesertas)
    '{{ $pesertas->jenjang}}',
    @endforeach],
    datasets: [{
      label: '# of Total',
      data: [@foreach($pesertaSistem as $pesertas)
      '{{ $pesertas->jumlah}}',
      @endforeach],
      backgroundColor: [
      '#F07124',
      '#CBE0E3',
      '#979193']
    }]
  },
  options: {
    legend: {
      display: false,
      labels: {
        display: false
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero:true,
          fontSize: 10,
          max: 200
        }
      }],
      xAxes: [{
        ticks: {
          beginAtZero:true,
          fontSize: 11
        }
      }]
    }
  }
});

});
    $(function () {
// var jumlah_daerah = parseInt(@foreach($prov as $provs) {{ $provs->jumlah}} @endforeach)+parseInt(@foreach($kota as $kotas) {{ $kotas->jumlah}} @endforeach)+parseInt(@foreach($kab as $kabs) {{ $kabs->jumlah}} @endforeach);
// var lembaga_menteri = parseInt(@foreach($kementrian as $kementrians) {{ $kementrians->jumlah}} @endforeach)+parseInt(@foreach($lembaga as $lembagas) {{ $lembagas->jumlah}} @endforeach);
var ctx = document.getElementById('CHbar3').getContext('2d');
new Chart(ctx, {
  type: 'bar',
  data: {
    labels: [
    @foreach($pesertaSistemJadwal as $pesertas)

    '{{ $pesertas->jenjang}}',
    @endforeach
    ],
    datasets: [{
      label: '# of Total',
      data: [@foreach($pesertaSistemJadwal as $pesertas)
      
      '{{ $pesertas->jumlah}}',
      @endforeach
      ],
      backgroundColor: [
      '#a991d3',
      '#a0ccf4',
      '#f094cc']
    }]
  },
  options: {
    legend: {
      display: false,
      labels: {
        display: false
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero:true,
          fontSize: 10,
          max: 200
        }
      }],
      xAxes: [{
        ticks: {
          beginAtZero:true,
          fontSize: 11
        }
      }]
    }
  }
});

});
    $(function () {

      @foreach($jadwal as $keys => $jadwals)
      var ctx = document.getElementById('CHbar4{{ $keys }}').getContext('2d');
      @endforeach
      new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: [
          'lulus'
          ],
          datasets: [{
            label: '# of Total',
            data: [

            @foreach($jadwal as $keys => $jadwals)
            @foreach($pesertajadwal as $pesertajadwals)
            @if($jadwals->id == $pesertajadwals->id_jadwal)

            '{{ $pesertajadwals->jumlah}}',
            @endif
            @endforeach
            @endforeach
            ],
            backgroundColor: [
            '#a991d3',
            '#a0ccf4',
            '#f094cc']
          }]
        },
        options: {
          legend: {
            display: false,
            labels: {
              display: false
            },

          },
          tooltips: {
            callbacks: {
              label: function(tooltipItem, data) {
                var dataset = data.datasets[tooltipItem.datasetIndex];
                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                var total = meta.total;
                var currentValue = dataset.data[tooltipItem.index];
                var percentage = parseFloat((currentValue/total*100).toFixed(1));
                return currentValue + ' (' + percentage + '%)';
              },
              title: function(tooltipItem, data) {
                return data.labels[tooltipItem[0].index];
              }
            }
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true,
                fontSize: 10,
                max: 80
              }
            }],
            xAxes: [{
              ticks: {
                beginAtZero:true,
                fontSize: 11
              }
            }]
          }
        }
      });

    });
  </script>

  @endsection
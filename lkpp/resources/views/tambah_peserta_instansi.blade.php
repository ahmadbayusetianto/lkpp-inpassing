@extends('layout.app2')
@section('title')
	Tambah Peserta Ujian Instansi
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}

	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px;
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px;
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default3{
		background: #fff;
		color: #000;
		font-weight: 500;
		width: 100px;
	}

	.row-input{
		padding-bottom: 10px;
	}

	.page div.verifikasi{
		display: none;
	}

	.btn-area{
		text-align: right;
	}

	.clickable-row:hover{
		cursor: pointer;
	}

	.checkmark {
		top: 0;
		left: 0;
		height: 25px;
		width: 25px;
		background-color: #00aaaa;
	}

	.modal .modal-dialog{
		max-width: 800px !important;
	}

	.line-center{
		line-height: 2.5;
	}
</style>
@endsection
@section('content')
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12" style="">
						<div class="row">
							<div class="col-md-9">
								{{--<h5>Pilih Peserta Ujian Instansi Tanggal {{ $jadwal->tanggal_tes_tertulis }}</h5>--}}
								<h5>Pilih Peserta Uji Kompetensi (Instansi) Tanggal {{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }} dengan Metode {{ $jadwal->metode == 'verifikasi_portofolio' ? 'Verifikasi Portofolio' : 'Tes Tertulis' }}</h5>
								@php
									$tanggal = $jadwal->batas_waktu_input;
									date_default_timezone_set("Asia/Jakarta");
									$date = $jadwal->batas_waktu_input;
									$today = strtotime("today midnight");

									if($today > strtotime($date)){
										$batas_input = "expired";
									} else {
										$batas_input = "active";
									}
								@endphp
								@if ($batas_input == "expired")
								<div class="alert alert-danger alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Ujian telah melewati batas akhir pendaftaran, silakan pilih tanggal ujian berikutnya.</strong>
								</div>
								@endif
								<hr>
								@if (session('msg'))
									@if (session('msg') == "penuh")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong> Ujian telah melewati batas kuota peserta, silakan pilih tanggal ujian yang lain.</strong>
										</div>
									@endif
									@if (session('msg') == "metode_kosong")
										<div class="alert alert-danger alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Anda Belum Memilih Peserta. Silakan cek pada kolom Daftarkan Peserta</strong>
										</div>
									@endif
									@if (session('msg') == "validated_berkas")
										<div class="alert alert-danger alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Anda Belum Mengunggah Portofolio. Silakan unggah berkas pada kolom Unggah Dokumen Portofolio</strong>
										</div>
									@endif
								@endif
								@if($jadwal->metode == 'verifikasi_portofolio')
									<div class="row alert-row">
										<div class="col-md-12">
											<div class="alert alert-danger" role="alert" style="text-align: justify;">
												Peserta dapat mengunggah Dokumen Portofolio untuk Jenjang di atasnya, tetapi tidak boleh Jenjang di bawahnya. Contoh: Calon JF PPBJ Pertama dapat mengunggah Dokumen Portofolio untuk JF PPBJ Muda/Madya, tetapi calon JF PPBJ Madya tidak boleh mengunggah Dokumen Portofolio JF PPBJ Pertama/Muda.
											</div>
										</div>
									</div>
								@endif
								<div class="row" style="margin-bottom: 1%;">
									<div class="col-md-12 text-center">
										<div class="badge bg-primary" style="color: #fff;padding:1%;">Sudah Mengunggah Surat Usulan</div>
										<div class="badge bg-success" style="color: #fff;padding:1%;">Terdaftar</div>
									</div>
								</div>
								@if (session('msg'))
									@if (session('msg') == "berhasil")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil simpan data</strong>
										</div>
									@elseif(session('msg') == "gagal")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal simpan data</strong>
										</div>
									@endif
								@endif
							</div>
							<div class="col-md-3" style="text-align:right">
								<i class="fa fa-bell"></i>
							</div>
							<div class="col-md-9 page">
								<div class="main-box" id="view">
									<div class="min-top">
										<div class="row">
											<div class="col-md-2 text-center">
												<b>Perlihatkan</b>
											</div>
											<div class="col-md-2">
												<select name='length_change' id='length_change' class="form-control form-control-sm">
													<option value='50'>50</option>
													<option value='100'>100</option>
													<option value='150'>150</option>
													<option value='200'>200</option>
												</select>
											</div>
											<div class="col-md-4 col-12">
												<div class="form-group" style="margin-bottom:0px !important">
													<div class="input-group input-group-sm">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-search"></i></span>
														</div>
														<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
													</div>
												</div>
											</div>
										</div>
									</div>
									<form action="" method="post">
									@csrf
										{{-- <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
											<input type="hidden" name="id_admin_ppk" value="{{ Auth::user()->id }}">
										--}}
										<input type="hidden" name="tanggal_uji" value="{{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }}">
										<input type="hidden" name="tanggal_ujian" value="{{ $jadwal->metode == 'tes_tulis' ? $jadwal->tanggal_tes : $jadwal->tanggal_verifikasi }}">
										<input type="hidden" name="lokasi_ujian" value="{{ $jadwal->lokasi_ujian }}">
										<input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
										<input type="hidden" name="id_admin_ppk" value="{{ Auth::user()->id }}">
										<div class="table-responsive">
											<table id="example2" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th width="5%">No</th>
														<th width="20%">Nama</th>
														<th width="15%">NIP</th>
														<th width="15%">Pangkat/Gol.</th>
														<th width="15%">Jenjang</th>
														<th width="5%">Daftarkan Peserta</th>
														{{-- @if($pesertaTidakLulus != "" || $pesertaTidakHadir != "" )
															<th width="25%"></th>
														@endif --}}
														@if ($jadwal->metode == 'verifikasi_portofolio')
															<th width="25%"></th>
														@endif
													</tr>
												</thead>
												<tbody>
												@php
													$no_urut = 1;
												@endphp
													@foreach ($peserta as $key => $pesertas)
														{{-- {{ $pesertas->id_pesertas }} --}}
														<tr>
															<td>{{ $no_urut++ }}</td>
															<td>{{ $pesertas->nama }}</td>
															<td>{{ $pesertas->nip }}</td>
															<td>{{ $pesertas->jabatan }}</td>
															<td>{{  ucwords($pesertas->jenjang) }}</td>
															@php
																$status_check = "";
																$status_color = "";
																if($jadwal->metode == 'verifikasi_portofolio'){
																	if($data != ""){
																		$chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
																		$chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
																		$chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
																		if($chechked1 != ""){
																			$status_check = "checked";
																			$status_color = "bg-success";
																			$metod = $chechked1->metode_ujian;
																		} elseif($chechked3 != "") {
																			$status_check = "checked";
																			$status_color = "bg-primary";
																			$metod = $chechked3->metode_ujian;
																		} elseif($chechked2 != "") {
																			$status_check = "";
																			$status_color = "bg-warning";
																			$metod = "";
																		} else {
																			$status_check = "";
																			$status_color = "";
																			$metod = "";
																		}
																	}
																}

																if($jadwal->metode == 'tes_tulis'){
																	if($data != ""){
																		$chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
																		$chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
																		if($chechked != ""){
																			$status_check = "checked";
																			$status_color = "bg-success";
																			$metod = $chechked->metode_ujian;
																		} elseif($chechked3 != "") {
																			$status_check = "checked";
																			// $status_color = "bg-success";
																			$metod = $chechked3->metode_ujian;
																		} else {
																			$status_check = "";
																			$status_color = "";
																			$metod = "";
																		}
																	}
																}
															@endphp
															@if($batas_input == 'active' || $status_check == "chechked")
															<td class="text-center {{ $status_color}}">
																<div class="checkbox">
																	<input type="hidden" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkboxx{{ $pesertas->id }}">
																	<input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox{{ $pesertas->id }}" {{ $status_check }}>
																	{{-- <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkboxDaftar"> --}}
															</td>
															@if ($jadwal->metode == 'verifikasi_portofolio')
															<td align="center">
																@if ($jadwal->metode == 'verifikasi_portofolio')
																	<form action="{{ url('unggah-dokumen-ujian-instansi') }}" method="post" id="unggah{{ $pesertas->id }}">
																	@csrf
																		<input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
																		<input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
																		<input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) {
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>
      @endif
                                {{-- @if ($jadwal->metode == 'tes_tulis')
                                      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                        <label>
                                                          Tes tertulis
                                                        </label>
                                                        @endif--}}
                                                      </td>
                                                      @endif
                                                      @elseif($jadwal->metode == 'tes_tulis')
      <td class="{{ $status_color}}"></td>
                                                      @endif
                                                      <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                                      <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                                      <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                                    </tr>
                                        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal2">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                              <form method="post" enctype="multipart/form-data" id="upload_form">
                                                @csrf
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Unggah Dokumen Portofolio cahne</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="container">
                                                    <input type="hidden" name="id" value="" id="id_peserta_input">
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_perencanaan_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pemilihan_pbj') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pbj_secara_swakelola') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
                                                  <button class="btn btn-sm btn-default1" type="submit">Unggah</button>
                                                </div>
                                              </form>
                                              </div>
                                            </div>
                                          </div> --}}
                                          @endforeach
                                          @foreach ($peserta_lulus_ulang as $key => $pesertas)
                                          @if($peserta_lulus_ulang != "" && $jadwal->metode == 'tes_tulis')
                                          <form action="" method="post"></form>
                                          <tr>
                                              <td>{{ $no_urut++ }}</td>
                                              <td>{{ $pesertas->nama }}</td>
                                              <td>{{ $pesertas->nip }}</td>
                                              <td>{{ $pesertas->jabatan }}</td>
                                              <td>{{ ucwords($pesertas->jenjang) }}</td>
                                              @php
                                                  $status_check = "";
                                                  $status_color = "";
                                                  $perbaikan_porto = "";
                                                  if($jadwal->metode == 'verifikasi_portofolio'){
                                                      if($data != ""){
                                                          $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                                          $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                                          $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                                          if($chechked1 != ""){
                                                              $status_check = "checked";
                                                              $status_color = "bg-success";
                                                              $metod = $chechked1->metode_ujian;
                                                              $perbaikanbaikan_porto = $chechked1->perbaikan_porto;
                                                          } elseif($chechked3 != "") {
                                                              $status_check = "checked";
                                                              $status_color = "bg-primary";
                                                              $metod = $chechked3->metode_ujian;
                                                              $perbaikan_porto = "";
                                                          } elseif($chechked2 != "") {
                                                              $status_check = "";
                                                              $status_color = "bg-warning";
                                                              $metod = "";
                                                              $perbaikan_porto = "";
                                                          } else {
                                                              $status_check = "";
                                                              $status_color = "";
                                                              $metod = "";
                                                          }
                                                      }
                                                  }

                                                  if($jadwal->metode == 'tes_tulis'){
                                                      if($data != ""){
                                                          $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                                          if($chechked != ""){
                                                              $status_check = "checked";
                                                              $status_color = "bg-success";
                                                              $metod = $chechked->metode_ujian;
                                                              $perbaikan_porto = "";
                                                          } else {
                                                              $status_check = "";
                                                              $perbaikan_porto = "";
                                                              $status_color = "";
                                                              $metod = "";
                                                          }
                                                      }
                                                  }
                                              @endphp
                                              @if ($batas_input == "active" || $perbaikan_porto == "open")
                                                  <td class="text-center {{ $status_color }}">
                                                      <div class="checkbox17">
                                                          <input type="hidden" class="" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox170{{ $pesertas->id }}">
                                                          <input type="hidden" name="tanggal_ujian" value="{{ $jadwal->tanggal_ujian }}">
                                                          <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox17{{ $pesertas->id }}" {{ $status_check }}>
                                                  </td>
                                                  @if ($jadwal->metode == 'verifikasi_portofolio')
                                                      <td align="center">
                                                      @if ($jadwal->metode == 'verifikasi_portofolio')
                                                          <form action="{{ url('unggah-dokumen-ujian') }}" method="post" id="unggah17{{ $pesertas->id }}">
                                                          @csrf
                                                              <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                                              <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                                              <input type="submit" value="Unggah Dokumen Portofolio" id="unggah17{{ $pesertas->id }}" form="unggah17{{ $pesertas->id }}" class="btn btn-secondary btn-sm"><br>
                                                          </form><br>
                                                      @endif
                                                      @if ($jadwal->metode == 'tes_tulis')
                                                          <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                          <label>Tes tertulis</label>
                                                      @endif
                                                      </td>
                                                  @endif
                                              @elseif($jadwal->metode == 'tes_tulis')
                                                  <td class="{{ $status_color}}"></td>
                                              @elseif($jadwal->metode == 'verifikasi_portofolio')
                                                  <td class="{{ $status_color}}"></td>
                                                  <td></td>
                                              @endif
                                              <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                              <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                              <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                          </tr>
                                      @endif
                                  @endforeach

                                          @foreach ($pesertaTidakLulus as $key => $pesertas)

                                    {{-- {{ $pesertas->id_pesertas }} --}}
                                    <tr>
                                      <td>{{ $no_urut++ }}</td>
                                      <td>{{ $pesertas->nama }}</td>
                                      <td>{{ $pesertas->nip }}</td>
                                      <td>{{ $pesertas->jabatan }}</td>
                                      <td>{{  ucwords($pesertas->jenjang) }}</td>
                                      @php
                                      $status_check = "";
                                      $status_color = "";
                                      if($jadwal->metode == 'verifikasi_portofolio'){
                                       if($data != ""){
                                        $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                        $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                        $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                        if($chechked1 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-success";
                                          $metod = $chechked1->metode_ujian;
                                        }
                                        elseif($chechked3 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-primary";
                                          $metod = $chechked3->metode_ujian;
                                        }elseif($chechked2 != ""){
                                          $status_check = "";
                                          $status_color = "bg-warning";
                                          $metod = "";
                                        }
                                        else{
                                          $status_check = "";
                                          $status_color = "";
                                          $metod = "";
                                        }
                                      }
                                    }
                                    if($jadwal->metode == 'tes_tulis'){
                                     if($data != ""){
                                       $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                       $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                       if($chechked != ""){
                                         $status_check = "checked";
                                         $status_color = "bg-success";
                                         $metod = $chechked->metode_ujian;
                                       }
                                       elseif($chechked3 != ""){
                                         $status_check = "checked";
                                          $status_color = "bg-primary";
                                         $metod = $chechked3->metode_ujian;
                                       }else{
                                         $status_check = "";
                                         $status_color = "";
                                         $metod = "";
                                       }
                                     }
                                   }
                                   @endphp
                                   @if($batas_input == 'active' || $status_check == "chechked")
                                   <td class="text-center {{ $status_color}}">
                                    <div class="checkbox6">

                                     <input type="hidden" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox66{{ $pesertas->id }}">
                                     <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox6{{ $pesertas->id }}" {{ $status_check }}>
                                     <a href="{{ url('unggah-surat-usulan-int/'.$jadwal->id.'/'.$pesertas->id)}}"><button type="button" id="usulan6{{ $pesertas->id}}" class="btn btn-primary btn-sm" style="-webkit-box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);
-moz-box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);
box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);">Unggah <br> Surat <br> Usulan</button></a>
                                     {{-- <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkboxDaftar"> --}}
                                   </td>
                                   @if ($jadwal->metode == 'verifikasi_portofolio')
                                   <td align="center">
                                    @if ($jadwal->metode == 'verifikasi_portofolio')
                                    <form action="{{ url('unggah-dokumen-ujian-instansi') }}" method="post" id="unggah{{ $pesertas->id }}">
                                      @csrf
                                      <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                      <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                      <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) {
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>
      @endif
                                {{-- @if ($jadwal->metode == 'tes_tulis')
                                      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                        <label>
                                                          Tes tertulis
                                                        </label>
                                                        @endif--}}
                                                      </td>
                                                      @endif
                                                      @elseif($jadwal->metode == 'tes_tulis')
      <td class="{{ $status_color}}"></td>
                                                      @endif
                                                       @if ($jadwal->metode == 'tes_tulis')
          {{-- <td align="center">
            <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">

          </td> --}}
            @endif
                                                      <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                                      <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                                      <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                                    </tr>
                                        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal2">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                              <form method="post" enctype="multipart/form-data" id="upload_form">
                                                @csrf
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Unggah Dokumen Portofolio cahne</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="container">
                                                    <input type="hidden" name="id" value="" id="id_peserta_input">
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_perencanaan_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pemilihan_pbj') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pbj_secara_swakelola') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
                                                  <button class="btn btn-sm btn-default1" type="submit">Unggah</button>
                                                </div>
                                              </form>
                                              </div>
                                            </div>
                                          </div> --}}
                                          @endforeach

                                           @foreach ($pesertaTidakHadir as $key => $pesertas)

                                    {{-- {{ $pesertas->id_pesertas }} --}}
                                    <tr>
                                      <td>{{ $no_urut++ }}</td>
                                      <td>{{ $pesertas->nama }}</td>
                                      <td>{{ $pesertas->nip }}</td>
                                      <td>{{ $pesertas->jabatan }}</td>
                                      <td>{{  ucwords($pesertas->jenjang) }}</td>
                                      @php
                                      $status_check = "";
                                      $status_color = "";
                                      if($jadwal->metode == 'verifikasi_portofolio'){
                                       if($data != ""){
                                        $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                        $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                        $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                        if($chechked1 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-success";
                                          $metod = $chechked1->metode_ujian;
                                        }
                                        elseif($chechked3 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-primary";
                                          $metod = $chechked3->metode_ujian;
                                        }elseif($chechked2 != ""){
                                          $status_check = "";
                                          $status_color = "bg-warning";
                                          $metod = "";
                                        }
                                        else{
                                          $status_check = "";
                                          $status_color = "";
                                          $metod = "";
                                        }
                                      }
                                    }
                                    if($jadwal->metode == 'tes_tulis'){
                                     if($data != ""){
                                       $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                       $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                       if($chechked != ""){
                                         $status_check = "checked";
                                         $status_color = "bg-success";
                                         $metod = $chechked->metode_ujian;
                                       }
                                       elseif($chechked3 != ""){
                                         $status_check = "checked";
                                         $status_color = "bg-primary";
                                         $metod = $chechked3->metode_ujian;
                                       }else{
                                         $status_check = "";
                                         $status_color = "";
                                         $metod = "";
                                       }
                                     }
                                   }
                                   @endphp
                                   @if($batas_input == 'active' || $status_check == "chechked")
                                   <td class="text-center {{ $status_color}}">
                                    <div class="checkbox7">

                                     <input type="hidden" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox77{{ $pesertas->id }}">
                                     <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox7{{ $pesertas->id }}" {{ $status_check }}>
                                     <a href="{{ url('unggah-surat-usulan-int/'.$jadwal->id.'/'.$pesertas->id)}}"><button type="button" id="usulan7{{ $pesertas->id}}" class="btn btn-primary btn-sm" style="-webkit-box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);
-moz-box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);
box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);">Unggah <br> Surat <br> Usulan</button></a>
                                     {{-- <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkboxDaftar"> --}}
                                   </td>
                                   @if ($jadwal->metode == 'verifikasi_portofolio')
                                   <td align="center">
                                    @if ($jadwal->metode == 'verifikasi_portofolio')
                                    <form action="{{ url('unggah-dokumen-ujian-instansi') }}" method="post" id="unggah{{ $pesertas->id }}">
                                      @csrf
                                      <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                      <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                      <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) {
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>
      @endif
                                {{-- @if ($jadwal->metode == 'tes_tulis')
                                      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                        <label>
                                                          Tes tertulis
                                                        </label>
                                                        @endif--}}
                                                      </td>
                                                      @endif
                                                      @elseif($jadwal->metode == 'tes_tulis')
      <td class="{{ $status_color}}"></td>
                                                      @endif
                                                       @if ($jadwal->metode == 'tes_tulis')
          {{-- <td align="center">
            <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">

          </td> --}}
            @endif
                                                      <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                                      <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                                      <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                                    </tr>
                                        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal2">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                              <form method="post" enctype="multipart/form-data" id="upload_form">
                                                @csrf
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Unggah Dokumen Portofolio cahne</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="container">
                                                    <input type="hidden" name="id" value="" id="id_peserta_input">
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_perencanaan_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pemilihan_pbj') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pbj_secara_swakelola') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
                                                  <button class="btn btn-sm btn-default1" type="submit">Unggah</button>
                                                </div>
                                              </form>
                                              </div>
                                            </div>
                                          </div> --}}
                                          @endforeach

                                           @foreach ($pesertaTerdaftar_usul as $key => $pesertas)

                                    {{-- {{ $pesertas->id_pesertas }} --}}
                                    <tr>
                                      <td>{{ $no_urut++ }}</td>
                                      <td>{{ $pesertas->nama }}</td>
                                      <td>{{ $pesertas->nip }}</td>
                                      <td>{{ $pesertas->jabatan }}</td>
                                      <td>{{  ucwords($pesertas->jenjang) }}</td>
                                      @php
                                      $status_check = "";
                                      $status_color = "";
                                      if($jadwal->metode == 'verifikasi_portofolio'){
                                       if($data != ""){
                                        $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                        $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                        $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                        if($chechked1 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-success";
                                          $metod = $chechked1->metode_ujian;
                                        }
                                        elseif($chechked3 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-primary";
                                          $metod = $chechked3->metode_ujian;
                                        }elseif($chechked2 != ""){
                                          $status_check = "";
                                          $status_color = "bg-warning";
                                          $metod = "";
                                        }
                                        else{
                                          $status_check = "";
                                          $status_color = "";
                                          $metod = "";
                                        }
                                      }
                                    }
                                    if($jadwal->metode == 'tes_tulis'){
                                     if($data != ""){
                                       $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                       $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                       if($chechked != ""){
                                         $status_check = "checked";
                                         $status_color = "bg-success";
                                         $metod = $chechked->metode_ujian;
                                       }
                                       elseif($chechked3 != ""){
                                         $status_check = "checked";
                                         $status_color = "bg-primary";
                                         $metod = $chechked3->metode_ujian;
                                       }else{
                                         $status_check = "";
                                         $status_color = "";
                                         $metod = "";
                                       }
                                     }
                                   }
                                   @endphp
                                   @if($batas_input == 'active' || $status_check == "chechked")
                                   <td class="text-center {{ $status_color}}">
                                    <div class="checkbox8">

                                     <input type="hidden" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox88{{ $pesertas->id }}">
                                     <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox8{{ $pesertas->id }}" {{ $status_check }}>
                                     <a href="{{ url('unggah-surat-usulan-int/'.$jadwal->id.'/'.$pesertas->id)}}"><button type="button" id="usulan8{{ $pesertas->id}}" class="btn btn-primary btn-sm" style="-webkit-box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);
-moz-box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);
box-shadow: -1px 2px 16px -2px rgba(0,0,0,0.44);">Unggah <br> Surat <br> Usulan</button></a>
                                     {{-- <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkboxDaftar"> --}}
                                   </td>
                                   @if ($jadwal->metode == 'verifikasi_portofolio')
                                   <td align="center">
                                    @if ($jadwal->metode == 'verifikasi_portofolio')
                                    <form action="{{ url('unggah-dokumen-ujian-instansi') }}" method="post" id="unggah{{ $pesertas->id }}">
                                      @csrf
                                      <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                      <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                      <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) {
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>
      @endif
                                {{-- @if ($jadwal->metode == 'tes_tulis')
                                      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                        <label>
                                                          Tes tertulis
                                                        </label>
                                                        @endif--}}
                                                      </td>
                                                      @endif
                                                      @elseif($jadwal->metode == 'tes_tulis')
      <td class="{{ $status_color}}"></td>
                                                      @endif
                                                       @if ($jadwal->metode == 'tes_tulis')
          {{-- <td align="center">
            <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">

          </td> --}}
            @endif
                                                      <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                                      <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                                      <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                                    </tr>
                                        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal2">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                              <form method="post" enctype="multipart/form-data" id="upload_form">
                                                @csrf
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Unggah Dokumen Portofolio cahne</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="container">
                                                    <input type="hidden" name="id" value="" id="id_peserta_input">
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_perencanaan_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pemilihan_pbj') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pbj_secara_swakelola') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
                                                  <button class="btn btn-sm btn-default1" type="submit">Unggah</button>
                                                </div>
                                              </form>
                                              </div>
                                            </div>
                                          </div> --}}
                                          @endforeach


                                          @foreach ($pesertaTerdaftar_tl as $key => $pesertas)
                                          {{-- {{ $pesertas->id_pesertas }} --}}
                                          <tr>
                                            <td>{{ $no_urut++ }}</td>
                                            <td>{{ $pesertas->nama }}</td>
                                            <td>{{ $pesertas->nip }}</td>
                                            <td>{{ $pesertas->jabatan }}</td>
                                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                                            @php
                                            $status_check = "";
                                            $status_color = "";
                                            if($jadwal->metode == 'verifikasi_portofolio'){
                                             if($data != ""){
                                              $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                              $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                              $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                              if($chechked1 != ""){
                                                $status_check = "checked";
                                                $status_color = "bg-success";
                                                $metod = $chechked1->metode_ujian;
                                              }
                                              elseif($chechked3 != ""){
                                                $status_check = "checked";
                                                $status_color = "bg-primary";
                                                $metod = $chechked3->metode_ujian;
                                              }elseif($chechked2 != ""){
                                                $status_check = "";
                                                $status_color = "bg-warning";
                                                $metod = "";
                                              }
                                              else{
                                                $status_check = "";
                                                $status_color = "";
                                                $metod = "";
                                              }
                                            }
                                          }
                                          if($jadwal->metode == 'tes_tulis'){
                                           if($data != ""){
                                             $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                             $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                             if($chechked != ""){
                                               $status_check = "checked";
                                               $status_color = "bg-success";
                                               $metod = $chechked->metode_ujian;
                                             }
                                             elseif($chechked3 != ""){
                                         $status_check = "checked";
                                         // $status_color = "bg-success";
                                         $metod = $chechked3->metode_ujian;
                                       }else{
                                               $status_check = "";
                                               $status_color = "";
                                               $metod = "";
                                             }
                                           }
                                         }
                                         @endphp
                                         @if($batas_input == 'active' || $status_check == "chechked")
                                         <td class="text-center {{ $status_color}}">
                                          <div class="checkbox4">
                                           <input type="hidden" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox44{{ $pesertas->id }}">
                                           <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox4{{ $pesertas->id }}" {{ $status_check }}>
                                           {{-- <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkboxDaftar"> --}}
                                         </td>
                                         @if ($jadwal->metode == 'verifikasi_portofolio')
                                         <td align="center">
                                          @if ($jadwal->metode == 'verifikasi_portofolio')
                                          <form action="{{ url('unggah-dokumen-ujian-instansi') }}" method="post" id="unggah{{ $pesertas->id }}">
                                            @csrf
                                            <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                            <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                            <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) {
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>
      @endif
                                {{-- @if ($jadwal->metode == 'tes_tulis')
                                      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                        <label>
                                                          Tes tertulis
                                                        </label>
                                                        @endif--}}
                                                      </td>
                                                      @endif
                                                      @elseif($jadwal->metode == 'tes_tulis')
      <td class="{{ $status_color}}"></td>
                                                      @endif
                                                      <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                                      <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                                      <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                                    </tr>
                                        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal2">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                              <form method="post" enctype="multipart/form-data" id="upload_form">
                                                @csrf
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Unggah Dokumen Portofolio cahne</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="container">
                                                    <input type="hidden" name="id" value="" id="id_peserta_input">
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_perencanaan_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pemilihan_pbj') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pbj_secara_swakelola') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
                                                  <button class="btn btn-sm btn-default1" type="submit">Unggah</button>
                                                </div>
                                              </form>
                                              </div>
                                            </div>
                                          </div> --}}
                                          @endforeach

                                          @if($pesertaTerdaftar_sm != "" && $pesertaTerdaftar != "")
                                          @foreach ($pesertaTerdaftar_sm as $key => $pesertas)
                                          {{--  {{ $pesertas->id_pesertas }} --}}
                                          <tr>
                                            <td>{{ $no_urut++ }}</td>
                                            <td>{{ $pesertas->nama }}</td>
                                            <td>{{ $pesertas->nip }}</td>
                                            <td>{{ $pesertas->jabatan }}</td>
                                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                                            @php
                                            $status_check = "";
                                            $status_color = "";
                                            if($jadwal->metode == 'verifikasi_portofolio'){
                                             if($data != ""){
                                              $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                              $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                              $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                              if($chechked1 != ""){
                                                $status_check = "checked";
                                                $status_color = "bg-success";
                                                $metod = $chechked1->metode_ujian;
                                              }
                                              elseif($chechked3 != ""){
                                                $status_check = "checked";
                                                $status_color = "bg-primary";
                                                $metod = $chechked3->metode_ujian;
                                              }elseif($chechked2 != ""){
                                                $status_check = "";
                                                $status_color = "bg-warning";
                                                $metod = "";
                                              }
                                              else{
                                                $status_check = "";
                                                $status_color = "";
                                                $metod = "";
                                              }
                                            }
                                          }
                                          if($jadwal->metode == 'tes_tulis'){
                                           if($data != ""){
                                             $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                             $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                             if($chechked != ""){
                                               $status_check = "checked";
                                               $status_color = "bg-success";
                                               $metod = $chechked->metode_ujian;
                                             }
                                             elseif($chechked3 != ""){
                                               $status_check = "checked";
                                               // $status_color = "bg-success";
                                               $metod = $chechked3->metode_ujian;
                                             }else{
                                               $status_check = "";
                                               $status_color = "";
                                               $metod = "";
                                             }
                                           }
                                         }
                                         @endphp
                                         @if($batas_input == 'active' || $status_check == "chechked")
                                         <td class="text-center {{ $status_color}}">
                                          <div class="checkbox2">
                                           <input type="hidden" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox22{{ $pesertas->id }}">
                                           <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox2{{ $pesertas->id }}" {{ $status_check }}>
                                           {{-- <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkboxDaftar"> --}}
                                         </td>
                                         @if ($jadwal->metode == 'verifikasi_portofolio')
                                         <td align="center">
                                          @if ($jadwal->metode == 'verifikasi_portofolio')
                                          <form action="{{ url('unggah-dokumen-ujian-instansi') }}" method="post" id="unggah{{ $pesertas->id }}">
                                            @csrf
                                            <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                            <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                            <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) {
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>
      @endif
                               {{--  @if ($jadwal->metode == 'tes_tulis')
                                      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                        <label>
                                                          Tes tertulis
                                                        </label>
                                                        @endif--}}
                                                      </td>
                                                      @endif
                                                      @elseif($jadwal->metode == 'tes_tulis')
      <td class="{{ $status_color}}"></td>
                                                      @endif
                                                      <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                                      <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                                      <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                                    </tr>
                                        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal2">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                              <form method="post" enctype="multipart/form-data" id="upload_form">
                                                @csrf
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Unggah Dokumen Portofolio cahne</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="container">
                                                    <input type="hidden" name="id" value="" id="id_peserta_input">
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_perencanaan_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pemilihan_pbj') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pbj_secara_swakelola') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
                                                  <button class="btn btn-sm btn-default1" type="submit">Unggah</button>
                                                </div>
                                              </form>
                                              </div>
                                            </div>
                                          </div> --}}
                                          @endforeach
                                          @elseif($pesertaTerdaftar_sm != "")
                                          @foreach ($pesertaTerdaftar_sm as $key => $pesertas)
                                          {{--  {{ $pesertas->id_pesertas }} --}}
                                          <tr>
                                            <td>{{ $no_urut++ }}</td>
                                            <td>{{ $pesertas->nama }}</td>
                                            <td>{{ $pesertas->nip }}</td>
                                            <td>{{ $pesertas->jabatan }}</td>
                                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                                            @php
                                      $status_check = "";
                                      $status_color = "";
                                      if($jadwal->metode == 'verifikasi_portofolio'){
                                       if($data != ""){
                                        $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                        $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                        $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                        if($chechked1 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-success";
                                          $metod = $chechked1->metode_ujian;
                                        }
                                        elseif($chechked3 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-primary";
                                          $metod = $chechked3->metode_ujian;
                                        }elseif($chechked2 != ""){
                                          $status_check = "";
                                          $status_color = "bg-warning";
                                          $metod = "";
                                        }
                                        else{
                                          $status_check = "";
                                          $status_color = "";
                                          $metod = "";
                                        }
                                      }
                                    }
                                    if($jadwal->metode == 'tes_tulis'){
                                     if($data != ""){
                                       $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                       $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                       if($chechked != ""){
                                         $status_check = "checked";
                                         $status_color = "bg-success";
                                         $metod = $chechked->metode_ujian;
                                       }
                                       if($chechked != ""){
                                         $status_check = "checked";
                                         // $status_color = "bg-success";
                                         $metod = $chechked3->metode_ujian;
                                       }else{
                                         $status_check = "";
                                         $status_color = "";
                                         $metod = "";
                                       }
                                     }
                                   }
                                   @endphp
                                   @if($batas_input == 'active' || $status_check == "chechked")
                                   <td class="text-center {{ $status_color}}">
                                    <div class="checkbox3">
                                               <input type="hidden" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox33{{ $pesertas->id }}">
                                               <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox3{{ $pesertas->id }}" {{ $status_check }}>
                                               {{-- <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkboxDaftar"> --}}
                                             </td>
                                             @if ($jadwal->metode == 'verifikasi_portofolio')
                                             <td align="center">
                                              @if ($jadwal->metode == 'verifikasi_portofolio')
                                              <form action="{{ url('unggah-dokumen-ujian-instansi') }}" method="post" id="unggah{{ $pesertas->id }}">
                                                @csrf
                                                <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                                <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                                <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) {
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>
      @endif
                            {{--     @if ($jadwal->metode == 'tes_tulis')
                                      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                        <label>
                                                          Tes tertulis
                                                        </label>
                                                        @endif--}}
                                                      </td>
                                                      @endif
                                                      @elseif($jadwal->metode == 'tes_tulis')
      <td class="{{ $status_color}}"></td>
                                                      @endif
                                                      <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                                      <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                                      <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                                    </tr>
                                        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal2">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                              <form method="post" enctype="multipart/form-data" id="upload_form">
                                                @csrf
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Unggah Dokumen Portofolio cahne</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="container">
                                                    <input type="hidden" name="id" value="" id="id_peserta_input">
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_perencanaan_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pemilihan_pbj') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pbj_secara_swakelola') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
                                                  <button class="btn btn-sm btn-default1" type="submit">Unggah</button>
                                                </div>
                                              </form>
                                              </div>
                                            </div>
                                          </div> --}}
                                          @endforeach
                                          @elseif($pesertaTerdaftar != "")
                                          @foreach ($pesertaTerdaftar as $key => $pesertas)
                                          {{-- {{ $pesertas->id_pesertas }} --}}
                                          <tr>
                                            <td>{{ $no_urut++ }}</td>
                                            <td>{{ $pesertas->nama }}</td>
                                            <td>{{ $pesertas->nip }}</td>
                                            <td>{{ $pesertas->jabatan }}</td>
                                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                                            @php
                                      $status_check = "";
                                      $status_color = "";
                                      if($jadwal->metode == 'verifikasi_portofolio'){
                                       if($data != ""){
                                        $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                        $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                        $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                        if($chechked1 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-success";
                                          $metod = $chechked1->metode_ujian;
                                        }
                                        elseif($chechked3 != ""){
                                          $status_check = "checked";
                                          $status_color = "bg-primary";
                                          $metod = $chechked3->metode_ujian;
                                        }elseif($chechked2 != ""){
                                          $status_check = "";
                                          $status_color = "bg-warning";
                                          $metod = "";
                                        }
                                        else{
                                          $status_check = "";
                                          $status_color = "";
                                          $metod = "";
                                        }
                                      }
                                    }
                                    if($jadwal->metode == 'tes_tulis'){
                                     if($data != ""){
                                       $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                       $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                       if($chechked != ""){
                                         $status_check = "checked";
                                         $status_color = "bg-success";
                                         $metod = $chechked->metode_ujian;
                                       }
                                       elseif($chechked3 != ""){
                                          $status_check = "checked";
                                          // $status_color = "bg-primary";
                                          $metod = $chechked3->metode_ujian;
                                        }else{
                                         $status_check = "";
                                         $status_color = "";
                                         $metod = "";
                                       }
                                     }
                                   }
                                   @endphp
                                   @if($batas_input == 'active' || $status_check == "chechked")
                                   <td class="text-center {{ $status_color}}">
                                    <div class="checkbox1">
                                               <input type="hidden" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox11{{ $pesertas->id }}">
                                               <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox1{{ $pesertas->id }}" {{ $status_check }}>
                                               {{-- <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkboxDaftar"> --}}
                                             </td>
                                             @if ($jadwal->metode == 'verifikasi_portofolio')
                                             <td align="center">
                                              @if ($jadwal->metode == 'verifikasi_portofolio')
                                              <form action="{{ url('unggah-dokumen-ujian-instansi') }}" method="post" id="unggah{{ $pesertas->id }}">
                                                @csrf
                                                <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                                <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                                <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) {
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>
      @endif
                               {{--  @if ($jadwal->metode == 'tes_tulis')
                                      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
                                                        <label>
                                                          Tes tertulis
                                                        </label>
                                                        @endif--}}
                                                      </td>
                                                      @endif
                                                      @elseif($jadwal->metode == 'tes_tulis')
      <td class="{{ $status_color}}"></td>
                                                      @endif
                                                      <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
                                                      <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
                                                      <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
                                                    </tr>
                                        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal2">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                              <form method="post" enctype="multipart/form-data" id="upload_form">
                                                @csrf
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Unggah Dokumen Portofolio cahne</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="container">
                                                    <input type="hidden" name="id" value="" id="id_peserta_input">
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_perencanaan_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pemilihan_pbj') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                                                        <div class="col-md-1 line-center">:</div>
                                                        <div class="col-md-6">
                                                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="customFile" value="{{ old('kompetensi_pbj_secara_swakelola') }}" required>
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
                                                  <button class="btn btn-sm btn-default1" type="submit">Unggah</button>
                                                </div>
                                              </form>
                                              </div>
                                            </div>
                                          </div> --}}
                                          @endforeach
                                          @endif
                                        </tbody>
                                      </table>
                                    </div>
                                    <a href="{{ url('daftar-instansi')}} "><button type="button" class="btn btn-sm btn-default2" >Batal</button></a>
                                    @if ($batas_input == "active")
                                    <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
                                    @endif
                                  </form>
                                </div>
                              </div>

                              <div class="col-md-3 text-center">
                                @include('layout.button_right_kpp')
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endsection

@section('js')
<script type="text/javascript">
	$('#btn-tambah').click(function (e) {
		e.preventDefault();
		document.getElementById("verif").style.display = "block";
		document.getElementById("view").style.display = "none";
	});

	$('#btn-back').click(function (e) {
		e.preventDefault();
		document.getElementById("verif").style.display = "none";
		document.getElementById("view").style.display = "block";
	});

	jQuery(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});

    $(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

	@foreach ($peserta as $key => $pesertas)
		$('.checkbox #checkbox{{ $pesertas->id }}').change(function() {
		// this will contain a reference to the checkbox
		if (this.checked) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox #checkboxx{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox #checkboxx{{ $pesertas->id }}").attr("disabled",false);
		}
	});
	@endforeach

	$(document).ready(function(){
		// this will contain a reference to the checkbox
		@foreach ($peserta as $key => $pesertas)
		if ( $('.checkbox #checkbox{{ $pesertas->id }}').attr('checked')) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox #checkboxx{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox #checkboxx{{ $pesertas->id }}").attr("disabled",false);
		}
		@endforeach
	});

	@foreach ($pesertaTerdaftar as $key => $pesertas)
	$('.checkbox1 #checkbox{{ $pesertas->id }}').change(function() {
		// this will contain a reference to the checkbox
		if (this.checked) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox1 #checkbox11{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox1 #checkbox11{{ $pesertas->id }}").attr("disabled",false);
		}
	});
	@endforeach

	$(document).ready(function(){
		// this will contain a reference to the checkbox
		@foreach ($pesertaTerdaftar as $key => $pesertas)
		if ( $('.checkbox1 #checkbox1{{ $pesertas->id }}').attr('checked')) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox1 #checkbox11{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox1 #checkbox11{{ $pesertas->id }}").attr("disabled",false);
		}
		@endforeach
	});

	@foreach ($pesertaTerdaftar_sm as $key => $pesertas)
	$('.checkbox2 #checkbox2{{ $pesertas->id }}').change(function() {
		// this will contain a reference to the checkbox
		if (this.checked) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox2 #checkbox22{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox2 #checkbox22{{ $pesertas->id }}").attr("disabled",false);
		}
	});
	@endforeach

	$(document).ready(function(){
		// this will contain a reference to the checkbox
		@foreach ($pesertaTerdaftar_sm as $key => $pesertas)
		if ( $('.checkbox2 #checkbox2{{ $pesertas->id }}').attr('checked')) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox2 #checkbox22{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox2 #checkbox22{{ $pesertas->id }}").attr("disabled",false);
		}
		@endforeach
	});

	@foreach ($pesertaTerdaftar_sm as $key => $pesertas)
	$('.checkbox3 #checkbox3{{ $pesertas->id }}').change(function() {
		// this will contain a reference to the checkbox
		if (this.checked) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox3 #checkbox33{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox3 #checkbox33{{ $pesertas->id }}").attr("disabled",false);
		}
	});
	@endforeach

	$(document).ready(function(){
		// this will contain a reference to the checkbox
		@foreach ($pesertaTerdaftar_sm as $key => $pesertas)
		if ( $('.checkbox3 #checkbox3{{ $pesertas->id }}').attr('checked')) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox3 #checkbox33{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox3 #checkbox33{{ $pesertas->id }}").attr("disabled",false);
		}
		@endforeach
	});

	@foreach ($pesertaTidakLulus as $key => $pesertas)
	$('.checkbox6 #checkbox6{{ $pesertas->id }}').change(function() {
		// this will contain a reference to the checkbox
		if (this.checked) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$('#usulan6{{ $pesertas->id }}').show();
			$(".checkbox6 #checkbox66{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$('#usulan6{{ $pesertas->id }}').hide();
			$(".checkbox6 #checkbox66{{ $pesertas->id }}").attr("disabled",false);
		}
	});
	@endforeach

	$(document).ready(function(){
		// this will contain a reference to the checkbox
		@foreach ($pesertaTidakLulus as $key => $pesertas)
		if ( $('.checkbox6 #checkbox6{{ $pesertas->id }}').attr('checked')) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$('#usulan6{{ $pesertas->id }}').show();
			$(".checkbox6 #checkbox66{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$('#usulan6{{ $pesertas->id }}').hide();
			$(".checkbox6 #checkbox66{{ $pesertas->id }}").attr("disabled",false);
		}
		@endforeach
	});

	@foreach ($pesertaTidakHadir as $key => $pesertas)
	$('.checkbox7 #checkbox7{{ $pesertas->id }}').change(function() {
		// this will contain a reference to the checkbox
		if (this.checked) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$('#usulan7{{ $pesertas->id }}').show();
			$(".checkbox7 #checkbox77{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$('#usulan7{{ $pesertas->id }}').hide();
			$(".checkbox7 #checkbox77{{ $pesertas->id }}").attr("disabled",false);
		}
	});
	@endforeach

	$(document).ready(function(){
		// this will contain a reference to the checkbox
		@foreach ($pesertaTidakHadir as $key => $pesertas)
		if ( $('.checkbox7 #checkbox7{{ $pesertas->id }}').attr('checked')) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$('#usulan7{{ $pesertas->id }}').show();
			$(".checkbox7 #checkbox77{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$('#usulan7{{ $pesertas->id }}').hide();
			$(".checkbox7 #checkbox77{{ $pesertas->id }}").attr("disabled",false);
		}
		@endforeach
	});

	@foreach ($pesertaTerdaftar_usul as $key => $pesertas)
	$('.checkbox8 #checkbox8{{ $pesertas->id }}').change(function() {
		// this will contain a reference to the checkbox
		if (this.checked) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$('#usulan8{{ $pesertas->id }}').show();
			$(".checkbox8 #checkbox88{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$('#usulan8{{ $pesertas->id }}').hide();
			$(".checkbox8 #checkbox88{{ $pesertas->id }}").attr("disabled",false);
		}
	});
	@endforeach

	$(document).ready(function(){
		// this will contain a reference to the checkbox
		@foreach ($pesertaTerdaftar_usul as $key => $pesertas)
		if ( $('.checkbox8 #checkbox8{{ $pesertas->id }}').attr('checked')) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$('#usulan8{{ $pesertas->id }}').show();
			$(".checkbox8 #checkbox88{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$('#usulan8{{ $pesertas->id }}').hide();
			$(".checkbox8 #checkbox88{{ $pesertas->id }}").attr("disabled",false);
		}
		@endforeach
	});

	@foreach ($pesertaTerdaftar_tl as $key => $pesertas)
	$('.checkbox4 #checkbox4{{ $pesertas->id }}').change(function() {
		// this will contain a reference to the checkbox
		if (this.checked) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox4 #checkbox44{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox4 #checkbox44{{ $pesertas->id }}").attr("disabled",false);
		}
	});
	@endforeach

	$(document).ready(function(){
		// this will contain a reference to the checkbox
		@foreach ($pesertaTerdaftar_tl as $key => $pesertas)
		if ( $('.checkbox4 #checkbox4{{ $pesertas->id }}').attr('checked')) {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').show();
			$(".checkbox4 #checkbox44{{ $pesertas->id }}").attr("disabled",true);
		} else {
			var select = this.value;
			$('#unggah{{ $pesertas->id }}').hide();
			$(".checkbox4 #checkbox44{{ $pesertas->id }}").attr("disabled",false);
		}
		@endforeach
	});

	$(document).ready(function(){
		$('#upload_form').on('submit', function(event){
			event.preventDefault();
			$.ajax({
				url:"{{ url('upload-portofolio') }}",
				method:"POST",
				data:new FormData(this),
				dataType:'JSON',
				contentType: false,
				cache: false,
				processData: false,
				success:function(data){
					if(data.msg == 'validator'){
						alert('validator error');
					}

					if(data.msg == 'berhasil'){
						var nameid = "#id_portofolio_" + data.id;
						$('#myModal').modal('hide');
						$(nameid).val(data.id_db);
						alert('Berhasil Input');
					}

					if(data.msg == 'gagal'){
						alert('Gagal Input');
					}
				}
			})
		});
	});
</script>
@endsection

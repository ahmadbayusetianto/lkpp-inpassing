@extends('layout.app')
@section('title')
@if ($action == 'add')
	Tambah Banner
@elseif ($action == 'edit')
	Ubah Banner
@elseif ($action == 'detail')
	Detail Data Asesor
@endif
@stop
@section('css')
<style type="text/css">
	.form-control
	{
		border-radius: 5px;
		height: 27px;
		padding: 0px;
		padding-left: 10px;
	}

	.bin{
		color: #dd4b39;
	}

	.row{
		margin: 0px 15px 15px 15px; 
	}

	.main-box{
		font-weight: 600;
	}
</style>
@stop
@section('content')
@if ($action == 'add')
	<form action="" method="post" enctype="multipart/form-data">
	@csrf
		<div class="main-box">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<h3>Tambah Banner</h3><hr>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					File
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="file" class="form-control" name="file" value="{{ old('file') }}">
					<span class="errmsg">{{ $errors->first('file') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Publish
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<select class="form-control" id="select-single" name="publish">
						<option value="" disabled="" selected="">Pilih Status</option>
						<option value="0">Tidak</option>
						<option value="1">Ya</option>
					</select>
					<span class="errmsg">{{ $errors->first('publish') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9" style="text-align:right">
					<button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Batal</button> <button type="submit" class="btn btn-default1">Simpan</button>
				</div>
			</div>
		</div>
	</form>
@endif

@if ($action == 'edit')
	<form action="" method="post" enctype="multipart/form-data">
	@csrf
		<div class="main-box">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<h3>Edit Banner</h3><hr>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					File
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="file" class="form-control" name="file" value="{{ old('file') }}">
					<span class="errmsg">{{ $errors->first('file') }}</span>
					Foto Lama : 
					@if ($data->file == "")
						Tidak Ada foto
						@else
						@php
						$foto = explode('.',$data->file);
						@endphp
						@if ($foto[1] == 'pdf' || $foto[1] == 'PDF')
						<a href="{{ url('priview-file')."/banner/".$data->file }}" target="_blank"><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="Lihat Foto"></i></a>
						@else
						<a href="{{ url('priview-file')."/banner/".$data->file }}" target="_blank"><img src="{{ asset('storage/data/banner/'.$data->file) }}" alt="" class="img-responsive" width="150px"></a>
						@endif
						@endif
					<span class="errmsg">{{ $data->file }}</span>
					<input type="hidden" name="old_file" value="{{ $data->file }}">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Urutan
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="text" class="form-control" name="urutan" value="{{ $data->urutan }}" onkeypress='validate(event)' maxlength="3">
					<span class="errmsg">{{ $errors->first('urutan') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Publish
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<select class="form-control" id="select-single" name="publish">
						<option value="" disabled="">Pilih Status</option>
						<option {{ $data->publish == 0 ? 'selected' : ''}} value="0">Tidak</option>
						<option {{ $data->publish == 1 ? 'selected' : ''}} value="1">Ya</option>
					</select>
					<span class="errmsg">{{ $errors->first('publish') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9" style="text-align:right">
					<button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Batal</button> <button type="submit" class="btn btn-default1">Simpan</button>
				</div>
			</div>
		</div>
	</form>
@endif

@stop

@section('js')
<script type="text/javascript">
	function showPassword() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
	
	function showPassword2() {
		var x = document.getElementById("c_password");
		if (x.type === "password") {
		x.type = "text";
		} else {
			x.type = "password";
		}
	}
</script>
@endsection
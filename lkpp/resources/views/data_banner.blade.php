@extends('layout.app')
@section('title')
	Setting Banner
@stop
@section('css')
	<style>
	.btn-tbh{
		text-align: right;
	}

	.btn-jadwal{
		width: 120px;
		background: #E8382A;
		color: #fff;
		font-weight: 600;
	}

	.btn-jadwal:hover{
		color: aliceblue;
	}
	</style>
@stop
@section('content')
@if (session('msg'))
	@if (session('msg') == "berhasil")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil simpan data</strong>
				</div>
			</div>
		</div>
	@endif 
	
	@if (session('msg') == "gagal")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal simpan data</strong>
				</div>
			</div>
		</div> 
	@endif
	
	@if (session('msg') == "berhasil_update")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil ubah data</strong>
				</div>
			</div>
		</div>
	@endif 
	
	@if (session('msg') == "gagal_update")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal ubah data</strong>
				</div>
			</div>
		</div> 
	@endif

	@if (session('msg') == "berhasil_hapus")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil Hapus Data</strong>
				</div>
			</div>
		</div>
	@endif 
	
	@if (session('msg') == "gagal_hapus")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal Hapus Data</strong>
				</div>
			</div>
		</div> 
	@endif
@endif
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2 col-6">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-3 col-6">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
			<div class="col-md-6 col-6 btn-tbh">
			@if (Auth::user()->role == 'superadmin')
				<a href="{{ url('tambah-banner') }}"><button class="btn btn-sm btn-jadwal">Tambah Banner</button></a>
			@endif
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Gambar</th>
					<th>Urutan</th>
					<th>Publish</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			@php
				$no_urut = 1;
			@endphp
			@foreach ($data as $key => $datas)
			@if(Auth::user()->role == 'superadmin')
				<tr>
					<td>{{ $no_urut++  }}</td>
					<td>
						@if ($datas->file == "")
						Tidak Ada foto
						@else
						@php
						$foto = explode('.',$datas->file);
						@endphp
						@if ($foto[1] == 'pdf' || $foto[1] == 'PDF')
						<a href="{{ url('priview-file')."/banner/".$datas->file }}" target="_blank"><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="Lihat Foto"></i></a>
						@else
						<a href="{{ url('priview-file')."/banner/".$datas->file }}" target="_blank"><img src="{{ asset('storage/data/banner/'.$datas->file) }}" alt="" class="img-responsive" width="60px"></a>
						@endif
						@endif
					</td>
					<td>{{ $datas->urutan }}</td>
					<td>{{ $datas->publish == 0 ? 'Tidak' : 'Ya' }}</td>
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu">
								<li><a href="{{ url('edit-banner')."/".$datas->id }}">Ubah</a></li>
								<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
							</ul>
						</div>
					</td>
				</tr>
			@endif					
					<div class="modal fade" id="modal-default{{ $datas->id }}">
						<div class="modal-dialog" style="width:30%">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="modal-title">Hapus Banner</h4>
								</div>
								<div class="modal-body">
									<p>Apakah Anda yakin menghapus Banner?</p>
								</div>
								<div class="modal-footer">
									<a href="{{ url('delete-banner/'.$datas->id) }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
									<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
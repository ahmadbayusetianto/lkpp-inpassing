@extends('layout.app')

@section('title')
    Dashboard
@endsection

@section('css')
<style type="text/css">
	/* .carousel-inner>.item>img{
		max-height: 500px;
	} */
	.w-100{
		width: 100%;
	}
</style>
@stop

@section('content')
@if (session('msg'))
	@if (session('msg') == "berhasil")
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Berhasil simpan data</strong>
			</div>
		</div>
	</div>
	@endif

	@if (session('msg') == "gagal")
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Gagal simpan data</strong>
			</div>
		</div>
	</div>
	@endif
@endif

{{-- <div class="main-box"> --}}
	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid">
            <!-- /.box-header -->
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="5000" style="height:371px;overflow:hidden">
                <ol class="carousel-indicators" >
                @foreach($data_banner as $key => $datas)
  				  @if($key == 0)
                  <li data-target="#carousel-example-generic" data-slide-to="{{ $key}}" class="active"></li>
                  @else
                  <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class=""></li>
                  @endif
                @endforeach
                </ol>
                <div class="carousel-inner">
                	@foreach($data_banner as $key => $datas)
  	@if($key == 0)
    <div class="item active">
      <img class="d-block w-100" src="{{ asset('storage/data/banner/'.$datas->file)}}" alt="First slide">
    </div>
    @else
    <div class="item ">
      <img class="d-block w-100" src="{{ asset('storage/data/banner/'.$datas->file)}}" alt="First slide">
    </div>
    @endif
    @endforeach
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      </div>
		</div>
	</div>
	</div>
{{-- </div> --}}
	@stop

@section('title')

@endsection

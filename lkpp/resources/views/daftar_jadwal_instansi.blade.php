@extends('layout.app')
@section('title')
    Jadwal Uji Kompetensi (Instansi)
@endsection
@section('css')
<style>
    .dropdown-menu{
        left: -100px;
    }
</style>
@endsection
@section('content')
	@if (session('msg'))
		@if (session('msg') == "berhasil")
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Berhasil simpan data</strong>
			</div> 
		@elseif(session('msg') == "gagal")
			<div class="alert alert-warning alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Gagal simpan data</strong>
			</div> 
		@elseif (session('msg') == "berhasil_hapus")
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Berhasil hapus data</strong>
			</div> 
		@elseif(session('msg') == "gagal_hapus")
			<div class="alert alert-warning alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Gagal hapus data</strong>
			</div>
		@endif
	@endif
	
	<div class="main-box">
		<div class="min-top">
			<div class="row">
				<div class="col-md-1 text-center">
					<b>Perlihatkan</b>
				</div>
				<div class="col-md-2">
					<select name='length_change' id='length_change' class="form-control">
						<option value='50'>50</option>
						<option value='100'>100</option>
						<option value='150'>150</option>
						<option value='200'>200</option>
					</select>
				</div>
				<div class="col-md-4 col-12">
					<div class="input-group">
						<div class="input-group addon">
							<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
							<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
						</div>
					</div>
				</div>
				<div class="col-md-2"></div>
				<div class="col-md-1 text-center">
					<b>Bulan</b>
				</div>
				<div class="col-md-2 col-6">
					<form action="" method="post">
					@csrf
					<select name='bulan' id='bulan' class="form-control form-control-sm" onchange="this.form.submit()">
						<option value='' {{ $bulan == '' ? 'selected' : '' }}>Pilih Bulan</option>
						<option value='1' {{ $bulan == 1 ? 'selected' : '' }}>Januari</option>
						<option value='2' {{ $bulan == 2 ? 'selected' : '' }}>Februari</option>
						<option value='3' {{ $bulan == 3 ? 'selected' : '' }}>Maret</option>
						<option value='4' {{ $bulan == 4 ? 'selected' : '' }}>April</option>
						<option value='5' {{ $bulan == 5 ? 'selected' : '' }}>Mei</option>
						<option value='6' {{ $bulan == 6 ? 'selected' : '' }}>Juni</option>
						<option value='7' {{ $bulan == 7 ? 'selected' : '' }}>Juli</option>
						<option value='8' {{ $bulan == 8 ? 'selected' : '' }}>Agustus</option>
						<option value='9' {{ $bulan == 9 ? 'selected' : '' }}>September</option>
						<option value='10' {{ $bulan == 10 ? 'selected' : '' }}>Oktober</option>
						<option value='11' {{ $bulan == 11 ? 'selected' : '' }}>November</option>
						<option value='12' {{ $bulan == 12 ? 'selected' : '' }}>Desember</option>
					</select>
					</form>
				</div>
			</div> 
		</div>
		<div class="table-responsive">
			<table id="example1" class="table table-bordered table-striped">
					<thead>
					<tr>
						<th>No</th>
						<th>Instansi</th>
						<th>Metode Ujian</th>
						<th>Tanggal Ujian</th>
						<th>Waktu Ujian</th>
						<th>Lokasi</th>
						<th>Kuota</th>
						<th>Batas Akhir Pendaftaran</th>
						<th>Status </th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody>
						@php
							$no_urut =1 ;
						@endphp
					@foreach ($data as $key => $datas)
					@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
                    <tr>
						<td>{{ $no_urut++ }}</td>
						<td>{{ $datas->instansis }}</td>
						<td>{{ $datas->metode == 'tes_tulis' ? 'Tes Tertulis' : 'Verifikasi Portofolio'}}</td>
                        <td>{{ Helper::tanggal_indo($datas->tanggal_ujian)}}</td>
                        <td>{{ $datas->metode == 'tes_tulis' ? $datas->waktu_ujian : $datas->waktu_verifikasi }}</td>
                        <td>{{ $datas->lokasi_ujian }}</td>
						<td>{{ $datas->jumlah_peserta.'/'.$datas->kapasitas }}</td>
						<td>
							@if($datas->batas_waktu_input == 0000-00-00 || $datas->batas_waktu_input == 1970-01-01)
							-
							@else
							{{  Helper::tanggal_indo($datas->batas_waktu_input) }}
							@endif
						</td>
                        <td>{{ Helper::getStatusPermohonan($datas->status_permohonan) }}</td>
                        <td>
                        <div class="dropdown">
                            <button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
                            <ul class="dropdown-menu">
                            		@if($datas->metode == 'tes_tulis' && Auth::user()->role != 'verifikator')
                            		<li><a href="{{ url('cetak-absensi-instansi')."/".$datas->id }}">Cetak Absensi</a></li>
                            		@endif
                            		@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
                            		<li><a href="{{ url('up-hasil-ujian-instansi')."/".$datas->id }}">Unggah Hasil Ujian</a></li>
                            		@endif
									@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp' )
									<li><a href="{{ url('verifikasi-permohonan-instansi')."/".$datas->id }}">Verifikasi Permohonan</a></li>
									@endif
									@if ($datas->status_permohonan == 'setuju')
									<li><a href="{{ url('lihat-peserta-instansi')."/".$datas->id }}">Lihat Peserta</a></li>
									@endif
									@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp' || Auth::user()->role == 'bangprof')
										{{-- <li><a href="{{ url('edit-jadwal-inpassing/'.$datas->id) }}">Ubah</a></li> --}}
										<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
									@endif
                            </ul>
                        </div>    
                        </td>
                        @endif
                        @if(Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator')
							{{-- @if(Auth::user()->id == $datas->asesor || Auth::user()->id == $datas->assign) --}}
						<tr>

							<td>{{ $no_urut++ }}</td>
						<td>{{ $datas->instansis }}</td>
						<td>{{ $datas->metode == 'tes_tulis' ? 'Tes Tertulis' : 'Verifikasi Portofolio'}}</td>
                        <td>
                        	{{ Helper::tanggal_indo($datas->tanggal_ujian)}}
                        	{{-- $datas->metode == 'tes_tulis' ? Helper::tanggal_indo($datas->tanggal_tes_tertulis) : Helper::tanggal_indo($datas->tanggal_verifikasi) --}}
                        </td>
                        <td>{{ $datas->metode == 'tes_tulis' ? $datas->waktu_ujian : $datas->waktu_verifikasi }}</td>
                        <td>{{ $datas->lokasi_ujian }}</td>
						<td>{{ $datas->jumlah_peserta.'/'.$datas->kapasitas }}</td>
						<td>
							@if($datas->batas_waktu_input == 0000-00-00 || $datas->batas_waktu_input == 1970-01-01)
							-
							@else
							{{  Helper::tanggal_indo($datas->batas_waktu_input) }}
							@endif
						</td>
                        <td>
                        @if ($datas->status_permohonan == "")
                            Belum Dilihat
                        @else
                            {{ $datas->status_permohonan == 'setuju' ? 'Setuju' : 'Tidak Setuju' }}
                        @endif
                        </td>
							<td>
									<div class="dropdown">
											<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
											<ul class="dropdown-menu">
													<li><a href="{{ url('lihat-peserta-instansi')."/".$datas->id }}">Lihat Peserta</a></li>
													@if($datas->metode == 'tes_tulis' && Auth::user()->role != 'verifikator')
													<li><a href="{{ url('cetak-absensi-reguller/'.$datas->id) }}">Cetak Absensi</a></li>
													@endif
													@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
													<li><a href="{{ url('up-hasil-ujian/'.$datas->id) }}">Unggah Hasil Ujian</a></li>
													@endif
													@if(Auth::user()->role == 'superadmin')
													<li><a href="{{ url('edit-jadwal-inpassing/'.$datas->id) }}">Ubah</a></li>
													<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
													@endif
													@if(Auth::user()->role == 'dsp')
													<li><a href="{{ url('edit-jadwal-inpassing/'.$datas->id) }}">Ubah</a></li>
													<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
													@endif
													@if(Auth::user()->role == 'bangprof')
													<li><a href="{{ url('edit-jadwal-inpassing/'.$datas->id) }}">Ubah</a></li>
													<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
													@endif --}}
											</ul>
									</div>
							</td>
							{{-- @endif --}}
							@endif
						</tr>


                    	<div class="modal fade" id="modal-default{{ $datas->id }}">
					        <div class="modal-dialog" style="width:30%">
				                <div class="modal-content">
				                    <div class="modal-header">
				                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                            <span aria-hidden="true">&times;</span></button>
				                        <h4 class="modal-title">Hapus Jadwal Instansi</h4>
				                    </div>
				                    <div class="modal-body">
				                        <p>Apakah Anda yakin menghapus Jadwal Instansi?</p>
				                    </div>
				                    <div class="modal-footer">
				                        <a href="{{ url('hapus-jadwal-instansi/'.$datas->id) }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
				                        <button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
				                    </div>
				                </div>
					        </div>
					    </div>
                    @endforeach	
					</tbody>
			</table>
		</div>
</div>  
@endsection
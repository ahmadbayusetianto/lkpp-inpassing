@extends('layout.app')

@section('title')
    Tambah Berkas Prosedur
@endsection

@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
</style>
@endsection

@section('content')
<form action="" method="post" enctype="multipart/form-data">
    @csrf
    <div class="main-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Tambah Berkas Prosedur</h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-xs-10">
                    Judul Berkas
                </div>
                <div class="col-md-1 col-xs-1">:</div>
                <div class="col-md-7 col-xs-12">
                    <div class="form-group">
                        <input type="text" name="judul_berkas" class="form-control" value="{{ old('judul_berkas') }}" >
                    </div>
                    <span class="errmsg">{{ $errors->first('judul_berkas') }}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-xs-10">
                    Berkas
                </div>
                <div class="col-md-1 col-xs-1">:</div>
                <div class="col-md-7 col-xs-12">
                    <div class="form-group">
                        <input type="file" name="berkas" accept="application/pdf, image/jpg, image/jpeg, .pdf, .jpg, .jpeg, .doc, .docx">
                    </div>
                    <span class="errmsg">{{ $errors->first('berkas') }}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9" style="text-align: right">
                    <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                    <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    </form>    
@endsection

@section('js')
<script>
ClassicEditor
.create( document.querySelector( '#editor' ) )
.then( editor => {
    console.log( editor );
} )
.catch( error => {
    console.error( error );
} );
</script>
@endsection
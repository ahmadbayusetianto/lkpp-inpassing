@extends('layout.app2')

@section('title')
Jadwal Dan Daftar Instansi
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }
  .box-container{
    background-color: white;
    border-radius: 5px;
    margin: 5% 10% 2% 7%;
    padding: 5%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 10px 2px rgba(0,0,0,0.85);
    max-width: 250px;
    max-height: 30px;
    line-height: 15px;
    width: 80%;
    margin: 5px;
  }

  .box-tab{
    -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 3px 7px 0px rgba(0,0,0,0.85);
    border-radius: 5px;padding: 10px;
  }

  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  input.checkbox{

  }

  .custom-file{
    margin: 5px;
  }

  .form-control {
    display: block;
    width: auto;
    height: calc(2.25rem + 2px);
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    height: 28px;
    margin: 5px;

  </style>
  @endsection

  @section('content')
  <div class="main-page">
    <div class="col-md-8">
      <div class="row">
        <div class="container box-container">
          <div class="row">
            <h4> Jadwal & Daftar Instansi</h4><br>
          </div>
          <div class="row">
            <hr style="width: 70%;margin-left: 5px;">
          </div>
          <div class="row" style="margin-right: -50px;">
            <div class="col-md-8">
              <div class="row">
                <table>
                  <tr>
                    <td>Tanggal Tes Tertulis</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Waktu Ujian Tertulis</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Lokasi Ujian</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Jumlah Ruang</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Kapasitas</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Nama CP 1</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Telp CP 1</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Nama CP 2</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Telp CP 2</td><td>:</td><td><input type="text" name="email"style="width: 75%" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>Surat Permohonan</td><td>:</td><td>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                      </div>
                    </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script type="text/javascript">
        jQuery(document).ready(function($) {
          $(".clickable-row").click(function() {
            window.location = $(this).data("href");
          });
        });
      </script>

      @endsection
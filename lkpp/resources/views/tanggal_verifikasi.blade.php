@extends('layout.app')

@section('title')
Daftar Verif portofolio
@stop

@section('css')
<style>
.dropdown-menu{
  left: -80px;
}
</style>
@stop

@section('content')
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2">
					<select name='length_change' id='length_change' class="form-control">
							<option value='50'>50</option>
							<option value='100'>100</option>
							<option value='150'>150</option>
							<option value='200'>200</option>
					</select>
			</div>
			<div class="col-md-4 col-12">
					<div class="input-group">
							<div class="input-group addon">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
								<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
							</div>
					</div>
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>No Ujian</th>
          <th>NIP</th>
          <th>Instansi</th>
          <th>jenjang</th>
          <th>Hasil Pleno</th>
					<th>Aksi</th>
				</tr>
				</thead>
				<tbody>
				@foreach ($data as $key => $datas)
        <tr>
          <td>{{ $key++ + 1 }}</td>
          <td>{{ $datas->nama_peserta }}</td>
          <td>-</td>
          <td>{{ $datas->nips }}</td>
          <td>-</td>
          <td>{{ $datas->jenjangs }}</td>
          <td>{{ $datas->hasil_pleno }}</td>
          <td>
            <div class="dropdown">
              <button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
              <ul class="dropdown-menu">
                     <li><a href="{{ url('edit-hasil-pleno/'.$datas->id) }}">Input hasil Pleno</a></li>
              </ul>
          </div>
          </td>
        </tr>
        @endforeach
				</tbody>
		</table>
	</div>
</div> 
@stop
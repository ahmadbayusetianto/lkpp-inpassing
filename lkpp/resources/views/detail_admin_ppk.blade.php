@extends('layout.app')

@section('title')
    Detail Admin PPK
@endsection

@section('css')
<style type="text/css">
.form-control
{
  border-radius: 5px;
  height: 27px;
  padding: 0px;
  padding-left: 10px;
}

.how{
  padding-bottom: 10px;
}

.row{
  margin-bottom: 10px;
  margin-left: 15px;
  margin-right: 15px;
}

.main-box img{
  width: 150px;
}

  
</style>
@stop
@section('content')
<div class="main-box">
<form action="" method="post">
@csrf
<div class="row">
  <div class="col-md-12">
      <h3>Detail Admin PPK</h3>
      <hr>
  </div>
  <div class="col-md-12">
      @if (session('msg'))
      @if (session('msg') == "berhasil")
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Berhasil Update Data</strong>
        </div> 
      @else
      <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Gagal Update Data</strong>
      </div> 
      @endif
  @endif
  </div>
  <div class="col-md-12">
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Nama</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $data->name }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>NIP </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $data->nip }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Jabatan </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $data->jabatan }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
            <label>Nama Instansi </label>
          </div>
          <div class="col-md-1 col-xs-1">
            <label>:</label>
          </div>
          <div class="col-md-7 col-xs-12">
            <label>{{ $data->instansis }}</label>
          </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Nama Satker/OPD</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $data->nama_satuan }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>No. Telepon/HP</label>
         </div>
         <div class="col-lg-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-lg-7 col-xs-12">
           <label>{{ $data->no_telp }}</label>
         </div>
        </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Email </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $data->email }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Foto</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
          @php
              $foto_profile = explode('.',$data->foto_profile);
          @endphp
          @if ($foto_profile[1] == 'pdf' || $foto_profile[1] == 'PDF')
              <a href="{{ url('priview-file')."/foto_profile/".$data->foto_profile }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
          @else
          <a href="{{ url('priview-file')."/foto_profile/".$data->foto_profile }}" target="_blank"><img src="{{ asset('storage/data/foto_profile')."/".$data->foto_profile }}" class="img-rounded"></a>
          @endif
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>SK Pengangkatan Administrator Penyesuaian/Inpassing</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
          <a href="{{ url('priview-file')."/sk_admin_ppk/".$data->sk_admin_ppk }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Username </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $data->username }}</label>
         </div>
        </div>
          {{-- <div class="row">
            <div class="col-md-3 col-xs-10">
             <label>No KTP </label>
           </div>
           <div class="col-md-1 col-xs-1">
             <label>:</label>
           </div>
           <div class="col-md-7 col-xs-12">
             <label>{{ $data->no_ktp == "" ? "-" : $data->no_ktp}}</label>
           </div>
          </div>

        <div class="row">
            <div class="col-md-3 col-xs-10">
             <label>Alamat </label>
           </div>
           <div class="col-lg-1 col-xs-1">
             <label>:</label>
           </div>
           <div class="col-lg-7 col-xs-12">
             <label>{{ $data->alamat == "" ? "-" : $data->alamat}}</label>
           </div>
          </div> --}}

          <div class="row">
            <div class="col-md-3 col-xs-10">
             <label>Verifikasi Email</label>
           </div>
           <div class="col-lg-1 col-xs-1">
             <label>:</label>
           </div>
           <div class="col-lg-7 col-xs-12">
             <label>{{ $data->email_verified_at != "" ? 'Verifikasi' : 'Belum Verifikasi' }}</label>
           </div>
          </div>
        
        <div class="row">
          <div class="col-md-3 col-xs-10">
         </div>
         <div class="col-md-1 col-xs-1">
        
         </div>
         <div class="col-md-7 col-xs-12">
        
         </div>
        </div>
</div>
<div class="col-md-11" style="text-align : right">
    <button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Kembali</button>
  </div>
</div>
</form>
</div>
@stop
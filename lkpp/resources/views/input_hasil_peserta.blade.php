@extends('layout.app')

@section('title')
    Input Hasil Tes Tulis Peserta
@endsection

@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }

    .main-box img{
        width: 60px;
    }
</style>
@endsection

@section('content')
<form action="" method="post">
    @csrf
<div class="main-box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Input Hasil Tes Tulis Peserta</h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Nama Peserta
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    {{ $data->nama }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    No Ujian
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    {{ $data->no_ujian}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    NIP
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    {{ $data->nip }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Pangkat/Gol.
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        @if($data->jabatans != "")
                        {{ $data->jabatans }}
                        @else
                        {{ $data->jabatan }}
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Jenjang
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        @if($data->jenjangs != "")
                        {{ $data->jenjangs }}
                        @else
                        {{ $data->jenjang }}
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    SK KP Terakhir
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    @if($data->sk_kenaikan_pangkat_terakhir == "")
                        -
                    @else
                    @php
                    $sk_kenaikan_pangkat_terakhir = explode('.',$data->sk_kenaikan_pangkat_terakhir);
                    @endphp
                    @if ($sk_kenaikan_pangkat_terakhir[1] == 'pdf' || $sk_kenaikan_pangkat_terakhir[1] == 'PDF')
                    <a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                    @else
                    <a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->sk_kenaikan_pangkat_terakhir }}" class="img-rounded"></a>
                    @endif
                    @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Hasil Ujian
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                      @php
                        $isiselect = [];

                        if ($data->statuss != 2) {
                            $isiselect = array('lulus' => 'Lulus','tidak_lulus' => 'Tidak Lulus','tidak_hadir' => 'Tidak Hadir Tes Tertulis');
                        }
                        if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp'){
                            if($data->statuss == 2){
                                $isiselect = array('lulus' => 'Lulus','tidak_lulus' => 'Tidak Lulus','tidak_hadir' => 'Tidak Hadir Tes Tertulis','tidak_lengkap' => 'Dokumen Persyaratan Tidak Lengkap');
                                // $isiselect = array('lulus' => 'Sesuai','tidak_lulus' => 'Tidak Sesuai','tidak_lengkap' => 'Dokumen Persyaratan Tidak Lengkap');
                            }
                        }

                    @endphp
                    <div class="col-lg-8 col-md-8 col-xs-12">
                            {!! Form::select('rekomendasi', $isiselect, $data->status_ujian, ['placeholder' => 'please select', 'class' => 'form-control form-pjg']); !!}
                            <input type="hidden" name="nama_peserta" value="{{ $data->namas}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="hidden" name="tanggal_ujian"  value="{{ $data->tanggal_ujian != '' ? Helper::tanggal_indo($data->tanggal_ujian) : '' }}"  >
                                <input type="checkbox" name="publish" id="publish" value="publish" class="publish" {{ $data->publish == 'publish' ? 'checked' : '' }}>
                                Publish
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10" style="text-align: right">
                <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection

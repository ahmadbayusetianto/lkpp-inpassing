@extends('layout.app')
@section('title')
    Edit Data Peserta Lulus   
@endsection
@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
</style>
@endsection
@section('content')
<form action="" method="post">
@csrf
	<div class="main-box">
		<div class="container">
			<div class="row">
				<div class="col-md-10">
					@if (session('msg'))					
						@if (session('msg') == "berhasil")
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>  Berhasil simpan data </strong>
							</div>
						@else
							<div class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>  Gagal simpan data </strong>
							</div>
						@endif
					@endif
				</div>
				<div class="col-md-12">
					<h3>Edit Data Peserta Lulus</h3><hr>
				</div>
			</div>
			@if($data != "")
				@foreach($data as $datas)
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Nama 
					</div>
					<div class="col-md-1 col-xs-1">:</div>
						<div class="col-md-5 col-xs-12">
							<div class="form-group">
								<input type='text' class="form-control" name="nama" value="{{ $datas->nama }}" required />
							</div>
							<span class="errmsg">{{ $errors->first('nama') }}</span>
						</div>
				</div>
				{{-- <div class="row">
				<div class="col-md-3 col-xs-10">
					NIP
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='number' class="form-control" name="nip" value="{{ $datas->nip }}" required onkeypress="validate()" />
					</div>
					<span class="errmsg">{{ $errors->first('nip') }}</span>
				</div>
			</div> --}}
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Jabatan
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="jabatan" value="{{ $datas->jabatan }}" />
					</div>
					<span class="errmsg">{{ $errors->first('jabatan') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Lokasi Ttd
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="lokasi_ttd" value="{{ $datas->lokasi_ttd }}" />
					</div>
					<span class="errmsg">{{ $errors->first('lokasi_ttd') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Posisi TTD
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<select name="posisi_ttd" class="form-control">
							<option {{ $datas->posisi_ttd == 1 ? 'selected' : ''  }} value="1">Kiri</option>
							<option {{ $datas->posisi_ttd == 2 ? 'selected' : ''  }} value="2">Tengah</option>
							<option {{ $datas->posisi_ttd == 3 ? 'selected' : ''  }} value="3">Kanan</option>
						</select>
					</div>
					<span class="errmsg">{{ $errors->first('posisi_ttd') }}</span>
				</div>
			</div>
			<div class="row">
            <div class="col-md-3 col-xs-10">
                Tulisan Rata
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <select name="align" class="form-control" required="">
                    		<option value="">Pilih Posisi</option>
							<option {{ $datas->align == 1 ? 'selected' : ''  }} value="1">Kiri</option>
							<option {{ $datas->align == 2 ? 'selected' : ''  }} value="2">Tengah</option>
							<option {{ $datas->align == 3 ? 'selected' : ''  }} value="3">Kanan</option>
						</select>
                  </div>
                  <span class="errmsg">{{ $errors->first('jabatan_english') }}</span>
            </div>
        </div>
			<div class="row">
			@endforeach
		@endif
		@if(count($data)== 0)
		<div class="row">
			<div class="col-md-3 col-xs-10">
				Nama
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
				<div class="form-group">
					<input type='text' class="form-control" name="nama" value="" required />
				</div>
				<span class="errmsg">{{ $errors->first('nama') }}</span>
			</div>
		</div>
	{{-- 	<div class="row">
			<div class="col-md-3 col-xs-10">
				NIP
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='number' class="form-control" name="nip" value="" required onkeypress="validate()" />
                  </div>
                  <span class="errmsg">{{ $errors->first('nip') }}</span>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Jabatan
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='text' class="form-control" name="jabatan" value="" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('jabatan') }}</span>
            </div>
        </div>
        <div class="row">
				<div class="col-md-3 col-xs-10">
					Lokasi Ttd
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="lokasi_ttd" value="" required="" />
					</div>
					<span class="errmsg">{{ $errors->first('lokasi_ttd') }}</span>
				</div>
			</div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Posisi TTD
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <select name="posisi_ttd" class="form-control" required="">
                    		<option value="">Pilih Posisi</option>
							<option value="1">Kiri</option>
							<option value="2">Tengah</option>
							<option value="3">Kanan</option>
						</select>
                  </div>
                  <span class="errmsg">{{ $errors->first('jabatan_english') }}</span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-xs-10">
                Tulisan Rata
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <select name="align" class="form-control" required="">
                    		<option value="">Pilih Posisi</option>
							<option value="1">Kiri</option>
							<option value="2">Tengah</option>
							<option value="3">Kanan</option>
						</select>
                  </div>
                  <span class="errmsg">{{ $errors->first('jabatan_english') }}</span>
            </div>
        </div>
        <div class="row">
        @endif
            <div class="col-md-9" style="text-align: right">
                <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
@section('js')
<script>
	$('#metode').on('change', function() {
		var select = this.value;
		if(select == 'verifikasi_portofolio'){
			$('#input_verif').show();
			$('#input_tes').hide();
		}
		
		if(select == 'tes_tulis'){
			$('#input_verif').hide();
			$('#input_tes').show();
		}
	});
	
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker3').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker4').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker2').datetimepicker({
			format: 'LT'
		});
	});
	
	$(document).ready(function() {
		$('#input_verif').hide();
		$('#input_tes').hide();
	});

	function AddBusinessDays(weekDaysToAdd) {
		var curdate = new Date();
		var realDaysToAdd = 0;
		while (weekDaysToAdd > 0){
			curdate.setDate(curdate.getDate()+1);
			realDaysToAdd++;
			if (noWeekendsOrHolidays(curdate)[0]) {
				weekDaysToAdd--;
			}
		}
		return realDaysToAdd;
    }

	var date_billed = $('#datebilled').datepicker('getDate');
	var date_overdue = new Date();
	var weekDays = AddBusinessDays(30);
	date_overdue.setDate(date_billed.getDate() + weekDays);
	date_overdue = $.datepicker.formatDate('mm/dd/yy', date_overdue);
	$('#datepd').val(date_overdue).prop('readonly', true);
</script>
@endsection
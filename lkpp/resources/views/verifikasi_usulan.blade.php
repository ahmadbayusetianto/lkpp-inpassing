@extends('layout.app')

@section('title')
Verifikasi Usulan Formasi
@stop
@section('css')
<style type="text/css">
.form-control
{
  border-radius: 5px;
  height: 27px;
  padding: 0px;
  padding-left: 10px;
}

.how{
  padding-bottom: 10px;
}

.row{
  margin-bottom: 10px;
  margin-left: 15px;
  margin-right: 15px;
}

.img-rounded{
  width: 250px;
}
  .ket_lam{
    font-size: 11px;

  }
  
</style>
@stop
@section('content')
<div class="main-box">
<form action="" method="post">
@csrf
<div class="row">
  <div class="col-md-12">
      <h3>Verifikasi Usulan Formasi</h3>
      <hr>
  </div>
  <div class="col-md-12">
      @if (session('msg'))
      @if (session('msg') == "berhasil")
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Berhasil ubah Data</strong>
        </div> 
      @else
      <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Gagal ubah Data</strong>
      </div> 
      @endif
  @endif
  </div>
  <div class="col-md-12">
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Nama</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $eformasi->names }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>NIP</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $eformasi->nips }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Jabatan</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $eformasi->jabatans }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Nama Instansi </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $eformasi->instansis }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label> Nama Satker/OPD</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $eformasi->nama_satuans }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>No. Telepon/HP</label>
         </div>
         <div class="col-lg-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-lg-7 col-xs-12">
           <label>{{ $eformasi->no_telps }}</label>
         </div>
      </div>
      <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Email </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
           <label>{{ $eformasi->emails }}</label>
         </div>
        </div>
        
        <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>SK Pengangkatan Administrator Penyesuaian/Inpassing</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
          {{-- <a href="{{ url('priview-file')."/sk_admin_ppk/".$eformasi->sk_admins }}"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a> --}}
          @php
          $sk_admins = explode('.',$eformasi->sk_admins);
          @endphp
          @if ($sk_admins[1] == 'pdf' || $sk_admins[1] == 'PDF')
          <a href="{{ url('priview-file')."/sk_admin_ppk/".$eformasi->sk_admins }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
          @else
          <a href="{{ url('priview-file')."/sk_admin_ppk/".$eformasi->sk_admins }}" target="_blank"><img src="{{ asset('storage/data/sk_admin_ppk')."/".$eformasi->sk_admins }}" class="img-rounded"></a>
          @endif
         </div>
        </div>
        
        <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Dokumen Hasil Penyusunan Kebutuhan <br> <span class="ket_lam">(sesuai dengan Lampiran III No.1 Peraturan LKPP 4/2019)</span></label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
            {{-- <a href="{{ url('priview-file')."/surat_usulan/".$eformasi->surat_usulan_rekomendasi }}"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a> --}}
            @php
            $dokumen_hasil_perhitungan_abk = explode('.',$eformasi->dokumen_hasil_perhitungan_abk);
            @endphp
            @if ($dokumen_hasil_perhitungan_abk[1] == 'pdf' || $dokumen_hasil_perhitungan_abk[1] == 'PDF')
            <a href="{{ url('priview-file')."/dokumen_hasil_perhitungan_abk/".$eformasi->dokumen_hasil_perhitungan_abk }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/dokumen_hasil_perhitungan_abk/".$eformasi->dokumen_hasil_perhitungan_abk }}" target="_blank"><img src="{{ asset('storage/data/dokumen_hasil_perhitungan_abk')."/".$eformasi->dokumen_hasil_perhitungan_abk }}" class="img-rounded"></a>
            @endif
         </div>
        </div>

        <div class="row">
          <div class="col-md-3 col-xs-10">
           <label> Surat Permohonan Mengikuti Penyesuaian/Inpassing <br> <span class="ket_lam">(sesuai dengan Lampiran III No.2 Peraturan LKPP 4/2019)</span></label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
            {{-- <a href="{{ url('priview-file')."/dokumen_hasil_perhitungan_abk/".$eformasi->dokumen_hasil_perhitungan_abk }}"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a> --}}
            @php
            $surat_usulan_rekomendasi = explode('.',$eformasi->surat_usulan_rekomendasi);
            @endphp
            @if ($surat_usulan_rekomendasi[1] == 'pdf' || $surat_usulan_rekomendasi[1] == 'PDF')
            <a href="{{ url('priview-file')."/surat_usulan/".$eformasi->surat_usulan_rekomendasi }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/surat_usulan/".$eformasi->surat_usulan_rekomendasi }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan')."/".$eformasi->surat_usulan_rekomendasi }}" class="img-rounded"></a>
            @endif
         </div>
        </div>

        <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>No. Surat Permohonan Mengikuti Penyesuaian/Inpassing</label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
         </div>
         <div class="col-md-7 col-xs-12">
            <label>{{ $eformasi->no_surat_usulan_rekomendasi }}</label>
         </div>
        </div>
        
        <div class="row">
          <div class="col-md-3 col-xs-10">
         </div>
         <div class="col-md-1 col-xs-1">
        
         </div>
         <div class="col-md-7 col-xs-12">
        
         </div>
        </div>
        
        
        
        <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Jumlah JF PPBJ Pertama: </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
           <b class="pull-right">{{ $eformasi->pertama }}</b>
         </div>
         <div class="col-md-3 how col-xs-12">
           {{-- {{ Form::select('size', array('L' => 'Large', 'S' => 'Small')), array(['class' => 'form-control']) }} --}}
           {!! Form::select('status_pertama', array('setuju' => 'Setuju', 'tidak_setuju' => 'Tidak Setuju'), $eformasi->status_pertama, ['placeholder' => 'Pilih Status', 'class' => 'form-control', 'id' => 'status_pertama']); !!}
        </div>
        <div class="col-md-4 col-xs-12" id="alasan_pertama">
          <textarea class="form-control" rows="3" placeholder="Masukan Alasan" style="padding-bottom: 10px;" name="deskripsi_pertama" id="txt_pertama">{{ $eformasi->deskripsi_pertama }}</textarea>
        </div>
        </div>
        
        
        <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Jumlah JF PPBJ Muda: </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
           <b class="pull-right">{{ $eformasi->muda }}</b>
         </div>
         <div class="col-md-3 how col-xs-12">
            {!! Form::select('status_muda', array('setuju' => 'Setuju', 'tidak_setuju' => 'Tidak Setuju'), $eformasi->status_muda, ['placeholder' => 'Pilih Status', 'class' => 'form-control', 'id' => 'status_muda']); !!}
        </div>
        <div class="col-md-4 col-xs-12" id="alasan_muda">
          <textarea class="form-control" rows="3" placeholder="Masukan Alasan" name="deskripsi_muda" id="txt_muda">{{ $eformasi->deskripsi_muda }}</textarea>
        </div>
        </div>
        
        <div class="row">
          <div class="col-md-3 col-xs-10">
           <label>Jumlah JF PPBJ Madya: </label>
         </div>
         <div class="col-md-1 col-xs-1">
           <label>:</label>
           <b class="pull-right">{{ $eformasi->madya }}</b>
         </div>
         <div class="col-md-3 how col-xs-12">
            {!! Form::select('status_madya', array('setuju' => 'Setuju', 'tidak_setuju' => 'Tidak Setuju'), $eformasi->status_madya, ['placeholder' => 'Pilih Status', 'class' => 'form-control', 'id' => 'status_madya']); !!}
        </div>
        <div class="col-md-4 col-xs-12" id="alasan_madya">
          <textarea class="form-control" rows="3" placeholder="Masukan Alasan" name="deskripsi_madya" id="txt_madya">{{ $eformasi->deskripsi_madya }}</textarea>
        </div>
      </div>
</div>
<div class="col-md-11" style="text-align : right">
    <button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Batal</button> <button type="submit" class="btn btn-default1">Simpan</button>
  </div>
</div>
</form>
</div>
@stop

@section('js')
<script>
	$(document).ready(function() {
		$('#alasan_madya').hide();
		$('#alasan_muda').hide();
		$('#alasan_pertama').hide();
	});

	$(document).ready(function() {
		var status_muda = $('#status_muda').val();
		var status_madya = $('#status_madya').val();
		var status_pertama = $('#status_pertama').val();
   
		if(status_muda == 'tidak_setuju'){
			$('#alasan_muda').show();
			$("#txt_muda").attr('required', '');
		}

		if(status_madya == 'tidak_setuju'){
			$('#alasan_madya').show();
			$("#txt_madya").attr('required', '');
		}

   if(status_pertama == 'tidak_setuju')
   {
      $('#alasan_pertama').show();
      $("#txt_pertama").attr('required', '');
   }
});

  $('#status_muda').on('change', function() {
    var select = this.value;
  
    if(select == 'tidak_setuju'){
      $('#alasan_muda').show();
      $("#txt_muda").attr('required', '');
    }

    if(select == 'setuju'){
      $('#alasan_muda').hide();
      $("#txt_muda").removeAttr('required');
    }

    if(select == ''){
      $('#alasan_muda').hide();
      $("#txt_muda").removeAttr('required');
    }
  });

  $('#status_madya').on('change', function() {
    var select = this.value;
  
    if(select == 'tidak_setuju'){
      $('#alasan_madya').show();
      $("#txt_madya").attr('required', '');
    }

    if(select == 'setuju'){
      $('#alasan_madya').hide();
      $("#txt_madya").removeAttr('required');
    }

    if(select == ''){
      $('#alasan_madya').hide();
      $("#txt_madya").removeAttr('required');
    }
  });

  $('#status_pertama').on('change', function() {
    var select = this.value;
  
    if(select == 'tidak_setuju'){
      $('#alasan_pertama').show();
      $("#txt_pertama").attr('required', '');
    }

    if(select == 'setuju'){
      $('#alasan_pertama').hide();
      $("#txt_pertama").removeAttr('required');
    }

    if(select == ''){
      $('#alasan_pertama').hide();
      $("#txt_pertama").removeAttr('required');
    }
  });

document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Kolom Ini Tidak Boleh Kosong");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
})

    document.addEventListener("DOMContentLoaded", function() {
        var elements = document.getElementsByTagName("INPUT");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Silakan isi kolom berikut.");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    })

    document.addEventListener("DOMContentLoaded", function() {
        var elements = document.getElementsByTagName("TEXTAREA");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Silakan isi kolom berikut.");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    })
</script>
@endsection
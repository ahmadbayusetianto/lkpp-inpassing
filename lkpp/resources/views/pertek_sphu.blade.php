@extends('layout.app')
@section('title')
	Pertek dan SPHU
@stop
@section('css')
<style>
	.btn-tbh{
		text-align: right;
	}

	.btn-jadwal{
		width: 120px;
		background: #E8382A;
		color: #fff;
		font-weight: 600;
	}

	.btn-jadwal:hover{
		color: aliceblue;
	}
</style>
@stop
@section('content')
@if (session('msg'))
	@if (session('msg') == "berhasil")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil simpan data</strong>
				</div>
			</div>
		</div>
	@endif

	@if (session('msg') == "gagal")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal simpan data</strong>
				</div>
			</div>
		</div>
	@endif

	@if (session('msg') == "berhasil_update")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil ubah data</strong>
				</div>
			</div>
		</div>
	@endif

	@if (session('msg') == "gagal_update")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal ubah data</strong>
				</div>
			</div>
		</div>
	@endif

	@if (session('msg') == "berhasil_hapus")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil hapus data</strong>
				</div>
			</div>
		</div>
	@endif

	@if (session('msg') == "gagal_hapus")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal hapus data</strong>
				</div>
			</div>
		</div>
	@endif
@endif
<div class="main-box">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li {{ $jenis == 'pertek' ? 'class=active' : '' }} @if (session('bagian') == 'pertek') class="active"  @endif ><a href="#tab_pertek" data-toggle="tab">Pertek</a></li>
			<li {{ $jenis == 'sphu' ? 'class=active' : '' }} @if (session('bagian') == 'sphu') class="active" @else @endif><a href="#tab_sphu" data-toggle="tab">SPHU</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane @if(session('bagian') != 'pertek' && session('bagian') != 'sphu') active @endif">	<h3>Silahkan Pilih Tab di atas </h3></div>
				<div class="tab-pane {{ $jenis == 'pertek' ? 'active' : '' }} @if (session('bagian') == 'pertek') active @endif" id="tab_pertek">
					<div class="min-top">
						<div class="row">
							<div class="col-md-1 text-center">
								<b>Perlihatkan</b>
							</div>
							<div class="col-md-2 col-6">
								<select name='length_change' id='length_change' class="form-control">
									<option value='50'>50</option>
									<option value='100'>100</option>
									<option value='150'>150</option>
									<option value='200'>200</option>
								</select>
							</div>
							<div class="col-md-3 col-6">
								<div class="input-group">
									<div class="input-group addon">
										<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
										<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
									</div>
								</div>
							</div>
							<div class="col-md-1 text-center">
								<b>Bulan</b>
							</div>
							<div class="col-md-2 col-6">
								<form action="" method="get">
								@csrf
									<select name='bulan_pertek' id='bulan' class="form-control form-control-sm" onchange="this.form.submit()">
										<option value='' {{ $bulan_p == '' ? 'selected' : '' }}>Pilih Bulan</option>
										<option value='1' {{ $bulan_p == 1 ? 'selected' : '' }}>Januari</option>
										<option value='2' {{ $bulan_p == 2 ? 'selected' : '' }}>Februari</option>
										<option value='3' {{ $bulan_p == 3 ? 'selected' : '' }}>Maret</option>
										<option value='4' {{ $bulan_p == 4 ? 'selected' : '' }}>April</option>
										<option value='5' {{ $bulan_p == 5 ? 'selected' : '' }}>Mei</option>
										<option value='6' {{ $bulan_p == 6 ? 'selected' : '' }}>Juni</option>
										<option value='7' {{ $bulan_p == 7 ? 'selected' : '' }}>Juli</option>
										<option value='8' {{ $bulan_p == 8 ? 'selected' : '' }}>Agustus</option>
										<option value='9' {{ $bulan_p == 9 ? 'selected' : '' }}>September</option>
										<option value='10' {{ $bulan_p == 10 ? 'selected' : '' }}>Oktober</option>
										<option value='11' {{ $bulan_p == 11 ? 'selected' : '' }}>November</option>
										<option value='12' {{ $bulan_p == 12 ? 'selected' : '' }}>Desember</option>
									</select>
									<input type="hidden" name="jenis" value="pertek">
								</form>
							</div>
							<div class="col-md-3 col-6 btn-tbh">
              				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator')
								<a href="{{ url('tambah-pertek') }}"><button class="btn btn-sm btn-jadwal">Buat Pertek</button></a>
              				@endif
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Instansi</th>
									<th>Tanggal Surat</th>
									<th>No Pertek</th>
									<th>Status</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($pertek as $key => $perteks)
								<tr>
									<td>{{ $perteks->nama_instansi }}</td>
									@if($perteks->tanggal_pertek != 0000-00-00)
										<td>{{ Helper::tanggal_indo($perteks->tanggal_pertek) }}</td>
									@else
										<td>{{ '-' }}</td>
									@endif
									@if($perteks->no_pertek != "")
										<td>{{ $perteks->no_pertek }}</td>
									@else
										<td>-</td>
									@endif
									<td>{{ ucwords($perteks->status_pertek) }}</td>
									<td>
										<div class="dropdown">
											<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
											<ul class="dropdown-menu">
											@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator')
												<li><a href="{{ url('edit-pertek/'.$perteks->id) }}">Ubah</a></li>
												<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $perteks->id }}">Hapus</a></li>
              								@endif
											</ul>
										</div>
									</td>
								</tr>
								<div class="modal fade" id="modal-default{{ $perteks->id }}">
									<div class="modal-dialog" style="width:30%">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title">Hapus Pertek</h4>
											</div>
              								<div class="modal-body">
              									<p>Apakah Anda yakin menghapus Pertek?</p>
              								</div>
              								<div class="modal-footer">
              									<a href="{{ url('hapus-pertek/'.$perteks->id) }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
              									<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
              								</div>
              							</div>
              						</div>
              					</div>
              				@endforeach
							</tbody>
              			</table>
              		</div>
				</div>
			<!-- /.tab-pane -->
			<div class="tab-pane {{ $jenis == 'sphu' ? 'active' : '' }} @if (session('bagian') == 'sphu') active @endif" id="tab_sphu">
				<div class="min-top">
					<div class="row">
						<div class="col-md-1 text-center">
							<b>Perlihatkan</b>
						</div>
						<div class="col-md-2 col-6">
							<select name='length_change' id='length_change1' class="form-control">
								<option value='50'>50</option>
								<option value='100'>100</option>
								<option value='150'>150</option>
								<option value='200'>200</option>
							</select>
						</div>
						<div class="col-md-3 col-6">
							<div class="input-group">
								<div class="input-group addon">
									<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
									<input type="text" class="form-control" id="myInputTextField1" name="search" placeholder="Cari">
								</div>
							</div>
						</div>
						<div class="col-md-1 text-center">
							<b>Bulan</b>
						</div>
						<div class="col-md-2 col-6">
							<form action="" method="get">
							@csrf
								<select name='bulan_sphu' id='bulan' class="form-control form-control-sm" onchange="this.form.submit()">
									<option value='' {{ $bulan_s == '' ? 'selected' : '' }}>Pilih Bulan</option>
									<option value='1' {{ $bulan_s == 1 ? 'selected' : '' }}>Januari</option>
									<option value='2' {{ $bulan_s == 2 ? 'selected' : '' }}>Februari</option>
									<option value='3' {{ $bulan_s == 3 ? 'selected' : '' }}>Maret</option>
									<option value='4' {{ $bulan_s == 4 ? 'selected' : '' }}>April</option>
									<option value='5' {{ $bulan_s == 5 ? 'selected' : '' }}>Mei</option>
									<option value='6' {{ $bulan_s == 6 ? 'selected' : '' }}>Juni</option>
									<option value='7' {{ $bulan_s == 7 ? 'selected' : '' }}>Juli</option>
									<option value='8' {{ $bulan_s == 8 ? 'selected' : '' }}>Agustus</option>
									<option value='9' {{ $bulan_s == 9 ? 'selected' : '' }}>September</option>
									<option value='10' {{ $bulan_s == 10 ? 'selected' : '' }}>Oktober</option>
									<option value='11' {{ $bulan_s == 11 ? 'selected' : '' }}>November</option>
									<option value='12' {{ $bulan_s == 12 ? 'selected' : '' }}>Desember</option>
								</select>
								<input type="hidden" name="jenis" value="sphu">
							</form>
						</div>
						<div class="col-md-3 col-6 btn-tbh">
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator')
							<a href="{{ url('tambah-sphu') }}"><button class="btn btn-sm btn-jadwal">Buat SPHU</button></a>
						@endif
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="example3" class="table table-bordered table-striped">
						<thead>
							<th>Instansi</th>
							<th>Tanggal Surat</th>
							<th>No SPHU</th>
							<th>Status</th>
							<th>Aksi</th>
						</thead>
						<tbody>
						@foreach ($sphu as $key => $sphus)
							<tr>
								<td>{{ $sphus->nama_instansi }}</td>
								@if($sphus->tanggal_sphu != 0000-00-00)
									<td>{{ Helper::tanggal_indo($sphus->tanggal_sphu) }}</td>
								@else
									<td>{{ '-' }}</td>
								@endif

								@if($sphus->no_sphu != "")
									<td>{{ $sphus->no_sphu }}</td>
								@else
									<td>{{ '-' }}</td>
								@endif
								<td>{{ ucwords($sphus->status_sphu) }}</td>
								<td>
									<div class="dropdown">
										<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
										<ul class="dropdown-menu">
										@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator')
											<li><a href="{{ url('edit-sphu/'.$sphus->id) }}">Ubah</a></li>
											<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $sphus->id }}">Hapus</a></li>
										@endif
										</ul>
									</div>
								</td>
							</tr>
							<div class="modal fade" id="modal-default{{ $sphus->id }}">
								<div class="modal-dialog" style="width:30%">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<h4 class="modal-title">Hapus SPHU</h4>
										</div>
										<div class="modal-body">
											<p>Apakah Anda yakin menghapus SPHU?</p>
										</div>
										<div class="modal-footer">
											<a href="{{ url('hapus-sphu/'.$sphus->id) }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
											<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		<!-- /.tab-pane -->
		<!-- /.tab-pane -->
		</div>
	<!-- /.tab-content -->
	</div>
</div>
@stop

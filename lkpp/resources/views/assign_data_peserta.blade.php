@extends('layout.app')
@section('title')
	Verifikasi Dokumen Persyaratan
@endsection
@section('css')
<style>
    .jud-register{
        padding: 15px;
        color: #3a394e;
    }

    .box{
        -webkit-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        -moz-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
    }

    .btn-default1{
        background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
        color: white;
        font-weight: 600;
        width: 100px;
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default2{
        background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
        color: black;
        font-weight: 600;
        width: 100px;
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default1:hover{
        color: #eee;
        background: #c10013;
    }

    .btn-area{
        text-align: right;
    }

    .navi ul{
		padding:0;
		font-size:0;
		overflow:hidden;
		display:inline-block;
		width: 100%;
    }

    .navi li{
		display:inline-block;
		text-align: center;
    }

    .navi a{
		font-size: 15px;
		position:relative;
		display:inline-block;
		background:#eee;
		text-decoration:none;
		color:#555;
		padding:13px 25px 13px 10px;
		width: 100%;
		font-weight: 600;
    }

	.navi a:after,
    .navi a:before{
		position: absolute;
		content: "";
		height: -10px;
		width: 0px;
		top: 50%;
		left: -28px;
		margin-top: -25px;
		border: 25px solid #eee;
		border-right: 2 !important;
		border-left-color: transparent !important;
    }

	.navi a:before{
		left:-26px;
		border: 24px solid #555;
    }

	/* ACTIVE STYLES */
    .navi a.active{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
	}

	.navi a.active:after{border-color:#e74c3c;}
	/* HOVER STYLES */
	.navi a:hover{
		background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
    }

    .navi a:hover:after{ border-color:#e74c3c;}

    #page-two{
        display: none;
    }

    #page-three{
        display: none;
    }

    div.container div.row{
        padding: 0px 2%;
        margin-bottom: 1%;
    }

    h5 span{
        color: #e74c3c;
    }

    label.custom-file-label{
        margin: 0px 15px;
    }

    .row .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }

    .row .custom-file-label{
        -webkit-box-shadow: 0px 0px 2px 2px rgba(0,0,0,0.1);
        -moz-box-shadow: 0px 0px 2px 2px rgba(0,0,0,0.1);
        box-shadow: 0px 0px 2px 2px rgba(0,0,0,0.1);
    }

    .{
        line-height: 2.5;
    }

    .form-control{
        width: 80%;
    }

    #page-one h5{
        font-weight: 600;
    }

    #page-two img{
        width: 150px;
    }
}
</style>
@endsection
@section('content')
@if (session('msg'))
	@if (session('msg') == "berhasil")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil Simpan Data</strong>
				</div>
			</div>
		</div>
	@endif
	@if (session('msg') == "gagal")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal Simpan Data</strong>
				</div>
			</div>
		</div>
	@endif
@endif
<div>
@if ($peserta != '')
    <div class="row">
		<div class="col-md-12">
			<form action="" method="post" enctype="multipart/form-data">
			@csrf
				<div class="box">
					<div class="navi">
						<ul>
							<li style="width:35%"><a href="#" id="li-one" class="active">1. Biodata</a></li>
							<li style="width:65%"><a href="#" id="li-two">2. Verifikasi Dokumen Persyaratan</a></li>
						</ul>
					</div>
					<div class="page container-fluid" id="page-one">
						<div class="container">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<label>Nama</label>
								</div>
								<div class="col-lg-1 col-md-1 col-1">
									<label>:</label>
								</div>
								<div class="col-lg-6 col-md-6 col-12">
									<label>{{ $peserta->nama }}</label>
								</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
									<label>NIP</label>
                                </div>
                                <div class="col-lg-1 col-md-1 col-1">
									<label>:</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
									<label>{{ $peserta->nip }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
									<label>Pangkat/Gol.</label>
                                </div>
                                <div class="col-lg-1 col-md-1 col-1">
									<label>:</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
									<label>{{ $peserta->jabatan }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
									<label>TMT Pangkat/Gol.</label>
                                </div>
                                <div class="col-lg-1 col-md-1 col-1">
									<label>:</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
									<label>{{ Helper::tanggal_indo($peserta->tmt_panggol) }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
									<label>Usulan Jenjang </label>
                                </div>
                                <div class="col-lg-1 col-md-1 col-1">
									<label>:</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
									<label>{{ ucwords($peserta->jenjang) }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
									<label>No. Surat Permohonan Mengikuti Penyesuaian/Inpassing</label>
                                </div>
                                <div class="col-lg-1 col-md-1 col-1">
									<label>:</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
									<label>{{ $peserta->no_surats }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
									<label>Tempat Lahir</label>
                                </div>
                                <div class="col-lg-1 col-xs-1">
									<label>:</label>
                                </div>
                                <div class="col-lg-7 col-xs-12">
									<label>{{ $peserta->tempat_lahir }}</label>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>Tanggal Lahir </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ Helper::getReadbleDateFormat($peserta->tanggal_lahir) }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>Jenis Kelamin </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>
											{{ Helper::getGenderStatus($peserta->jenis_kelamin) }}
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>No. KTP </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->no_ktp }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>No. Sertifikat PBJ Tk. Dasar</label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->no_sertifikat }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>Pendidikan Terakhir </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->pendidikan_terakhir }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>Email </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->email }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>No. Telp/HP </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->no_telp }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>Nama Instansi </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->instansis }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>Nama Satuan Kerja/OPD </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->satuan_kerja }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>Provinsi </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->provinsis }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <label>Kabupaten/Kota </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <label>{{ $peserta->kotas }}</label>
                                    </div>
								</div>
						</div>
                        <div class="col-md-8 btn-area">
							<a href="{{ url('data-peserta') }}" class="btn btn-sm btn-default2"><i class="fa fa-angle-left"></i> Batal</a>
							<button class="btn btn-sm btn-default1" id="btnOne" onclick="btnOne()">Selanjutnya <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
                    <div class="page container-fluid" id="page-two">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>Surat Ket Bertugas di bidang PBJ min. 2 tahun </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->surat_bertugas_pbj == "")
                                        -
                                    @else
										@php
											$surat_bertugas_pbj = explode('.',$peserta->surat_bertugas_pbj);
										@endphp
										@if ($surat_bertugas_pbj[1] == 'pdf' || $surat_bertugas_pbj[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_bertugas_pbj/".$peserta->surat_bertugas_pbj }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_bertugas_pbj/".$peserta->surat_bertugas_pbj }}" target="_blank"><img src="{{ asset('storage/data/surat_bertugas_pbj')."/".$peserta->surat_bertugas_pbj }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>Bukti SK di bidang PBJ </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->bukti_sk_pbj == "")
                                        -
                                    @else
                                    @php
										$bukti_sk_pbj = explode('.',$peserta->bukti_sk_pbj);
                                    @endphp
										@if ($bukti_sk_pbj[1] == 'pdf' || $bukti_sk_pbj[1] == 'PDF')
											<a href="{{ url('priview-file')."/bukti_sk_pbj/".$peserta->bukti_sk_pbj }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/bukti_sk_pbj/".$peserta->bukti_sk_pbj }}" target="_blank"><img src="{{ asset('storage/data/bukti_sk_pbj')."/".$peserta->bukti_sk_pbj }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <b>Ijazah terakhir (min. S1/D4)</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->ijazah_terakhir == "")
                                        -
                                    @else
										@php
											$ijazah_terakhir = explode('.',$peserta->ijazah_terakhir);
										@endphp
										@if ($ijazah_terakhir[1] == 'pdf' || $ijazah_terakhir[1] == 'PDF')
											<a href="{{ url('priview-file')."/ijazah_terakhir/".$peserta->ijazah_terakhir }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/ijazah_terakhir/".$peserta->ijazah_terakhir }}" target="_blank"><img src="{{ asset('storage/data/ijazah_terakhir')."/".$peserta->ijazah_terakhir }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>SK Kenaikan Pangkat Terakhir </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->sk_kenaikan_pangkat_terakhir == "")
                                        -
                                    @else
										@php
											$sk_kenaikan_pangkat_terakhir = explode('.',$peserta->sk_kenaikan_pangkat_terakhir);
										@endphp
										@if ($sk_kenaikan_pangkat_terakhir[1] == 'pdf' || $sk_kenaikan_pangkat_terakhir[1] == 'PDF')
											<a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$peserta->sk_kenaikan_pangkat_terakhir }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$peserta->sk_kenaikan_pangkat_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$peserta->sk_kenaikan_pangkat_terakhir }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>SK Pengangkatan ke dalam Jabatan Terakhir</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->sk_pengangkatan_jabatan_terakhir == "")
                                        -
                                    @else
										@php
											$sk_pengangkatan_jabatan_terakhir = explode('.',$peserta->sk_pengangkatan_jabatan_terakhir);
										@endphp
										@if ($sk_pengangkatan_jabatan_terakhir[1] == 'pdf' || $sk_pengangkatan_jabatan_terakhir[1] == 'PDF')
											<a href="{{ url('priview-file')."/sk_pengangkatan_jabatan_terakhir/".$peserta->sk_pengangkatan_jabatan_terakhir }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sk_pengangkatan_jabatan_terakhir/".$peserta->sk_pengangkatan_jabatan_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_jabatan_terakhir')."/".$peserta->sk_pengangkatan_jabatan_terakhir }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>Sertifikat PBJ Tk. Dasar </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->sertifikasi_dasar_pbj == "")
                                        -
                                    @else
                                    @php
										$sertifikasi_dasar_pbj = explode('.',$peserta->sertifikasi_dasar_pbj);
                                    @endphp
										@if ($sertifikasi_dasar_pbj[1] == 'pdf' || $sertifikasi_dasar_pbj[1] == 'PDF')
											<a href="{{ url('priview-file')."/sertifikasi_dasar_pbj/".$peserta->sertifikasi_dasar_pbj }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sertifikasi_dasar_pbj/".$peserta->sertifikasi_dasar_pbj }}" target="_blank"><img src="{{ asset('storage/data/sertifikasi_dasar_pbj')."/".$peserta->sertifikasi_dasar_pbj }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>SKP (2 tahun terakhir)</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->skp_dua_tahun_terakhir == "")
                                        -
                                    @else
										@php
											$skp_dua_tahun_terakhir = explode('.',$peserta->skp_dua_tahun_terakhir);
										@endphp
										@if ($skp_dua_tahun_terakhir[1] == 'pdf' || $skp_dua_tahun_terakhir[1] == 'PDF')
											<a href="{{ url('priview-file')."/skp_dua_tahun_terakhir/".$peserta->skp_dua_tahun_terakhir }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/skp_dua_tahun_terakhir/".$peserta->skp_dua_tahun_terakhir }}" target="_blank"><img src="{{ asset('storage/data/skp_dua_tahun_terakhir')."/".$peserta->skp_dua_tahun_terakhir }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>Surat Ket Tidak Sedang Dijatuhi Hukuman</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->surat_ket_tidak_dijatuhi_hukuman == "")
                                        -
                                    @else
										@php
											$surat_ket_tidak_dijatuhi_hukuman = explode('.',$peserta->surat_ket_tidak_dijatuhi_hukuman);
										@endphp
										@if ($surat_ket_tidak_dijatuhi_hukuman[1] == 'pdf' || $surat_ket_tidak_dijatuhi_hukuman[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_ket_tidak_dijatuhi_hukuman/".$peserta->surat_ket_tidak_dijatuhi_hukuman }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_ket_tidak_dijatuhi_hukuman/".$peserta->surat_ket_tidak_dijatuhi_hukuman }}" target="_blank"><img src="{{ asset('storage/data/surat_ket_tidak_dijatuhi_hukuman')."/".$peserta->surat_ket_tidak_dijatuhi_hukuman }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>Formulir Kesediaan Mengikuti Penyesuaian/Inpassing </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->formulir_kesediaan_mengikuti_inpassing == "")
                                        -
                                    @else
										@php
											$formulir_kesediaan_mengikuti_inpassing = explode('.',$peserta->formulir_kesediaan_mengikuti_inpassing);
										@endphp
										@if ($formulir_kesediaan_mengikuti_inpassing[1] == 'pdf' || $formulir_kesediaan_mengikuti_inpassing[1] == 'PDF')
											<a href="{{ url('priview-file')."/formulir_kesediaan_mengikuti_inpassing/".$peserta->formulir_kesediaan_mengikuti_inpassing }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/formulir_kesediaan_mengikuti_inpassing/".$peserta->formulir_kesediaan_mengikuti_inpassing }}" target="_blank"><img src="{{ asset('storage/data/formulir_kesediaan_mengikuti_inpassing')."/".$peserta->formulir_kesediaan_mengikuti_inpassing }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>SK Pengangkatan CPNS</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->sk_pengangkatan_cpns == "")
                                        -
                                    @else
										@php
											$sk_pengangkatan_cpns = explode('.',$peserta->sk_pengangkatan_cpns);
										@endphp
										@if ($sk_pengangkatan_cpns[1] == 'pdf' || $sk_pengangkatan_cpns[1] == 'PDF')
											<a href="{{ url('priview-file')."/sk_pengangkatan_cpns/".$peserta->sk_pengangkatan_cpns }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sk_pengangkatan_cpns/".$peserta->sk_pengangkatan_cpns }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_cpns')."/".$peserta->sk_pengangkatan_cpns }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>SK Pengangkatan PNS</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    @if ($peserta->sk_pengangkatan_pns == "")
                                        -
                                    @else
										@php
											$sk_pengangkatan_pns = explode('.',$peserta->sk_pengangkatan_pns);
										@endphp
										@if ($sk_pengangkatan_pns[1] == 'pdf' || $sk_pengangkatan_pns[1] == 'PDF')
											<a href="{{ url('priview-file')."/sk_pengangkatan_pns/".$peserta->sk_pengangkatan_pns }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sk_pengangkatan_pns/".$peserta->sk_pengangkatan_pns }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_pns')."/".$peserta->sk_pengangkatan_pns }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-lg-4 col-md-4 col-10">
                                        <b>KTP </b>
                                    </div>
                                    <div class="div col-lg-1 col-md-1 col-1">
                                        :
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        @if ($peserta->ktp == "")
                                           -
                                        @else
											@php
												$ktp = explode('.',$peserta->ktp);
											@endphp
											@if ($ktp[1] == 'pdf' || $ktp[1] == 'PDF')
												<a href="{{ url('priview-file')."/ktp/".$peserta->ktp }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
											@else
												<a href="{{ url('priview-file')."/ktp/".$peserta->ktp }}" target="_blank"><img src="{{ asset('storage/data/ktp')."/".$peserta->ktp }}" class="img-rounded"></a>
											@endif
                                        @endif
                                    </div>
							</div>
                            <div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<b>Pas Foto 3 X 4 </b>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-6 col-md-6 col-12">
								@if ($peserta->pas_foto_3_x_4 == "")
									-
								@else
									@php
										$pas_foto_3_x_4 = explode('.',$peserta->pas_foto_3_x_4);
									@endphp
									@if ($pas_foto_3_x_4[1] == 'pdf' || $pas_foto_3_x_4[1] == 'PDF')
										<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$peserta->pas_foto_3_x_4 }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
									@else
										<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$peserta->pas_foto_3_x_4 }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4')."/".$peserta->pas_foto_3_x_4 }}" class="img-rounded"></a>
									@endif
								@endif
								</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10 ">
                                    <b>Surat Pernyataan Bersedia diangkat JFPPBJ </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 ">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
								@if ($peserta->surat_pernyataan_bersedia_jfppbj == "")
									-
								@else
									@php
										$surat_pernyataan_bersedia_jfppbj = explode('.',$peserta->surat_pernyataan_bersedia_jfppbj);
                                    @endphp
										@if ($surat_pernyataan_bersedia_jfppbj[1] == 'pdf' || $surat_pernyataan_bersedia_jfppbj[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_pernyataan_bersedia_jfppbj/".$peserta->surat_pernyataan_bersedia_jfppbj }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_pernyataan_bersedia_jfppbj/".$peserta->surat_pernyataan_bersedia_jfppbj }}" target="_blank"><img src="{{ asset('storage/data/surat_pernyataan_bersedia_jfppbj')."/".$peserta->surat_pernyataan_bersedia_jfppbj }}" class="img-rounded"></a>
										@endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
								<div class="col-lg-4 col-md-4 col-10 ">
									<b>Surat Permohonan Mengikuti Penyesuaian/Inpassing</b>
								</div>
								<div class="div col-lg-1 col-md-1 col-1 ">
									:
								</div>
								@if($dataSuratUsulan != "")
									<div class="col-lg-6 col-md-6 col-12">
									@if (is_null($dataSuratUsulan->file))
										-
									@else
										@php
											$surat_usulan_mengikuti_inpassing = explode('.',$dataSuratUsulan->file);
										@endphp
										@if ($surat_usulan_mengikuti_inpassing[1] == 'pdf' || $surat_usulan_mengikuti_inpassing[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_usulan_inpassing/".$dataSuratUsulan->file }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_usulan_inpassing/".$dataSuratUsulan->file }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan_inpassing')."/".$dataSuratUsulan->file }}" class="img-rounded"></a>
										@endif
									@endif
									</div>
								@else
									<div class="col-lg-6 col-md-6 col-12">
									@if (is_null($peserta->surat_usulan_mengikuti_inpassing))
										-
									@else
										@php
											$surat_usulan_mengikuti_inpassing = explode('.',$peserta->surat_usulan_mengikuti_inpassing);
										@endphp
											@if ($surat_usulan_mengikuti_inpassing[1] == 'pdf' || $surat_usulan_mengikuti_inpassing[1] == 'PDF')
												<a href="{{ url('priview-file')."/surat_usulan_mengikuti_inpassing/".$peserta->surat_usulan_mengikuti_inpassing }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
											@else
												<a href="{{ url('priview-file')."/surat_usulan_mengikuti_inpassing/".$peserta->surat_usulan_mengikuti_inpassing }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan_mengikuti_inpassing')."/".$peserta->surat_usulan_mengikuti_inpassing }}" class="img-rounded"></a>
											@endif
										@endif
									</div>
									@endif
							</div>
                            <div class="row">
								<div class="col-lg-4 col-md-4 col-10 ">
									<b>No. Surat Permohonan Mengikuti Penyesuaian/Inpassing </b>
								</div>
								<div class="div col-lg-1 col-md-1 col-1 ">
									:
								</div>
								@if($dataSuratUsulan != "")
									<div class="col-lg-6 col-md-6 col-12">
									@if (is_null($dataSuratUsulan->no_surat_usulan_peserta))
										<label>-</label>
									@else
										<label>{{$dataSuratUsulan->no_surat_usulan_peserta}}</label>
									@endif
									</div>
								@else
									<div class="col-lg-6 col-md-6 col-12">
									@if (is_null($peserta->no_surat_usulan_peserta))
										<label>-</label>
									@else
										<label>{{ $peserta->no_surat_usulan_peserta }}</label>
									@endif
									</div>
								@endif
							</div>
                            @if (Auth::user()->role == 'superadmin')
                            <div class="row">
								<div class="col-lg-4 col-md-4 col-10 ">
									<b>Hasil Verifikasi</b>
								</div>
								<div class="div col-lg-1 col-md-1 col-1 ">
									:
								</div>
								<div class="col-lg-6 col-md-6 col-12">
									{!! Form::select('verifikasi_berkas', array('verified' => 'Disetujui', 'not_verified' => 'Ditolak'), $peserta->verifikasi_berkas, ['placeholder' => 'Pilih Hasil Verifikasi ', 'class' => 'form-control', 'id' => 'berkas_verif']); !!}
								</div>
							</div>
                            <div class="row">
								<div class="col-lg-4 col-md-4 col-10 ">
									<b>Status</b>
								</div>
								<div class="div col-lg-1 col-md-1 col-1 ">
									:
								</div>
								<div class="col-lg-6 col-md-6 col-12">
									{!! Form::select('status', $status, $peserta->status_inpassing, ['placeholder' => 'please select', 'class' => 'form-control', 'id' => 'status_inpassing', 'readonly', 'disabled']); !!}
								</div>
							</div>
                            <div class="row">
								<div class="col-lg-4 col-md-4 col-10 ">
									<b>Catatan</b>
								</div>
								<div class="div col-lg-1 col-md-1 col-1 ">
									:
								</div>
								<div class="col-lg-6 col-md-6 col-12">
									<textarea name="catatan" id="" cols="30" rows="10" class="form-control">{{ $peserta->catatan }}</textarea>
								</div>
                            </div>
                            @endif
                            @if (Auth::user()->role == 'verifikator')
								@if ($peserta->assign == Auth::user()->id)
									<div class="row">
										<div class="col-lg-4 col-md-4 col-10 ">
											<b>Verifikasi Berkas</b>
										</div>
										<div class="div col-lg-1 col-md-1 col-1 ">
											:
										</div>
										<div class="col-lg-6 col-md-6 col-12">
											{!! Form::select('verifikasi_berkas', array('verified' => 'Disetujui', 'not_verified' => 'Ditolak'), $peserta->verifikasi_berkas, ['placeholder' => 'Pilih Hasil Verifikasi ', 'class' => 'form-control', 'id' => 'berkas_verif']); !!}
										</div>
									</div>
									<div class="row">
										<div class="col-lg-4 col-md-4 col-10 ">
											<b>Status</b>
										</div>
										<div class="div col-lg-1 col-md-1 col-1 ">
											:
										</div>
										<div class="col-lg-6 col-md-6 col-12">
											{!! Form::select('status', $status, $peserta->status_inpassing, ['placeholder' => 'please select', 'class' => 'form-control', 'id' => 'status_inpassing', 'readonly', 'disabled']); !!}
										</div>
									</div>
									<div class="row">
										<div class="col-lg-4 col-md-4 col-10 ">
											<b>Catatan</b>
										</div>
										<div class="div col-lg-1 col-md-1 col-1 ">
											:
										</div>
										<div class="col-lg-6 col-md-6 col-12">
											<textarea name="catatan" id="" cols="30" rows="10" class="form-control">{{ $peserta->catatan }}</textarea>
										</div>
									</div>
								@endif
                            @endif
                            @if (Auth::user()->role == 'asesor')
								@if ($peserta->asesor == Auth::user()->id)
									<div class="row">
										<div class="col-lg-4 col-md-4 col-10 ">
											<b>Verifikasi Berkas</b>
										</div>
										<div class="div col-lg-1 col-md-1 col-1 ">
											:
										</div>
										<div class="col-lg-6 col-md-6 col-12">
											{!! Form::select('verifikasi_berkas', array('verified' => 'Disetujui', 'not_verified' => 'Ditolak'), $peserta->verifikasi_berkas, ['placeholder' => 'Pilih Hasil Verifikasi ', 'class' => 'form-control', 'id' => 'berkas_verif']); !!}
										</div>
									</div>
									<div class="row">
										<div class="col-lg-4 col-md-4 col-10 ">
											<b>Status</b>
										</div>
										<div class="div col-lg-1 col-md-1 col-1 ">
											:
										</div>
										<div class="col-lg-6 col-md-6 col-12">
											{!! Form::select('status', $status, $peserta->status_inpassing, ['placeholder' => 'please select', 'class' => 'form-control', 'id' => 'status_inpassing', 'readonly', 'disabled']); !!}
										</div>
									</div>
									<div class="row">
										<div class="col-lg-4 col-md-4 col-10 ">
											<b>Catatan</b>
										</div>
										<div class="div col-lg-1 col-md-1 col-1 ">
											:
										</div>
										<div class="col-lg-6 col-md-6 col-12">
											<textarea name="catatan" id="" cols="30" rows="10" class="form-control">{{ $peserta->catatan }}</textarea>
										</div>
									</div>
								@endif
                            @endif
                        </div>
                        @if (Auth::user()->role == 'superadmin')
                        <div class="col-md-10 btn-area">
							<button class="btn btn-sm btn-default2" id="btnTwoBack" onclick="btnTwoBack()"><i class="fa fa-angle-left"></i> Kembali</button>
							<button class="btn btn-sm btn-default1" type="submit">Simpan <i class="fa fa-angle-right"></i></button>
                        </div>
                        @endif
                        @if (Auth::user()->role == 'verifikator')
							@if ($peserta->assign == Auth::user()->id)
								<div class="col-md-10 btn-area">
									<button class="btn btn-sm btn-default2" id="btnTwoBack" onclick="btnTwoBack()"><i class="fa fa-angle-left"></i> Kembali</button>
									<button class="btn btn-sm btn-default1" type="submit">Simpan <i class="fa fa-angle-right"></i></button>
								</div>
							@endif
                        @endif
                        @if (Auth::user()->role == 'asesor')
							@if ($peserta->asesor == Auth::user()->id)
							<div class="col-md-10 btn-area">
								<button class="btn btn-sm btn-default2" id="btnTwoBack" onclick="btnTwoBack()"><i class="fa fa-angle-left"></i> Kembali</button>
								<button class="btn btn-sm btn-default1" type="submit">Simpan <i class="fa fa-angle-right"></i></button>
							</div>
							@endif
                        @endif
                    </div>
                </div>
            </form>
		</div>
    </div>
	@endif
</div>
@endsection
@section('js')
<script>
    $('#btnOne').click(function (e) {
        e.preventDefault();
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
    });

    $('#btnTwoBack').click(function (e) {
        e.preventDefault();
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
    });

    $('#btnTwoNext').click(function (e) {
        e.preventDefault();
        $("ul li a.active").removeClass("active");
        $("ul li a#li-three").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "none";
    });

    $('#li-one').click(function (e) {
        e.preventDefault();
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
    });

    $('#li-two').click(function (e) {
        e.preventDefault();
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
    });


    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});

    $('#berkas_verif').on('change', function() {
		var select = this.value;

		if(select == 'verified'){
			$('#status_inpassing').val('3');
		}

		if(select == 'not_verified'){
			$('#status_inpassing').val('2');
		}
    });

	$(document).ready(function()  {
		var select = $('#berkas_verif').val();

		if(select == 'verified'){
			$('#status_inpassing').val('3');
		}

		if(select == 'not_verified'){
			$('#status_inpassing').val('2');
		}
    });
</script>
@endsection

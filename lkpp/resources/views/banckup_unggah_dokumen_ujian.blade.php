@extends('layout.app2')

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }
  .main-page{
    margin-top: 20px;
  }

  .box-container{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    background-color: white;
    border-radius: 5px;
    padding: 3%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    max-width: 250px;
    line-height: 15px;
    width: 100%;
    margin: 5px;
  }

  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  p{
    font-weight: 500;
  }

  b{
    font-weight: 500;
  }

  .row a.btn{
    text-align: left;
    font-weight: 600;
    font-size: small;
  }

  .btn-default1{
      background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
      color: white;
      font-weight: 600;
      width: 100px; 
      margin: 10px;
      -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .btn-default2{
      background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
      color: black;
      font-weight: 600;
      width: 100px; 
      margin: 10px;
      -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .btn-default3{
    background: #fff;
    color: #000;
    font-weight: 500;
    width: 100px;
  }


  .row-input{
    padding-bottom: 10px;
  }

  .page div.verifikasi{
    display: none;
  }

  .btn-area{
        text-align: right;
    }

    .clickable-row:hover{
        cursor: pointer;
    }

.checkmark {
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #00aaaa;
}

    .modal .modal-dialog{
        max-width: 800px !important;
    }

    .line-center{
        line-height: 2.5;
    }

    span.berkas{
        font-size: 10px;
        font-weight: 600;
    }

    span.berkas label{
        margin-bottom: 0px !important;
    }
    [data-toggle="collapse"] .fa:before {  
      content: "\f139";
    }

    [data-toggle="collapse"].collapsed .fa:before {
      content: "\f13a";
    }

    .accor{
      margin-top: 35px;
    }

    .accor-body{
      margin: 0px 10px;
    }
    ol.ol-sub-a{
      padding-inline-start: 18px !important;
    }

    .btn-link{
      color: #000 !important;
    }
</style>
@endsection

@section('content')
<div class="main-page">
        <div class="container">
            <div class="row box-container">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12" style="">
                      <div class="row">
                      <div class="col-md-9">
                      <h5>Unggah Dokumen portofolio peserta ujian tanggal {{ Helper::tanggal_indo($jadwal->tanggal_verifikasi) }} (Verifikasi Portofolio)</h5>
                      {{-- {{ $data }} --}}
                      <hr>
                      @if (session('msg'))
                          @if (session('msg') == "berhasil")
                            <div class="alert alert-success alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>Berhasil Simpan Data</strong>
                            </div> 
                          @else
                          <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Gagal Simpan Data</strong>
                          </div> 
                          @endif
                      @endif
                      </div>
                      <div class="col-md-3" style="text-align:right">
                      <i class="fa fa-bell"></i>
                      </div>
                      <div class="col-md-9 page">
                      {{-- <form action="{{ url('store-dokumen-ujian') }}" method="post" enctype="multipart/form-data" id="store"> --}}
                        @csrf
                      <input type="hidden" name="tanggal_ujian" value="{{ Helper::tanggal_indo($jadwal->tanggal_verifikasi) }}">
                      <input type="hidden" name="lokasi_ujian" value="{{ $jadwal->lokasi_ujian }}">
                      <div class="row">
                        <div class="col-md-3">
                            Nama
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-8">
                            {{ $peserta->nama }}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                            Pang/Gol
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-8">
                            {{ $peserta->jabatan }}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                            Jenjang
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-8">
                            {{ $peserta->jenjang }}
                        </div>
                      </div>
                      <input type="hidden" name="id_peserta" value="{{ $peserta->id }}">
                      <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                      {{-- <div class="row">
                        <div class="col-md-12">
                            <br>
                            <h4>Unggah Dokumen Portofolio</h4>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
                        <div class="col-md-1 line-center">:</div>
                        <div class="col-md-6">
                            <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="kompetensi_perencanaan_pbjp" value="{{ old('kompetensi_perencanaan_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            @if ($dokumen != "")
                            <span class="berkas">*berkas sebelumnya:
                              @if ($dokumen->kompetensi_perencanaan_pbjp == "")
                                - 
                              @else
                                @php
                                $kompetensi_perencanaan_pbjp = explode('.',$dokumen->kompetensi_perencanaan_pbjp);
                                @endphp
                                @if ($kompetensi_perencanaan_pbjp[1] == 'pdf' || $kompetensi_perencanaan_pbjp[1] == 'PDF')
                                <a href="{{ url('priview-file')."/kompetensi_perencanaan_pbjp/".$dokumen->kompetensi_perencanaan_pbjp }}" target="_blank"><label>{{ $dokumen->kompetensi_perencanaan_pbjp }}</label></a>
                                @else
                                <a href="{{ url('priview-file')."/kompetensi_perencanaan_pbjp/".$dokumen->kompetensi_perencanaan_pbjp }}" target="_blank"><img src="{{ asset('storage/data/kompetensi_perencanaan_pbjp')."/".$dokumen->kompetensi_perencanaan_pbjp }}" class="img-rounded"></a>
                                @endif
                              @endif
                            </span>
                            @endif
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
                        <div class="col-md-1 line-center">:</div>
                        <div class="col-md-6">
                            <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="kompetensi_pemilihan_pbj" value="{{ old('kompetensi_pemilihan_pbj') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            @if ($dokumen != "")
                            <span class="berkas">*berkas sebelumnya:
                              @if ($dokumen->kompetensi_pemilihan_pbj == "")
                                - 
                              @else
                                @php
                                $kompetensi_pemilihan_pbj = explode('.',$dokumen->kompetensi_pemilihan_pbj);
                                @endphp
                                @if ($kompetensi_pemilihan_pbj[1] == 'pdf' || $kompetensi_pemilihan_pbj[1] == 'PDF')
                                <a href="{{ url('priview-file')."/kompetensi_pemilihan_pbj/".$dokumen->kompetensi_pemilihan_pbj }}" target="_blank"><label>{{ $dokumen->kompetensi_pemilihan_pbj }}</label></a>
                                @else
                                <a href="{{ url('priview-file')."/kompetensi_pemilihan_pbj/".$dokumen->kompetensi_pemilihan_pbj }}" target="_blank"><img src="{{ asset('storage/data/kompetensi_pemilihan_pbj')."/".$dokumen->kompetensi_pemilihan_pbj }}" class="img-rounded"></a>
                                @endif
                              @endif
                            </span>
                            @endif
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
                        <div class="col-md-1 line-center">:</div>
                        <div class="col-md-6">
                            <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="kompetensi_pengelolaan_kontrak_pbjp" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            @if ($dokumen != "")
                            <span class="berkas">*berkas sebelumnya:
                              @if ($dokumen->kompetensi_pengelolaan_kontrak_pbjp == "")
                                - 
                              @else
                                @php
                                $kompetensi_pengelolaan_kontrak_pbjp = explode('.',$dokumen->kompetensi_pengelolaan_kontrak_pbjp);
                                @endphp
                                @if ($kompetensi_pengelolaan_kontrak_pbjp[1] == 'pdf' || $kompetensi_pengelolaan_kontrak_pbjp[1] == 'PDF')
                                <a href="{{ url('priview-file')."/kompetensi_pengelolaan_kontrak_pbjp/".$dokumen->kompetensi_pengelolaan_kontrak_pbjp }}" target="_blank"><label>{{ $dokumen->kompetensi_pengelolaan_kontrak_pbjp }}</label></a>
                                @else
                                <a href="{{ url('priview-file')."/kompetensi_pengelolaan_kontrak_pbjp/".$dokumen->kompetensi_pengelolaan_kontrak_pbjp }}" target="_blank"><img src="{{ asset('storage/data/kompetensi_pengelolaan_kontrak_pbjp')."/".$dokumen->kompetensi_pengelolaan_kontrak_pbjp }}" class="img-rounded"></a>
                                @endif
                              @endif
                            </span>
                            @endif
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
                        <div class="col-md-1 line-center">:</div>
                        <div class="col-md-6">
                            <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="kompetensi_pbj_secara_swakelola" value="{{ old('kompetensi_pbj_secara_swakelola') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            @if ($dokumen != "")
                            <span class="berkas">*berkas sebelumnya:
                              @if ($dokumen->kompetensi_pbj_secara_swakelola == "")
                                - 
                              @else
                                @php
                                $kompetensi_pbj_secara_swakelola = explode('.',$dokumen->kompetensi_pbj_secara_swakelola);
                                @endphp
                                @if ($kompetensi_pbj_secara_swakelola[1] == 'pdf' || $kompetensi_pbj_secara_swakelola[1] == 'PDF')
                                <a href="{{ url('priview-file')."/kompetensi_pbj_secara_swakelola/".$dokumen->kompetensi_pbj_secara_swakelola }}" target="_blank"><label>{{ $dokumen->kompetensi_pbj_secara_swakelola }}</label></a>
                                @else
                                <a href="{{ url('priview-file')."/kompetensi_pbj_secara_swakelola/".$dokumen->kompetensi_pbj_secara_swakelola }}" target="_blank"><img src="{{ asset('storage/data/kompetensi_pbj_secara_swakelola')."/".$dokumen->kompetensi_pbj_secara_swakelola }}" class="img-rounded"></a>
                                @endif
                              @endif
                            </span>
                            @endif
                        </div>
                      </div> --}}
                      <div class="row accor">
                        <div class="container">
                          <div id="accordion">
                              <div id="headingOne">
                                <h5 class="mb-0">
                                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" type="button">
                                  A. Perencanaan Pengadaan Barang/Jasa Pemerintah <i class="fa" aria-hidden="true"></i>
                                  </button>
                                </h5>
                              </div>
                        
                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                              <form action="{{ url('unggah-dokumen-portofolio') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="poin" value="a">
                                <input type="hidden" name="id_peserta" value="{{ $peserta->id }}">
                                <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                <div class="accor-body">
                                  <ol type="1">
                                    <li>Penyusunan Spesifikasi Teknis dan KAK
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_penyusunan_spesifikasi_teknis_dan_kak" class="custom-file-input" id="surat_penyusunan_spesifikasi_teknis_dan_kak" value="{{ old('surat_penyusunan_spesifikasi_teknis_dan_kak') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_penyusunan_spesifikasi_teknis_dan_kak == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_penyusunan_spesifikasi_teknis_dan_kak = explode('.',$dokumen->surat_penyusunan_spesifikasi_teknis_dan_kak);
                                                    @endphp
                                                    @if ($surat_penyusunan_spesifikasi_teknis_dan_kak[1] == 'pdf' || $surat_penyusunan_spesifikasi_teknis_dan_kak[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_spesifikasi_teknis_dan_kak/".$dokumen->surat_penyusunan_spesifikasi_teknis_dan_kak }}" target="_blank"><label>{{ $dokumen->surat_penyusunan_spesifikasi_teknis_dan_kak }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_spesifikasi_teknis_dan_kak/".$dokumen->surat_penyusunan_spesifikasi_teknis_dan_kak }}" target="_blank"><img src="{{ asset('storage/data/surat_penyusunan_spesifikasi_teknis_dan_kak')."/".$dokumen->surat_penyusunan_spesifikasi_teknis_dan_kak }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_penyusunan_spesifikasi_teknis_dan_kak">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_penyusunan_spesifikasi_teknis_dan_kak" class="custom-file-input" id="dokumen_penyusunan_spesifikasi_teknis_dan_kak" value="{{ old('dokumen_penyusunan_spesifikasi_teknis_dan_kak') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_penyusunan_spesifikasi_teknis_dan_kak == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_penyusunan_spesifikasi_teknis_dan_kak = explode('.',$dokumen->dokumen_penyusunan_spesifikasi_teknis_dan_kak);
                                                    @endphp
                                                    @if ($dokumen_penyusunan_spesifikasi_teknis_dan_kak[1] == 'pdf' || $dokumen_penyusunan_spesifikasi_teknis_dan_kak[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_spesifikasi_teknis_dan_kak/".$dokumen->dokumen_penyusunan_spesifikasi_teknis_dan_kak }}" target="_blank"><label>{{ $dokumen->dokumen_penyusunan_spesifikasi_teknis_dan_kak }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_spesifikasi_teknis_dan_kak/".$dokumen->dokumen_penyusunan_spesifikasi_teknis_dan_kak }}" target="_blank"><img src="{{ asset('storage/data/dokumen_penyusunan_spesifikasi_teknis_dan_kak')."/".$dokumen->dokumen_penyusunan_spesifikasi_teknis_dan_kak }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_penyusunan_spesifikasi_teknis_dan_kak">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Penyusunan Perkiraan Harga untuk setiap tahapan PBJP
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp" class="custom-file-input" id="surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp" value="{{ old('surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp = explode('.',$dokumen->surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp);
                                                    @endphp
                                                    @if ($surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp[1] == 'pdf' || $surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp/".$dokumen->surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp }}" target="_blank"><label>{{ $dokumen->surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp/".$dokumen->surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp }}" target="_blank"><img src="{{ asset('storage/data/surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp')."/".$dokumen->surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp" class="custom-file-input" id="dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp" value="{{ old('dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp = explode('.',$dokumen->dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp);
                                                    @endphp
                                                    @if ($dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp[1] == 'pdf' || $dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp/".$dokumen->dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp }}" target="_blank"><label>{{ $dokumen->dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp/".$dokumen->dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp }}" target="_blank"><img src="{{ asset('storage/data/dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp')."/".$dokumen->dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Identifikasi/ Reviu Kebutuhan dan Penetapan Barang/Jasa
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang" class="custom-file-input" id="surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang" value="{{ old('surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang = explode('.',$dokumen->surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang);
                                                    @endphp
                                                    @if ($surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang[1] == 'pdf' || $surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang/".$dokumen->surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang }}" target="_blank"><label>{{ $dokumen->surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang/".$dokumen->surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang }}" target="_blank"><img src="{{ asset('storage/data/surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang')."/".$dokumen->surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang" class="custom-file-input" id="dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang" value="{{ old('dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang = explode('.',$dokumen->dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang);
                                                    @endphp
                                                    @if ($dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang[1] == 'pdf' || $dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang/".$dokumen->dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang }}" target="_blank"><label>{{ $dokumen->dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang/".$dokumen->dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang }}" target="_blank"><img src="{{ asset('storage/data/dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang')."/".$dokumen->dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_identifikasi_reviu_kebutuhan_dan_penetapan_barang">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Perumusan Strategi Pengadaan,Pemaketan dan cara Pengadaan
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_perumusan_strategi_pengadaan_pemaketan" class="custom-file-input" id="surat_perumusan_strategi_pengadaan_pemaketan" value="{{ old('surat_perumusan_strategi_pengadaan_pemaketan') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_perumusan_strategi_pengadaan_pemaketan == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_perumusan_strategi_pengadaan_pemaketan = explode('.',$dokumen->surat_perumusan_strategi_pengadaan_pemaketan);
                                                    @endphp
                                                    @if ($surat_perumusan_strategi_pengadaan_pemaketan[1] == 'pdf' || $surat_perumusan_strategi_pengadaan_pemaketan[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_perumusan_strategi_pengadaan_pemaketan/".$dokumen->surat_perumusan_strategi_pengadaan_pemaketan }}" target="_blank"><label>{{ $dokumen->surat_perumusan_strategi_pengadaan_pemaketan }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_perumusan_strategi_pengadaan_pemaketan/".$dokumen->surat_perumusan_strategi_pengadaan_pemaketan }}" target="_blank"><img src="{{ asset('storage/data/surat_perumusan_strategi_pengadaan_pemaketan')."/".$dokumen->surat_perumusan_strategi_pengadaan_pemaketan }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_perumusan_strategi_pengadaan_pemaketan">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_perumusan_strategi_pengadaan_pemaketan" class="custom-file-input" id="dokumen_perumusan_strategi_pengadaan_pemaketan" value="{{ old('dokumen_perumusan_strategi_pengadaan_pemaketan') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_perumusan_strategi_pengadaan_pemaketan == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_perumusan_strategi_pengadaan_pemaketan = explode('.',$dokumen->dokumen_perumusan_strategi_pengadaan_pemaketan);
                                                    @endphp
                                                    @if ($dokumen_perumusan_strategi_pengadaan_pemaketan[1] == 'pdf' || $dokumen_perumusan_strategi_pengadaan_pemaketan[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_perumusan_strategi_pengadaan_pemaketan/".$dokumen->dokumen_perumusan_strategi_pengadaan_pemaketan }}" target="_blank"><label>{{ $dokumen->dokumen_perumusan_strategi_pengadaan_pemaketan }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_perumusan_strategi_pengadaan_pemaketan/".$dokumen->dokumen_perumusan_strategi_pengadaan_pemaketan }}" target="_blank"><img src="{{ asset('storage/data/dokumen_perumusan_strategi_pengadaan_pemaketan')."/".$dokumen->dokumen_perumusan_strategi_pengadaan_pemaketan }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_dokumen_perumusan_strategi_pengadaan_pemaketan">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                  </ol>  
                                </div>
                                <div class="accor-footer">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <button type="submit" class="btn btn-default1 float-right">Simpan</button>
                                    </div>
                                  </div>
                                </div>
                              </form>
                              </div>
                              <div id="headingTwo">
                                <h5 class="mb-0">
                                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" type="button">
                                  B. Pemilihan Penyedia Barang/Jasa <i class="fa" aria-hidden="true"></i>
                                  </button>
                                </h5>
                              </div>
                              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="accor-body">
                                  <ol type="1">
                                    <li>Reviu terhadap Dokumen Persiapan PBJP
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_reviu_terhadap_dokumen_persiapan_pbjp" class="custom-file-input" id="surat_reviu_terhadap_dokumen_persiapan_pbjp" value="{{ old('surat_reviu_terhadap_dokumen_persiapan_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_reviu_terhadap_dokumen_persiapan_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_reviu_terhadap_dokumen_persiapan_pbjp = explode('.',$dokumen->surat_reviu_terhadap_dokumen_persiapan_pbjp);
                                                    @endphp
                                                    @if ($surat_reviu_terhadap_dokumen_persiapan_pbjp[1] == 'pdf' || $surat_reviu_terhadap_dokumen_persiapan_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_reviu_terhadap_dokumen_persiapan_pbjp/".$dokumen->surat_reviu_terhadap_dokumen_persiapan_pbjp }}" target="_blank"><label>{{ $dokumen->surat_reviu_terhadap_dokumen_persiapan_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_reviu_terhadap_dokumen_persiapan_pbjp/".$dokumen->surat_reviu_terhadap_dokumen_persiapan_pbjp }}" target="_blank"><img src="{{ asset('storage/data/surat_reviu_terhadap_dokumen_persiapan_pbjp')."/".$dokumen->surat_reviu_terhadap_dokumen_persiapan_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_reviu_terhadap_dokumen_persiapan_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_reviu_terhadap_dokumen_persiapan_pbjp" class="custom-file-input" id="dokumen_reviu_terhadap_dokumen_persiapan_pbjp" value="{{ old('dokumen_reviu_terhadap_dokumen_persiapan_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_reviu_terhadap_dokumen_persiapan_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_reviu_terhadap_dokumen_persiapan_pbjp = explode('.',$dokumen->dokumen_reviu_terhadap_dokumen_persiapan_pbjp);
                                                    @endphp
                                                    @if ($dokumen_reviu_terhadap_dokumen_persiapan_pbjp[1] == 'pdf' || $dokumen_reviu_terhadap_dokumen_persiapan_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_reviu_terhadap_dokumen_persiapan_pbjp/".$dokumen->dokumen_reviu_terhadap_dokumen_persiapan_pbjp }}" target="_blank"><label>{{ $dokumen->dokumen_reviu_terhadap_dokumen_persiapan_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_reviu_terhadap_dokumen_persiapan_pbjp/".$dokumen->dokumen_reviu_terhadap_dokumen_persiapan_pbjp }}" target="_blank"><img src="{{ asset('storage/data/dokumen_reviu_terhadap_dokumen_persiapan_pbjp')."/".$dokumen->dokumen_reviu_terhadap_dokumen_persiapan_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_reviu_terhadap_dokumen_persiapan_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Penyusunan dan Penjelasan Dokumen Pemilihan
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_penyusunan_dan_penjelasan_dokumen_pemilihan" class="custom-file-input" id="surat_penyusunan_dan_penjelasan_dokumen_pemilihan" value="{{ old('surat_penyusunan_dan_penjelasan_dokumen_pemilihan') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_penyusunan_dan_penjelasan_dokumen_pemilihan == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_penyusunan_dan_penjelasan_dokumen_pemilihan = explode('.',$dokumen->surat_penyusunan_dan_penjelasan_dokumen_pemilihan);
                                                    @endphp
                                                    @if ($surat_penyusunan_dan_penjelasan_dokumen_pemilihan[1] == 'pdf' || $surat_penyusunan_dan_penjelasan_dokumen_pemilihan[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_dan_penjelasan_dokumen_pemilihan/".$dokumen->surat_penyusunan_dan_penjelasan_dokumen_pemilihan }}" target="_blank"><label>{{ $dokumen->surat_penyusunan_dan_penjelasan_dokumen_pemilihan }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_dan_penjelasan_dokumen_pemilihan/".$dokumen->surat_penyusunan_dan_penjelasan_dokumen_pemilihan }}" target="_blank"><img src="{{ asset('storage/data/surat_penyusunan_dan_penjelasan_dokumen_pemilihan')."/".$dokumen->surat_penyusunan_dan_penjelasan_dokumen_pemilihan }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_penyusunan_dan_penjelasan_dokumen_pemilihan">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan" class="custom-file-input" id="dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan" value="{{ old('dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan = explode('.',$dokumen->dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan);
                                                    @endphp
                                                    @if ($dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan[1] == 'pdf' || $dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan/".$dokumen->dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan }}" target="_blank"><label>{{ $dokumen->dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan/".$dokumen->dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan }}" target="_blank"><img src="{{ asset('storage/data/dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan')."/".$dokumen->dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_penyusunan_dan_penjelasan_dokumen_pemilihan">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Evaluasi Penawaran
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_evaluasi_penawaran" class="custom-file-input" id="surat_evaluasi_penawaran" value="{{ old('surat_evaluasi_penawaran') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_evaluasi_penawaran == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_evaluasi_penawaran = explode('.',$dokumen->surat_evaluasi_penawaran);
                                                    @endphp
                                                    @if ($surat_evaluasi_penawaran[1] == 'pdf' || $surat_evaluasi_penawaran[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_evaluasi_penawaran/".$dokumen->surat_evaluasi_penawaran }}" target="_blank"><label>{{ $dokumen->surat_evaluasi_penawaran }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_evaluasi_penawaran/".$dokumen->surat_evaluasi_penawaran }}" target="_blank"><img src="{{ asset('storage/data/surat_evaluasi_penawaran')."/".$dokumen->surat_evaluasi_penawaran }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_evaluasi_penawaran">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_evaluasi_penawaran" class="custom-file-input" id="dokumen_evaluasi_penawaran" value="{{ old('dokumen_evaluasi_penawaran') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_evaluasi_penawaran == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_evaluasi_penawaran = explode('.',$dokumen->dokumen_evaluasi_penawaran);
                                                    @endphp
                                                    @if ($dokumen_evaluasi_penawaran[1] == 'pdf' || $dokumen_evaluasi_penawaran[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_evaluasi_penawaran/".$dokumen->dokumen_evaluasi_penawaran }}" target="_blank"><label>{{ $dokumen->dokumen_evaluasi_penawaran }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_evaluasi_penawaran/".$dokumen->dokumen_evaluasi_penawaran }}" target="_blank"><img src="{{ asset('storage/data/dokumen_evaluasi_penawaran')."/".$dokumen->dokumen_evaluasi_penawaran }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_evaluasi_penawaran">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Penilaian Kualifikasi
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_penilian_kualifikasi" class="custom-file-input" id="surat_penilian_kualifikasi" value="{{ old('surat_penilian_kualifikasi') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_penilian_kualifikasi == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_penilian_kualifikasi = explode('.',$dokumen->surat_penilian_kualifikasi);
                                                    @endphp
                                                    @if ($surat_penilian_kualifikasi[1] == 'pdf' || $surat_penilian_kualifikasi[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_penilian_kualifikasi/".$dokumen->surat_penilian_kualifikasi }}" target="_blank"><label>{{ $dokumen->surat_penilian_kualifikasi }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_penilian_kualifikasi/".$dokumen->surat_penilian_kualifikasi }}" target="_blank"><img src="{{ asset('storage/data/surat_penilian_kualifikasi')."/".$dokumen->surat_penilian_kualifikasi }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_penilian_kualifikasi">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_penilian_kualifikasi" class="custom-file-input" id="dokumen_penilian_kualifikasi" value="{{ old('dokumen_penilian_kualifikasi') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_penilian_kualifikasi == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_penilian_kualifikasi = explode('.',$dokumen->dokumen_penilian_kualifikasi);
                                                    @endphp
                                                    @if ($dokumen_penilian_kualifikasi[1] == 'pdf' || $dokumen_penilian_kualifikasi[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_/".$dokumen->dokumen_penilian_kualifikasi }}" target="_blank"><label>{{ $dokumen->dokumen_penilian_kualifikasi }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_penilian_kualifikasi/".$dokumen->dokumen_penilian_kualifikasi }}" target="_blank"><img src="{{ asset('storage/data/dokumen_penilian_kualifikasi')."/".$dokumen->dokumen_penilian_kualifikasi }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_penilian_kualifikasi">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Pengelolaan Sanggahan
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_pengelolaan_sanggahan" class="custom-file-input" id="surat_pengelolaan_sanggahan" value="{{ old('surat_pengelolaan_sanggahan') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_pengelolaan_sanggahan == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_pengelolaan_sanggahan = explode('.',$dokumen->surat_pengelolaan_sanggahan);
                                                    @endphp
                                                    @if ($surat_pengelolaan_sanggahan[1] == 'pdf' || $surat_pengelolaan_sanggahan[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_pengelolaan_sanggahan/".$dokumen->surat_pengelolaan_sanggahan }}" target="_blank"><label>{{ $dokumen->surat_pengelolaan_sanggahan }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_pengelolaan_sanggahan/".$dokumen->surat_pengelolaan_sanggahan }}" target="_blank"><img src="{{ asset('storage/data/surat_pengelolaan_sanggahan')."/".$dokumen->surat_pengelolaan_sanggahan }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_pengelolaan_sanggahan">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_pengelolaan_sanggahan" class="custom-file-input" id="dokumen_pengelolaan_sanggahan" value="{{ old('dokumen_pengelolaan_sanggahan') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_pengelolaan_sanggahan == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_pengelolaan_sanggahan = explode('.',$dokumen->dokumen_pengelolaan_sanggahan);
                                                    @endphp
                                                    @if ($dokumen_pengelolaan_sanggahan[1] == 'pdf' || $dokumen_pengelolaan_sanggahan[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_pengelolaan_sanggahan/".$dokumen->dokumen_pengelolaan_sanggahan }}" target="_blank"><label>{{ $dokumen->dokumen_pengelolaan_sanggahan }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_pengelolaan_sanggahan/".$dokumen->dokumen_pengelolaan_sanggahan }}" target="_blank"><img src="{{ asset('storage/data/dokumen_pengelolaan_sanggahan')."/".$dokumen->dokumen_pengelolaan_sanggahan }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_pengelolaan_sanggahan">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Penyusunan Daftar Penyedia
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_penyusunan_daftar_penyedia" class="custom-file-input" id="surat_penyusunan_daftar_penyedia" value="{{ old('surat_penyusunan_daftar_penyedia') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_penyusunan_daftar_penyedia == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_penyusunan_daftar_penyedia = explode('.',$dokumen->surat_penyusunan_daftar_penyedia);
                                                    @endphp
                                                    @if ($surat_penyusunan_daftar_penyedia[1] == 'pdf' || $surat_penyusunan_daftar_penyedia[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_daftar_penyedia/".$dokumen->surat_penyusunan_daftar_penyedia }}" target="_blank"><label>{{ $dokumen->surat_penyusunan_daftar_penyedia }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_daftar_penyedia/".$dokumen->surat_penyusunan_daftar_penyedia }}" target="_blank"><img src="{{ asset('storage/data/surat_penyusunan_daftar_penyedia')."/".$dokumen->surat_penyusunan_daftar_penyedia }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_penyusunan_daftar_penyedia">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_penyusunan_daftar_penyedia" class="custom-file-input" id="dokumen_penyusunan_daftar_penyedia" value="{{ old('dokumen_penyusunan_daftar_penyedia') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_penyusunan_daftar_penyedia == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_penyusunan_daftar_penyedia = explode('.',$dokumen->dokumen_penyusunan_daftar_penyedia);
                                                    @endphp
                                                    @if ($dokumen_penyusunan_daftar_penyedia[1] == 'pdf' || $dokumen_penyusunan_daftar_penyedia[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_daftar_penyedia/".$dokumen->dokumen_penyusunan_daftar_penyedia }}" target="_blank"><label>{{ $dokumen->dokumen_penyusunan_daftar_penyedia }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_daftar_penyedia/".$dokumen->dokumen_penyusunan_daftar_penyedia }}" target="_blank"><img src="{{ asset('storage/data/dokumen_penyusunan_daftar_penyedia')."/".$dokumen->dokumen_penyusunan_daftar_penyedia }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_penyusunan_daftar_penyedia">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Negosiasi dalam PBJP
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_negosiasi_dalam_pbjp" class="custom-file-input" id="surat_negosiasi_dalam_pbjp" value="{{ old('surat_negosiasi_dalam_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_negosiasi_dalam_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_negosiasi_dalam_pbjp = explode('.',$dokumen->surat_negosiasi_dalam_pbjp);
                                                    @endphp
                                                    @if ($surat_negosiasi_dalam_pbjp[1] == 'pdf' || $surat_negosiasi_dalam_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_negosiasi_dalam_pbjp/".$dokumen->surat_negosiasi_dalam_pbjp }}" target="_blank"><label>{{ $dokumen->surat_negosiasi_dalam_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_negosiasi_dalam_pbjp/".$dokumen->surat_negosiasi_dalam_pbjp }}" target="_blank"><img src="{{ asset('storage/data/surat_negosiasi_dalam_pbjp')."/".$dokumen->surat_negosiasi_dalam_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_surat_negosiasi_dalam_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_surat_negosiasi_dalam_pbjp" class="custom-file-input" id="dokumen_surat_negosiasi_dalam_pbjp" value="{{ old('dokumen_surat_negosiasi_dalam_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_surat_negosiasi_dalam_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_surat_negosiasi_dalam_pbjp = explode('.',$dokumen->dokumen_surat_negosiasi_dalam_pbjp);
                                                    @endphp
                                                    @if ($dokumen_surat_negosiasi_dalam_pbjp[1] == 'pdf' || $dokumen_surat_negosiasi_dalam_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_surat_negosiasi_dalam_pbjp/".$dokumen->dokumen_surat_negosiasi_dalam_pbjp }}" target="_blank"><label>{{ $dokumen->dokumen_surat_negosiasi_dalam_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_surat_negosiasi_dalam_pbjp/".$dokumen->dokumen_surat_negosiasi_dalam_pbjp }}" target="_blank"><img src="{{ asset('storage/data/dokumen_surat_negosiasi_dalam_pbjp')."/".$dokumen->dokumen_surat_negosiasi_dalam_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_surat_negosiasi_dalam_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                  </ol>  
                                </div>
                              </div>
                              <div id="headingThree">
                                <h5 class="mb-0">
                                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" type="button">
                                  C. Pengelolaan Kontrak Pengadaan Barang/Jasa Pemerintah <i class="fa" aria-hidden="true"></i>
                                  </button>
                                </h5>
                              </div>
                              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="accor-body">
                                  <ol type="1">
                                    <li>Perumusan Kontrak PBJP
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_perumusan_kontrak_pbjp" class="custom-file-input" id="surat_perumusan_kontrak_pbjp" value="{{ old('surat_perumusan_kontrak_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_perumusan_kontrak_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_perumusan_kontrak_pbjp = explode('.',$dokumen->surat_perumusan_kontrak_pbjp);
                                                    @endphp
                                                    @if ($surat_perumusan_kontrak_pbjp[1] == 'pdf' || $surat_perumusan_kontrak_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_perumusan_kontrak_pbjp/".$dokumen->surat_perumusan_kontrak_pbjp }}" target="_blank"><label>{{ $dokumen->surat_perumusan_kontrak_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_perumusan_kontrak_pbjp/".$dokumen->surat_perumusan_kontrak_pbjp }}" target="_blank"><img src="{{ asset('storage/data/surat_perumusan_kontrak_pbjp')."/".$dokumen->surat_perumusan_kontrak_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_perumusan_kontrak_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_perumusan_kontrak_pbjp" class="custom-file-input" id="dokumen_perumusan_kontrak_pbjp" value="{{ old('dokumen_perumusan_kontrak_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_perumusan_kontrak_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_perumusan_kontrak_pbjp = explode('.',$dokumen->dokumen_perumusan_kontrak_pbjp);
                                                    @endphp
                                                    @if ($dokumen_perumusan_kontrak_pbjp[1] == 'pdf' || $dokumen_perumusan_kontrak_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_perumusan_kontrak_pbjp/".$dokumen->dokumen_perumusan_kontrak_pbjp }}" target="_blank"><label>{{ $dokumen->dokumen_perumusan_kontrak_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_perumusan_kontrak_pbjp/".$dokumen->dokumen_perumusan_kontrak_pbjp }}" target="_blank"><img src="{{ asset('storage/data/dokumen_perumusan_kontrak_pbjp')."/".$dokumen->dokumen_perumusan_kontrak_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_perumusan_kontrak_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Pengendalian Pelaksanaan Kontrak PBJP
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_pengendalian_pelaksanaan_kontrak_pbjp" class="custom-file-input" id="surat_pengendalian_pelaksanaan_kontrak_pbjp" value="{{ old('surat_pengendalian_pelaksanaan_kontrak_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_pengendalian_pelaksanaan_kontrak_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_pengendalian_pelaksanaan_kontrak_pbjp = explode('.',$dokumen->surat_pengendalian_pelaksanaan_kontrak_pbjp);
                                                    @endphp
                                                    @if ($surat_pengendalian_pelaksanaan_kontrak_pbjp[1] == 'pdf' || $surat_pengendalian_pelaksanaan_kontrak_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_pengendalian_pelaksanaan_kontrak_pbjp/".$dokumen->surat_pengendalian_pelaksanaan_kontrak_pbjp }}" target="_blank"><label>{{ $dokumen->surat_pengendalian_pelaksanaan_kontrak_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_pengendalian_pelaksanaan_kontrak_pbjp/".$dokumen->surat_pengendalian_pelaksanaan_kontrak_pbjp }}" target="_blank"><img src="{{ asset('storage/data/surat_pengendalian_pelaksanaan_kontrak_pbjp')."/".$dokumen->surat_pengendalian_pelaksanaan_kontrak_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_pengendalian_pelaksanaan_kontrak_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_pengendalian_pelaksanaan_kontrak_pbjp" class="custom-file-input" id="dokumen_pengendalian_pelaksanaan_kontrak_pbjp" value="{{ old('dokumen_pengendalian_pelaksanaan_kontrak_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_pengendalian_pelaksanaan_kontrak_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_pengendalian_pelaksanaan_kontrak_pbjp = explode('.',$dokumen->dokumen_pengendalian_pelaksanaan_kontrak_pbjp);
                                                    @endphp
                                                    @if ($dokumen_pengendalian_pelaksanaan_kontrak_pbjp[1] == 'pdf' || $dokumen_pengendalian_pelaksanaan_kontrak_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_pengendalian_pelaksanaan_kontrak_pbjp/".$dokumen->dokumen_pengendalian_pelaksanaan_kontrak_pbjp }}" target="_blank"><label>{{ $dokumen->dokumen_pengendalian_pelaksanaan_kontrak_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_pengendalian_pelaksanaan_kontrak_pbjp/".$dokumen->dokumen_pengendalian_pelaksanaan_kontrak_pbjp }}" target="_blank"><img src="{{ asset('storage/data/dokumen_pengendalian_pelaksanaan_kontrak_pbjp')."/".$dokumen->dokumen_pengendalian_pelaksanaan_kontrak_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_pengendalian_pelaksanaan_kontrak_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Serah Terima Hasil PBJP
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_serah_terima_hasil_pbjp" class="custom-file-input" id="surat_serah_terima_hasil_pbjp" value="{{ old('surat_serah_terima_hasil_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_serah_terima_hasil_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_serah_terima_hasil_pbjp = explode('.',$dokumen->surat_serah_terima_hasil_pbjp);
                                                    @endphp
                                                    @if ($surat_serah_terima_hasil_pbjp[1] == 'pdf' || $surat_serah_terima_hasil_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_serah_terima_hasil_pbjp/".$dokumen->surat_serah_terima_hasil_pbjp }}" target="_blank"><label>{{ $dokumen->surat_serah_terima_hasil_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_serah_terima_hasil_pbjp/".$dokumen->surat_serah_terima_hasil_pbjp }}" target="_blank"><img src="{{ asset('storage/data/surat_serah_terima_hasil_pbjp')."/".$dokumen->surat_serah_terima_hasil_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_serah_terima_hasil_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_serah_terima_hasil_pbjp" class="custom-file-input" id="dokumen_serah_terima_hasil_pbjp" value="{{ old('dokumen_serah_terima_hasil_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_serah_terima_hasil_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_serah_terima_hasil_pbjp = explode('.',$dokumen->dokumen_serah_terima_hasil_pbjp);
                                                    @endphp
                                                    @if ($dokumen_serah_terima_hasil_pbjp[1] == 'pdf' || $dokumen_serah_terima_hasil_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_serah_terima_hasil_pbjp/".$dokumen->dokumen_serah_terima_hasil_pbjp }}" target="_blank"><label>{{ $dokumen->dokumen_serah_terima_hasil_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_serah_terima_hasil_pbjp/".$dokumen->dokumen_serah_terima_hasil_pbjp }}" target="_blank"><img src="{{ asset('storage/data/dokumen_serah_terima_hasil_pbjp')."/".$dokumen->dokumen_serah_terima_hasil_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_serah_terima_hasil_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Evaluasi Kinerja Penyedia PBJP
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_evaluasi_kinerja_penyedia_pbjp" class="custom-file-input" id="surat_evaluasi_kinerja_penyedia_pbjp" value="{{ old('surat_evaluasi_kinerja_penyedia_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_evaluasi_kinerja_penyedia_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_evaluasi_kinerja_penyedia_pbjp = explode('.',$dokumen->surat_evaluasi_kinerja_penyedia_pbjp);
                                                    @endphp
                                                    @if ($surat_evaluasi_kinerja_penyedia_pbjp[1] == 'pdf' || $surat_evaluasi_kinerja_penyedia_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_evaluasi_kinerja_penyedia_pbjp/".$dokumen->surat_evaluasi_kinerja_penyedia_pbjp }}" target="_blank"><label>{{ $dokumen->surat_evaluasi_kinerja_penyedia_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_evaluasi_kinerja_penyedia_pbjp/".$dokumen->surat_evaluasi_kinerja_penyedia_pbjp }}" target="_blank"><img src="{{ asset('storage/data/surat_evaluasi_kinerja_penyedia_pbjp')."/".$dokumen->surat_evaluasi_kinerja_penyedia_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_evaluasi_kinerja_penyedia_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_evaluasi_kinerja_penyedia_pbjp" class="custom-file-input" id="dokumen_evaluasi_kinerja_penyedia_pbjp" value="{{ old('dokumen_evaluasi_kinerja_penyedia_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_evaluasi_kinerja_penyedia_pbjp == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_evaluasi_kinerja_penyedia_pbjp = explode('.',$dokumen->dokumen_evaluasi_kinerja_penyedia_pbjp);
                                                    @endphp
                                                    @if ($dokumen_evaluasi_kinerja_penyedia_pbjp[1] == 'pdf' || $dokumen_evaluasi_kinerja_penyedia_pbjp[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_evaluasi_kinerja_penyedia_pbjp/".$dokumen->dokumen_evaluasi_kinerja_penyedia_pbjp }}" target="_blank"><label>{{ $dokumen->dokumen_evaluasi_kinerja_penyedia_pbjp }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_evaluasi_kinerja_penyedia_pbjp/".$dokumen->dokumen_evaluasi_kinerja_penyedia_pbjp }}" target="_blank"><img src="{{ asset('storage/data/dokumen_evaluasi_kinerja_penyedia_pbjp')."/".$dokumen->dokumen_evaluasi_kinerja_penyedia_pbjp }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_evaluasi_kinerja_penyedia_pbjp">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                  </ol>  
                                </div>
                              </div>
                              <div id="headingFour">
                                <h5 class="mb-0">
                                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" type="button">
                                  D. Pengelolaan Pengadaan Barang/Jasa Secara Swakelola <i class="fa" aria-hidden="true"></i>
                                  </button>
                                </h5>
                              </div>
                              <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="accor-body">
                                  <ol type="1">
                                    <li>Penyusunan rencana dan persiapan pengadaan barang/jasa secara swakelola
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_penyusunan_rencana_dan_persiapan_pengadaan" class="custom-file-input" id="surat_penyusunan_rencana_dan_persiapan_pengadaan" value="{{ old('surat_penyusunan_rencana_dan_persiapan_pengadaan') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_penyusunan_rencana_dan_persiapan_pengadaan == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_penyusunan_rencana_dan_persiapan_pengadaan = explode('.',$dokumen->surat_penyusunan_rencana_dan_persiapan_pengadaan);
                                                    @endphp
                                                    @if ($surat_penyusunan_rencana_dan_persiapan_pengadaan[1] == 'pdf' || $surat_penyusunan_rencana_dan_persiapan_pengadaan[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_rencana_dan_persiapan_pengadaan/".$dokumen->surat_penyusunan_rencana_dan_persiapan_pengadaan }}" target="_blank"><label>{{ $dokumen->surat_penyusunan_rencana_dan_persiapan_pengadaan }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_penyusunan_rencana_dan_persiapan_pengadaan/".$dokumen->surat_penyusunan_rencana_dan_persiapan_pengadaan }}" target="_blank"><img src="{{ asset('storage/data/surat_penyusunan_rencana_dan_persiapan_pengadaan')."/".$dokumen->surat_penyusunan_rencana_dan_persiapan_pengadaan }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_penyusunan_rencana_dan_persiapan_pengadaan">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_penyusunan_rencana_dan_persiapan_pengadaan" class="custom-file-input" id="dokumen_penyusunan_rencana_dan_persiapan_pengadaan" value="{{ old('dokumen_penyusunan_rencana_dan_persiapan_pengadaan') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_penyusunan_rencana_dan_persiapan_pengadaan == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_penyusunan_rencana_dan_persiapan_pengadaan = explode('.',$dokumen->dokumen_penyusunan_rencana_dan_persiapan_pengadaan);
                                                    @endphp
                                                    @if ($dokumen_penyusunan_rencana_dan_persiapan_pengadaan[1] == 'pdf' || $dokumen_penyusunan_rencana_dan_persiapan_pengadaan[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_rencana_dan_persiapan_pengadaan/".$dokumen->dokumen_penyusunan_rencana_dan_persiapan_pengadaan }}" target="_blank"><label>{{ $dokumen->dokumen_penyusunan_rencana_dan_persiapan_pengadaan }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_penyusunan_rencana_dan_persiapan_pengadaan/".$dokumen->dokumen_penyusunan_rencana_dan_persiapan_pengadaan }}" target="_blank"><img src="{{ asset('storage/data/dokumen_penyusunan_rencana_dan_persiapan_pengadaan')."/".$dokumen->dokumen_penyusunan_rencana_dan_persiapan_pengadaan }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_penyusunan_rencana_dan_persiapan_pengadaan">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                    <li>Pelaksanaan dan Pengawasan pengadaan barang/jasa secara swakelola
                                      <ol type="a" class="ol-sub-a">
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5">SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa" class="custom-file-input" id="surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa" value="{{ old('surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa = explode('.',$dokumen->surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa);
                                                    @endphp
                                                    @if ($surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa[1] == 'pdf' || $surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa/".$dokumen->surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa }}" target="_blank"><label>{{ $dokumen->surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa/".$dokumen->surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa }}" target="_blank"><img src="{{ asset('storage/data/surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa')."/".$dokumen->surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5">Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa">
                                            </div>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Dokumen Hasil Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6">
                                                <input type="file" name="dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa" class="custom-file-input" id="dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa" value="{{ old('dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa') }}" accept="image/jpg, application/pdf, image/jpeg" required >
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if ($dokumen != "")
                                                <span class="berkas">*berkas sebelumnya:
                                                  @if ($dokumen->dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa == "")
                                                    - 
                                                  @else
                                                    @php
                                                    $dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa = explode('.',$dokumen->dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa);
                                                    @endphp
                                                    @if ($dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa[1] == 'pdf' || $dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa[1] == 'PDF')
                                                    <a href="{{ url('priview-file')."/dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa/".$dokumen->dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa }}" target="_blank"><label>{{ $dokumen->dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa }}</label></a>
                                                    @else
                                                    <a href="{{ url('priview-file')."/dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa/".$dokumen->dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa }}" target="_blank"><img src="{{ asset('storage/data/dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa')."/".$dokumen->dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa }}" class="img-rounded"></a>
                                                    @endif
                                                  @endif
                                                </span>
                                                @endif
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-5 line-center">Tahun Paket Pekerjaan</div>
                                            <div class="col-md-1 line-center">:</div>
                                            <div class="col-md-6 p-0">
                                                <input type="text" class="form-control" name="tahun_paket_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa">
                                            </div>
                                          </div>
                                        </li>
                                      </ol>
                                    </li>
                                  </ol>  
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</button>
                                {{-- <button class="btn btn-sm btn-default1" type="submit" form="store">Simpan</button>     --}}
                            </div>
                      </div>
                      {{-- <button data-toggle="collapse" data-target="#demo">Collapsible</button>

                      <div id="demo" class="collapse">
                      Lorem ipsum dolor text....
                      </div> --}}
                      {{-- </form> --}}
                      </div>                     
                          <div class="col-md-3 text-center">
                              @include('layout.button_right_kpp')
                          </div>
                      </div>
                      </div>
                  </div>
                </div> 
              </div>
        </div>
        </div>
    </div>
@endsection

@section('js')
<script>
  $('#btn-tambah').click(function (e) {
        e.preventDefault();
        document.getElementById("verif").style.display = "block";
        document.getElementById("view").style.display = "none";
    });
    
    $('#btn-back').click(function (e) {
        e.preventDefault();
        document.getElementById("verif").style.display = "none";
        document.getElementById("view").style.display = "block";
    });

    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });

    $('input[type=radio][id=radioVerifikasi]').change(function() {
        if (this.value == 'verifikasi') {
            jQuery.noConflict(); 
            $('#myModal').modal('show');
        }
        else if (this.value == 'tes') {
            
        }
    });

    $('input[type=radio][id=radioVerifikasi]').click(function() {
        if (this.value == 'verifikasiada') {
            jQuery.noConflict(); 
            $('#myModal2').modal('show');
        }
        else if (this.value == 'tes') {
            
        }
    });

    // $(document).ready(function() {
    //     $(".form-check #radioVerifikasi").attr('disabled', true);
    // });

    $('.checkbox #checkboxDaftar').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {
        var input = "input[name=exampleRadios" + $(this).val() + "]";
        $(input).attr('disabled', false);
        $('#id_peserta_input').val($(this).val());
    } else {
        var input = "input[name=exampleRadios" + $(this).val() + "]";
        $(input).attr('disabled', true);
        $('#id_peserta_input').val('');
    }
    });

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    
</script> 
<script>
    $(document).ready(function(){
    
     $('#upload_form').on('submit', function(event){
      event.preventDefault();
      $.ajax({
       url:"{{ url('upload-portofolio') }}",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data)
       {
        // $('#message').css('display', 'block');
        // $('#message').html(data.message);
        // $('#message').addClass(data.class_name);
        // $('#uploaded_image').html(data.uploaded_image);
        if(data.msg == 'validator'){
          alert('validator error');
        }

        if(data.msg == 'berhasil'){
          var nameid = "#id_portofolio_" + data.id;
          $('.modal-body .custom-file-input').siblings(".custom-file-label").addClass("selected").html(' ');
          $('#myModal').modal('hide');
          $(nameid).val(data.id_db);
          alert('Berhasil Input');
        }

        if(data.msg == 'gagal'){
          alert('Gagal Input');
        }
       }
      })
     });
    
    });
    </script>
@endsection
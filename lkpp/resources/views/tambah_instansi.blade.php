@extends('layout.app')

@section('title')
    Tambah Instansi    
@endsection

@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
</style>
@endsection

@section('content')
@if ($action == 'add')
<form action="" method="post">
@csrf
<div class="main-box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Tambah Instansi</h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                ID
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='text' class="form-control" name="id" value="{{ old('id') }}" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('id') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                PRP ID
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='number' class="form-control" name="prp_id" value="{{ old('prp_id') }}" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('prp_id') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                KBP ID
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='number' class="form-control" name="kbp_id" value="{{ old('kbp_id') }}" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('kbp_id') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Nama
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='text' class="form-control" name="nama" value="{{ old('nama') }}" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('nama') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
              Jenis
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
              {{-- <select name="jenis" id="metode" class="form-control" required>
                <option value="" disabled selected>Pilih Jenis</option>
                <option value="PROVINSI">PROVINSI</option>
                <option value="KOTA">KOTA</option>
                <option value="KABUPATEN">KABUPATEN</option>
              </select> --}}
              {!! Form::select('jenis', array('KEMENTRIAN' => 'KEMENTRIAN','LEMBAGA' => 'LEMBAGA','PROVINSI' => 'PROVINSI', 'KOTA' => 'KOTA', 'KABUPATEN' => 'KABUPATEN') , old('jenis'), ['placeholder' => 'pilih jenis', 'class' => 'form-control']); !!}
              <span class="errmsg">{{ $errors->first('jenis') }}</span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Website 
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                  <input type='text' class="form-control" name="website" value="{{ old('website') }}" />
                </div>
                <span class="errmsg">{{ $errors->first('website') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9" style="text-align: right">
                <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
            </div>
        </div>
    </div>
</div>
</form>
@endif
@if ($action == 'edit')
<form action="" method="post">
@csrf
<div class="main-box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Edit Instansi</h3>
                <hr>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-md-3 col-xs-10">
                ID
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='text' class="form-control" name="id" value="{{ $data->id }}" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('id') }}</span>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-3 col-xs-10">
                PRP ID
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='number' class="form-control" name="prp_id" value="{{ $data->prp_id }}" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('prp_id') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                KBP ID
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='number' class="form-control" name="kbp_id" value="{{ $data->kbp_id }}" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('kbp_id') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Nama
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='text' class="form-control" name="nama" value="{{ $data->nama }}" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('nama') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
              Jenis
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
              {{-- <select name="jenis" id="metode" class="form-control" required>
                <option value="" disabled selected>Pilih Jenis</option>
                <option value="PROVINSI">PROVINSI</option>
                <option value="KOTA">KOTA</option>
                <option value="KABUPATEN">KABUPATEN</option>
              </select> --}}
              {!! Form::select('jenis', array('PROVINSI' => 'PROVINSI', 'KOTA' => 'KOTA', 'KABUPATEN' => 'KABUPATEN') , $data->jenis, ['placeholder' => 'pilih jenis', 'class' => 'form-control']); !!}
              <span class="errmsg">{{ $errors->first('jenis') }}</span>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Website 
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                  <input type='text' class="form-control" name="website" value="{{ $data->website }}" />
                </div>
                <span class="errmsg">{{ $errors->first('website') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9" style="text-align: right">
                <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
            </div>
        </div>
    </div>
</div>
</form>
@endif
@endsection

@section('js')
<script>
$('#metode').on('change', function() {
  var select = this.value;

  if(select == 'verifikasi_portofolio'){
    $('#input_verif').show();
    $('#input_tes').hide();
  }

  if(select == 'tes_tulis'){
    $('#input_verif').hide();
    $('#input_tes').show();
  }
});
</script>
<script type="text/javascript">
  $(function () {
      $('#datetimepicker1').datetimepicker({
        format: 'DD/MM/YYYY'
      });
  });

  $(function () {
      $('#datetimepicker3').datetimepicker({
        format: 'DD/MM/YYYY'
      });
  });

  $(function () {
      $('#datetimepicker4').datetimepicker({
        format: 'DD/MM/YYYY'
      });
  });
</script>
<script type="text/javascript">
  $(function () {
      $('#datetimepicker2').datetimepicker({
        format: 'LT'
      });
  });
</script>
<script>
$(document).ready(function() {
    $('#input_verif').hide();
    $('#input_tes').hide();
});
</script>
<script>
function AddBusinessDays(weekDaysToAdd) {
      var curdate = new Date();
      var realDaysToAdd = 0;
      while (weekDaysToAdd > 0){
        curdate.setDate(curdate.getDate()+1);
        realDaysToAdd++;
        //check if current day is business day
        if (noWeekendsOrHolidays(curdate)[0]) {
          weekDaysToAdd--;
        }
      }
      return realDaysToAdd;

    }


var date_billed = $('#datebilled').datepicker('getDate');
var date_overdue = new Date();
var weekDays = AddBusinessDays(30);
date_overdue.setDate(date_billed.getDate() + weekDays);
date_overdue = $.datepicker.formatDate('mm/dd/yy', date_overdue);
$('#datepd').val(date_overdue).prop('readonly', true);
</script>
@endsection


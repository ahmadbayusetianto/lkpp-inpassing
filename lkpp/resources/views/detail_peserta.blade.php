@extends('layout.app2')
@section('title')
	Detail Peserta
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}

	.main-page{
		margin-top: 20px;
		font-weight: 600;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.form-control{
		margin :5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		border-radius: 5px;
	}

	textarea{
		margin :5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		border-radius: 5px;
	}

	.form-control{
		height: 30px;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.custom-file label.custom-file-label{
		margin: 5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
	}

	.row-input{
		padding-bottom: 10px;
	}

	.main-page img{
		width: 150px;
	}

	i.fa-file-pdf{
		font-size: 50px !important;
	}
</style>
@endsection
@section('content')
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-9" style="">
						<div class="row">
							<div class="col-md-12">
								<h5>Detail Peserta</h5><hr>
								@if (session('msg'))
									@if (session('msg') == "berhasil")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil simpan data</strong>
										</div> 
									@else
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal simpan data</strong>
										</div> 
									@endif
								@endif
							</div>
							<div class="col-md-11">
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Nama</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->nama }}</b>
										<span class="errmsg">{{ $errors->first('pertama') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">NIP</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										{{ $data->nip }}
										<span class="errmsg">{{ $errors->first('nip') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Pangkat/Gol.</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->jabatan }}</b>
										<span class="errmsg">{{ $errors->first('jabatan') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">TMT Pangkat/Gol.</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ Helper::tanggal_indo($data->tmt_panggol) }}</b>
										<span class="errmsg">{{ $errors->first('tmt_panggol') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Usulan Jenjang</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ ucwords($data->jenjang) }}</b>
										<span class="errmsg">{{ $errors->first('jenjang') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">No. Surat Permohonan Mengikuti Penyesuaian/Inpassing</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->no_surats }}</b>
										<span class="errmsg">{{ $errors->first('jenjang') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Tempat Lahir</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->tempat_lahir }}</b>
										<span class="errmsg">{{ $errors->first('jenjang') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Tanggal Lahir</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ Helper::tanggal_indo($data->tanggal_lahir) }}</b>
										<span class="errmsg">{{ $errors->first('jenjang') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Jenis Kelamin</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ Helper::getGenderStatus($data->jenis_kelamin) }}</b>
										<span class="errmsg">{{ $errors->first('jenjang') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">No. KTP</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->no_ktp }}</b>
										<span class="errmsg">{{ $errors->first('jenjang') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">No. Sertifikat PBJ Tk. Dasar</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->no_sertifikat }}</b>
										<span class="errmsg">{{ $errors->first('jenjang') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Pendidikan Terakhir</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->pendidikan_terakhir }}</b>
										<span class="errmsg">{{ $errors->first('jenjang') }}</span>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Email</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->email }}</b>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">No. Telp/HP</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->no_telp }}</b>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Nama Instansi</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->instansis }}</b>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Nama Satuan Kerja/OPD</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->satuan_kerja }}</b>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Provinsi</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->provinsis }}</b>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Kabupaten/Kota</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
										<b>{{ $data->kotas }}</b>
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Surat Ket Bertugas di bidang PBJ min. 2 tahun</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->surat_bertugas_pbj))
										-
									@else
										@php
											$surat_bertugas_pbj = explode('.',$data->surat_bertugas_pbj);
										@endphp
										@if ($surat_bertugas_pbj[1] == 'pdf' || $surat_bertugas_pbj[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_bertugas_pbj/".$data->surat_bertugas_pbj }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_bertugas_pbj/".$data->surat_bertugas_pbj }}" target="_blank"><img src="{{ asset('storage/data/surat_bertugas_pbj')."/".$data->surat_bertugas_pbj }}" class="img-rounded"></a>
										@endif 
									@endif     
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Bukti SK di bidang PBJ</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->bukti_sk_pbj))
										-
									@else
										@php
											$bukti_sk_pbj = explode('.',$data->bukti_sk_pbj);
										@endphp
										@if ($bukti_sk_pbj[1] == 'pdf' || $bukti_sk_pbj[1] == 'PDF')
											<a href="{{ url('priview-file')."/bukti_sk_pbj/".$data->bukti_sk_pbj }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/bukti_sk_pbj/".$data->bukti_sk_pbj }}" target="_blank"><img src="{{ asset('storage/data/bukti_sk_pbj')."/".$data->bukti_sk_pbj }}" class="img-rounded"></a>
										@endif     
									@endif 
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Ijazah terakhir (min. S1/D4)</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->ijazah_terakhir))
										-
									@else
										@php
											$ijazah_terakhir = explode('.',$data->ijazah_terakhir);
										@endphp
										@if ($ijazah_terakhir[1] == 'pdf' || $ijazah_terakhir[1] == 'PDF')
											<a href="{{ url('priview-file')."/ijazah_terakhir/".$data->ijazah_terakhir }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/ijazah_terakhir/".$data->ijazah_terakhir }}" target="_blank"><img src="{{ asset('storage/data/ijazah_terakhir')."/".$data->ijazah_terakhir }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">SK Kenaikan Pangkat Terakhir</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->sk_kenaikan_pangkat_terakhir))
										-
									@else
										@php
											$sk_kenaikan_pangkat_terakhir = explode('.',$data->sk_kenaikan_pangkat_terakhir);
										@endphp
										@if ($sk_kenaikan_pangkat_terakhir[1] == 'pdf' || $sk_kenaikan_pangkat_terakhir[1] == 'PDF')
											<a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->sk_kenaikan_pangkat_terakhir }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">SK Pengangkatan ke dalam Jabatan Terakhir</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->sk_pengangkatan_jabatan_terakhir))
										-
									@else
										@php
											$sk_pengangkatan_jabatan_terakhir = explode('.',$data->sk_pengangkatan_jabatan_terakhir);
										@endphp
										@if ($sk_pengangkatan_jabatan_terakhir[1] == 'pdf' || $sk_pengangkatan_jabatan_terakhir[1] == 'PDF')
											<a href="{{ url('priview-file')."/sk_pengangkatan_jabatan_terakhir/".$data->sk_pengangkatan_jabatan_terakhir }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sk_pengangkatan_jabatan_terakhir/".$data->sk_pengangkatan_jabatan_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_jabatan_terakhir')."/".$data->sk_pengangkatan_jabatan_terakhir }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10"> Sertifikat PBJ Tk. Dasar </div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->sertifikasi_dasar_pbj))
										-
									@else
										@php
											$sertifikasi_dasar_pbj = explode('.',$data->sertifikasi_dasar_pbj);
										@endphp
										@if ($sertifikasi_dasar_pbj[1] == 'pdf' || $sertifikasi_dasar_pbj[1] == 'PDF')
											<a href="{{ url('priview-file')."/sertifikasi_dasar_pbj/".$data->sertifikasi_dasar_pbj }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sertifikasi_dasar_pbj/".$data->sertifikasi_dasar_pbj }}" target="_blank"><img src="{{ asset('storage/data/sertifikasi_dasar_pbj')."/".$data->sertifikasi_dasar_pbj }}" class="img-rounded"></a>
										@endif
									@endif       
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">SKP (2 tahun terakhir)</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->skp_dua_tahun_terakhir))
										-
									@else
										@php
											$skp_dua_tahun_terakhir = explode('.',$data->skp_dua_tahun_terakhir);
										@endphp
										@if ($skp_dua_tahun_terakhir[1] == 'pdf' || $skp_dua_tahun_terakhir[1] == 'PDF')
											<a href="{{ url('priview-file')."/skp_dua_tahun_terakhir/".$data->skp_dua_tahun_terakhir }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/skp_dua_tahun_terakhir/".$data->skp_dua_tahun_terakhir }}" target="_blank"><img src="{{ asset('storage/data/skp_dua_tahun_terakhir')."/".$data->skp_dua_tahun_terakhir }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Surat Ket Tidak Sedang Dijatuhi Hukuman</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->surat_ket_tidak_dijatuhi_hukuman))
										-
									@else
										@php
											$surat_ket_tidak_dijatuhi_hukuman = explode('.',$data->surat_ket_tidak_dijatuhi_hukuman);
										@endphp
										@if ($surat_ket_tidak_dijatuhi_hukuman[1] == 'pdf' || $surat_ket_tidak_dijatuhi_hukuman[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_ket_tidak_dijatuhi_hukuman/".$data->surat_ket_tidak_dijatuhi_hukuman }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_ket_tidak_dijatuhi_hukuman/".$data->surat_ket_tidak_dijatuhi_hukuman }}" target="_blank"><img src="{{ asset('storage/data/surat_ket_tidak_dijatuhi_hukuman')."/".$data->surat_ket_tidak_dijatuhi_hukuman }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Formulir Kesediaan Mengikuti Penyesuaian/Inpassing</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->formulir_kesediaan_mengikuti_inpassing))
										-
									@else
										@php
											$formulir_kesediaan_mengikuti_inpassing = explode('.',$data->formulir_kesediaan_mengikuti_inpassing);
										@endphp
										@if ($formulir_kesediaan_mengikuti_inpassing[1] == 'pdf' || $formulir_kesediaan_mengikuti_inpassing[1] == 'PDF')
											<a href="{{ url('priview-file')."/formulir_kesediaan_mengikuti_inpassing/".$data->formulir_kesediaan_mengikuti_inpassing }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/formulir_kesediaan_mengikuti_inpassing/".$data->formulir_kesediaan_mengikuti_inpassing }}" target="_blank"><img src="{{ asset('storage/data/formulir_kesediaan_mengikuti_inpassing')."/".$data->formulir_kesediaan_mengikuti_inpassing }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">SK Pengangkatan CPNS</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->sk_pengangkatan_cpns))
										-
									@else
										@php
											$sk_pengangkatan_cpns = explode('.',$data->sk_pengangkatan_cpns);
										@endphp
										@if ($sk_pengangkatan_cpns[1] == 'pdf' || $sk_pengangkatan_cpns[1] == 'PDF')
											<a href="{{ url('priview-file')."/sk_pengangkatan_cpns/".$data->sk_pengangkatan_cpns }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sk_pengangkatan_cpns/".$data->sk_pengangkatan_cpns }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_cpns')."/".$data->sk_pengangkatan_cpns }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">SK Pengangkatan PNS</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->sk_pengangkatan_pns))
										-
									@else
										@php
											$sk_pengangkatan_pns = explode('.',$data->sk_pengangkatan_pns);
										@endphp
										@if ($sk_pengangkatan_pns[1] == 'pdf' || $sk_pengangkatan_pns[1] == 'PDF')
											<a href="{{ url('priview-file')."/sk_pengangkatan_pns/".$data->sk_pengangkatan_pns }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/sk_pengangkatan_pns/".$data->sk_pengangkatan_pns }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_pns')."/".$data->sk_pengangkatan_pns }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">KTP</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->ktp))
										-
									@else
										@php
											$ktp = explode('.',$data->ktp);
										@endphp
										@if ($ktp[1] == 'pdf' || $ktp[1] == 'PDF')
											<a href="{{ url('priview-file')."/ktp/".$data->ktp }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/ktp/".$data->ktp }}" target="_blank"><img src="{{ asset('storage/data/ktp')."/".$data->ktp }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Pas Foto 3 X 4</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->pas_foto_3_x_4))
										-
									@else
										@php
											$pas_foto_3_x_4 = explode('.',$data->pas_foto_3_x_4);
										@endphp
										@if ($pas_foto_3_x_4[1] == 'pdf' || $pas_foto_3_x_4[1] == 'PDF')
											<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$data->pas_foto_3_x_4 }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$data->pas_foto_3_x_4 }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4')."/".$data->pas_foto_3_x_4 }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
									<div class="col-lg-4 col-md-11 col-10">Surat Pernyataan Bersedia diangkat JF PPBJ</div>
									<div class="col-lg-1 col-md-1 col-1">:</div>
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->surat_pernyataan_bersedia_jfppbj))
										-
									@else
										@php
											$surat_pernyataan_bersedia_jfppbj = explode('.',$data->surat_pernyataan_bersedia_jfppbj);
										@endphp
										@if ($surat_pernyataan_bersedia_jfppbj[1] == 'pdf' || $surat_pernyataan_bersedia_jfppbj[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_pernyataan_bersedia_jfppbj/".$data->surat_pernyataan_bersedia_jfppbj }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_pernyataan_bersedia_jfppbj/".$data->surat_pernyataan_bersedia_jfppbj }}" target="_blank"><img src="{{ asset('storage/data/surat_pernyataan_bersedia_jfppbj')."/".$data->surat_pernyataan_bersedia_jfppbj }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								</div>
								<div class="row row-input">
								<div class="col-lg-4 col-md-11 col-10">Surat Permohonan Mengikuti Penyesuaian/Inpassing</div>
								<div class="col-lg-1 col-md-1 col-1">:</div>
								@if($dataSuratUsulan !="")
									<div class="col-lg-7 col-md-12">
									@if (is_null($dataSuratUsulan->file))
										-
									@else
										@php
											$surat_usulan_mengikuti_inpassing = explode('.',$dataSuratUsulan->file);
										@endphp
										@if ($surat_usulan_mengikuti_inpassing[1] == 'pdf' || $surat_usulan_mengikuti_inpassing[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_usulan_inpassing/".$dataSuratUsulan->file }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_usulan_inpassing/".$dataSuratUsulan->file }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan_inpassing')."/".$dataSuratUsulan->file }}" class="img-rounded"></a>
										@endif
									@endif      
									</div>
								@else
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->surat_usulan_mengikuti_inpassing))
										-
									@else
										@php
											$surat_usulan_inpassing = explode('.',$data->surat_usulan_mengikuti_inpassing);
										@endphp
										@if ($surat_usulan_inpassing[1] == 'pdf' || $surat_usulan_inpassing[1] == 'PDF')
											<a href="{{ url('priview-file')."/surat_usulan_mengikuti_inpassing/".$data->surat_usulan_mengikuti_inpassing }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:30px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
										@else
											<a href="{{ url('priview-file')."/surat_usulan_mengikuti_inpassing/".$data->surat_usulan_mengikuti_inpassing }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan_mengikuti_inpassing')."/".$data->surat_usulan_mengikuti_inpassing }}" class="img-rounded"></a>
										@endif
									@endif
									</div>
								@endif
                            </div>
                            <div class="row row-input">
								<div class="col-lg-4 col-md-11 col-10">No. Surat Permohonan Mengikuti Penyesuaian/Inpassing</div>
								<div class="col-lg-1 col-md-1 col-1">:</div>
								@if($dataSuratUsulan != "")
									<div class="col-lg-7 col-md-12">
									@if (is_null($dataSuratUsulan->no_surat_usulan_peserta))
										-
									@else
										{{ $dataSuratUsulan->no_surat_usulan_peserta }}
									@endif
									</div>
								@else
									<div class="col-lg-7 col-md-12">
									@if (is_null($data->no_surat_usulan_peserta))
										<label>-</label>
									@else
										<label>{{$data->no_surat_usulan_peserta}}</label>
									@endif
									</div>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style="text-align:right">
								<button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Kembali</button>
							</div>
                        </div>
					</div>
				</div>
				<div class="col-md-3 text-center">
					@include('layout.button_right_kpp')
				</div>
			</div>
		</div> 
	</div>
</div>   
@endsection
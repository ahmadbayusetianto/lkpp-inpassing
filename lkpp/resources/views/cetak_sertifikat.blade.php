@extends('layout.app')
@section('title')
	Cetak Sertifikat Inpassing
@stop
@section('css')
<style>
	.btn-tbh{
		text-align: right;
	}

	.btn-jadwal{
		width: 120px;
		background: #E8382A;
		color: #fff;
		font-weight: 600;
	}

	.btn-jadwal:hover{
		color: #000;
	}

	.btn-generate{
		width: 120px;
		background: #4C3E59;
		color: #fff;
		font-weight: 600;
		margin:1%;
		transition-duration: 0.5s;
	}
	
	.btn-generate:hover{
		background: #0B0411;
		color: #fff;
	}
</style>
@stop
@section('content')
<h2>Cetak Sertifikat Inpassing</h2><br>
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2 col-6">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-3 col-6">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal Inpassing</th>
					<th>Lokasi Ujian</th>
					<th>Metode Inpassing</th>
					<th>Aksi</th> 
				</tr>
			</thead>
			<tbody>
			@foreach($jadwal as $key => $jadwals)
				<tr>
					<td>{{ $key++ + 1 }}</td>
					<td>{{ Helper::tanggal_indo($jadwals->tanggal_ujian) }}</td>
					<td>{{ $jadwals->lokasi_ujian }}</td>
					<td>{{ $jadwals->metode == 'tes_tulis' ? 'Uji Tertulis' : 'Verifikasi Portofolio'}}</td>
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu">
								<li><a href="{{ url('cetak-sertifikat-peserta/'.$jadwals->tipe_ujian.'/'.$jadwals->id)}}">Lihat Peserta</a></li>
							</ul>
						</div>
					</td> 
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
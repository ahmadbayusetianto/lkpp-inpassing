<!DOCTYPE html>
<html>
	<head>
		<title>Peserta Lulus</title>
	</head>
	<style>

		body {
			font-family: 'calibri',\"Verdana\", Georgia, Serif;
		}

		table{
			border-collapse: collapse;
			width: 100%;
		}





		/*table.tbl, td, th{
			border: 1px solid black;
		}*/


		.head th{
			border-top: 3px solid #000;
			border-bottom: 1px solid #000;
		}
		.tbl tr td{
			padding: 5px;
		}
		.tbl tr th{
			padding: 5px;
		}

		.data tr td{
			padding: 5px;
		}
		.info li{
			font-size: 12px
		}
	</style>
	<body>
	<p style="text-align: center;">
		<b>
			DAFTAR PESERTA LULUS<br>
			UJI KOMPETENSI PENYESUAIAN/INPASSING
		</b>
	</p>
	<br>
	<table border="0" class="data">
		<tr>
			<td width="20%">Penyelenggara</td>
			<td width="2%">:</td>
			<td width="25%">{{ $penyelengara }}</td>

			<td width="8%"></td>

			<td width="35%">Total Peserta</td>
			<td width="2%">:</td>
			<td width="10%">{{ $jumlah_peserta }}</td>

		</tr>
		<tr>
			<td width="20%">Lokasi</td>
			<td width="2%">:</td>
			<td width="25%">{{ $lokasi }}</td>

			<td width="8%"></td>

			<td width="35%">Peserta Memenuhi Persyaratan</td>
			<td width="2%">:</td>
			<td width="10%">{{ $jumlah_memenuhi_syarat }}</td>

		</tr>
		<tr>
			<td width="20%">Metode Uji</td>
			<td width="2%">:</td>
			<td width="25%">{{ $metode_ujian }}</td>

			<td width="8%"></td>

			<td width="35%">Peserta Lulus</td>
			<td width="2%">:</td>
			<td width="10%">{{ $jumlah_lulus }}</td>

		</tr>
		<tr>
			<td width="20%">Tanggal</td>
			<td width="2%">:</td>
			<td width="25%">{{ $tanggal_ujian }}</td>

			<td width="8%"></td>

			<td width="20%"></td>
			<td width="2%"></td>
			<td width="25%"></td>

		</tr>
	</table>
	<br>
	<div class="content" style="margin-top: 5px">
		<table class="tbl"  style="font-size: 12px;">
			<thead>
			<tr class="head">
				<th style="text-align: left;">No</th>
				<th style="text-align: left;">Nama</th>
				<th style="text-align: left;">Jenjang</th>
				<th style="text-align: left;">Instansi</th>
			</tr>
			</thead>
			<tbody>
			@php
			$n = count($data);
			$i = 1;
			@endphp
			@if(count($data) > 0)
			@foreach($data as $key => $datas)

			@if($i == $n)
			<tr>
				<td style="text-align: left;border-bottom: 1px solid #000">{{ $key++ + 1 }}</td>
				<td style="text-align: left;border-bottom: 1px solid #000">{{ $datas->nama }}</td>
				@if($datas->jenjangs == "")
				<td style="text-align: left;border-bottom: 1px solid #000">{{ $datas->jenjang }}</td>
				@else
				<td style="text-align: left;border-bottom: 1px solid #000">{{ $datas->jenjangs }}</td>
				@endif
				<td style="text-align: left;border-bottom: 1px solid #000">{{ $datas->instansis }}</td>
			</tr>
			@else
				<tr>
				<td style="text-align: left;">{{ $key++ + 1 }}</td>
				<td style="text-align: left;">{{ $datas->nama }}</td>
				@if($datas->jenjangs == "")
				<td style="text-align: left;">{{ $datas->jenjang }}</td>
				@else
				<td style="text-align: left;">{{ $datas->jenjangs }}</td>
				@endif
				<td style="text-align: left;">{{ $datas->instansis }}</td>
			</tr>
			@endif
			@php
			$i++;
			@endphp
			@endforeach
			@else
			<tr>
				<td colspan="4" style="text-align: center;">
					Tidak ada peserta
				</td>
			</tr>
			@endif
			</tbody>
		</table>
		<br>
		<p style="font-size: 11px;">
			<b>Informasi Tambahan :</b>
			<br>
			<ul type="A" class="info">
				<li>Peserta yang tidak tercantum namanya dinyatakan Belum Lulus Uji Kompetensi Inpassing.</li>
				<li>Peserta Uji Kompetensi yang belum Lulus dapat mengikuti Uji Kompetensi ulang dengan metode Verifikasi Portofolio/Tes Tertulis terakhir; atau
					<ul type="1">
						<li>Mengusulkan kembali Portofolio paling cepat 2(dua) bulan setelah hasil Uji Kompetensi dengan metode Verifikasi Portofolio/Tes Tertulis terakhir; atau</li>
						<li> Mengikuti Tes Tertulis sesuai jadwal tersedia tanpa dibatasi jumlahnya.</li>
					</ul>
				</li>
			</ul>


		</p>
		<br>
		<br>
		@if($set != "")
		@php
		$align = "text-align:left";
		if($set->align == 1){
			$align = "text-align:left";
		}elseif ($set->align == 2) {
			$align = "text-align:center";
		}elseif ($set->align == 3) {
			$align = "text-align:right";
		}
		@endphp
		@if($set->posisi_ttd == 1)
		<div style="margin-left: 0%;font-size: 12px;width: 32%;{{ $align }}">
		@elseif($set->posisi_ttd == 2)
		<div style="margin-left: 35%;font-size: 12px;width: 32%;{{ $align }}">
		@elseif($set->posisi_ttd == 3)
		<div style="margin-left: 65%;font-size: 12px;width: 32%;{{ $align }}">
		@endif
		@endif
		{{ $set->lokasi_ttd }},&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ ' '.Helper::bulan_tahun(date('m-Y'))  }}<br>
		@if($set != "")
		{{ $set->jabatan }}
		@else
		Direktur Sertifikasi Profesi, selaku <br>
		Ketua Lembaga Sertifikasi Profesi LKPP
		@endif
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		@if($set != "")
		{{ $set->nama }}
		@else
		Dwi Wahyuni Kartianingsih
		@endif

		</div>
	</div>
	</body>
</html>

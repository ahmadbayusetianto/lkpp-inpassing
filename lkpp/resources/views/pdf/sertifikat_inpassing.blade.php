@php
	$no_seri = $data->no_seri_sertifikat;
@endphp
<br><br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<barcode code="{{ $no_seri }}" type="C39" height="0.9" size="0.5" bgcolor="#FFFFFF" align="left"/><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<font face="calibri" size="11">{{ $no_seri }}</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div align="center">
    <img src="{{ asset('storage/data/pas_foto_3_x_4')."/".$data->pas_foto_3_x_4 }}" style="width:113px;height:151px;" />
</div>
<br>
<table width="100%">
    <tr>
        <td align="center">
            <h2>
                <i>
                  {{ $data->nama }}
                </i>
                <br>
                <i>
                    {{ $data->nip }}
                </i>
            </h2>
        </td>
    </tr>
</table>
<br>
<br>
&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
<table width="100%">
    <tr>
        <td align="center">
			<font face="calibri" size="14"><b>Jabatan Fungsional Pengelola Pengadaan Barang/Jasa {{ $data->jenjang }}</b></font>
            <br><font face="calibri" size="14"><b>Lulus dengan Metode {{ $metode_inpassing }}</b></font><br><br>
            <font face="calibri" size="14">Jakarta, {{ Helper::tanggal_indo($tgl_sertifikat) }} </font>
            <br>
            <b><font face="calibri" size="12">{{ $data_deputi->jabatan }}</font></b>
            <br>
            <font face="calibri" size="12" color="#808080"><i>{{ $data_deputi->jabatan_english }}</i></font>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <font face="calibri" size="12"><b>{{ $data_deputi->nama_deputi }}</b></font>
            <br>
            <font face="calibri" size="12"><b>{{ $data_deputi->nip_deputi }}</b></font>
        </td>
    </tr>
</table>
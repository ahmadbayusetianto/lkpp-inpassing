@extends('layout.app')
@section('title')
	Ubah Jadwal Inpassing
@stop
@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
</style>
@endsection
@section('content')
@if (session('msg'))
@if (session('msg') == "gagal")
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Tanggal Batas Waktu Pendaftaran tidak boleh lebih dari tanggal tes</strong>
        </div>
    </div>
</div> 
@endif
@endif
<form action="" method="post">
    @csrf
    <div class="main-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Ubah Jadwal Inpassing Reguler</h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-10">
                  Metode Ujian
              </div>
              <div class="col-md-1 col-xs-1">:</div>
              <div class="col-md-5 col-xs-12">
                {{ Form::select('metode_ujian_2', array('verifikasi_portofolio' => 'Verifikasi Portofolio', 'tes_tulis' => 'Tes Tertulis'),$jadwal->metode, ['class' => "form-control", 'id' => "metode", 'disabled' ]) }}
                <input type="hidden" name="metode_ujian" value="{{ $jadwal->metode }}">
                <span class="errmsg">{{ $errors->first('metode_ujian') }}</span>
            </div>
        </div>
        <br>
        @if ($jadwal->metode == 'verifikasi_portofolio')
        <div id="input_verif">
            <div class="row">
                <div class="col-md-3 col-xs-10">
                    Tanggal verifikasi Portofolio
                </div>
                <div class="col-md-1 col-xs-1">:</div>
                <div class="col-md-5 col-xs-12">
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" name="tanggal_verifikasi" value="{{ Helper::getFormDate($jadwal->tanggal_verifikasi) }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <span class="errmsg">{{ $errors->first('tanggal_verifikasi') }}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-10">
                    Waktu Verifikasi Portofolio
                </div>
                <div class="col-md-1 col-xs-1">:</div>
                <div class="col-md-5 col-xs-12">
                    <div class="form-group">
                        <input type='text' class="form-control" name="waktu_verifikasi" value="{{ $jadwal->waktu_verifikasi }}" />
                    </div>
                    <span class="errmsg">{{ $errors->first('waktu_verifikasi') }}</span>
                </div>
            </div>
        </div>
        @endif
        @if ($jadwal->metode == 'tes_tulis')
        <div id="input_tes">
            <div class="row">
                <div class="col-md-3 col-xs-10">
                  Tanggal Tes Tertulis
              </div>
              <div class="col-md-1 col-xs-1">:</div>
              <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' class="form-control" name="tanggal_tes" value="{{ Helper::getFormDate($jadwal->tanggal_tes) }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <span class="errmsg">{{ $errors->first('tanggal_tes') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Waktu Tes Tertulis
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                  <input type='text' class="form-control" name="waktu_ujian" value="{{ $jadwal->waktu_ujian }}" />
              </div>
              <span class="errmsg">{{ $errors->first('waktu_ujian') }}</span>
          </div>
      </div>
  </div>
  <div class="row">
    <div class="col-md-3 col-xs-10">
        Lokasi Ujian
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
          <input type='text' class="form-control" name="lokasi_ujian" value="{{ $jadwal->lokasi_ujian }}" />
      </div>
      <span class="errmsg">{{ $errors->first('lokasi_ujian') }}</span>
  </div>
</div>
<div class="row">
    <div class="col-md-3 col-xs-10">
        Jumlah Ruang
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
          <input type='text' class="form-control" name="jumlah_ruang" value="{{ $jadwal->jumlah_ruang }}" maxlength="20" onkeypress='validate(event)' required />
      </div>
      <span class="errmsg">{{ $errors->first('jumlah_ruang') }}</span>
  </div>
</div>
@endif
<div class="row">
    <div class="col-md-3 col-xs-10">
        Kuota
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
          <input type='text' class="form-control" name="kapasitas" value="{{ $jadwal->kapasitas }}" maxlength="20" onkeypress='validate(event)' required/>
      </div>
      <span class="errmsg">{{ $errors->first('kapasitas') }}</span>
  </div>
</div>
@if ($jadwal->metode == 'tes_tulis')
<div class="row">
    <div class="col-md-3 col-xs-10">
        Jumlah Unit
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
          <input type='text' class="form-control" name="jumlah_unit" value="{{ $jadwal->jumlah_unit }}"  maxlength="20" onkeypress='validate(event)' required/>
      </div>
      <span class="errmsg">{{ $errors->first('jumlah_unit') }}</span>
  </div>
</div>
@endif
<div class="row">
    <div class="col-md-3 col-xs-10">
        Batas Akhir Pendaftaran
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
            <div class='input-group date' id='datetimepicker3'>
                <input type='text' class="form-control" name="batas_waktu_input" value="{{ Helper::getFormDate($jadwal->batas_waktu_input) }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                <span class="errmsg">{{ $errors->first('batas_waktu_input') }}</span>
            </div> 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-xs-10">
        Publish Jadwal 
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
            {{ Form::select('publish_jadwal', array('ya' => 'Ya', 'tidak' => 'Tidak'),$jadwal->publish_jadwal, ['class' => "form-control"]) }}
        </div>
        <span class="errmsg">{{ $errors->first('publish_jadwal') }}</span>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-xs-10">
        Nama CP 1 
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
            <input type='text' class="form-control" name="nama_cp_1" value="{{ $jadwal->nama_cp_1 }}" />
        </div>
        <span class="errmsg">{{ $errors->first('nama_cp_1') }}</span>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-xs-10">
        No.Telp CP 1 
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
            <input type='text' class="form-control" name="no_telp_cp_1" value="{{ $jadwal->no_telp_cp_1 }}" maxlength="12" onkeypress='validate(event)' required/>
        </div>
        <span class="errmsg">{{ $errors->first('no_telp_cp_1') }}</span>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-xs-10">
        Nama CP 2 
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
            <input type='text' class="form-control" name="nama_cp_2" value="{{ $jadwal->nama_cp_2 }}" />
        </div>
        <span class="errmsg">{{ $errors->first('nama_cp_2') }}</span>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-xs-10">
        No.Telp CP 2 
    </div>
    <div class="col-md-1 col-xs-1">:</div>
    <div class="col-md-5 col-xs-12">
        <div class="form-group">
            <input type='text' class="form-control" name="no_telp_cp_2" value="{{ $jadwal->no_telp_cp_2 }}" maxlength="12" onkeypress='validate(event)' required/>
        </div>
        <span class="errmsg">{{ $errors->first('no_telp_cp_2') }}</span>
    </div>
</div>
<div class="row">
    <div class="col-md-9" style="text-align: right">
        <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
        <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
    </div>
</div>
</div>
</div>
</form>
@endsection
@section('js')
<script type="text/javascript">
    $('#metode').on('change', function() {
		var select = this.value;

		if(select == 'verifikasi_portofolio'){
			$('#input_verif').show();
			$('#input_tes').hide();
		}

		if(select == 'tes_tulis'){
			$('#input_verif').hide();
			$('#input_tes').show();
		}
	});

	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker3').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker4').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker2').datetimepicker({
			format: 'LT'
		});
	});

    function AddBusinessDays(weekDaysToAdd) {
		var curdate = new Date();
		var realDaysToAdd = 0;
		while (weekDaysToAdd > 0){
			curdate.setDate(curdate.getDate()+1);
			realDaysToAdd++;
			//check if current day is business day
			if (noWeekendsOrHolidays(curdate)[0]) {
				weekDaysToAdd--;
			}
		}
		
		return realDaysToAdd;
	}

	var date_billed = $('#datebilled').datepicker('getDate');
	var date_overdue = new Date();
	var weekDays = AddBusinessDays(30);
	date_overdue.setDate(date_billed.getDate() + weekDays);
	date_overdue = $.datepicker.formatDate('mm/dd/yy', date_overdue);
	$('#datepd').val(date_overdue).prop('readonly', true);
</script>
@endsection
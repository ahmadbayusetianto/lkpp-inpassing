@extends('layout.app2')

@section('title')
    Input Ulang Berkas
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }

  .main-page{
    margin-top: 20px;
    font-weight: 600;
  }

  .box-container{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    background-color: white;
    border-radius: 5px;
    padding: 3%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    max-width: 250px;
    line-height: 15px;
    width: 100%;
    margin: 5px;
  }

  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  p{
    font-weight: 500;
  }

  b{
    font-weight: 500;
  }

  .row a.btn{
    text-align: left;
    font-weight: 600;
    font-size: small;
  }

  .form-control{
    margin :5px;
    -webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    border-radius: 5px;
  }

  textarea{
    margin :5px;
    -webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    border-radius: 5px;
  }

  .form-control{
    height: 30px;
  }

  .btn-default1{
      background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
      color: white;
      font-weight: 600;
      width: 100px; 
      margin: 10px;
      -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .btn-default2{
      background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
      color: black;
      font-weight: 600;
      width: 100px; 
      margin: 10px;
      -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .custom-file label.custom-file-label{
      margin: 5px;
      -webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
      -moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
      box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
  }

  .row-input{
    padding-bottom: 10px;
  }

  .main-page img{
      width: 150px;
  }

  label.custom-file-label{
        margin: 0px 15px;
    }

    span.berkas{
        font-size: 10px;
    }

    span.berkas label{
        margin-bottom: 0px !important;
    }

    .btn-area{
        text-align: right
    }

</style>
@endsection

@section('content')
@inject('request', 'Illuminate\Http\Request')
<div class="main-page">
        <div class="container">
            <div class="row box-container">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-9" style="">
                      <div class="row">
                      <div class="col-md-12">
                      <h5>Input Ulang Berkas</h5>
                         <hr>
                      @if (session('msg'))
                          @if (session('msg') == "berhasil")
                            <div class="alert alert-success alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>Berhasil simpan Data</strong>
                            </div> 
                          @else
                          <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Gagal simpan Data</strong>
                          </div> 
                          @endif
                          @if (session('msg') == "jenjang_penuh")
                            <div class="alert alert-warning alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>Gagal Tambah Peserta, Jenjang Peserta Yang Anda pilih Sudah Memenuhi batas</strong>
                            </div> 
                          @endif
                      @endif
                      </div>
                      <form action="" method="post" enctype="multipart/form-data">
                        @csrf
                      <div class="col-md-11">
                          <div class="row row-input">
                            <div class="col-lg-4 col-md-11 col-10">Nama</div>
                            <div class="col-lg-1 col-md-1 col-1">:</div>
                            <div class="col-lg-7 col-md-12">
                                <input type="text" style="width: 100%" class="form-control" name="nama" value="{{ $data->nama }}" required>
                            <span class="errmsg">{{ $errors->first('nama') }}</span>
                            </div>
                          </div>
                          <div class="row row-input">
                            <div class="col-lg-4 col-md-11 col-10">NIP</div>
                            <div class="col-lg-1 col-md-1 col-1">:</div>
                            <div class="col-lg-7 col-md-12">
                                    <input type="text" style="width: 100%" class="form-control" name="nip" minlength="18" value="{{ $data->nip }}" required>
                            <span class="errmsg">{{ $errors->first('nip') }}</span>
                            </div>
                          </div>
                          <div class="row row-input">
                            <div class="col-lg-4 col-md-11 col-10">Pangkat/Gol.</div>
                            <div class="col-lg-1 col-md-1 col-1">:</div>
                            <div class="col-lg-7 col-md-12">
                              <select name="jabatan" id="pangkat" class="form-control form-control-sm" onchange="getval(this);">
                                  <option value="" {{ $data->jabatan == "" ? 'selected' : ''}}>Pilih Pangkat/Gol.</option>
                                  <option value="Penata Muda (III/a)" {{ $data->jabatan == "Penata Muda (III/a)" ? 'selected' : ''}}>Penata Muda (III/a)</option>
                                  <option value="Penata Muda Tk.I (III/b)" {{ $data->jabatan == "Penata Muda Tk.I (III/b)" ? 'selected' : ''}}>Penata Muda Tk.I (III/b)</option>
                                  <option value="Penata (III/c)" {{ $data->jabatan == "Penata (III/c)" ? 'selected' : ''}}>Penata (III/c)</option>
                                  <option value="Penata Tk.I (III/d)" {{ $data->jabatan == "Penata Tk.I (III/d)" ? 'selected' : ''}}>Penata Tk.I (III/d)</option>
                                  <option value="Pembina (IV/a)" {{ $data->jabatan == "Pembina (IV/a)" ? 'selected' : ''}}>Pembina (IV/a)</option>
                                  <option value="Pembina Tk.I (IV/b)" {{ $data->jabatan == "Pembina Tk.I (IV/b)" ? 'selected' : ''}}>Pembina Tk.I (IV/b)</option>
                                  <option value="Pembina Utama Muda (IV/c)" {{ $data->jabatan == "Pembina Utama Muda (IV/c)" ? 'selected' : ''}}>Pembina Utama Muda (IV/c)</option>
                              </select>
                            <span class="errmsg">{{ $errors->first('jabatan') }}</span>
                            <span class="errmsg" id="errPangkat"></span>
                            </div>
                          </div>
                          <div class="row row-input">
                            <div class="col-lg-4 col-md-11 col-10">TMT Pangkat/Gol.</div>
                            <div class="col-lg-1 col-md-1 col-1">:</div>
                            <div class="col-lg-7 col-md-12">
                                <input type="text" name="tmt_panggol" id="datepicker2" class="form-control form-control-sm tmt_panggol" value={{ date('d/m/Y',strtotime($data->tmt_panggol)) }} placeholder="dd/mm/yyyy" required>
                                <span class="err" id="errTmtPanggol"></span>
                                <span class="errmsg">{{ $errors->first('tmt_panggol') }}</span>
                            </div>
                          </div>
                          <div class="row row-input">
                            <div class="col-lg-4 col-md-11 col-10">Usulan Jenjang</div>
                            <div class="col-lg-1 col-md-1 col-1">:</div>
                            <div class="col-lg-7 col-md-12">
                                    <input type="text" style="width: 100%" class="form-control" name="jenjang" id="jenjang" value="{{ ucwords($data->jenjang) }}" required readonly>
                                    {{-- <input type="hidden" name="jenjang" id="jenjang" value="{{ $data->jenjang }}"> --}}
                                    <input type="hidden" name="jenjang_old" id="jenjang_old" value="{{ $data->jenjang }}">
                                    <span class="errmsg">{{ $errors->first('jenjang') }}</span>
                                    <span class="errmsg" id="errJenjang"></span>
                            </div>
                          </div>
                          <div class="row row-input">
                            <div class="col-lg-4 col-md-11 col-10">No. Surat Usulan Mengikuti Penyesuaian/Inpassing</div>
                            <div class="col-lg-1 col-md-1 col-1">:</div>
                            <div class="col-lg-7 col-md-12">
                                {!! Form::select('no_surat', $no_surat , $data->id_formasi, ['placeholder' => 'Pilih No. Surat Usulan', 'class' => 'form-control js-example-basic-single', 'id' => 'no_surat'   ]); !!}
                                <input type="hidden" name="no_surat_old" id="no_surat_old" value="{{ $data->id_formasi }}">
                                <span class="errmsg" id="errno_surat"></span>
                                <span class="errmsg">{{ $errors->first('no_surat') }}</span>
                                <span class="errmsg" id="errno_surat"></span>
                            </div>
                          </div>
                          <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    Tempat Lahir
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control form-control-sm" value="{{ $data->tempat_lahir }}" required onkeypress="validateHuruf(event)">
                                    <span class="errmsg">{{ $errors->first('tempat_lahir') }}</span>
                                </div>
                          </div>
                          <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    Tanggal Lahir
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="tanggal_lahir" id="datepicker" class="form-control form-control-sm" value="{{ date('d/m/Y',strtotime($data->tanggal_lahir)) }}" placeholder="dd/mm/yyyy" required>
                                    <span class="errmsg">{{ $errors->first('tanggal_lahir') }}</span>
                                </div>
                          </div>
                          <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    Jenis Kelamin
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {{-- {!! Form::select('jenis_kelamin', array('pria' => 'LAKI - LAKI', 'wanita' => 'PEREMPUAN') , $data->jenis_kelamin, ['placeholder' => 'please select', 'class' => 'form-control', 'id' => 'select-single']); !!} --}}
                                    <label class="radio-inline">
                                        <input type="radio" name="jenis_kelamin" value="pria" {{ $data->jenis_kelamin == 'pria' ? 'checked' : '' }}>Laki-laki
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="jenis_kelamin" value="wanita" {{ $data->jenis_kelamin == 'wanita' ? 'checked' : '' }}>Perempuan
                                    </label>
                                </div>
                          </div>
                          <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    No. KTP
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="no_ktp" id="no_ktp" class="form-control form-control-sm" value="{{ $data->no_ktp }}" maxlength="16" onkeypress='validate(event)' required>
                                    <span class="errmsg">{{ $errors->first('no_ktp') }}</span>
                                </div>
                          </div>
                          <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                  No. Sertifikat PBJ Tk. Dasar
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="no_sertifikat" id="no_sertifikat" class="form-control form-control-sm" value="{{ $data->no_sertifikat }}" maxlength="15" onkeypress='validate(event)' required>
                                    <span class="errmsg">{{ $errors->first('no_sertifikat') }}</span>
                                </div>
                          </div>
                          <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    Pendidikan Terakhir
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {!! Form::select('pendidikan_terakhir', array('D4' => 'D4', 'S1' => 'S1', 'S2' => 'S2', 'S3' => 'S3') , $data->pendidikan_terakhir, ['placeholder' => 'please select', 'class' => 'form-control form-control-sm', 'id' => 'select-single']); !!}
                                    <span class="errmsg">{{ $errors->first('pendidikan_terakhir') }}</span>
                                </div>
                          </div>
                          <div class="row row-input">
                            <div class="col-lg-4 col-md-11 col-10">Email</div>
                            <div class="col-lg-1 col-md-1 col-1">:</div>
                            <div class="col-lg-7 col-md-12">
                                    <input type="email" style="width: 100%" class="form-control" name="email" value="{{ $data->email }}" required>
                                    <span class="errmsg">{{ $errors->first('email') }}</span>
                            </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">No. Telp/HP</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                    <input type="no_telp" style="width: 100%" class="form-control" name="no_telp" value="{{ $data->no_telp }}" required>
                                    <span class="errmsg">{{ $errors->first('no_telp') }}</span>
                              </div>
                          </div>
                          <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    Nama Instansi
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="nama_instansi" id="nama_instansi" class="form-control form-control-sm" value="{{ $request->session()->get('instansis_user') }}" required disabled>
                                    <span class="errmsg">{{ $errors->first('nama_instansi') }}</span>
                                </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-4 col-10">
                                Nama Satuan Kerja/OPD 
                              </div>
                              <div class="div col-lg-1 col-md-1 col-1">
                                  :
                              </div>
                              <div class="col-lg-7 col-md-7 col-12">
                                  <input type="text" name="satuan_kerja" id="satuan_kerja" class="form-control form-control-sm" value="{{ $data->satuan_kerja }}">
                                  <span class="errmsg">{{ $errors->first('satuan_kerja') }}</span>
                              </div>
                        </div>
                          {{-- <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    Satuan Kerja/OPD
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="satuan_kerja" id="satuan_kerja" class="form-control form-control-sm" value="{{ $data->satuan_kerja }}" required>
                                    <span class="errmsg">{{ $errors->first('satuan_kerja') }}</span>
                                </div>
                            </div> --}}
                            <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    Provinsi
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {!! Form::select('provinsi', $provinsi , $data->provinsi, ['placeholder' => 'please select', 'class' => 'form-control js-example-basic-single', 'id' => 'provinsi']); !!}
                                    <span class="errmsg">{{ $errors->first('provinsi') }}</span>
                                </div>
                            </div>
                            <div class="row row-input">
                                <div class="col-lg-4 col-md-4 col-10">
                                    Kabupaten/Kota
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {{-- <select name="kab_kota" id="kab_kota" class="form-control js-example-basic-single-2">

                                    </select> --}}
                                    {!! Form::select('kab_kota', $regencies , $data->kab_kota, ['placeholder' => 'please select', 'class' => 'form-control js-example-basic-single-2', 'id' => 'kab_kota']); !!}
                                    <span class="errmsg">{{ $errors->first('kab_kota') }}</span>
                                </div>
                            </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Surat Ket Bertugas di Bidang PBJ minimal 2 Tahun</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="surat_bertugas_pbj" class="custom-file-input" id="customFile" value="{{ old('surat_bertugas_pbj') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_surat_bertugas_pbj" value="{{ $data->surat_bertugas_pbj }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('surat_bertugas_pbj') }}</span>
                                berkas lama:
                                @if (is_null($data->surat_bertugas_pbj))
                                  -
                                @else
                                    @php
                                    $surat_bertugas_pbj = explode('.',$data->surat_bertugas_pbj);
                                    @endphp
                                    @if ($surat_bertugas_pbj[1] == 'pdf' || $surat_bertugas_pbj[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/surat_bertugas_pbj/".$data->surat_bertugas_pbj }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/surat_bertugas_pbj/".$data->surat_bertugas_pbj }}" target="_blank"><img src="{{ asset('storage/data/surat_bertugas_pbj')."/".$data->surat_bertugas_pbj }}" class="img-rounded"></a>
                                    @endif 
                                @endif     
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Bukti SK di bidang PBJ</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="bukti_sk_pbj" class="custom-file-input" id="customFile" value="{{ old('bukti_sk_pbj') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_bukti_sk_pbj" value="{{ $data->bukti_sk_pbj }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('bukti_sk_pbj') }}</span>
                                berkas lama:
                                @if (is_null($data->bukti_sk_pbj))
                                  -
                                @else
                                    @php
                                    $bukti_sk_pbj = explode('.',$data->bukti_sk_pbj);
                                    @endphp
                                    @if ($bukti_sk_pbj[1] == 'pdf' || $bukti_sk_pbj[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/bukti_sk_pbj/".$data->bukti_sk_pbj }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/bukti_sk_pbj/".$data->bukti_sk_pbj }}" target="_blank"><img src="{{ asset('storage/data/bukti_sk_pbj')."/".$data->bukti_sk_pbj }}" class="img-rounded"></a>
                                    @endif     
                                @endif 
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Ijazah terakhir (min. S1/D4)</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="ijazah_terakhir" class="custom-file-input" id="customFile" value="{{ old('ijazah_terakhir') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_ijazah_terakhir" value="{{ $data->ijazah_terakhir }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('ijazah_terakhir') }}</span>
                                berkas lama:
                                @if (is_null($data->ijazah_terakhir))
                                  -
                                @else
                                    @php
                                    $ijazah_terakhir = explode('.',$data->ijazah_terakhir);
                                    @endphp
                                    @if ($ijazah_terakhir[1] == 'pdf' || $ijazah_terakhir[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/ijazah_terakhir/".$data->ijazah_terakhir }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/ijazah_terakhir/".$data->ijazah_terakhir }}" target="_blank"><img src="{{ asset('storage/data/ijazah_terakhir')."/".$data->ijazah_terakhir }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">SK Kenaikan Pangkat Terakhir</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="sk_kenaikan_pangkat_terakhir" class="custom-file-input" id="customFile" value="{{ old('sk_kenaikan_pangkat_terakhir') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_sk_kenaikan_pangkat_terakhir" value="{{ $data->sk_kenaikan_pangkat_terakhir }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('sk_kenaikan_pangkat_terakhir') }}</span>
                                berkas lama:
                                @if (is_null($data->sk_kenaikan_pangkat_terakhir))
                                  -
                                @else
                                    @php
                                    $sk_kenaikan_pangkat_terakhir = explode('.',$data->sk_kenaikan_pangkat_terakhir);
                                    @endphp
                                    @if ($sk_kenaikan_pangkat_terakhir[1] == 'pdf' || $sk_kenaikan_pangkat_terakhir[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->sk_kenaikan_pangkat_terakhir }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">SK Pengangkatan ke dalam Jabatan Terakhir</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="sk_pengangkatan_jabatan_terakhir" class="custom-file-input" id="customFile" value="{{ old('sk_pengangkatan_jabatan_terakhir') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_sk_pengangkatan_jabatan_terakhir" value="{{ $data->sk_pengangkatan_jabatan_terakhir }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('sk_pengangkatan_jabatan_terakhir') }}</span>
                                berkas lama:
                                @if (is_null($data->sk_pengangkatan_jabatan_terakhir))
                                  -
                                @else
                                    @php
                                    $sk_pengangkatan_jabatan_terakhir = explode('.',$data->sk_pengangkatan_jabatan_terakhir);
                                    @endphp
                                    @if ($sk_pengangkatan_jabatan_terakhir[1] == 'pdf' || $sk_pengangkatan_jabatan_terakhir[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/sk_pengangkatan_jabatan_terakhir/".$data->sk_pengangkatan_jabatan_terakhir }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/sk_pengangkatan_jabatan_terakhir/".$data->sk_pengangkatan_jabatan_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_jabatan_terakhir')."/".$data->sk_pengangkatan_jabatan_terakhir }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Sertifikat PBJ Tk. Dasar</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="sertifikasi_dasar_pbj" class="custom-file-input" id="customFile" value="{{ old('sertifikasi_dasar_pbj') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_sertifikasi_dasar_pbj" value="{{ $data->sertifikasi_dasar_pbj }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('sertifikasi_dasar_pbj') }}</span>
                                berkas lama:
                                @if (is_null($data->sertifikasi_dasar_pbj))
                                  -
                                @else
                                    @php
                                    $sertifikasi_dasar_pbj = explode('.',$data->sertifikasi_dasar_pbj);
                                    @endphp
                                    @if ($sertifikasi_dasar_pbj[1] == 'pdf' || $sertifikasi_dasar_pbj[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/sertifikasi_dasar_pbj/".$data->sertifikasi_dasar_pbj }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/sertifikasi_dasar_pbj/".$data->sertifikasi_dasar_pbj }}" target="_blank"><img src="{{ asset('storage/data/sertifikasi_dasar_pbj')."/".$data->sertifikasi_dasar_pbj }}" class="img-rounded"></a>
                                    @endif
                                @endif       
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">SKP (2 Tahun Terakhir)</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="skp_dua_tahun_terakhir" class="custom-file-input" id="customFile" value="{{ old('skp_dua_tahun_terakhir') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_skp_dua_tahun_terakhir" value="{{ $data->skp_dua_tahun_terakhir }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('skp_dua_tahun_terakhir') }}</span>
                                berkas lama:
                                @if (is_null($data->skp_dua_tahun_terakhir))
                                  -
                                @else
                                    @php
                                    $skp_dua_tahun_terakhir = explode('.',$data->skp_dua_tahun_terakhir);
                                    @endphp
                                    @if ($skp_dua_tahun_terakhir[1] == 'pdf' || $skp_dua_tahun_terakhir[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/skp_dua_tahun_terakhir/".$data->skp_dua_tahun_terakhir }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/skp_dua_tahun_terakhir/".$data->skp_dua_tahun_terakhir }}" target="_blank"><img src="{{ asset('storage/data/skp_dua_tahun_terakhir')."/".$data->skp_dua_tahun_terakhir }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Surat Ket Tidak Sedang Dijatuhi Hukuman
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="surat_ket_tidak_dijatuhi_hukuman" class="custom-file-input" id="customFile" value="{{ old('surat_ket_tidak_dijatuhi_hukuman') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_surat_ket_tidak_dijatuhi_hukuman" value="{{ $data->surat_ket_tidak_dijatuhi_hukuman }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('surat_ket_tidak_dijatuhi_hukuman') }}</span>
                                berkas lama:
                                @if (is_null($data->surat_ket_tidak_dijatuhi_hukuman))
                                  -
                                @else
                                    @php
                                    $surat_ket_tidak_dijatuhi_hukuman = explode('.',$data->surat_ket_tidak_dijatuhi_hukuman);
                                    @endphp
                                    @if ($surat_ket_tidak_dijatuhi_hukuman[1] == 'pdf' || $surat_ket_tidak_dijatuhi_hukuman[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/surat_ket_tidak_dijatuhi_hukuman/".$data->surat_ket_tidak_dijatuhi_hukuman }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/surat_ket_tidak_dijatuhi_hukuman/".$data->surat_ket_tidak_dijatuhi_hukuman }}" target="_blank"><img src="{{ asset('storage/data/surat_ket_tidak_dijatuhi_hukuman')."/".$data->surat_ket_tidak_dijatuhi_hukuman }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Formulir Kesediaan Mengikuti Penyesuaian/Inpassing
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="formulir_kesediaan_mengikuti_inpassing" class="custom-file-input" id="customFile" value="{{ old('formulir_kesediaan_mengikuti_inpassing') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_formulir_kesediaan_mengikuti_inpassing" value="{{ $data->formulir_kesediaan_mengikuti_inpassing }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('formulir_kesediaan_mengikuti_inpassing') }}</span>
                                berkas lama:
                                @if (is_null($data->formulir_kesediaan_mengikuti_inpassing))
                                  -
                                @else
                                    @php
                                    $formulir_kesediaan_mengikuti_inpassing = explode('.',$data->formulir_kesediaan_mengikuti_inpassing);
                                    @endphp
                                    @if ($formulir_kesediaan_mengikuti_inpassing[1] == 'pdf' || $formulir_kesediaan_mengikuti_inpassing[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/formulir_kesediaan_mengikuti_inpassing/".$data->formulir_kesediaan_mengikuti_inpassing }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/formulir_kesediaan_mengikuti_inpassing/".$data->formulir_kesediaan_mengikuti_inpassing }}" target="_blank"><img src="{{ asset('storage/data/formulir_kesediaan_mengikuti_inpassing')."/".$data->formulir_kesediaan_mengikuti_inpassing }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">SK Pengangkatan CPNS
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="sk_pengangkatan_cpns" class="custom-file-input" id="customFile" value="{{ old('sk_pengangkatan_cpns') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_sk_pengangkatan_cpns" value="{{ $data->sk_pengangkatan_cpns }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('sk_pengangkatan_cpns') }}</span>
                                berkas lama:
                                @if (is_null($data->sk_pengangkatan_cpns))
                                  -
                                @else
                                    @php
                                    $sk_pengangkatan_cpns = explode('.',$data->sk_pengangkatan_cpns);
                                    @endphp
                                    @if ($sk_pengangkatan_cpns[1] == 'pdf' || $sk_pengangkatan_cpns[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/sk_pengangkatan_cpns/".$data->sk_pengangkatan_cpns }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/sk_pengangkatan_cpns/".$data->sk_pengangkatan_cpns }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_cpns')."/".$data->sk_pengangkatan_cpns }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">SK Pengangkatan PNS
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="sk_pengangkatan_pns" class="custom-file-input" id="customFile" value="{{ old('sk_pengangkatan_pns') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_sk_pengangkatan_pns" value="{{ $data->sk_pengangkatan_pns }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('sk_pengangkatan_pns') }}</span>
                                berkas lama:
                                @if (is_null($data->sk_pengangkatan_pns))
                                  -
                                @else
                                    @php
                                    $sk_pengangkatan_pns = explode('.',$data->sk_pengangkatan_pns);
                                    @endphp
                                    @if ($sk_pengangkatan_pns[1] == 'pdf' || $sk_pengangkatan_pns[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/sk_pengangkatan_pns/".$data->sk_pengangkatan_pns }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/sk_pengangkatan_pns/".$data->sk_pengangkatan_pns }}" target="_blank"><img src="{{ asset('storage/data/sk_pengangkatan_pns')."/".$data->sk_pengangkatan_pns }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">KTP
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="ktp" class="custom-file-input" id="customFile" value="{{ old('ktp') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_ktp" value="{{ $data->ktp }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('ktp') }}</span>
                                berkas lama:
                                @if (is_null($data->ktp))
                                  -
                                @else
                                    @php
                                    $ktp = explode('.',$data->ktp);
                                    @endphp
                                    @if ($ktp[1] == 'pdf' || $ktp[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/ktp/".$data->ktp }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/ktp/".$data->ktp }}" target="_blank"><img src="{{ asset('storage/data/ktp')."/".$data->ktp }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Pas Foto 3 X 4
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="pas_foto_3_x_4" class="custom-file-input" id="customFile" value="{{ old('pas_foto_3_x_4') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_pas_foto_3_x_4" value="{{ $data->pas_foto_3_x_4 }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('pas_foto_3_x_4') }}</span>
                                berkas lama:
                                @if (is_null($data->pas_foto_3_x_4))
                                  -
                                @else
                                    @php
                                    $pas_foto_3_x_4 = explode('.',$data->pas_foto_3_x_4);
                                    @endphp
                                    @if ($pas_foto_3_x_4[1] == 'pdf' || $pas_foto_3_x_4[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/pas_foto_3_x_4/".$data->pas_foto_3_x_4 }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/pas_foto_3_x_4/".$data->pas_foto_3_x_4 }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4')."/".$data->pas_foto_3_x_4 }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10"> Surat Pernyataan Bersedia diangkat JF PPBJ
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="surat_pernyataan_bersedia_jfppbj" class="custom-file-input" id="customFile" value="{{ old('surat_pernyataan_bersedia_jfppbj') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_surat_pernyataan_bersedia_jfppbj" value="{{ $data->surat_pernyataan_bersedia_jfppbj }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('surat_pernyataan_bersedia_jfppbj') }}</span>
                                berkas lama:
                                @if (is_null($data->surat_pernyataan_bersedia_jfppbj))
                                  -
                                @else
                                    @php
                                    $surat_pernyataan_bersedia_jfppbj = explode('.',$data->surat_pernyataan_bersedia_jfppbj);
                                    @endphp
                                    @if ($surat_pernyataan_bersedia_jfppbj[1] == 'pdf' || $surat_pernyataan_bersedia_jfppbj[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/surat_pernyataan_bersedia_jfppbj/".$data->surat_pernyataan_bersedia_jfppbj }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/surat_pernyataan_bersedia_jfppbj/".$data->surat_pernyataan_bersedia_jfppbj }}" target="_blank"><img src="{{ asset('storage/data/surat_pernyataan_bersedia_jfppbj')."/".$data->surat_pernyataan_bersedia_jfppbj }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          {{-- <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Bukti Portofolio
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                @if (is_null($data->bukti_portofolio))
                                  -
                                @else
                                    @php
                                    $bukti_portofolio = explode('.',$data->bukti_portofolio);
                                    @endphp
                                    @if ($bukti_portofolio[1] == 'pdf' || $bukti_portofolio[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/bukti_portofolio/".$data->bukti_portofolio }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/bukti_portofolio/".$data->bukti_portofolio }}" target="_blank"><img src="{{ asset('storage/data/bukti_portofolio')."/".$data->bukti_portofolio }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div> --}}
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Surat Usulan Mengikuti Penyesuaian/Inpassing
                                </div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                <input type="file" name="surat_usulan_mengikuti_inpassing" class="custom-file-input" id="customFile" value="{{ old('surat_usulan_mengikuti_inpassing') }}" accept="image/jpg, application/pdf, image/jpeg">
                                <input type="hidden" name="old_surat_usulan_mengikuti_inpassing" value="{{ $data->surat_usulan_mengikuti_inpassing }}">
                                <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                <span class="errmsg">{{ $errors->first('surat_usulan_mengikuti_inpassing') }}</span>
                                berkas lama:
                                @if (is_null($data->surat_usulan_mengikuti_inpassing))
                                  -
                                @else
                                    @php
                                    $surat_usulan_mengikuti_inpassing = explode('.',$data->surat_usulan_mengikuti_inpassing);
                                    @endphp
                                    @if ($surat_usulan_mengikuti_inpassing[1] == 'pdf' || $surat_usulan_mengikuti_inpassing[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/surat_usulan_mengikuti_inpassing/".$data->surat_usulan_mengikuti_inpassing }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                    @else
                                    <a href="{{ url('priview-file')."/surat_usulan_mengikuti_inpassing/".$data->surat_usulan_mengikuti_inpassing }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan_mengikuti_inpassing')."/".$data->surat_usulan_mengikuti_inpassing }}" class="img-rounded"></a>
                                    @endif
                                @endif      
                              </div>
                          </div>
                          <div class="row row-input">
                              <div class="col-lg-4 col-md-4 col-10">
                                No. Surat Usulan Mengikuti Penyesuaian/Inpassing
                              </div>
                              <div class="div col-lg-1 col-md-1 col-1">
                                  :
                              </div>
                              <div class="col-lg-7 col-md-7 col-12">
                                  <input type="text" name="no_surat_usulan_mengikuti_inpassing" id="no_surat_usulan_mengikuti_inpassing" class="form-control form-control-sm" value="{{ $data->no_surat_usulan_peserta }}" required="">
                                  <span class="errmsg">{{ $errors->first('no_surat_usulan_mengikuti_inpassing') }}</span>
                              </div>
                        </div>
                        <div class="row row-input">
                              <div class="col-lg-4 col-md-11 col-10">Keikutsertaan Peserta</div>
                              <div class="col-lg-1 col-md-1 col-1">:</div>
                              <div class="col-lg-7 col-md-12">
                                    <select name="status_peserta" class="form-control form-control-sm" >
                                      <option value="" {{ $data->status_peserta == "" ? 'selected' : ''}}>Pilih Status</option>
                                      <option value="aktif" {{ $data->status_peserta == "aktif" ? 'selected' : ''}}>Aktif</option>
                                      <option value="tidak_aktif" {{ $data->status_peserta == "tidak_aktif" ? 'selected' : ''}}>Tidak Aktif</option>
                                    </select>
                                    <span class="errmsg">{{ $errors->first('status_peserta') }}</span>
                              </div>
                          </div>
                          <input type="hidden" name="id_dokumen" value="{{ $data->id_dokumen }}">
                          <input type="hidden" name="id_usulan" @if($suratusulan != "") value={{$suratusulan->id_usulan}} @else @endif>
                          <input type="hidden" name="id_peserta" value="{{ $data->id_peserta }}">
                          <div class="row">
                            <div class="col-md-12" style="text-align:right">
                                <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                                <button type="submit" class="btn btn-sm btn-default1">Ubah</button>
                            </div>
                          </div>
                        </div>
                      </form>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      @include('layout.button_right_kpp')
                    </div>
                  </div>
                </div> 
              </div>
          </div>
@endsection

@section('js')
<script>
  function getval(sel){
        jQuery(document).ready(function(){
            var value = sel.value;
            if(value == 'Penata Muda (III/a)'){
                $('#jenjang').val('Pertama');
            }
            if(value == 'Penata Muda Tk.I (III/b)'){
                $('#jenjang').val('Pertama');
            }
            if(value == 'Penata (III/c)'){
                $('#jenjang').val('Muda');
            }
            if(value == 'Penata Tk.I (III/d)'){
                $('#jenjang').val('Muda');
            }
            if(value == 'Pembina (IV/a)'){
                $('#jenjang').val('Madya');
            }
            if(value == 'Pembina Tk.I (IV/b)'){
                $('#jenjang').val('Madya');
            }
            if(value == 'Pembina Utama Muda (IV/c)'){
                $('#jenjang').val('Madya');
            }
            if(value == ''){
                $('#jenjang').val('');
                $('#no_surat').attr('disabled',true);
            }
            $('#no_surat').prop('disabled',false);     
        });
     }
</script>
<script>
  $('#datepicker').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'dd/mm/yyyy'
  });

  $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'dd/mm/yyyy'
  });

  $(document).ready(function() {
      $('.js-example-basic-single').select2();
  });
  $(document).ready(function() {
      $('.js-example-basic-single-2').select2();
  });

  $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
<script>
$('#provinsi').on('change', function() {
  var select = this.value;
//   alert(select);
  event.preventDefault();
  $.ajaxSetup({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    jQuery.ajax({
        method: 'get',
        url: "../get-kota/" + select,
        success: function(result){
            if (result.msg == 'berhasil') {
                // alert('berhasil');
                // alert(result.data);
                // console.log(result);
                $('#kab_kota').find('option').remove().end();
                $('#kab_kota').append(result.data);
            }else{
                $('#kab_kota').find('option').remove().end();
            }
        }});
});
</script>
<script>
$('#no_surat').on('change', function() {
  var select = this.value;
  var jenjang = $('#jenjang').val().toLowerCase();
  var jenjang_old = $('#jenjang_old').val().toLowerCase();
  var no_surat_old = $('#no_surat_old').val();
  event.preventDefault();
  $.ajaxSetup({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    jQuery.ajax({
        method: 'get',
        url: "../cek-jenjang-edit/" + jenjang + "/"+ select + "/"+ jenjang_old + "/"+ no_surat_old,
        success: function(result){
            // alert(result.msg);
            // alert(result.peserta_terdaftar);
            // alert(result.jenjang);
            if (result.msg == 'penuh') {
                console.log('penuh');
                alert('kuota penuh, silahkan pilih jenjang/no surat usulan yang lainnya.');
                // $('#no_surat').val('');
                $('#jabatan').val('');
                $('#errPangkat').html('jenjang penuh,silahkan pilih jenjang yang lain');
                $('#errno_surat').html('jenjang penuh,silahkan pilih jenjang yang lain');
            }else{
                console.log('lanjut');
                $('#errno_surat').html('');
                $('#errPangkat').html('');
            }
        }});
});

$('#pangkat').on('change', function() {
  var value = this.value;
  if(value == 'Penata Muda (III/a)'){
      var jenjang = 'pertama';
  }
  if(value == 'Penata Muda Tk.I (III/b)'){
      var jenjang = 'pertama';
  }
  if(value == 'Penata (III/c)'){
      var jenjang = 'muda';
  }
  if(value == 'Penata Tk.I (III/d)'){
      var jenjang = 'muda';
  }
  if(value == 'Pembina (IV/a)'){
      var jenjang = 'madya';
  }
  if(value == 'Pembina Tk.I (IV/b)'){
      var jenjang = 'madya';
  }
  if(value == 'Pembina Utama Muda (IV/c)'){
      var jenjang = 'madya';
  }
  if(value == ''){
      var jenjang = '';
  }
  var select = $('#no_surat :selected').val();
  var jenjang_old = $('#jenjang_old').val().toLowerCase();
  var no_surat_old = $('#no_surat_old').val();
  event.preventDefault();
  $.ajaxSetup({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    jQuery.ajax({
        method: 'get',
        url: "../cek-jenjang-edit/" + jenjang + "/"+ select + "/"+ jenjang_old + "/"+ no_surat_old,
        success: function(result){
            // alert(result.msg);
            // alert(result.peserta_terdaftar);
            if (result.msg == 'penuh') {
              console.log('penuh');
                // alert('kuota penuh, silahkan pilih jenjang/no surat usulan yang lainnya.');
                $('#jabatan').val('');
                $('#errPangkat').html('jenjang penuh,silahkan pilih jenjang yang lain');
                // $('select#no_surat').val('');
                $('#errno_surat').html('jenjang penuh,silahkan pilih jenjang yang lain');
            }else{
                console.log('lanjut');
                $('#errPangkat').html('');
                $('#errno_surat').html('');
            }
        }});
});
</script>
@endsection
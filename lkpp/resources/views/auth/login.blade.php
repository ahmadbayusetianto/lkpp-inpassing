@extends('layout.app2')
@section('title')
	Sistem Informasi Penyesuaian/Inpassing JF PPBJ
@stop
@section('css')
<style>
	body{
		width: 100%;
	}

	.bg-top{
		width: 100%;
		background-image: url('{{ asset('assets/img/back.jpg') }}'); /* The image used */
		background-color: #cccccc; /* Used if the image is unavailable */
		height: 300px; /* You must set a specified height */
		background-position: center; /* Center the image */
		background-repeat: no-repeat; /* Do not repeat the image */
		background-size: cover; /* Resize the background image to cover the entire container */
	}

	.info-txt{
		padding: 50px;
	}

	.info-txt hr{
		border-top: 1px solid rgba(0, 0, 0, 0.43);
	}

	.txt-tp{
		font-weight: 600;
	}

	.txt-btm{
		font-size: 22px;
	}

	.lbl-form{
		width: 100%;
		font-weight: 600;
		color: gray;
	}

	.card-head{
		color: white;
		padding: 10px 20px 10px 20px;
		background: #ff4141;
		margin: -1px;
	}

	.card-head h3{
		font-weight: 600;
	}

	.card-body{
		padding-bottom: 10px;
	}

	.card-bottom{
		flex: 1 1 auto;
		padding: 0px 20px 10px 20px;
		margin: 35px 0px;
	}

	.card{
		margin-top: -110px;
		-webkit-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		-moz-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
	}

	.card .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
	}

	.btn-log{
		background: #ff4141;
		font-weight: 600;
		color: white;
		width: 100px;
	}

	.btn-daf{
		background: slategray;
		font-weight: 600;
		color: white;
		width: 100px;
	}

	.p-daf{
		margin: 15px 0px;
	}

	.running{
		height: 50px;
		background: #3a394e;
	}

	#type {
		margin-bottom: 15px;
		font-size: 18px;
		font-weight: 200;
		color: #fff;
	}

	@media(min-width: 320px) and (max-width: 425px) {
		.card{
			margin-top: 10px;
		} 
		.running{
			height: 100px;
			background: #3a394e;
		}

		#type {
			margin-bottom: 15px;
			font-size: 18px;
			font-weight: 200;
			color: #fff;
		}
	}	
	@media screen and (min-width: 768px) {
		#type {
			font-size: 20px;
		}
	}

	.txt-running{
		line-height: 2;
	}

	span.badge{
		vertical-align: -webkit-baseline-middle;
		width: 100%;
		font-size: 16px;
		font-weight: 600;
		color: #3a394e;
	}

	.btn-login{
		margin: 10px;
	}

	.alert-login{
		padding: .75rem 0.45rem !important;
	}

	.icon-cell{
		font-size: 50px;
	}
	.carousel-inner{
		max-height: 500px;
	}

</style>
@endsection
@section('content')
{{-- <div class="bg-top"></div> --}}
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="5000">
	<ol class="carousel-indicators">
                @foreach($data_banner as $key => $datas)
  				  @if($key == 0)
                  <li data-target="#carouselExampleSlidesOnly" data-slide-to="{{ $key}}" class="active"></li>
                  @else
                  <li data-target="#carouselExampleSlidesOnly" data-slide-to="{{$key}}" class=""></li>
                  @endif
                @endforeach
                </ol>
  <div class="carousel-inner">
  	@if($data_banner != "")
  	@foreach($data_banner as $key => $datas)
  	@if($key == 0)
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('storage/data/banner/'.$datas->file)}}" alt="First slide">
    </div>
    @else
    <div class="carousel-item ">
      <img class="d-block w-100" src="{{ asset('storage/data/banner/'.$datas->file)}}" alt="First slide">
    </div>
    @endif
    @endforeach
    @else
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('assets/img/back.jpg') }}" alt="First slide">
    </div>
    
    @endif
  </div>
</div>
<div class="running">
	<div class="row txt-running container">
		<div class="col-md-1" style="line-height: 2.5;">
			<span class="badge badge-warning"><i class="fa fa-info"></i> Info</span>
		</div>
		<div class="col-md-11">
			<marquee direction="left" scrollamount="4" behavior="scroll"  onmouseover="this.stop()" width="100%"  onmouseout="this.start()" loop="infinite" style="line-height: 2.5;">
				<div id="type">
					@foreach($text as $texts)
					{{ $texts->text }} <b style="color: #ff4141">||</b>  
					@endforeach
				</div>
			</marquee>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9 col-md-12 info-txt">
			@if (session('msg'))			
				@if (session('msg') == "berhasil_verifikasi")
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong class="strong-cell">
						<div class="row">
							<div class="col-md-1"><i class="fa fa-check icon-cell" aria-hidden="true"></i></div>
							<div class="col-md-11">Selamat akun Admin Instansi/Admin PPK Anda telah Aktif.<br> Silakan MASUK dengan menggunakan username/email dan password yang tertera pada email Anda.</div>
						</div>
					</strong>
				</div> 
				@endif			

				@if (session('msg') == "gagal_verifikasi")
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal Verifikasi Akun</strong>
				</div> 
				@endif
			
				@if (session('msg') == "akun_tidak_aktif")
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Anda Belum Melakukan verifikasi email, Silakan cek email anda atau hubungi admin</strong>
				</div> 
				@endif
			
				@if (session('msg') == "akun_dihapus")
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Email/Username belum terdaftar, silahkan klik Daftar.</strong>
				</div> 
				@endif			
			@endif
			<h5 class="txt-tp">Selamat datang di Sistem Informasi Penyesuaian/Inpassing JF PPBJ</h5><hr>

			<p class="txt-btm">Sebelum mendaftar silakan membaca Peraturan LKPP Nomor 4 Tahun 2019 dan <a href="{{ url('prosedur') }}">Prosedur Pendaftaran</a> terlebih dahulu.
				<br><br> 
				<b>HATI-HATI!</b>
				<br>Banyak penipuan mengatasnamakan LKPP, baik dalam bentuk kegiatan Rapat Koordinasi, Bimtek/Pelatihan maupun Ujian. LKPP tidak memperjualbelikan kunci jawaban dan soal.</p>
		</div>
		<div class="col-lg-3 col-md-12">
			<div class="card">
				<form method="POST" action="{{ route('login') }}" autocomplete="off">
				@csrf
					<div class="card-head"><h3>Masuk</h3></div>
					<div class="card-body">
						@if ($errors->has('email'))
						<div class="alert alert-warning alert-login">
							{{ $errors->first('email') }}
						</div>
						@endif
						{{-- @if ($errors->has('email'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif --}}
						<label for="email" class="lbl-form">Email
							<input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autocomplete="false" placeholder="Masukkan email Anda" required autofocus>
						</label>
						<label for="password" class="lbl-form">Password
							<input id="password" type="password" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="password" placeholder="Masukkan password Anda" required>
							<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword()"></span>
							@if ($errors->has('password'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</label>
						<label for="email" class="lbl-form">
							<div class="captcha">
								<span>{!! captcha_img() !!}</span>
								{{-- <button type="button" class="btn btn-success" id="refresh"><i class="fa fa-refresh"></i></button> --}}
								<label for="password" class="lbl-form">Hasil Hitung CAPTCHA
									<input id="captcha" type="text" class="form-control" placeholder="Masukkan angka hasil perhitungan di atas" name="captcha" required></div>
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('captcha') }}</strong>
									</span>
								</label>
							</div>
						</label>
						<div class="row btn-login">
							<div class="col-md-12" style="font-size:12px">
								Belum Punya Akun?
							</div>
							<div class="col-md-6">
								<a href="{{ url('register') }}" class="btn btn-sm btn-daf">Daftar</a>
							</div>
							<div class="col-md-6" style="text-align:right">
								<button type="submit" class="btn btn-sm btn-log">Masuk</button>
							</div>
							<div class="col-md-12" style="text-align:right;font-size:11px">
								<b>Lupa Password ? </b><br><a href="{{ url('lupa-password') }}">Klik di sini</a>
							</div>
						</div>
					</div>
				</form> 
				<div class="card-bottom">
					{{-- <p class="p-daf">Belum Punya Akun? Klik &nbsp;<a href="{{ url('register') }}"><button class="btn btn-sm btn-daf">Daftar</button></a> </p> --}}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	$('#refresh').click(function(){
		$.ajax({
			type:'GET',
			url:'refreshcaptcha',
			success:function(data){
				$(".captcha span").html(data.captcha);
			}
		});
	});
	
	$(".alert-login").fadeTo(1500, 1000).slideUp(1000, function(){
		$(".alert-login").slideUp(500);
	});
	
	document.addEventListener("DOMContentLoaded", function() {
		var elements = document.getElementsByTagName("INPUT");
		for (var i = 0; i < elements.length; i++) {
			elements[i].oninvalid = function(e) {
				e.target.setCustomValidity("");
				if (!e.target.validity.valid) {
					e.target.setCustomValidity("Kolom Ini Tidak Boleh Kosong");
				}
			};
			
			elements[i].oninput = function(e) {
				e.target.setCustomValidity("");
			};
		}
	})
	
	function showPassword() {
		$('.toggle-password').toggleClass("fa-eye fa-eye-slash");
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
</script>
@endsection
@extends('layout.app2')
@section('title')
    Register
@endsection
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .jud-register{
        padding: 15px;
        color: #3a394e;
    }

    .box{
        -webkit-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        -moz-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
    }

    .btn-default1{
        background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
        color: white;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default2{
        background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
        color: black;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default1:hover{
        color: #eee;
        background: #c10013;
    }

    .btn-area{
        text-align: right;
    }

    .navi ul{
		padding:0;
		font-size:0;
		overflow:hidden;
		display:inline-block;
		width: 100%;
    }
    
	.navi li{
		display:inline-block;
		text-align: center;
    }
    
	.navi a{
		font-size:1rem;
		position:relative;
		display:inline-block;
		background:#eee;
		text-decoration:none;
		color:#555;
		padding:13px 25px 13px 10px;
		width: 100%;
		font-weight: 600;
    }
    
	.navi a:after,
    .navi a:before{
		position: absolute;
		content: "";
		height: -10px;
		width: 0px;
		top: 50%;
		left: -28px;
		margin-top: -25px;
		border: 25px solid #eee;
		border-right: 2 !important;
		border-left-color: transparent !important;
    }

    .navi a:before{
		left:-26px;
		border: 24px solid #555;
    }

    /* ACTIVE STYLES */
    .navi a.active{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
	}
	
    .navi a.active:after{border-color:#e74c3c;}
    
	/* HOVER STYLES */
    .navi a:hover{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
    }
    
	.navi a:hover:after{ border-color:#e74c3c;}

    #page-two{
        display: none;
    }

    #page-three{
        display: none;
    }

    div#page-two div.container div.row{
        padding: 0px 5%; 
        margin-bottom: 1%;
    }

    div#page-two h5 span{
        color: #e74c3c;
    }

    label.custom-file-label{
        margin: 0px 15px;
    }

    .row .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }

    .row .custom-file-label{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19); 
    }

    span.ket{
        font-size: 12px;
        color: #000 !important;
    }

    .lh {
        line-height: 2.5;
    }

    .select2-container{
        border: 1px solid #ced4da;
        width: 100% !important;
        padding: 1px;
        box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }   
    
	.select2-container--default .select2-selection--single{
        border:0px !important;
    }
    
	input[type="number"] {
		-webkit-appearance: textfield;
        -moz-appearance: textfield;
		appearance: textfield;
    }
    
	input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none;
    }    

    #page-two span{
        color: red;
        font-size: 12px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered{
        color: #000 !important;
        font-size: 14px;
    }
</style>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3 class="jud-register">Daftar Akun Administrator Penyesuaian/Inpassing (Admin Instansi/Admin PPK)</h3>
		</div>
		<div class="col-md-12">
			<div class="box">
				<div class="navi">
					<ul>
						<li style="width:30%"><a href="#" id="li-one" class="active">1.Keterangan</a></li>
						<li style="width:40%"><a href="#" id="li-two">2.Data Pribadi Admin</a></li>
						<li style="width:30%"><a href="#" id="li-three">3.Konfirmasi</a></li>
					</ul>
				</div>
				<div class="page container-fluid" id="page-one">
					<p>{!! $keterangan->text !!}</p>
					<div class="col-md-12 btn-area">
						<button class="btn btn-sm btn-default1" id="btnOne" onclick="btnOne()">Lanjut<i class="fa fa-angle-right"></i></button>
					</div>
				</div>
				<form action="{{ url('register-store') }}" method="POST" enctype="multipart/form-data">
				@csrf
					<div class="page container-fluid" id="page-two">
						<div class="container">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>Nama<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="text" name="nama" id="nama" class="form-control form-control-sm" value="{{ old('nama') }}" required>
									<span id="errNama"></span>
									<span>{{ $errors->first('nama') }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>NIP<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="text" name="nip" id="nip" class="form-control form-control-sm" value="{{ old('nip') }}" maxlength="18" onkeypress='validate(event)' onblur="duplicateNip(this)" required>
									<span id="errNip"></span>
									<span>{{ $errors->first('nip') }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>Jabatan<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="text" name="jabatan" id="jabatan" class="form-control form-control-sm" value="{{ old('jabatan') }}" required>
									<span id="errJabatan"></span>
									<span>{{ $errors->first('jabatan') }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>Nama Instansi<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									{!! Form::select('nama_instansi', $instansi , old('nama_instansi'), ['placeholder' => 'Pilih Nama Instansi', 'class' => 'form-control js-example-basic-single', 'id' => 'select-single']); !!}
									<span id="errInstansi"></span>
									<span>{{ $errors->first('nama_instansi') }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>Nama Satuan Kerja/OPD<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div> 
								<div class="col-lg-7 col-md-7 col-12">
									<input type="text" name="nama_satuan" id="nama_satuan" class="form-control form-control-sm" value="{{ old('nama_satuan') }}" required>
									<span id="errSatuan"></span>
									<span>{{ $errors->first('nama_satuan') }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>No. Telepon/HP<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="text" name="no_telp" id="no_telp" class="form-control form-control-sm" value="{{ old('no_telp') }}" minlength="2" maxlength="12" onkeypress='validate(event)' onchange="checkTelp(this)" required>
									<span id="errTelp"></span>
									<span>{{ $errors->first('no_telp') }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>Email<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="email" name="email" id="email" class="form-control form-control-sm" value="{{ old('email') }}" onblur="duplicateEmail(this)" autocomplete="off" onkeypress="return AvoidSpace(event)" required>
									<span id="errEmail"></span>
									<span>{{ $errors->first('email') }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5 class="lh">Foto<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1 lh">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="file" name="foto" class="custom-file-input input-foto" id="customFile" value="{{ old('foto') }}" accept=".jpg, .jpeg" required>
									<label class="custom-file-label" for="customFile">Pilih Foto</label>
									<span class="ket"><i>*min. 100KB max. 2MB, format JPG, JPEG</i></span>
									<span>{{ $errors->first('foto') }}</span>
									<span id="errFoto"></span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>SK Pengangkatan Administrator Penyesuaian/Inpassing<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1 lh">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="file" name="image" class="custom-file-input input-berkas" id="customFile" value="{{ old('image') }}" accept="application/pdf, .jpg, .jpeg" required>
									<label class="custom-file-label" for="customFile">Pilih Dokumen</label>
									<span class="ket"><i>*min. 100KB max. 2MB, format PDF, JPG, JPEG</i></span>
									<span>{{ $errors->first('image') }}</span>
									<span id="errImage"></span>
								</div>
							</div>
						</div>
						<div class="col-md-12 btn-area">
							<button class="btn btn-sm btn-default2" id="btnTwoBack" onclick="btnTwoBack()"><i class="fa fa-angle-left"></i> Kembali</button>
							<button class="btn btn-sm btn-default1" id="btnTwoNext">Lanjut <i class="fa fa-angle-right"></i></button>
						</div>
					</div>
					<div class="page container-fluid" id="page-three">
						<p>{!! $konfirmasi->text !!}</p>
						<div class="col-md-12 btn-area">
							<button class="btn btn-sm btn-default2" id="btnThreeBack" onclick="btnThreeBack()"><i class="fa fa-angle-left"></i> Kembali</button>
							<button class="btn btn-sm btn-default1" type="submit">Ajukan <i class="fa fa-angle-right"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>
    $("#email").bind("paste",function(){
        return false;
    });
	
    function AvoidSpace(event) {
        var k = event ? event.which : window.event.keyCode;
        if (k == 32) return false;
    }
	
    function duplicateEmail(element){
        var email = $(element).val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var validate = re.test(email);
        if (validate) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{url('checkemail')}}',
                data: {email:email},
                dataType: "json",
                success: function(res) {
                    if(res.exists){
                        setToTrue(validasi_email);
                        $('#errEmail').html("")
                    } else {
                        setToFalse(validasi_email);
                        $('#errEmail').html("Email sudah terdaftar, silakan gunakan email lain.")
                    }
                },
                error: function (jqXHR, exception) {

                }
            });   
        } else {
            setToFalse(validasi_email);
            $('#errEmail').html("Alamat email tidak valid.");
        }
    }

  
    function duplicateNip(element){
        var nip = $(element).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{url('checknip')}}',
            data: {nip:nip},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                    setToTrue(validasi_nip);
                    $('#errNip').html("")
                } else {
                    setToFalse(validasi_nip);
                    $('#errNip').html("NIP sudah Terdaftar, NIP hanya boleh di gunakan sekali.")
                }
            },
            error: function (jqXHR, exception) {

            }
        });
    }

    function modifyVar(obj, val) {
        obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
    }

    function setToFalse(boolVar) {
        modifyVar(boolVar, false);
    }

    function setToTrue(boolVar) {
        modifyVar(boolVar, true);
    }

    var validasi_nama = new Boolean(true);
    var validasi_nip = new Boolean(true);
    var validasi_jabatan = new Boolean(true);
    var validasi_instansi = new Boolean(true);
    var validasi_satuan = new Boolean(true);
    var validasi_telp = new Boolean(false);
    var validasi_email = new Boolean(true);
    var validasi_foto = new Boolean(true);
    var validasi_image = new Boolean(true);

     function checkTelp(element){
		if ($('#no_telp').val().length >= 13 || $('#no_telp').val().length <=1) {
			$('#errTelp').html("No. Telepon/HP harus berjumlah 2 hingga 12 digit.")
            setToFalse(validasi_telp);
		} else {
            setToTrue(validasi_telp);
            $('#errTelp').html("")
        }
    }


    $('.input-foto').bind('change', function() {
        var size_input_foto = this.files[0].size;
        if(size_input_foto > 2097152){
            $('#errFoto').html("file tidak boleh lebih dari 2MB");
            setToFalse(validasi_foto);
        }

        if(size_input_foto < 102796){
            $('#errFoto').html("file tidak boleh kurang dari 100kb");
            setToFalse(validasi_foto);
        }

        if (102796 < size_input_foto && size_input_foto < 2097152) {
            $('#errFoto').html("");
            setToTrue(validasi_foto);
        }
    });

    $('.input-berkas').bind('change', function() {
        var size_input_berkas = this.files[0].size;
        if(size_input_berkas > 2097152){
            $('#errImage').html("file tidak boleh lebih dari 2MB");
            setToFalse(validasi_image);
        }

        if(size_input_berkas < 102796){
            $('#errImage').html("file tidak boleh kurang dari 100kb");
            setToFalse(validasi_image);
        }

        if (102796 < size_input_berkas && size_input_berkas < 2097152) {
            $('#errImage').html("");
            setToTrue(validasi_image);
        }
    });

    $('#btnOne').click(function (e) {
        e.preventDefault();
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#btnTwoBack').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#btnTwoNext').click(function (e) {
        e.preventDefault();

        if ($('#nama').val().length == 0) {
            $('#errNama').html("Nama Tidak Boleh Kosong");
            setToFalse(validasi_nama); 
        } else {
            $('#errNama').html("");
            setToTrue(validasi_nama);
        }

        if ($('#nip').val().length == 0) {
            $('#errNip').html("NIP Tidak Boleh Kosong")
            setToFalse(validasi_nip);
        }

        if ($('#jabatan').val().length == 0) {
            $('#errJabatan').html("Jabatan Tidak Boleh Kosong")
            setToFalse(validasi_jabatan);
        } else {
            $('#errJabatan').html("")
            setToTrue(validasi_jabatan); 
        }

        if ($('#select-single').val().length == 0) {
            $('#errInstansi').html("Instansi Tidak Boleh Kosong")
            setToFalse(validasi_instansi);
        } else {
            $('#errInstansi').html("")
            setToTrue(validasi_instansi); 
        }

        if ($('#nama_satuan').val().length == 0) {
            $('#errSatuan').html("Satker Tidak Boleh Kosong")
            setToFalse(validasi_satuan);
        } else {
            $('#errSatuan').html("")
            setToTrue(validasi_satuan); 
        }

        if ($('#no_telp').val().length < 2) {
            $('#errTelp').html("No. Telepon/HP harus berjumlah 2 hingga 12 digit")
            setToFalse(validasi_telp);
        } else {
            if ($('#no_telp').val().length > 12) {
                $('#errTelp').html("No. Telepon/HP harus berjumlah 2 hingga 12 digit")
                setToFalse(validasi_telp);
            } else {
                $('#errTelp').html("")
                setToTrue(validasi_telp); 
            }
        }

        if ($('#email').val().length == 0) {
            $('#errEmail').html("Email Tidak Boleh Kosong")
            setToFalse(validasi_email);
        }

        if ($('.input-foto').val().length == 0) {
            $('#errFoto').html("Foto Diri Tidak Boleh Kosong")
            setToFalse(validasi_foto);
        }

        if ($('.input-berkas').val().length == 0) {
            $('#errImage').html("Berkas SK Tidak Boleh Kosong")
            setToFalse(validasi_image);
        }


        if(validasi_nama == true && validasi_nip == true && validasi_jabatan == true && validasi_instansi == true && validasi_satuan == true && validasi_telp == true && validasi_email == true && validasi_foto == true && validasi_image == true){
			$("ul li a.active").removeClass("active");
			$("ul li a#li-three").addClass("active");
			document.getElementById("page-one").style.display = "none";
			document.getElementById("page-two").style.display = "none";
			document.getElementById("page-three").style.display = "block";
		} else {
			alert('kolom/Berkas inputan masih ada yang tidak sesuai atau kosong.\nSilakan cek kembali kolom/Berkas inputan anda.');
		}        
	});

    $('#btnThreeBack').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";
    });

    $('#li-one').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#li-two').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";        
    });

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(document).ready(function() {
		$('.js-example-basic-single').select2();
    });
</script>
@endsection
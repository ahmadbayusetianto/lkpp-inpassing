@extends('layout.app')

@section('title')
Jadwal Verifikasi Usulan
@stop
@section('css')
<style type="text/css">
.form-control
{
  border-radius: 5px;
  height: 27px;
  padding: 0px;
  padding-left: 10px;
}

.row{
  margin-bottom: 0px;
  margin-left: 15px;
  margin-right: 15px;
}

.down{
margin-bottom: 10px;
}



  
</style>
@stop
@section('content')
<div class="col-md-11">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title" style="padding-top: 10px;">Verifikasi Portofolio</h3>
      <hr>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
     <div class="row">
      <div class="col-md-2">
       <label>Nama Peserta </label>
     </div>
     <div class="col-md-1">
       <label>:</label>
     </div>
     <div class="col-md-3">
       <label>Abio</label>
     </div>
     <div class="col-md-2">
        <label>Rekomendasi     :  </label>
     </div>
     
     <div class="col-md-3 how">
        <select class="form-control">
            <option>option 1</option>
            <option>option 2</option>
            <option>option 3</option>
            <option>option 4</option>
            <option>option 5</option>
        </select>
      </div>
   </div>

   <div class="row">
    <div class="col-md-2">
     <label>No. Ujian </label>
   </div>
   <div class="col-md-1">
     <label>:</label>
   </div>
   <div class="col-md-4">
     <label>1592821</label>
   </div>
   <div style="margin-left: 80px;">
     <div class="col-md-1">
    <label>
           <input type="checkbox" class="minimal" style="width: 100px">
    </label>
   </div>
   <div class="col-md-2"><label>Publish</label></div>
   </div>
   
 </div>

 <div class="row">
  <div class="col-md-2">
   <label>NIP </label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>15192831</label>
 </div>
</div>

<div class="row">
  <div class="col-md-2">
   <label>Pangkat/Gol. </label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>III/3</label>
 </div>
</div>

<div class="row">
  <div class="col-md-2">
   <label>Jenjang </label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>Pertama</label>
 </div>
</div>

<div class="row">
  <div class="col-md-2">
   <label>SK KP Terakhir </label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>skkp.pdf</label>
 </div>
</div>

<div class="row">
  <div class="col-md-2">
   <label>Asesor</label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>xxxx</label>
 </div>
</div>

<hr>
<div class="row">
  <div class="col-md-2">
 </div>
 <div class="col-md-1">

 </div>
 <div class="col-md-7">

 </div>
</div>



<div class="row down">
  <div class="col-md-4">
   <label>Kompetensi Perencana PBJP</label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>perencana.pdf</label>
 </div>
</div>

<div class="row down">
  <div class="col-md-4">
   <label>Kompetensi Pemilihan PBJP</label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>pemilihan.pdf</label>
 </div>
</div>

<div class="row down">
  <div class="col-md-4">
   <label>Kompetensi Pengelolaan Kontrak PBJP</label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>kontrak.pdf</label>
 </div>
</div>

<div class="row down">
  <div class="col-md-4">
   <label>Kompetensi PBJ Secara Swakelola</label>
 </div>
 <div class="col-md-1">
   <label>:</label>
 </div>
 <div class="col-md-7">
   <label>swakelola.pdf</label>
 </div>
</div>
<div class="col-md-7"></div>
<div class="col-md-2"><button type="button" class="btn btn-block btn-default">Batal</button></div>
<div class="col-md-2"><button type="button" class="btn btn-block btn-danger">Simpan</button></div>
<div class="col-md-1"></div>
</div>


</div>




</div>
</div>




</div>
</div>
<!-- /.box-body -->
</div>
</div>
@stop
@extends('layout.app2')

@section('title')
Statistik Pelaksanaan Ujian 
@stop

@section('css')
<style>
  body{
    width: 100%;
  }

  .bg-top{
    width: 100%;
    background-image: url('{{ asset('assets/img/back.jpg') }}'); /* The image used */
    background-color: #cccccc; /* Used if the image is unavailable */
    height: 300px; /* You must set a specified height */
    background-position: center; /* Center the image */
    background-repeat: no-repeat; /* Do not repeat the image */
    background-size: cover; /* Resize the background image to cover the entire container */
  }

  .info-txt{ 
    padding: 50px;
  }

  .info-txt hr{
    border-top: 1px solid rgba(0, 0, 0, 0.43);
  }

  .txt-tp{
    font-weight: 600;
  }

  .txt-btm{
    font-size: 22px;
  }

  .lbl-form{
    width: 100%;
    font-weight: 600;
    color: gray;
  }

  .card-head{
    color: white;
    padding: 10px 20px 10px 20px;
    background: #ff4141;
    margin: -1px;
  }

  .card-head h3{
    font-weight: 600;
  }

  .card-body{
    padding-bottom: 10px;
  }

  .card-bottom{
    flex: 1 1 auto;
    padding: 0px 20px 10px 20px;
    margin: 35px 0px;
  }

  .card{
    margin-top: -110px;
    -webkit-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
    -moz-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
    box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
  }

  .card .form-control{
    -webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    -moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
  }

  .btn-log{
    background: #ff4141;
    font-weight: 600;
    color: white;
    width: 100px;
  }

  .btn-daf{
    background: slategray;
    font-weight: 600;
    color: white;
    width: 100px;
  }

  .p-daf{
    margin: 15px 0px;
  }

  @media(min-width: 320px) and (max-width: 768px) {
    .card{
      margin-top: 10px;
    } 
  }
  .running{
    height: 50px;
    background: #3a394e;
  }

  #type {
    margin-bottom: 15px;
    font-size: 18px;
    font-weight: 200;
    color: #ffffff;
  }
  @media screen and (min-width: 768px) {
    #type {
      font-size: 23px;
    }
  }

  .txt-running{
    line-height: 2;
  }

  span.badge{
    vertical-align: -webkit-baseline-middle;
    width: 100%;
    font-size: 16px;
    font-weight: 600;
    color: #3a394e;
    text-align: left;
  }

  .submit-btn{
    width: 100%;
    text-align: right;
  }
  
  .submit-btn button{
    margin: 10px;
  }

  ul {
    list-style-type: none;
  }
  h4{
    color: #fff;
  }
</style>
@endsection

@section('content')
<div class="bg-top">

</div>
<div class="running">
  <div class="row txt-running container-fluid">
    <div class="col-md-1" style="line-height: 2.5;">
      <span class="badge badge-warning" style="margin-left: 15px;"><i class="fa fa-info"></i> Info</span>
    </div>
    <div class="col-md-11">
      <marquee direction="left" scrollamount="4" behavior="scroll"  onmouseover="this.stop()" width="100%"  onmouseout="this.start()" loop="infinite" >
        <div id="type">
          @foreach($text as $texts)
          {{$texts->text }} <b style="color: #ff4141">||</b>  
          @endforeach
        </div>
      </marquee>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 col-md-12 info-txt">
      <h5 class="txt-tp">Statistik Pelaksanaan Ujian </h5>
      <hr>
      <p class="txt-btm">
        @php
        echo $statistik->text;
        @endphp
      </p>
    </div>
      <div class="col-md-12 info-txt" style="margin-top: 3%;">
    <div class="row">
      {{-- <div class="col-3">  </div> --}}
      <div class="col-12">
        <div class="card">
          @foreach($data as $datas)
          @if($datas->metode == 'verifikasi_portofolio')
          <div class="card-header bg-success">
          @else
          <div class="card-header bg-info">
          @endif
            <h4 style="font-weight: bold;text-align: center">Data Hasil Uji Kompetensi Regular Metode {{ $datas->metode == 'tes_tulis' ? 'Tes Tertulis' : 'Verifikasi Portofolio'}} tanggal {{ Helper::tanggal_indo($datas->tanggal_ujian)}}</h4>
          </div>
          @endforeach
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                  <div class="table-responsive">
                 <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Peserta didaftarkan</th>
                      <th>Peserta Memenuhi Persyaratan</th>
                      <th>Peserta Lulus</th>
                    </tr>
                  </thead>
                     @php
                      $pertama = array();
                      $muda = array();
                      $madya = array();

                      foreach($dataTotalJenjang as $datatotal){
                        if ($datatotal->jenjang == 'Pertama') {
                          $pertama['total_daftar'] = $datatotal->jumlah;
                        }
                        if ($datatotal->jenjang == 'Muda') {
                          $muda['total_daftar'] = $datatotal->jumlah;
                        }
                        if ($datatotal->jenjang == 'Madya') {
                          $madya['total_daftar'] = $datatotal->jumlah;
                        }
                      }

                      foreach($dataMemenuhiJenjang as $datatotal){
                        if ($datatotal->jenjang == 'Pertama') {
                          $pertama['total_memenuhi'] = $datatotal->jumlah;
                        }
                        if ($datatotal->jenjang == 'Muda') {
                          $muda['total_memenuhi'] = $datatotal->jumlah;
                        }
                        if ($datatotal->jenjang == 'Madya') {
                          $madya['total_memenuhi'] = $datatotal->jumlah;
                        }
                      }

                      foreach($dataLulusJenjang as $datatotal){
                        if ($datatotal->jenjang == 'Pertama') {
                          $pertama['total_lulus'] = $datatotal->jumlah;
                        }
                        if ($datatotal->jenjang == 'Muda') {
                          $muda['total_lulus'] = $datatotal->jumlah;
                        }
                        if ($datatotal->jenjang == 'Madya') {
                          $madya['total_lulus'] = $datatotal->jumlah;
                        }
                      }
                    @endphp

                  <tbody>
                  <tr>
                    <td><b>Pertama</b></td>
                    <td>{{ isset($pertama['total_daftar']) ? $pertama['total_daftar'] : 0}}</td>
                    <td>{{ isset($pertama['total_memenuhi']) ?  $pertama['total_memenuhi'] : 0 }}</td>
                    <td>{{ isset($pertama['total_lulus']) ? $pertama['total_lulus'] : 0}}</td>
                  </tr>
                  <tr>
                    <td><b>Muda</b></td>
                    <td>{{ isset($muda['total_daftar']) ? $muda['total_daftar'] : 0}}</td>
                    <td>{{ isset($muda['total_memenuhi']) ? $muda['total_memenuhi'] : 0}}</td>
                    <td>{{ isset($muda['total_lulus']) ? $muda['total_lulus'] : 0 }}</td>
                  </tr>
                  <tr>
                    <td><b>Madya</b></td>

                    <td>{{ isset($madya['total_daftar']) ? $madya['total_daftar'] : 0}}</td>
                    <td>{{ isset($madya['total_memenuhi']) ? $madya['total_memenuhi'] : 0}}</td>
                    <td>{{ isset($madya['total_lulus']) ? $madya['total_lulus'] : 0}}</td> 
                  </tr>
                    <tr>
                    <td><b>Total</b></td>
                    @foreach($dataTotal as $pesertas) 
                    @if($pesertas->jumlah != 0)
                      <td>{{$pesertas->jumlah}}</td>   
                    @else
                      <td>0</td>                   
                    @endif
                    @endforeach 
                    @foreach($dataMemenuhiTotal as $pesertas) 
                   @if($pesertas->jumlah != 0)
                      <td>{{$pesertas->jumlah}}</td>   
                    @else
                      <td>0</td>                   
                    @endif
                    @endforeach 
                    @foreach($dataLulusTotal as $pesertas) 
                    @if($pesertas->jumlah != 0)
                      <td>{{$pesertas->jumlah}}</td>   
                    @else
                      <td>0</td>                   
                    @endif
                    @endforeach 
                  </tr>
                  </tbody>
                </table>
                </div> 
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12"><h3><center>Peserta didaftarkan</center></h3></div>
                  <div class="col-md-12"><canvas id="CHbarDaftar"></canvas></div>
                </div>
                <hr>
              <div class="row mt-4">
                  <div class="col-md-12"><h3><center>Peserta Memenuhi Persyaratan</center></h3></div>
                  <div class="col-md-12"><canvas id="CHbarSyarat"></canvas></div>
                </div>
                 <hr>
                   <div class="row mt-4">
                  <div class="col-md-12"><h3><center>Peserta Lulus</center></h3></div>
                  <div class="col-md-12"><canvas id="CHbarLulus"></canvas></div>
                </div>
              </div>
            </div>
          {{--   <hr>
            <span class="badge" style="background:#a0ccf4 ;color:#fff " id="jumlah">  </span>
            <span class="badge" style="background:#a991d3 ;color:#fff " id="lulus">  </span>
            <span class="badge" style="background:#a0ccf4 ;color:#fff " id="tidak_lulus">  </span>
            <span class="badge" style="background:#a991d3 ;color:#fff " id="jumlah_lulus_madya">  </span>
            <span class="badge" style="background:#a0ccf4 ;color:#fff " id="jumlah_tidak_lulus_madya">  </span>
            <span class="badge" style="background:#a991d3 ;color:#fff " id="jumlah_lulus_muda">  </span>
            <span class="badge" style="background:#a0ccf4 ;color:#fff " id="jumlah_tidak_lulus_muda">  </span>
            <span class="badge" style="background:#a991d3 ;color:#fff " id="jumlah_lulus_pertama">  </span>
            <span class="badge" style="background:#a0ccf4 ;color:#fff " id="jumlah_tidak_lulus_pertama">  </span>
            <span class="badge" style="background:#a991d3 ;color:#fff " id="jumlah_tidak_syarat">  </span> --}}
          </div>
        </div>
      </div>
      {{-- <div class="col-3">  </div> --}}
    </div>    
  </div>
  
@endsection

@section('js')
<!-- ChartJS -->



<!--<script>
  $.fn.typer = function(text, options){
    options = $.extend({}, {
      char: ' ',
      delay: 2000,
      duration: 1000,
      endless: true
    }, options || text);

    text = $.isPlainObject(text) ? options.text : text;

    var elem = $(this),
    isTag = false,
    c = 0;
    
    (function typetext(i) {
      var e = ({string:1, number:1}[typeof text] ? text : text[i]) + options.char,
      char = e.substr(c++, 1);

      if( char === '<' ){ isTag = true; }
      if( char === '>' ){ isTag = false; }
      elem.html(e.substr(0, c));
      if(c <= e.length){
        if( isTag ){
          typetext(i);
        } else {
          setTimeout(typetext, options.duration/10, i);
        }
      } else {
        c = 0;
        i++;
        
        if (i === text.length && !options.endless) {
          return;
        } else if (i === text.length) {
          i = 0;
        }
        setTimeout(typetext, options.delay, i);
      }
    })(0);
  };

  $('#type').typer([
    @foreach($text as $texts)
    '{{ $texts->text }}',
    @endforeach
    ]);
  
  </script>-->
  <script>
    $('#refresh').click(function(){
      alert('coba');
      $.ajax({
       type:'GET',
       url:'refreshcaptcha',
       success:function(data){
        $(".captcha span").html(data.captcha);
      }
    });
    });
  </script>
  <script type="text/javascript">
  $(document).ready(function() {
    var jumlah_syarat = parseInt(@foreach($dataMemenuhiTotal as $memenuhi) {{ $memenuhi->jumlah}} @endforeach);
    var jumlah_lulus = parseInt(@foreach($dataLulusTotal as $lulus) {{ $lulus->jumlah}} @endforeach);
    var total = parseInt(@if(count($dataTotal) > 0) @foreach($dataTotal as $pesertas) {{$pesertas->jumlah}} @endforeach @else 0 @endif);
                                
      


  });

  $(function () {
      var jumlah_lulus = parseInt(@if(count($dataLulusTotal) > 0)@foreach($dataLulusTotal as $lulus) {{ $lulus->jumlah}} @endforeach @else 0 @endif);
      var jumlah_syarat = parseInt(@if(count($dataMemenuhiTotal) > 0)@foreach($dataMemenuhiTotal as $memenuhi) {{ $memenuhi->jumlah}} @endforeach @else 0 @endif);      
      var total = parseInt(@if(count($dataTotal) > 0) @foreach($dataTotal as $pesertas) {{$pesertas->jumlah}} @endforeach @else 0 @endif);   
                                
      // DIDAFTARKAN
      var ctx = document.getElementById('CHbarDaftar').getContext('2d');
      
      ctx.height = 50;
      new Chart(ctx, {
        type: 'horizontalBar',
        data: {
          labels: [
          'Pertama','Muda','Madya','Total'
          ],
          datasets: [{
            label: ['Lulus','Tidak Lulus'],
            data: [  {{ isset($pertama['total_daftar']) ? $pertama['total_daftar'] : ''}},
            {{ isset($muda['total_daftar']) ? $muda['total_daftar'] : ''}},
            {{ isset($madya['total_daftar']) ? $madya['total_daftar'] : ''}},
            total
            ],
            backgroundColor: [
            '#e3c209',
            '#25c213',
            '#e05307',
            '#095fe0']
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
            "hover": {
          "animationDuration": 0
        },
           "animation": {
          "duration": 1,
            "onComplete": function () {
              var chartInstance = this.chart,
                ctx = chartInstance.ctx;
              
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);

              this.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                var total = @if(count($dataTotal) > 0) @foreach($dataTotal as $pesertas) {{$pesertas->jumlah}}; @endforeach @else 0 @endif
                
                meta.data.forEach(function (bar, index) {

                  var currentValue = dataset.data[index];
                  var percentage = parseFloat((currentValue/total*100).toFixed(1));
                  var data = dataset.data[index]; 
                  var percentage_val = "";
                  if (percentage != 0) {                           
                    percentage_val = '('+percentage+'%)';
                    }
                  else{
                    percentage_val = "";
                  }
                  ctx.fillText(data+' '+percentage_val, bar._model.x + 5, bar._model.y + 5);
                  
                  // ctx.fillText('('+percentage+'%)', bar._model.x + 18, bar._model.y + 5);
                  
                });
              });
            }
        },
          legend: {
            display: false,
            labels: {
              display: false
            },

          },
          tooltips: {
            callbacks: {
              label: function(tooltipItem, data) {
                var dataset = data.datasets[tooltipItem.datasetIndex];
                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                var total = @if(count($dataTotal) > 0) @foreach($dataTotal as $pesertas) {{$pesertas->jumlah}}; @endforeach @else 0 @endif
                var currentValue = dataset.data[tooltipItem.index];
                var percentage = parseFloat((currentValue/total*100).toFixed(1));
                return currentValue + ' (' + percentage + '%)';
              },
              title: function(tooltipItem, data) {
                return data.labels[tooltipItem[0].index];
              }
            }
          },
          scales: {
              yAxes: [{
              ticks: {
              beginAtZero:true,
              fontSize: 14,
              
            }
          }],
            xAxes: [{
            ticks: {
              beginAtZero:true,
              fontSize: 11,
              max: 100
            }
          }]
          }
        }
      });

      // LULUS
       var ctx = document.getElementById('CHbarLulus').getContext('2d');
      
      ctx.height = 50;
      new Chart(ctx, {
        type: 'horizontalBar',
        data: {
          labels: [
          'Pertama','Muda','Madya','Total'
          ],
          datasets: [{
            label: ['Lulus','Tidak Lulus'],
            data: [{{ isset($pertama['total_lulus']) ? $pertama['total_lulus'] : ''}},
            {{ isset($muda['total_lulus']) ? $muda['total_lulus'] : ''}},
            {{ isset($madya['total_lulus']) ? $madya['total_lulus'] : ''}},
             jumlah_lulus],
            backgroundColor: [
            '#e3c209',
            '#25c213',
            '#e05307',
            '#095fe0']
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
            "hover": {
          "animationDuration": 0
        },
           "animation": {
          "duration": 1,
            "onComplete": function () {
              var chartInstance = this.chart,
                ctx = chartInstance.ctx;
              
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);

              this.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                var total = @if(count($dataLulusTotal) > 0) @foreach($dataLulusTotal as $pesertas) {{$pesertas->jumlah}}; @endforeach @else 0 @endif
                
                meta.data.forEach(function (bar, index) {
                  var currentValue = dataset.data[index];
                  var percentage = parseFloat((currentValue/total*100).toFixed(1));
                  var data = dataset.data[index];                            
                  var percentage_val = "";
                  if (percentage != 0) {                           
                    percentage_val = '('+percentage+'%)';
                    }
                  else{
                    percentage_val = "";
                  }
                  ctx.fillText(data+' '+percentage_val, bar._model.x + 5, bar._model.y + 5);
                });
              });
            }
        },
          legend: {
            display: false,
            labels: {
              display: false
            },

          },
          tooltips: {
            callbacks: {
              label: function(tooltipItem, data) {
                var dataset = data.datasets[tooltipItem.datasetIndex];
                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                var total = @if(count($dataLulusTotal) > 0) @foreach($dataLulusTotal as $pesertas) {{$pesertas->jumlah}}; @endforeach @else 0 @endif
                var currentValue = dataset.data[tooltipItem.index];
                var percentage = parseFloat((currentValue/total*100).toFixed(1));
                return currentValue + ' (' + percentage + '%)';
              },
              title: function(tooltipItem, data) {
                return data.labels[tooltipItem[0].index];
              }
            }
          },
          scales: {
              yAxes: [{
              ticks: {
              beginAtZero:true,
              fontSize: 14,
              
            }
          }],
            xAxes: [{
            ticks: {
              beginAtZero:true,
              fontSize: 11,
              max: 100
            }
          }]
          }
        }
      });

       // MEMENUHI SYARAT
       var ctx = document.getElementById('CHbarSyarat').getContext('2d');
      
      ctx.height = 50;
      new Chart(ctx, {
        type: 'horizontalBar',
        data: {
          labels: [
          'Pertama','Muda','Madya','Total'
          ],
          datasets: [{
            label: ['Lulus','Tidak Lulus'],
            data: [ {{ isset($pertama['total_memenuhi']) ? $pertama['total_memenuhi'] : ''}},
            {{ isset($muda['total_memenuhi']) ? $muda['total_memenuhi'] : ''}},
            {{ isset($madya['total_memenuhi']) ? $madya['total_memenuhi'] : ''}},
            jumlah_syarat ],
            backgroundColor: [
            '#e3c209',
            '#25c213',
            '#e05307',
            '#095fe0']
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
            "hover": {
          "animationDuration": 0
        },
           "animation": {
          "duration": 1,
            "onComplete": function () {
              var chartInstance = this.chart,
                ctx = chartInstance.ctx;
              
              ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);

              this.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                var total = @if(count($dataMemenuhiTotal) > 0) @foreach($dataMemenuhiTotal as $pesertas) {{$pesertas->jumlah}}; @endforeach @else 0 @endif
                
                meta.data.forEach(function (bar, index) {
                  var currentValue = dataset.data[index];
                  var percentage = parseFloat((currentValue/total*100).toFixed(1));
                  var data = dataset.data[index];                            
                 var percentage_val = "";
                  if (percentage != 0) {                           
                    percentage_val = '('+percentage+'%)';
                    }
                  else{
                    percentage_val = "";
                  }
                  ctx.fillText(data+' '+percentage_val, bar._model.x + 5, bar._model.y + 5);
                });
              });
            }
        },
          legend: {
            display: false,
            labels: {
              display: false
            },

          },
          tooltips: {
            callbacks: {
              label: function(tooltipItem, data) {
                var dataset = data.datasets[tooltipItem.datasetIndex];
                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                var total = @if(count($dataMemenuhiTotal) > 0) @foreach($dataMemenuhiTotal as $pesertas) {{$pesertas->jumlah}}; @endforeach @else 0 @endif
                var currentValue = dataset.data[tooltipItem.index];
                var percentage = parseFloat((currentValue/total*100).toFixed(1));
                return currentValue + ' (' + percentage + '%)';
              },
              title: function(tooltipItem, data) {
                return data.labels[tooltipItem[0].index];
              }
            }
          },
          scales: {
              yAxes: [{
              ticks: {
              beginAtZero:true,
              fontSize: 14,
              
            }
          }],
            xAxes: [{
            ticks: {
              beginAtZero:true,
              fontSize: 11,
              max: 100
            }
          }]
          }
        }
      });

    });
  </script>

  @endsection
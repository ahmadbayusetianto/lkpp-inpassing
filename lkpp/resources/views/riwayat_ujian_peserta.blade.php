@extends('layout.app')
@section('title')
	Riwayat Ujian Peserta
@stop
@section('css')
<style type="text/css">
	.clickable-row:hover{
		cursor: pointer;
	}
	.div-print{
		text-align: right;
	}
</style>
@stop
@section('content')
<div class="main-box">
	{{-- <div class="row" style="padding: 3%;">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">
					<label>Masukkan NIP</label>
				</div>
				<div class="col-md-6">
					<form action="" method="post">
						@csrf
						<input type="text" class="form-control form-control-sm" value="{{ old('nip') }}" id="nip" name="nip" placeholder="">
				</div>
				<div class="col-md-auto">
					<button type="submit" class="btn btn-primary">Cari</button>
					</form>
				</div>
			</div>
		</div>
	</div> --}}
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-4 col-12">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
			<div class="col-md-5 col-12 div-print">
					<a href="{{ url('riwayat-ujian-peserta-inpassing-excell') }}"><button class="btn btn-success" style="color: #fff"><i class="fa fa-print"></i> Print Excel</button></a>
			</div>
		</div>
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>No Ujian</th>
					<th>Nama</th>
					<th>NIP</th>
					<th>Instansi</th>
					<th>Tanggal Ujian</th>
					<th>Metode Ujian</th>
					<th>Jenjang</th>
					<th>No. Surat Usul Inpassing</th>
					<th>Surat Usul Inpassing</th>
					<th>Hasil Ujian</th>
					<th>Jenis Kompetensi (Lulus)</th>
				</tr>
			</thead>
			<tbody>
				@php
					$no = 1;
				@endphp
				@foreach ($datanih as $key => $datas)
				<tr>
					<td>{{ $no++}}</td>
				@if($datas->no_ujian != "")
					<td>{{ $datas->no_ujian }}</td>
				@else
					<td>{{ '-' }}</td>
				@endif
					<td>{{ $datas->nama_peserta }}</td>
					<td>{{ $datas->nip }}</td>
					<td>{{ $datas->nama_instansi }}</td>
					<td>
					@if($datas->tgl_ujian != "")
						{{ Helper::tanggal_indo($datas->tgl_ujian) }}
					@else
						{{ '-' }}
					@endif
					</td>
					<td>
					@if($datas->metode == 'tes')
						{{ 'Tes Tertulis' }}
					@else
						{{ 'Verifikasi Portofolio' }}
					@endif
                    </td>
                    @if($datas->jenjangs != "")
                    <td>{{ $datas->jenjangs}}</td>
                    @else
                    <td>{{ $datas->jenjang}}</td>
                    @endif
					@if ($datas->no_surat_usulan_peserta == "")
						<td>{{'-'}}</td>
					@else
						<td>{{$datas->no_surat_usulan_peserta}}</td>
					@endif
					<td>
					@if ($datas->file == "")
						{{ '-' }}
                    @else
						@php
							$surat_usulan_mengikuti_inpassing = explode('.',$datas->file);
						@endphp
						@if ($surat_usulan_mengikuti_inpassing[1] == 'pdf' || $surat_usulan_mengikuti_inpassing[1] == 'PDF')
							<a href="{{ url('priview-file')."/surat_usulan_inpassing/".$datas->file }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
						@else
							<a href="{{ url('priview-file')."/surat_usulan_inpassing/".$datas->file }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan_inpassing')."/".$datas->file }}" class="img-responsive" style="width: 80px;" ></a>
						@endif
					@endif
					</td>
					@if($datas->hasil_ujian == "lulus")
						<td>{{ 'Lulus' }}</td>
					@elseif($datas->hasil_ujian == "tidak_lulus")
						<td>{{ 'Tidak Lulus' }}</td>
					@elseif($datas->hasil_ujian == "tidak_hadir")
						<td>{{ 'Tidak Hadir' }}</td>
					@elseif($datas->hasil_ujian == "tidak_lengkap")
						<td>{{ 'Dokumen Persyaratan Tidak Lengkap' }}</td>
					@else
						<td>-</td>
					@endif
					<td>
						@php
						$kompetensi = [];
						$kompetensi = explode(',', $datas->poin_lulus);
						$jum = count($kompetensi);
						@endphp
						@if($jum > 0)
                                <ol type="1">
                                	@for($i=0;$i < $jum; $i++)
                                	@if($kompetensi[$i] != "")
                                	@if((isset($nama_poin[$kompetensi[$i]])))
									<li>	{{ $nama_poin[$kompetensi[$i]] }}</li>
                                	@endif
                                	@endif
                                	@endfor
                                </ol>
                        @else
                        -
                        @endif
						{{-- @if((isset($nama_poin[$datas->poin_lulus])))
							{{ $nama_poin[$datas->poin_lulus] }}
						@else
						-
						@endif --}}

					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('js')
<script>
	jQuery(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});
</script>
@endsection

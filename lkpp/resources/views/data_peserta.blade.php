@extends('layout.app')

@section('title')
Data Peserta
@stop

@section('css')
<style type="text/css">
	.clickable-row:hover{
		cursor: pointer;
	}
	
	ul.dropdown-menu{
		left: -150px;
	}
	
	.btn-default2{
		margin: 10px 10px 0px 5px !important;
	}
	
	.div-print{
		text-align: right;
	}
	
	.div-print button{
		text-align: right;
		color: black;
	}
</style>
@stop
@section('content')
@if (session('msg'))
	@if (session('msg') == "berhasil")
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Berhasil simpan data</strong>
			</div>
		</div>
	</div> 
	@endif
	
	@if (session('msg') == "gagal")
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Gagal simpan data</strong>
			</div>
		</div>
	</div> 
	@endif
@endif

<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-4 col-12">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
			<div class="col-md-5 col-12 div-print">
					<a href="{{ url('data-peserta-excell') }}"><button class="btn btn-success" style="color: #fff"><i class="fa fa-print"></i> Print Excel</button></a>
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIP</th>
					<th>Instansi</th>
					<th>Pangkat/Gol.</th>
					<th>Jenjang</th>
					<th>Status</th>
					<th>Verifikator Dokumen Persyaratan</th>
					<th>Keikusertaan Peserta</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($peserta as $key => $pesertas)
				<tr>
					<td>{{ $key++ + 1 }}</td>
					<td class='clickable-row' data-href='{{ $sites."/".$pesertas->id }}'>{{ $pesertas->nama }}</td>
					<td>{{ $pesertas->nip }}</td>
					<td>{{ $pesertas->nama_instansis }}</td>
					<td>{{ $pesertas->jabatan }}</td>
					<td>{{ ucwords($pesertas->jenjang) }}</td>
					<td>{{ $pesertas->status }}</td>
					<td>{{ $pesertas->bangprof == "" ? '-' : $pesertas->bangprof }}</td>
					<td>{{ Helper::getStatusActive($pesertas->status_peserta) }}</td>
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu">
								<li><a href="{{ url('detail-peserta-lkpp/'.$pesertas->id) }}">Lihat Detail</a></li>
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof')
								<li><a href="#" data-toggle="modal" data-target="#myModal{{ $pesertas->id }}">Assign Verifikator Dokumen Persyaratan</a></li>
								@endif
								@if (Auth::user()->role == 'superadmin')
								<li><a href="{{ url('verifikasi-peserta/'.$pesertas->id) }}">Verifikasi Dokumen Persyaratan</a></li>
								@endif
								@if (Auth::user()->role == 'verifikator')
									@if ($pesertas->assign == Auth::user()->id )
										<li><a href="{{ url('verifikasi-peserta/'.$pesertas->id) }}">Verifikasi Dokumen Persyaratan</a></li>	
									@endif
								@endif
								<li><a href="{{ url('riwayat-user-lkpp/'.$pesertas->id) }}">Riwayat</a></li>
								@if (Auth::user()->role == 'superadmin')
								<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $pesertas->id }}">Hapus</a></li>
								@endif
							</ul>
						</div>
					</td>
				</tr>
				<!-- Modal -->
				<div class="modal fade" id="modal-default{{ $pesertas->id }}" role="dialog">
					<div class="modal-dialog" style="width:30%">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Hapus Data Peserta</h4>
								</div>
								<div class="modal-body">
									<p>Apakah Anda yakin menghapus Data Peserta?</p>
								</div>
								<div class="modal-footer">
									<a href="{{ url('hapus-peserta-lkpp')."/".$pesertas->id }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
									<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>

					<div class="modal fade" id="myModal{{ $pesertas->id }}" role="dialog">
						<div class="modal-dialog">
							<form method="post" action="{{ url('assign-verifikator-peserta') }}">
								@csrf
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Assign Verifikator Dokumen Persyaratan</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-3">
												Nama
											</div>
											<div class="col-md-1">
												:
											</div>
											<div class="col-md-8">
												{{ $pesertas->nama }}
											</div>
										</div>
										<div class="row">
											<div class="col-md-3">
												Instansi
											</div>
											<div class="col-md-1">
												:
											</div>
											<div class="col-md-8">
												{{ $pesertas->nama_instansis }}
											</div>
										</div>
										<div class="row">
											<div class="col-md-3">
												Pangkat/Gol.
											</div>
											<div class="col-md-1">
												:
											</div>
											<div class="col-md-8">
												{{ $pesertas->jabatan }}
											</div>
										</div>
										<div class="row">
											<div class="col-md-3">
												Jenjang
											</div>
											<div class="col-md-1">
												:
											</div>
											<div class="col-md-8">
												{{ ucwords($pesertas->jenjang) }}
											</div>
										</div>
										<div class="row" style="margin-top:5px">
											<div class="col-md-3">
												Nama Verifikator
											</div>
											<div class="col-md-1">
												:
											</div>
											<div class="col-md-8">
												{!! Form::select('verifikator', $admin_bangprof , $pesertas->assign, ['placeholder' => 'Pilih Verifikator', 'class' => 'form-control', 'id' => 'select-single', 'required']); !!}
											</div>
											<input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
											<input type="hidden" name="nama_peserta1" value="{{ $pesertas->nama }}">
											<input type="hidden" name="modal" value="myModal{{ $pesertas->id }}">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-sm btn-default1">Assign</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	@stop

@section('js')
<script>
	jQuery(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});
</script>	
@endsection
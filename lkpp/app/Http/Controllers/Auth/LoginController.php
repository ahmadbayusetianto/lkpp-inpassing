<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\User;
use App\MasterBanner;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        $identity  = request()->get('email');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }

    protected function validateLogin(\Illuminate\Http\Request $request)
    {
        $this->validate($request, [$this->username() => 'required', 'password' => 'required', 'captcha' => 'required|captcha']);
    }

    public function showLoginForm()
    {
        $text = DB::table('text_runnings')->orderBy('urutan','asc')->get();

        $data_banner = MasterBanner::where('publish','1')->orderBy('urutan','asc')->get();

        return view('auth.login', compact('text','data_banner'));
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed_username')];

        // Load user from database
        $user = User::where($this->username(), $request->{$this->username()})->first();

		if ($user) {
			$errors = [$this->username() => trans('auth.failed_password')];
		} else {
			$errors = [$this->username() => trans('auth.failed_username')];
		}

		if ($request->expectsJson()) {
		   return response()->json($errors, 422);
        }

		return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function captchaValidate(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'captcha' => 'required|captcha'
        ]);
    }

    public function refreshCaptcha()
    {
        $data = "{{ captcha_img() }}";
        return response()->json(['captcha'=> $data]);
    }
}

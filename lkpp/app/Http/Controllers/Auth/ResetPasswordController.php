<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use View;
use Mail;
use App\User;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $msg = 'lupa_password';
        return View::make('auth.passwords.email', compact('msg'));
    }

    public function send(Request $request)
    {
        $email = $request->input('email');
        $user = User::where('email',$email)->count();
        if ($user == 0) {
            $msg = 'email_tidak_terdaftar';
        } else {
            $hash = Str::random(40);
            $update = User::where('email',$email)->first();
            $update->hash =  $hash;
            $update->save();
            $from = env('MAIL_USERNAME');
            $data = array('name' => $update->name, 'email' => $email, 'hash' => $hash, 'from' => $from);
            
			Mail::send('mail.lupa_password', $data, function($message) use ($data) {
				$message->to($data['email'], $data['name'])->subject('Lupa Password Admin PPK/Admin Instansi');
				$message->from($data['from'],'LKPP');
            });
            
			$msg = 'berhasil_kirim';
        }
        return View::make('auth.passwords.email', compact('msg'))->with('status','Berhasil Kirim Link Reset Password Ke Email !');
    }

    public function reset($hash)
    {
        $cek = User::where('hash',$hash)->count();
        if ($cek == 0) {
            $msg = 'salah';
            $id_user = '';
        } else {
            $msg = 'benar';
            $user = User::where('hash',$hash)->first();
            $id_user = $user->id;

            return View::make('auth.passwords.reset', compact('msg','id_user'));
        }
        
    }

    public function resetStore(Request $request,$hash)
    {
        $rules = array('password' => 'required|min:6',
					   'c_password' => 'required|same:password');

        $message = ['c_password.same' => 'password harus sama dengan password baru.'];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $cek = User::where('hash',$hash)->count();

        if ($cek == 0) {
            $msg = 'salah';
        } else {
            $password = $request->input('password');
            $data = User::where('hash',$hash)->first();
            $data->password = Hash::make($password);
            if($data->save()){
                $msg = 'berhasil_reset';
            } else {
                $msg = 'gagal_reset';
            }
        }

        return View::make('auth.passwords.email', compact('msg'));
    }
}
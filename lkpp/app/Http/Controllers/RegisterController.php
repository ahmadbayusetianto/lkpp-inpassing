<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Image_uploaded;
use App\User;
use App\Instansi;
use Validator;
use Carbon\Carbon;
use Mail;
use Image;
use Redirect;
use File;
use Illuminate\Support\Facades\Hash;
use View;

class RegisterController extends Controller
{
    public $path;

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = storage_path('data/images/sk_admin_ppk');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index()
    {
        $instansi = Instansi::pluck('nama','id');
        return View::make('register', compact('instansi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array('nip' => 'unique:users|required|digits_between:2,18',
					   'nama' => 'required',
					   'jabatan' => 'required',
					   'nama_instansi' => 'required',
					   'nama_satuan' => 'required',
					   'email' => 'unique:users|required',
					   'no_telp' => 'required|digits_between:2,12',
					   'foto' => 'required|mimes:jpg,jpeg|min:100|max:2000',
					   'image' => 'required|mimes:pdf,jpg,jpeg|min:100|max:2000');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('register')
                ->withErrors($validator)
                ->withInput();
        }
		
        //JIKA FOLDERNYA BELUM ADA
        if (!File::isDirectory($this->path)) {
            //MAKA FOLDER TERSEBUT AKAN DIBUAT
            File::makeDirectory($this->path, 0777, true);
        } 
		
        //MENGAMBIL FILE IMAGE DARI FORM
        $file = $request->file('image');
        
		//MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
        $uniqueFileName = Carbon::now()->timestamp ."_". $request->input('nip') .".". $file->getClientOriginalExtension();
        
		//UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
        $destinationPath = 'storage/data/sk_admin_ppk';
        $file->move($destinationPath,$uniqueFileName);
        
		//MENGAMBIL FILE IMAGE DARI FORM
        $foto = $request->file('foto');
        
		//MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
        $fotoName = Carbon::now()->timestamp ."_foto_profile.". $foto->getClientOriginalExtension();
        
		//UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
        $fotoPath = 'storage/data/foto_profile';
        $foto->move($fotoPath,$fotoName);
        $password = Str::random();
        $nip = $request->input('nip');
        $hash = Str::random(40);
        $lower = strtolower($request->input('nama'));
        $potong_nip = substr($nip, -3);
        $username = preg_replace('/\s+/', '', $lower).$potong_nip;
        
		$data = new User;
        $data->name = $request->input('nama');
        $data->email = $request->input('email');
        $data->username = $username;
        $data->password = Hash::make($password);
        $data->hash = $hash;
        $data->nip = $request->input('nip');
        $data->jabatan = $request->input('jabatan');
        $data->nama_instansi = $request->input('nama_instansi');
        $data->nama_satuan = $request->input('nama_satuan');
        $data->no_telp = $request->input('no_telp');
        $data->sk_admin_ppk = $uniqueFileName;
        $data->foto_profile = $fotoName;
        $data->admin_type = "kpp";
        if($data->save()){
            //send email
            $nama_user = $request->input('nama');
            $nip = $request->input('nip');
            $email = $request->input('email');
            $from = env('MAIL_USERNAME');
            $data = array('name' => $nama_user, 'nip' => $nip,  'email' => $email, 'password' => $password, 'username' => $username, 'hash' => $hash, 'from' => $from);
            Mail::send('mail.register', $data, function($message) use ($data) {
				$message->to($data['email'], $data['name'])->subject('Registrasi Akun Admin Instansi/Admin PPK');
				$message->from($data['from'],'LKPP');
            });
            //end email
            return Redirect::to('register-info')->with('msg','berhasil kirim email');
        } else {
            return Redirect::to('register-info')->with('msg','gagal kirim email');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $msg = session('msg');
        return View::make('register_info', compact('msg'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function verifikasiEmail($hash)
    {
        $data = User::where('hash',$hash)->first();
        
		if($data != ""){
            $data->email_verified_at = Carbon::now()->timestamp;
            $data->save();            
			return Redirect::to('inpassing')->with('msg','berhasil_verifikasi');
        } else {
            return Redirect::to('inpassing')->with('msg','gagal_verifikasi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo Hash::make($id);
    }

    public function checkEmail(Request $request)
    {
        $email = $request->input('email');

        $cek = User::where('email',$email)->count();
        if($cek == 0){
            return response()->json(array("exists" => true));
        } else {
            return response()->json(array("exists" => false));
        }
    }

    public function checkNip(Request $request)
    {
        $nip = $request->input('nip');

        $cek = User::where('nip',$nip)->count();
        if($cek == 0) {
            return response()->json(array("exists" => true));
        } else {
            return response()->json(array("exists" => false));
        }
    }
}

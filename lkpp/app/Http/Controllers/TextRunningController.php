<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Validator;
use App\TextRunning;
use App\Peserta;
use Illuminate\Support\Facades\Input;
use Redirect;
use DB;

class TextRunningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
	public function index()
    {
        $data = DB::table('text_runnings')->orderBy('urutan','asc')->get();
        return View::make('running_text', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
	public function create()
    {
        return View::make('create_text_running');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
	public function store(Request $request)
    {
        $rules = array('text' => 'required');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('tambah-text-running')
					->withErrors($validator)
					->withInput();
        }

        $data = new TextRunning();
        $last = DB::table('text_runnings')->latest('urutan')->first();
        if ($last) {
            $data->urutan = $last->urutan + 1;
        }else{
            $data->urutan = 1;
        }
        $data->text = $request->input('text');
        if($data->save()){
			return Redirect::to('text-running')->with('msg','berhasil');  
        } else {
            return Redirect::to('text-running')->with('msg','gagal');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = TextRunning::find($id);
        return View::make('edit_text_running', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array('text' => 'required');
		$validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('tambah-text-running')
					->withErrors($validator)
					->withInput();
        }

        $data = TextRunning::find($id);
        $urutan_old = $data->urutan;
        $cek_urut = TextRunning::where('urutan',$request->input('urutan'))->first();

        if ($cek_urut != "") {
            $cek_urut->urutan = $urutan_old;
            $cek_urut->save();
        }
        $data->urutan = $request->input('urutan'); 
        $data->text = $request->input('text');
        if($data->save()){
			return Redirect::to('text-running')->with('msg','berhasil_update');  
        } else {
            return Redirect::to('text-running')->with('msg','gagal_update');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TextRunning::find($id);
        if($data->delete()){
			return Redirect::to('text-running')->with('msg','berhasil_hapus');  
        } else {
            return Redirect::to('text-running')->with('msg','gagal_hapus');
        }
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use View;
use DB;
use App\MasterBanner;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }



    public function dashboard()
    {   
        $data_banner = MasterBanner::where('publish','1')->orderBy('urutan','asc')->get();
        return View::make('dashboard_lkpp',compact('data_banner'));
    }

    public function pindah()
    {
        return View::make('pindah_jadwal');
    }

    public function login()
    {
        return view('login');
    }

    public function FileView($path,$file)
    {
		try{

            ob_end_clean();
            ob_start();

            $pathToFile1 =  'storage/data'."/".$path."/".$file;
			$pathToFile = storage_path('data'."/".$path."/".$file);

          //   $data_pisah = explode("." , $file);

          //   if ($data_pisah[1] == 'pdf' || $data_pisah[1] == 'PDF') {
          //       return response()->file($pathToFile1);
          //   }else{
          //           $headers = array(
          //   'Content-Type:image/jpg',
          // );

          //   return response()->file($pathToFile1,$headers);
          //   }


    if (!File::exists($pathToFile1)) {

        abort(404);

    }



    $file = File::get($pathToFile1);

    $type = File::mimeType($pathToFile1);



    $response = Response::make($file, 200);

    $response->header("Content-Type", $type);



    return $response;


            
		}
		catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}
    }

    
}

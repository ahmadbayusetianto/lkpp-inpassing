<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exports\DataRiwayatUjianExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use View;
use App\JudulInput;

class RiwayatUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id)
    {
        $data = DB::table('riwayat_users')
				->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
				->join('users', 'riwayat_users.id_admin','=','users.id','left')
				->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
				// ->join('riwayat_hasil_verifs', 'riwayat_users.id','=','riwayat_hasil_verifs.id_riwayat','left')
                ->select('riwayat_users.*','pesertas.nama as nama_peserta','users.name as nama_admin','admin.name as admin_lkpp','riwayat_users.keterangan as keterangan','riwayat_users.id as id_riwayat')
				->where('pesertas.id',$id)
				->orderby('riwayat_users.id','desc')
				->groupby('riwayat_users.id')
                ->get();


		$namaInput =  DB::table('detail_inputs')
                    ->join('judul_inputs', 'detail_inputs.id_judul_input','=','judul_inputs.id')
                    ->join('detail_file_inputs', 'detail_inputs.id','=','detail_file_inputs.id_detail_input')
                    ->join('riwayat_hasil_verifs','riwayat_hasil_verifs.id_file_input','=','detail_file_inputs.id','left')
                    ->select('detail_file_inputs.title_1 as nama_file_input','detail_inputs.nama as nama_input',
                    'detail_file_inputs.id as file_input','detail_inputs.id as detail_input',
                    'detail_inputs.id_judul_input as JudulInput','judul_inputs.id as poins','detail_file_inputs.id_detail_input as detail_input2','judul_inputs.nama as judul_input','riwayat_hasil_verifs.keterangan','riwayat_hasil_verifs.id_riwayat')
                    ->get();


		return View::make('riwayat_user_ppk', compact('data','namaInput','riwayat_verif'));
    }

    public function indexlkpp($id){
        $perihal = "hasil_verif_regular";
        $data = DB::table('riwayat_users')
				->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
				->join('users', 'riwayat_users.id_admin','=','users.id','left')
				->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
				// ->join('riwayat_hasil_verifs', 'riwayat_users.id','=','riwayat_hasil_verifs.id_riwayat','left')
                ->select('riwayat_users.*','pesertas.nama as nama_peserta','users.name as nama_admin','admin.name as admin_lkpp','riwayat_users.keterangan as keterangan','riwayat_users.id as id_riwayat')
				->where('pesertas.id',$id)
				->orderby('riwayat_users.id','desc')
				->groupby('riwayat_users.id')
                ->get();

        $riwayat = [];
        foreach ($data as $key => $value) {
            $riwayat[] = $value->id;
         }

        // $riwayat_verif = DB::table('riwayat_hasil_verifs')->select('id_file_input','id_riwayat','keterangan')->whereIn('id_riwayat',$riwayat)->where('id_user',$id)->get();

		$namaInput =  DB::table('detail_inputs')
					->join('judul_inputs', 'detail_inputs.id_judul_input','=','judul_inputs.id')
					->join('detail_file_inputs', 'detail_inputs.id','=','detail_file_inputs.id_detail_input')
                    ->join('riwayat_hasil_verifs','riwayat_hasil_verifs.id_file_input','=','detail_file_inputs.id','left')
                    ->select('detail_file_inputs.title_1 as nama_file_input','detail_inputs.nama as nama_input',
                    'detail_file_inputs.id as file_input','detail_inputs.id as detail_input',
                    'detail_inputs.id_judul_input as JudulInput','judul_inputs.id as poins','detail_file_inputs.id_detail_input as detail_input2','judul_inputs.nama as judul_input','riwayat_hasil_verifs.keterangan','riwayat_hasil_verifs.id_riwayat')
					->get();


        // $keterangan_riwayat = [];
        // foreach ($namaInput as $key => $inputs) {
        //     foreach ($riwayat_verif as $verifs => $value) {
        //         if ($inputs->file_input == $value->id_file_input) {
        //             $keterangan_riwayat[$value->id_riwayat] = $value->keterangan;
        //         }
                
        //     }
        // }




		return View::make('riwayat_user_lkpp', compact('data','namaInput'));
    }

	public function ujianPeserta(request $request){
        $nip = $request->input('nip');

        if ($nip == "") {
            $data = DB::table('riwayat_users')
					->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
					->join('peserta_jadwals', 'riwayat_users.id_user','=','peserta_jadwals.id_peserta','left')
					->join('dokumens', 'riwayat_users.id_user','=','dokumens.id_peserta','left')
					->join('instansis', 'pesertas.nama_instansi','=','instansis.id','left')
					->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
					->join('users', 'riwayat_users.id_admin','=','users.id','left')
					->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
					->leftJoin('surat_usulans', function($leftJoin){
						$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'surat_usulans.id_jadwal')
								->on('peserta_jadwals.id_peserta', '=', 'surat_usulans.id_peserta')
								->on('surat_usulans.jenis_ujian', '=', DB::Raw("'regular'"));
					})
                    ->select('riwayat_users.*',
                    'pesertas.nama as nama_peserta','pesertas.nip as nip','users.name as nama_admin',
                    'admin.name as admin_lkpp','jadwals.tanggal_ujian as tgl_ujian',
                    'peserta_jadwals.status_ujian as hasil_ujian','pesertas.jenjang as jenjang',
                    'peserta_jadwals.metode_ujian as metode','peserta_jadwals.no_ujian as no_ujian',
                    'instansis.nama as nama_instansi','dokumens.*',
                    'peserta_jadwals.publish as publish','surat_usulans.*',
                    'surat_usulans.file as file','peserta_jadwals.poin_lulus',
                    'peserta_jadwals.jenjang as jenjangs')
					->where('peserta_jadwals.publish','publish')
					->groupby('peserta_jadwals.id')
					->orderby('peserta_jadwals.id','desc')
					->orderby('peserta_jadwals.updated_at','desc')
					->get();
        } else {
             $data = DB::table('riwayat_users')
					->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
					->join('peserta_jadwals', 'riwayat_users.id_user','=','peserta_jadwals.id_peserta','left')
					->join('dokumens', 'riwayat_users.id_user','=','dokumens.id_peserta','left')
					->join('instansis', 'pesertas.nama_instansi','=','instansis.id','left')
					->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
					->join('users', 'riwayat_users.id_admin','=','users.id','left')
					->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
					->select('riwayat_users.*','pesertas.nama as nama_peserta','pesertas.nip as nip','users.name as nama_admin','admin.name as admin_lkpp','jadwals.tanggal_ujian as tgl_ujian','peserta_jadwals.status_ujian as hasil_ujian','pesertas.jenjang as jenjang','peserta_jadwals.metode_ujian as metode','peserta_jadwals.no_ujian as no_ujian','instansis.nama as nama_instansi','dokumens.*','peserta_jadwals.publish as publish')
					->where('peserta_jadwals.publish','publish')
					->where('pesertas.nip',$nip)
					->groupby('peserta_jadwals.id')
					->orderby('riwayat_users.id','desc')
					->get();
        }

        if ($nip == "") {
            $data_int = DB::table('riwayat_users')
						->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
						->join('peserta_instansis', 'riwayat_users.id_user','=','peserta_instansis.id_peserta','left')
						->join('peserta_jadwals', 'riwayat_users.id_user','=','peserta_jadwals.id_peserta','left')
						->join('dokumens', 'riwayat_users.id_user','=','dokumens.id_peserta','left')
						->join('instansis', 'pesertas.nama_instansi','=','instansis.id','left')
						->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
						->join('users', 'riwayat_users.id_admin','=','users.id','left')
						->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
						->leftJoin('surat_usulans', function($leftJoin){
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'surat_usulans.id_jadwal')
									->on('peserta_instansis.id_peserta', '=', 'surat_usulans.id_peserta')
									->on('surat_usulans.jenis_ujian', '=', DB::Raw("'instansi'"));
						})
						->select('riwayat_users.*','pesertas.nama as nama_peserta','pesertas.nip as nip','users.name as nama_admin','admin.name as admin_lkpp','jadwal_instansis.tanggal_ujian as tgl_ujian','peserta_instansis.status_ujian as hasil_ujian','pesertas.jenjang as jenjang','peserta_instansis.metode_ujian as metode','peserta_instansis.no_ujian as no_ujian','instansis.nama as nama_instansi','dokumens.*','peserta_instansis.publish as publish','surat_usulans.*','surat_usulans.file as file','peserta_jadwals.poin_lulus','peserta_instansis.jenjang as jenjangs')
						->where('peserta_instansis.publish','publish')
						->groupby('peserta_instansis.id')
						->orderby('peserta_instansis.id','desc')
						->orderby('peserta_instansis.updated_at','desc')
						->get();
		} else {
			$data_int = DB::table('riwayat_users')
						->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
						->join('peserta_instansis', 'riwayat_users.id_user','=','peserta_instansis.id_peserta','left')
						->join('dokumens', 'riwayat_users.id_user','=','dokumens.id_peserta','left')
						->join('instansis', 'pesertas.nama_instansi','=','instansis.id','left')
						->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
						->join('users', 'riwayat_users.id_admin','=','users.id','left')
						->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
						->select('riwayat_users.*','pesertas.nama as nama_peserta','pesertas.nip as nip','users.name as nama_admin','admin.name as admin_lkpp','jadwal_instansis.tanggal_ujian as tgl_ujian','peserta_instansis.status_ujian as hasil_ujian','pesertas.jenjang as jenjang','peserta_instansis.metode_ujian as metode','peserta_instansis.no_ujian as no_ujian','instansis.nama as nama_instansi','dokumens.*','peserta_instansis.publish as publish','peserta_instansis.jenjang as jenjangs')
						->where('peserta_instansis.publish','publish')
						->where('pesertas.nip',$nip)
						->groupby('peserta_instansis.id')
						->orderby('riwayat_users.id','desc')
						->get();
		}

		$merged = $data->merge($data_int);
		$datanih = $merged->all();

		$poin = JudulInput::get();
		$nama_poin = array();

		foreach ($poin as $key => $value) {
			$nama_poin[$value->id] = $value->nama;
		}

		return View::make('riwayat_ujian_peserta', compact('data','nip','data_int','datanih','nama_poin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public function printExcell($id)
    // {
    //     $jadwal = DB::table('jadwals')->where('id',$id)->first();
    //     if ($jadwal->metode == 'tes_tulis') {
    //         $tanggal_jadwal = Helper::tanggal_indo($jadwal->tanggal_tes);
    //     } else {
    //         $tanggal_jadwal = Helper::tanggal_indo($jadwal->tanggal_verifikasi);
    //     }

    //     $nama_excel = 'list peserta jadwal reguler '.$tanggal_jadwal.'.xlsx';
    //     return Excel::download(new PesertaJadwalExport($id), $nama_excel);
    // }


     public function printExcell()
    {
        $nama_excel = 'Data Riwayat Ujian Peserta.xlsx';
        return Excel::download(new DataRiwayatUjianExport(), $nama_excel);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use View;
use App\MenuProsedur;
use App\BerkasProsedur;
use Redirect;
use Validator;
use DB;
use Carbon\Carbon;

class ProsedurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('menu_prosedurs')->orderBy('urutan','ASC')->get();
        $data_berkas = DB::table('berkas_prosedurs')->orderBy('urutan','ASC')->get();
        return View::make('data_prosedur',compact('data','data_berkas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('add_berkas_prosedur');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'judul_berkas'    => 'required',
            'berkas' => 'required|mimes:pdf,jpg,jpeg,doc,docx|min:100|max:10000',
        );

        $message = [
            'berkas.min' => array(
                'file' => 'Berkas min. 100kb maks. 10MB'
            ),
            'berkas.max' => array(
                'file' => 'Berkas min. 100kb maks. 10MB'
            )
        ];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $berkas = $request->file('berkas');
        $berkas_name = Carbon::now()->timestamp ."_berkas".".".$berkas->getClientOriginalExtension();
        $berkasPath = 'storage/data/berkas';
        $berkas->move($berkasPath,$berkas_name);

        $data = new BerkasProsedur();
        $last = DB::table('berkas_prosedurs')->latest('urutan')->first();
        $data->judul_berkas = $request->input('judul_berkas');
        if ($last) {
            $data->urutan = $last->urutan + 1;
        }else{
            $data->urutan = 1;
        }
        $data->berkas = $berkas_name;
        if($data->save()){
            return Redirect::to('data-prosedur')->with('msg','berhasil');
        }else{
            return Redirect::to('data-prosedur')->with('msg','gagal');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MenuProsedur::find($id);
        return View::make('edit_prosedur', compact('data'));
    }

    public function editBerkas($id)
    {
        $data = BerkasProsedur::find($id);
        return View::make('edit_berkas_prosedur', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'document'    => 'required',
        );

        $message = [
            'berkas.min' => array(
                'file' => 'Berkas min. 100kb maks. 10MB'
            ),
            'berkas.max' => array(
                'file' => 'Berkas min. 100kb maks. 10MB'
            )
        ];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = MenuProsedur::find($id);
        $data->text = $request->input('document');
        $data->video_1 = $request->input('documentone');
        $data->video_2 = $request->input('documenttwo');
        if($data->save()){
            return Redirect::to('data-prosedur')->with('msg','berhasil');
        }else{
            return Redirect::to('data-prosedur')->with('msg','gagal');
        }
    }

    public function updateBerkas(Request $request,$id)
    {
        if ($request->input('berkas') != "") {
            $rules = array(
                'judul_berkas'    => 'required',
                'berkas' => 'mimes:pdf,jpg,jpeg,doc,docx|min:100|max:10000',
            );
        }else{
            $rules = array(
                'judul_berkas'    => 'required',
            );
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('berkas')) {
        $berkas = $request->file('berkas');
        $berkas_name = Carbon::now()->timestamp ."_berkas".".". $berkas->getClientOriginalExtension();
        $berkasPath = 'storage/data/berkas';
        $berkas->move($berkasPath,$berkas_name);
        $old_berkas = $request->input('old_berkas');
        if($old_berkas != ""){
            unlink('storage/data/berkas/'.$old_berkas);
        }
        }else{
            $berkas_name = $request->input('old_berkas');
        }

        $data = BerkasProsedur::find($id);
        $urutan_old = $data->urutan;
        $cek_urut = BerkasProsedur::where('urutan',$request->input('urutan'))->first();

        if ($cek_urut != "") {
            $cek_urut->urutan = $urutan_old;
            $cek_urut->save();
        }
        $data->urutan = $request->input('urutan');    

        $data->judul_berkas = $request->input('judul_berkas');
        $data->berkas = $berkas_name;
        if($data->save()){
            return Redirect::to('data-prosedur')->with('msg','berhasil_update');
        }else{
            return Redirect::to('data-prosedur')->with('msg','gagal_update');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BerkasProsedur::find($id);
        $berkas = $data->berkas;
        unlink('storage/data/berkas/'.$berkas);
        if ($data->delete()){
            return Redirect::to('data-prosedur')->with('msg','berhasil_hapus');
        }else{
            return Redirect::to('data-prosedur')->with('msg','gagal_hapus');
        }
    }
}

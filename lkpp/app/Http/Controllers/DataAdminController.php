<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Exports\DataAdminExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Validator;
use Redirect;
use View;
use Mail;
use App\User;
use Carbon\Carbon;
use DB;

class DataAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('users')
				->join('instansis', 'users.nama_instansi','=','instansis.id','left')
				->select('users.*','instansis.nama as instansis')
				->where('admin_type','kpp')
				->where('deleted_at',null)
				->orderBy('id','DESC')
				->get();
        return View::make('data_admin', compact('data'));
    }

    public function indexLkpp()
    {
        if(Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
        {
            return Redirect::back();
        }

        $data = DB::table('users')
				->where('admin_type','lkpp')
				->where('role','!=','asesor')
				->where('role','!=','verifikator')
				->where('deleted_at',null)
				->orderBy('id','DESC')->get();

		return View::make('data_adminlkpp', compact('data'));
    }

    public function gantiPassword()
    {
        return View::make('ganti_password');
    }

    public function updatePassword(Request $request)
    {
        $rules = array('password_old' => 'required|min:6', 'password_new' => 'required|min:6', 'c_password_new' => 'required|same:password_new');

        $messages = [
            'password_old.required' => 'Password harus di isi.',
            'password_old.min' => ['string' => 'password minimal 6 karakter.'],
            'password_new.required' => 'Password Baru harus di isi.',
            'password_new.min' => ['string' => 'password minimal 6 karakter.'],
            'c_password_new.required' => 'Ulangi Password baru harus di isi.',
            'c_password_new.same' => 'password harus sama dengan password baru.'
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails()) {
            return Redirect::to('ganti-password')
					->withErrors($validator)
					->withInput();
        }

        $password_old = Hash::make($request->input('password_old'));
        $password_new = Hash::make($request->input('password_new'));
        $password_ori = Auth::user()->password;
        $email = Auth::user()->email;
        $id = Auth::user()->id;

		if(Auth::attempt(['email' => $email, 'password' => request('password_old')])){
            $user = User::find($id);
            $user->password = $password_new;
            if ($user->save()){
                return Redirect::to('ganti-password')->with('msg','berhasil');
            } else {
                return Redirect::to('ganti-password')->with('msg','gagal');
            }
        } else {
            return Redirect::to('ganti-password')->with('msg','salah');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createLkpp()
    {
        $action = 'add';
        $unit_kerja = DB::table('unit_kerjas')->pluck('name','id');
        return View::make('add_admin_lkpp', compact('action','unit_kerja'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLkpp(Request $request)
    {
        $rules = array('nama' => 'required',
					   'email' => 'required|unique:users',
					   'password' => 'required|min:6',
					   'ulang_password' => 'required|same:password',
					   'role_admin' => 'required');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('tambah-admin-lkpp')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new User();
        $data->name = $request->input('nama');
        $data->email = $request->input('email');
        $data->password = Hash::make($request->input('password'));
        $data->role = $request->input('role_admin');
        $data->admin_type = 'lkpp';
        $data->email_verified_at = Carbon::now();
        $data->unit_kerja = $request->input('unit_kerja');
        $data->nip = '-';
        $data->jabatan = '-';
        $data->nama_instansi = '-';
        $data->no_telp = '-';
        $data->sk_admin_ppk = '-';
        $data->status_admin = $request->input('status_admin');
        if($data->save())
        {
            return Redirect::to('data-admin-lkpp')->with('msg','berhasil');
        } else {
            return Redirect::to('data-admin-lkpp')->with('msg','berhasil');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('users')
				->join('instansis', 'users.nama_instansi','=','instansis.id','left')
				->select('users.*','instansis.nama as instansis')
				->where('users.id',$id)
				->first();

		return View::make('detail_admin_ppk', compact('data'));
    }

    public function updateStatus($id)
    {
        if(Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor'){
            return Redirect::back();
        }

        $data = User::find($id);
        if($data->email_verified_at == ""){
            $data->email_verified_at = Carbon::now();
        } else {
            $data->email_verified_at = null;
        }

        if($data->save()){
            return Redirect::back()->with('msg','berhasil_status');
        } else {
            return Redirect::back()->with('msg','gagal_status');
        }
    }

    public function updateStatusNot($id)
    {
        if(Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor'){
            return Redirect::back();
        }

		$data = User::find($id);
        if($data->email_verified_at !=""){
            $data->email_verified_at = null;
        } else {
            $data->email_verified_at = null;
        }

        if($data->save()){
            return Redirect::back()->with('msg','berhasil_status');
        } else {
            return Redirect::back()->with('msg','gagal_status');
        }
    }

	public function updateUlangAktivasi($id){
        if(Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor'){
            return Redirect::back();
        }

        $password = Str::random();
        $hash = Str::random(40);
        $data = User::find($id);
        $data->password = Hash::make($password);
        $data->hash = $hash;
        if ($data->Save()) {
			$from = env('MAIL_USERNAME');
			$data = array('name' => $data->name, 'nip' => $data->nip,  'email' => $data->email, 'password' => $password, 'username' => $data->username, 'hash' => $hash, 'from' => $from);
			Mail::send('mail.register', $data, function($message) use ($data) {
				$message->to($data['email'], $data['name'])->subject('Registrasi Akun Admin Instansi/Admin PPK');
				$message->from($data['from'],'LKPP');
			});
			//end email

			return Redirect::back()->with('msg','berhasil_aktif_ulang');
        } else {
              return Redirect::back()->with('msg','gagal_aktif_ulang');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function editLkpp($id)
    {
        $action = 'edit';
        $data = User::find($id);
        $unit_kerja = DB::table('unit_kerjas')->pluck('name','id');
        return View::make('add_admin_lkpp', compact('action','data','unit_kerja'));
    }

    public function editPassword($id)
    {
        return View::make('ganti_password_lkpp', compact('id'));
    }

    public function updatePasswordLkpp(Request $request,$id)
    {
        $rules = array('password' => 'required|min:6', 'ulang_password' => 'required|same:password');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $password = $request->input('password');
        $data = User::find($id);
        $data->password = Hash::make($request->input('password'));
        if ($data->save()){
            $from = env('MAIL_USERNAME');
            $data = array('name' => $data->name, 'email' => $data->email, 'from' => $from, 'password' => $password);
            Mail::send('mail.ganti_password_lkpp', $data, function($message) use ($data) {
				$message->to($data['email'], $data['name'])->subject('Ganti Password Oleh Admin LKPP');
				$message->from($data['from'],'LKPP');
            });
            return Redirect::to('data-admin')->with('msg','berhasil_ganti');
		} else {
            return Redirect::to('data-admin')->with('msg','gagal_ganti');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

	public function updateLkpp(Request $request, $id)
    {
        if($request->input('password') != ""){
			$rules = array('nama'  => 'required',
						   'email' => 'required',
						   'password' => 'min:6',
						   'ulang_password' => 'same:password',
						   'role_admin' => 'required');
		} else {
            $rules = array(
                'nama'  => 'required',
                'email' => 'required',
                'role_admin' => 'required');
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('edit-admin-lkpp/'.$id)
					->withErrors($validator)
					->withInput();
        }

        $data = User::find($id);
        $data->name = $request->input('nama');
        $data->email = $request->input('email');

		if($request->input('password') != ""){
			$data->password = Hash::make($request->input('password'));
        }

        $data->role = $request->input('role_admin');
        $data->unit_kerja = $request->input('unit_kerja');
        $data->status_admin = $request->input('status_admin');
        if($data->save())
        {
            return Redirect::to('data-admin-lkpp')->with('msg','berhasil');
        } else {
            return Redirect::to('data-admin-lkpp')->with('msg','gagal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroyLkpp($id)
    {
        $data = User::find($id);
        if($data->delete())
        {
            return Redirect::to('data-admin-lkpp')->with('msg','berhasil_hapus');
        } else {
            return Redirect::to('data-admin-lkpp')->with('msg','gagal_hapus');
        }
    }

    public function destroy($id)
    {
        $data = User::find($id);
        if($data->delete())
        {
            return Redirect::to('data-admin')->with('msg','berhasil_hapus');
        } else {
            return Redirect::to('data-admin')->with('msg','gagal_hapus');
        }
    }

    public function printExcell()
    {
        $nama_excel = 'Data Admin PPK.xlsx';
        return Excel::download(new DataAdminExport(), $nama_excel);
    }
}

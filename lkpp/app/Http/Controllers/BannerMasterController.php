<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use App\Asesor;
use App\User;
use App\Peserta;
use App\PesertaJadwal;
use View;
use Redirect;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use App\MasterBanner;

class BannerMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = DB::table('t_banner_master')
				->select('t_banner_master.*')
				->orderBy('urutan','ASC')
				->get();
        
		return View::make('data_banner', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function create()
    {
        if(Auth::user()->role == 'bangprof'){
			return Redirect::back();
		}

        $action = "add";
        
		
        return View::make('create_banner', compact('action'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
	public function store(Request $request)
    {
        $rules = array(        
                        'file' => 'required',
                        'publish' => 'required',
        );
            
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = new MasterBanner();
        $last = DB::table('t_banner_master')->latest('urutan')->first();

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $file_name = Carbon::now()->timestamp ."_banner".".". $file->getClientOriginalExtension();
            $filePath = 'storage/data/banner';
            $file->move($filePath,$file_name);
        } else {
            $file_name = null;
        }
        $data->file = $file_name;
        if ($last) {
            $data->urutan = $last->urutan + 1;
        }else{
            $data->urutan = 1;
        }
        
        $data->publish = $request->input('publish');
        
        if($data->save())
        {
            return Redirect::to('data-banner')->with('msg','berhasil');
        } else {
            return Redirect::to('data-banner')->with('msg','berhasil');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
	public function show($id)
    {
        $action = 'detail';
        $data = DB::table('users')
				->join('unit_kerjas','users.unit_kerja','=','unit_kerjas.id','left')
				->select('users.*','unit_kerjas.name as unit_kerjas')
				->where('users.id',$id)
				->first();
    
		return View::make('create_asesor', compact('action','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        if(Auth::user()->role == 'bangprof'){
			return Redirect::back();
		}

        $action = 'edit';
		
        $data = MasterBanner::find($id);
		return View::make('create_banner', compact('action','data'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $rules = array(
						   'publish' => 'required',
						   'urutan' => 'required',
						   );
            
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
				->withErrors($validator)
				->withInput();
        }

        $data = MasterBanner::find($id);
        $urutan_old = $data->urutan;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $file_name = Carbon::now()->timestamp ."_banner".".". $file->getClientOriginalExtension();
            $filePath = 'storage/data/banner';
            $file->move($filePath,$file_name);
            $old_file = $request->input('old_file');
            if($old_file != ""){
                unlink('storage/data/banner/'.$old_file);
            }
        } else {
            $old_file = $request->input('old_file');
            $file_name = $old_file;
        }
        $data->file = $file_name;
        $cek_urut = MasterBanner::where('urutan',$request->input('urutan'))->first();

        if ($cek_urut != "") {
            $cek_urut->urutan = $urutan_old;
            $cek_urut->save();
        }
        $data->urutan = $request->input('urutan');    
    
        
        $data->publish = $request->input('publish');
        if($data->save())
        {
            return Redirect::to('data-banner')->with('msg','berhasil_update');
        } else {
            return Redirect::to('data-banner')->with('msg','berhasil_update');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MasterBanner::find($id);
        if($data->delete())
        {
            return Redirect::to('data-banner')->with('msg','berhasil_hapus');
        } else {
            return Redirect::to('data-banner')->with('msg','gagal_hapus');
        }
    }
}
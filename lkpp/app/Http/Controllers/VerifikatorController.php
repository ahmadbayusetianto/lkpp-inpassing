<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;
use Redirect;
use Validator;
use View;
use DB;

class VerifikatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor')
        {
            return Redirect::back();
        }
        $data = DB::table('users')
				->leftjoin('pesertas','users.id','=','pesertas.assign')
				// ->selectRaw('count(pesertas.id) as jumlah_assign')
                ->select('users.*','pesertas.nama as namas','pesertas.status_inpassing',DB::raw('count(pesertas.id) as jumlah_assign'))
				->where('users.role','verifikator')

				->where('users.deleted_at',null)
				->orderBy('users.id','DESC')
				->groupBy('users.id')
				->get();

        $data2 = DB::table('users')
                ->leftjoin('pesertas','users.id','=','pesertas.assign')
                // ->selectRaw('count(pesertas.id) as jumlah_assign')
                ->select('users.id',DB::raw('count(pesertas.id) as jumlah_tidak_lengkap'))
                ->where('users.role','verifikator')
                ->where('pesertas.status_inpassing',2)
                ->where('users.deleted_at',null)
                ->orderBy('users.id','DESC')
                ->groupBy('users.id')
                ->get();

        $tidak_lengkap = array();

        foreach($data2 as $datas){
            $tidak_lengkap[$datas->id] = $datas->jumlah_tidak_lengkap;
        }

        // dd($tidak_lengkap);

        $data3 = DB::table('users')
                ->leftjoin('pesertas','users.id','=','pesertas.assign')

                // ->selectRaw('count(pesertas.id) as jumlah_assign')
                ->select('users.id',DB::raw('count(pesertas.id) as jumlah_menunggu'))
                ->where('users.role','verifikator')
                ->where('pesertas.status_inpassing',1)
                ->where('users.deleted_at',null)
                ->orderBy('users.id','DESC')
                ->groupBy('users.id')
                ->get();

        $menunggu = array();

        foreach($data3 as $datas){
            $menunggu[$datas->id] = $datas->jumlah_menunggu;
        }






         return View::make('data_verifikator', compact('data','data2','data3','tidak_lengkap','menunggu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function create(){
        if(Auth::user()->role == 'dsp')
        {
            return Redirect::back();
        }
        $action = 'add';
        return View::make('add_verifikator', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

	public function store(Request $request){
        $rules = array(
            'nama'  => 'required',
            'email' => 'required|unique:users',
            'password'    => 'required|min:6',
            'c_password'    => 'required|same:password',
            'status_admin'    => 'required');

        $message = ['c_password.same' => 'Password harus sama.'];
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::to('tambah-verifikator')
					->withErrors($validator)
					->withInput();
        }

        $data = new User();
        $data->name = $request->input('nama');
        $data->email = $request->input('email');
        $data->password = Hash::make($request->input('password'));
        $data->role = 'verifikator';
        $data->admin_type = 'lkpp';
        $data->email_verified_at = Carbon::now();
        $data->nip = '-';
        $data->jabatan = '-';
        $data->nama_instansi = '-';
        $data->no_telp = '-';
        $data->sk_admin_ppk = '-';
        $data->status_admin = $request->input('status_admin');
        if($data->save())
        {
            return Redirect::to('data-verifikator')->with('msg','berhasil');
        } else {
            return Redirect::to('data-verifikator')->with('msg','berhasil');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $action = 'detail';
        $data = DB::table('users')
				->where('users.id',$id)
				->first();
        return View::make('add_verifikator', compact('action','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id){
        if(Auth::user()->role == 'dsp')
        {
            return Redirect::back();
        }

        $action = 'edit';
        $data = User::find($id);
        return View::make('add_verifikator', compact('action','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $password = $request->input('password');
        if ($password != "") {
            $rules = array('nama' => 'required',
						   'email' => 'required',
						   'password' => 'required|min:6',
						   'c_password' => 'required|same:password',
						   'status_admin' => 'required');
			$password_input = Hash::make($request->input('password'));
		} else {
            $rules = array('nama' => 'required',
						   'email' => 'required',
						   'status_admin' => 'required');
            $password_input = $request->input('old_password');
        }

        $message = ['c_password.same' => 'Password harus sama.'];
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
					->withErrors($validator)
					->withInput();
        }

        $data = User::find($id);
        $data->name = $request->input('nama');
        $data->email = $request->input('email');
        $data->password = $password_input;
        $data->role = 'verifikator';
        $data->admin_type = 'lkpp';
        $data->status_admin = $request->input('status_admin');
        if($data->save())
        {
            return Redirect::to('data-verifikator')->with('msg','berhasil_update');
        } else {
            return Redirect::to('data-verifikator')->with('msg','berhasil_update');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        $data->email = 'akun_terhapus_'.$data->email;
        $data->deleted_at = Carbon::now();

		if($data->save()){
            return Redirect::to('data-verifikator')->with('msg','berhasil_hapus');
        } else {
            return Redirect::to('data-verifikator')->with('msg','gagal_hapus');
        }
    }
}

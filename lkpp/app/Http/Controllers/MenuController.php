<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use PDF;
use DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bulan = $request->input('bulan');
        if($bulan == ""){
            $jadwal = DB::table('jadwals')
                        ->leftJoin('peserta_jadwals', function($leftJoin)
                        {
                            $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
                            ->on('peserta_jadwals.status', '=', DB::raw("'1'"));
                        })
                        ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
                        ->groupBy('jadwals.id')
                        ->orderBy('jadwals.tanggal_ujian','desc')
                        ->where('publish_jadwal','ya')
                        ->get();
        } else {
            $jadwal = DB::table('jadwals')
                      ->leftJoin('peserta_jadwals', function($leftJoin){
                            $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
                            ->on('peserta_jadwals.status', '=', DB::raw("'1'"));
                        })
                      ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
                      ->groupBy('jadwals.id')
                      ->orderBy('jadwals.tanggal_ujian','desc')
                      ->whereMonth('tanggal_ujian', '=', $bulan)
                      ->where('publish_jadwal','ya')
                      ->get();
        }

        return View::make('menu_jadwal_inpassing', compact('jadwal','bulan'));
    }

    public function indextwo(Request $request)
    {
        $bulan = $request->input('bulan');
        if($bulan == ""){
            $data = DB::table('jadwal_instansis')
                    ->join('peserta_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                    ->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
                    ->join('instansis','users.nama_instansi','=','instansis.id','left')
                    ->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"))
                    ->where('jadwal_instansis.status_permohonan','setuju')
                    ->where('jadwal_instansis.deleted_at',NULL)
                    ->groupBy('jadwal_instansis.id')
                    ->orderBy('tanggal_tes','desc')
                    ->get();
        } else {
            $data = DB::table('jadwal_instansis')
                    ->join('peserta_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                    ->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
                    ->join('instansis','users.nama_instansi','=','instansis.id','left')
                    ->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"))
                    ->where('jadwal_instansis.status_permohonan','setuju')
                    ->where('jadwal_instansis.deleted_at',NULL)
                    ->groupBy('jadwal_instansis.id')
                    ->whereMonth('tanggal_ujian', '=', $bulan)
                    ->orderBy('tanggal_tes','desc')
                    ->get();
        }

        return View::make('menu_jadwal_penyelenggara',compact('data','bulan'));
    }

    public function prosedur()
    {
        $text = DB::table('text_runnings')->orderBy('urutan','asc')->get();
        $prosedur = DB::table('menu_prosedurs')->orderBy('id','desc')->first();
        $berkas = DB::table('berkas_prosedurs')->orderBy('urutan','asc')->orderBy('id','asc')->get();
        $data_banner = DB::table('t_banner_master')->orderBy('urutan','asc')->get();

        return View::make('menu_prosedur', compact('text','prosedur','berkas','data_banner'));
    }

    public function kontak()
    {
        $text = DB::table('text_runnings')->orderBy('urutan','asc')->get();
        $kontak = DB::table('menu_kontaks')->first();
        $data_banner = DB::table('t_banner_master')->orderBy('urutan','asc')->get();
        return View::make('menu_kontak', compact('text','kontak','data_banner'));
    }

    public function statik(Request $request)
    {
        $bulan = $request->input('bulan');
        $data_banner = DB::table('t_banner_master')->orderBy('urutan','asc')->get();
        $bulan_int = $request->input('bulan_int');
        $text = DB::table('text_runnings')->orderBy('urutan','asc')->get();
        $statistik = DB::table('menu_statistiks')->first();
        $prov = DB::table('instansis')
                ->join('users','instansis.id','=','users.nama_instansi','left')
                ->select('instansis.*','users.*',DB::raw("count(users.id) as jumlah"),'instansis.jenis as jenis_instansi')
                ->where('users.deleted_at',null)
                ->where('instansis.jenis','PROVINSI')
                ->get();

        $kota = DB::table('instansis')
                ->join('users','instansis.id','=','users.nama_instansi','left')
                ->select('instansis.*','users.*',DB::raw("count(users.id) as jumlah"),'instansis.jenis as jenis_instansi')
                ->where('users.deleted_at',null)
                ->where('instansis.jenis','KOTA')
                ->get();

        $kab = DB::table('instansis')
                ->join('users','instansis.id','=','users.nama_instansi','left')
                ->select('instansis.*','users.*',DB::raw("count(users.id) as jumlah"),'instansis.jenis as jenis_instansi')
                ->where('users.deleted_at',null)
                ->where('instansis.jenis','KABUPATEN')
                ->get();

        $lembaga = DB::table('instansis')
                    ->join('users','instansis.id','=','users.nama_instansi','left')
                    ->select('instansis.*','users.*',DB::raw("count(users.id) as jumlah"),'instansis.jenis as jenis_instansi')
                    ->where('users.deleted_at',null)
                    ->where('instansis.jenis','LEMBAGA')
                    ->get();

        $kementrian = DB::table('instansis')
                    ->join('users','instansis.id','=','users.nama_instansi','left')
                    ->select('instansis.*','users.*',DB::raw("count(users.id) as jumlah"),'instansis.jenis as jenis_instansi')
                    ->where('users.deleted_at',null)
                    ->where('instansis.jenis','KEMENTRIAN')
                    ->get();

        $instansis = DB::table('instansis')
                    ->join('users','instansis.id','=','users.nama_instansi','left')
                    ->select('instansis.*','users.*',DB::raw("count(users.id) as jumlah_instansis"),'instansis.jenis as jenis_instansi')
                    ->where('users.deleted_at',null)
                    ->groupBy('instansis.jenis')
                    ->get();

        $pesertaSistem = DB::table('pesertas')
                        ->select('pesertas.*',DB::raw("count(pesertas.id) as jumlah"),'pesertas.jenjang as jenjang')
                        ->where('pesertas.deleted_at',null)
                        ->where('pesertas.status_peserta','aktif')
                        ->groupBy('pesertas.jenjang')
                        ->orderBy('pesertas.jenjang','desc')
                        ->get();

          $pesertaSistem_total = DB::table('pesertas')
                        ->select('pesertas.*',DB::raw("count(pesertas.id) as jumlah"),'pesertas.jenjang as jenjang')
                        ->where('pesertas.deleted_at',null)
                        ->where('pesertas.status_peserta','aktif')
                        ->get();

        $pesertaSistemJadwal = DB::table('pesertas')
                               ->select('pesertas.*',DB::raw("count(pesertas.id) as jumlah"),'pesertas.jenjang as jenjang')
                               ->where('pesertas.deleted_at',null)
                               ->where('status_inpassing','!=',1)
                               ->where('status_inpassing','!=',2)
                               ->where('status_inpassing','!=',3)
                               ->where('pesertas.status_peserta','aktif')
                               ->groupBy('pesertas.jenjang')
                               ->orderBy('pesertas.jenjang','desc')
                               ->get();

            $pesertaSistemJadwal_total = DB::table('pesertas')
                               ->select('pesertas.*',DB::raw("count(pesertas.id) as jumlah"),'pesertas.jenjang as jenjang')
                               ->where('pesertas.deleted_at',null)
                               ->where('status_inpassing','!=',1)
                               ->where('status_inpassing','!=',2)
                               ->where('status_inpassing','!=',3)
                               ->where('pesertas.status_peserta','aktif')
                               ->get();

          $dataLulusTotal = DB::table('peserta_jadwals')
                    ->select(DB::raw("count(peserta_jadwals.id) as jumlah"))
                    ->where('peserta_jadwals.status_ujian','lulus')
                    ->where('peserta_jadwals.publish','publish')
                    ->where('peserta_jadwals.status',1)
                    ->get();


          $dataLulusJenjang = DB::table('peserta_jadwals')
                    ->join('pesertas','pesertas.id','=','peserta_jadwals.id_peserta','left')
                    ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'pesertas.jenjang')
                    ->where('peserta_jadwals.status_ujian','lulus')
                    ->where('peserta_jadwals.publish','publish')
                    ->where('peserta_jadwals.status',1)
                    ->groupBy('pesertas.jenjang')
                    ->orderBy('pesertas.jenjang','desc')
                    ->get();

          $datalengkapJenjang = DB::table('pesertas')
                    ->select(DB::raw("count(pesertas.id) as jumlah"),'pesertas.jenjang')
                    ->where('pesertas.verifikasi_berkas','verified')
                    ->where('pesertas.deleted_at',null)
                    ->where('pesertas.status_peserta','aktif')
                    ->groupBy('pesertas.jenjang')
                    ->orderBy('pesertas.jenjang','desc')
                    ->get();

          $datalengkapTotal = DB::table('pesertas')
                    ->select(DB::raw("count(pesertas.id) as jumlah"),'pesertas.jenjang')
                    ->where('pesertas.verifikasi_berkas','verified')
                    ->where('pesertas.deleted_at',null)
                    ->where('pesertas.status_peserta','aktif')
                    ->get();

          $datadisetujuipertama = DB::table('pengusulan_eformasis')
                    ->select(DB::raw("sum(pengusulan_eformasis.pertama) as jumlah"))
                    ->where('pengusulan_eformasis.status_pertama','setuju')
                    ->get();

          $datadisetujuimuda = DB::table('pengusulan_eformasis')
                    ->select(DB::raw("count(pengusulan_eformasis.muda) as jumlah"))
                    ->where('pengusulan_eformasis.status_muda','setuju')
                    ->get();

          $datadisetujuimadya = DB::table('pengusulan_eformasis')
                    ->select(DB::raw("count(pengusulan_eformasis.madya) as jumlah"))
                    ->where('pengusulan_eformasis.status_madya','setuju')
                    ->get();

        if ($bulan == "") {
            $jadwal = DB::table('jadwals')
                      ->where('deleted_at',null)
                      ->where('publish_jadwal','ya')
                      ->groupBy('jadwals.id')
                      ->orderByDesc('tanggal_ujian')
                      ->get();
        } else {
            $jadwal = DB::table('jadwals')
                      ->where('deleted_at',null)
                      ->where('publish_jadwal','ya')
                      ->whereMonth('tanggal_ujian','=',$bulan)
                      ->groupBy('jadwals.id')
                      ->orderByDesc('tanggal_ujian')
                      ->get();
        }

        if ($bulan_int == "") {
            $jadwal_instansi = DB::table('jadwal_instansis')
                               ->where('deleted_at',null)
                               ->where('status_permohonan','setuju')
                               ->groupBy('jadwal_instansis.id')
                               ->orderByDesc('tanggal_ujian')
                               ->get();
        } else {
             $jadwal_instansi = DB::table('jadwal_instansis')
                                ->where('deleted_at',null)
                                ->where('status_permohonan','setuju')
                                ->whereMonth('tanggal_ujian','=',$bulan_int)
                                ->groupBy('jadwal_instansis.id')
                                ->orderByDesc('tanggal_ujian')
                                ->get();
        }


        $pesertajadwal = DB::table('peserta_jadwals')
                         ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
                         ->select('jadwals.*','peserta_jadwals.*',DB::raw("count(peserta_jadwals.id) as jumlah"),'peserta_jadwals.id_jadwal as id_jadwal')
                         ->where('status_ujian','lulus')
                         ->where('publish','publish')
                         ->get();

        return View::make('menu_statik', compact('text','statistik','instansis','prov','kota','kab','lembaga','kementrian','instansis','pesertaSistem','pesertaSistemJadwal','jadwal','pesertajadwal','jadwal_instansi','bulan','bulan_int','pesertaSistem_total','pesertaSistemJadwal_total','dataLulusJenjang','dataLulusTotal','datadisetujuipertama','datadisetujuimuda','datadisetujuimadya','datalengkapJenjang','datalengkapTotal','data_banner'));
    }

    public function statikRegular($id){
        $text = DB::table('text_runnings')->orderBy('urutan','asc')->get();
        $statistik = DB::table('menu_statistiks')->first();

    $dataTotal = DB::table('peserta_jadwals')
                    ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
                    ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal')
                    ->where('peserta_jadwals.id_jadwal',$id)
                    ->where('peserta_jadwals.status',1)
                    ->get();

    $dataMemenuhiTotal = DB::table('pesertas')
                          ->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta','left')
                          ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
                          ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal','pesertas.jenjang')
                          ->where('peserta_jadwals.id_jadwal',$id)
                          ->whereNotIn('peserta_jadwals.status_ujian',['tidak_lengkap'])
                          ->where('peserta_jadwals.publish','publish')
                          ->where('peserta_jadwals.status',1)
                          ->get();

    $dataLulusTotal = DB::table('peserta_jadwals')
                    ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
                    ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal')
                    ->where('peserta_jadwals.id_jadwal',$id)
                    ->where('peserta_jadwals.status_ujian','lulus')
                    ->where('peserta_jadwals.publish','publish')
                    ->where('peserta_jadwals.status',1)
                    ->get();

    $dataTotalJenjang = DB::table('pesertas')
                          ->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta','left')
                          ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
                          ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal','pesertas.jenjang')
                          ->where('peserta_jadwals.id_jadwal',$id)
                          ->where('peserta_jadwals.status',1)
                          ->groupBy('pesertas.jenjang')
                          ->get();

    $dataMemenuhiJenjang = DB::table('pesertas')
                          ->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta','left')
                          ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
                          ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal','pesertas.jenjang')
                          ->where('peserta_jadwals.id_jadwal',$id)
                          ->whereNotIn('peserta_jadwals.status_ujian',['tidak_lengkap'])
                          ->where('peserta_jadwals.publish','publish')
                          ->where('peserta_jadwals.status',1)
                          ->groupBy('pesertas.jenjang')
                          ->orderBy('pesertas.jenjang','asc')
                          ->get();

    $dataLulusJenjang = DB::table('peserta_jadwals')
                    ->join('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
                    ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
                    ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal','pesertas.jenjang')
                    ->where('peserta_jadwals.id_jadwal',$id)
                    ->where('peserta_jadwals.status_ujian','lulus')
                    ->where('peserta_jadwals.publish','publish')
                    ->where('peserta_jadwals.status',1)
                    ->groupBy('pesertas.jenjang')
                    ->orderBy('pesertas.jenjang','asc')
                    ->get();

    // $dataLulusTotal = DB::table('peserta_jadwals')
    //                 ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
    //                 ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal')
    //                 ->where('peserta_jadwals.id_jadwal',$id)
    //                 ->where('peserta_jadwals.status_ujian','lulus')
    //                 ->where('peserta_jadwals.publish','publish')
    //                 ->get();

    //  $dataTidakSyarat = DB::table('peserta_jadwals')
    //                 ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal')
    //                 ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal')
    //                 ->where('peserta_jadwals.id_jadwal',$id)
    //                 ->where('peserta_jadwals.publish',NULL)
    //                 ->get();


    // $dataTidakLulusTotal = DB::table('peserta_jadwals')
    //                       ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
    //                       ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal')
    //                       ->where('peserta_jadwals.id_jadwal',$id)
    //                       ->where('peserta_jadwals.status_ujian','tidak_lulus')
    //                       ->where('peserta_jadwals.publish','publish')
    //                       ->get();

    // $dataLulusJenjang = DB::table('peserta_jadwals')
    //                 ->join('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
    //                 ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
    //                 ->select(DB::raw("count(peserta_jadwals.id) as jumlah"),'jadwals.tanggal_ujian as tanggal','pesertas.jenjang')
    //                 ->where('peserta_jadwals.id_jadwal',$id)
    //                 ->where('peserta_jadwals.status_ujian','lulus')
    //                 ->where('peserta_jadwals.publish','publish')
    //                 ->groupBy('pesertas.jenjang')
    //                 ->get();




        $data = DB::table('jadwals')
                ->select('tanggal_ujian','metode')
                ->where('id',$id)
                ->get();

// echo count($dataTotal);

        return View::make('menu_statik_regular', compact('text','statistik','dataTotal','dataLulusTotal','dataMemenuhiTotal','dataTidakLulusTotal','dataLulusJenjang','dataTotalJenjang','dataMemenuhiJenjang','dataTidakLulusJenjang','data','dataTidakSyarat'));
    }

    public function statikInstansi($id){
        $text = DB::table('text_runnings')->orderBy('urutan','asc')->get();
        $statistik = DB::table('menu_statistiks')->first();
        // $dataLulus = DB::table('peserta_instansis')
        //             ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
        //             ->select(DB::raw("count(peserta_instansis.id) as jumlah"),'jadwal_instansis.tanggal_ujian as tanggal')
        //             ->where('peserta_instansis.id_jadwal',$id)
        //             ->where('peserta_instansis.status_ujian','lulus')
        //             ->where('peserta_instansis.publish','publish')
        //             ->get();

        // $dataTidakLulus = DB::table('peserta_instansis')
        //                 ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
        //                 ->select(DB::raw("count(peserta_instansis.id) as jumlah"),'jadwal_instansis.tanggal_ujian as tanggal')
        //                 ->where('peserta_instansis.id_jadwal',$id)
        //                 ->where('peserta_instansis.status_ujian','tidak_lulus')
        //                 ->where('peserta_instansis.publish','publish')
        //                 ->get();

     $dataTotal = DB::table('peserta_instansis')
                    ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                    ->select(DB::raw("count(peserta_instansis.id) as jumlah"),'jadwal_instansis.tanggal_ujian as tanggal')
                    ->where('peserta_instansis.id_jadwal',$id)
                    ->where('peserta_instansis.status',1)
                    ->get();

    $dataMemenuhiTotal = DB::table('pesertas')
                          ->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta','left')
                          ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                          ->select(DB::raw("count(peserta_instansis.id) as jumlah"),'jadwal_instansis.tanggal_ujian as tanggal','pesertas.jenjang')
                          ->where('peserta_instansis.id_jadwal',$id)
                          ->whereNotIn('peserta_instansis.status_ujian',['tidak_lengkap'])
                          ->where('peserta_instansis.publish','publish')
                          ->where('peserta_instansis.status',1)
                          ->get();

    $dataLulusTotal = DB::table('peserta_instansis')
                    ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                    ->select(DB::raw("count(peserta_instansis.id) as jumlah"),'jadwal_instansis.tanggal_ujian as tanggal')
                    ->where('peserta_instansis.id_jadwal',$id)
                    ->where('peserta_instansis.status_ujian','lulus')
                    ->where('peserta_instansis.publish','publish')
                    ->where('peserta_instansis.status',1)
                    ->get();

    $dataTotalJenjang = DB::table('pesertas')
                          ->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta','left')
                          ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                          ->select(DB::raw("count(peserta_instansis.id) as jumlah"),'jadwal_instansis.tanggal_ujian as tanggal','pesertas.jenjang')
                          ->where('peserta_instansis.id_jadwal',$id)
                          ->where('peserta_instansis.status',1)
                          ->groupBy('pesertas.jenjang')
                          ->get();

    $dataMemenuhiJenjang = DB::table('pesertas')
                          ->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta','left')
                          ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                          ->select(DB::raw("count(peserta_instansis.id) as jumlah"),'jadwal_instansis.tanggal_ujian as tanggal','pesertas.jenjang')
                          ->where('peserta_instansis.id_jadwal',$id)
                          ->whereNotIn('peserta_instansis.status_ujian',['tidak_lengkap'])
                          ->where('peserta_instansis.publish','publish')
                          ->where('peserta_instansis.status',1)
                          ->groupBy('pesertas.jenjang')
                          ->orderBy('pesertas.jenjang','asc')
                          ->get();

    $dataLulusJenjang = DB::table('peserta_instansis')
                    ->join('pesertas','pesertas.id','=','peserta_instansis.id_peserta')
                    ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                    ->select(DB::raw("count(peserta_instansis.id) as jumlah"),'jadwal_instansis.tanggal_ujian as tanggal','pesertas.jenjang')
                    ->where('peserta_instansis.id_jadwal',$id)
                    ->where('peserta_instansis.status_ujian','lulus')
                    ->where('peserta_instansis.publish','publish')
                    ->where('peserta_instansis.status',1)
                    ->groupBy('pesertas.jenjang')
                    ->orderBy('pesertas.jenjang','asc')
                    ->get();

        $data = DB::table('jadwal_instansis')
                ->select('tanggal_ujian','metode')
                ->where('id',$id)
                ->get();

        return View::make('menu_statik_instansi', compact('text','statistik','dataTotal','dataLulusTotal','dataMemenuhiTotal','dataTidakLulusTotal','dataLulusJenjang','dataTotalJenjang','dataMemenuhiJenjang','dataTidakLulusJenjang','data','dataTidakSyarat'));
    }

    public function FileView($path,$file)
    {
        $pathToFile = storage_path('data'."/".$path."/".$file);
        return response()->file($pathToFile);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('peserta_jadwals')
                ->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
                ->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
                ->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
                ->select('peserta_jadwals.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids', 'peserta_jadwals.status_ujian as status_ujians','instansis.nama as instansis','pesertas.nama','pesertas.nip','pesertas.jabatan','peserta_jadwals.jabatan as jabatans','pesertas.status as statuss','peserta_jadwals.jenjang as jenjangs','pesertas.jenjang as jenjang')
                ->where('peserta_jadwals.id_jadwal',$id)
                ->where('peserta_jadwals.status','1')
                ->groupBy('peserta_jadwals.id')
                ->get();

        $jadwal = DB::table('jadwals')->where('id',$id)->first();
        return View::make('menu_peserta_inpassing', compact('data','jadwal'));
    }

    public function showTwo($id)
    {
        $data = DB::table('peserta_instansis')
                ->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
                ->join('pesertas','peserta_instansis.id_peserta','=','pesertas.id')
                ->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
                ->select('peserta_instansis.*','peserta_instansis.metode_ujian as metodes'
                ,'peserta_instansis.id as ids','peserta_instansis.status_ujian as statuss'
                ,'instansis.nama as instansis','pesertas.nama','pesertas.nip','pesertas.jabatan','peserta_instansis.jabatan as jabatans'
                ,'pesertas.jenjang','pesertas.status as statusj','peserta_instansis.jenjang as jenjangs','pesertas.jenjang as jenjang')
                ->where('peserta_instansis.id_jadwal',$id)
                ->where('peserta_instansis.status','1')
                ->groupBy('peserta_instansis.id')
                ->get();

        $jadwal = DB::table('jadwal_instansis')->where('id',$id)->first();
        return View::make('menu_peserta_instansi', compact('data','jadwal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function TestPdf()
    {
        $data = [
            'foo' => 'bar'
        ];
        $pdf = PDF::loadView('pdf.test_pdf', $data);
        return $pdf->stream('document.pdf');
    }

    public function HasilUjian($jenisUjian,$file)
    {
        $pathToFile = storage_path('data'."/hasil_ujian/".$jenisUjian."/".$file);
        return response()->file($pathToFile);
    }
}

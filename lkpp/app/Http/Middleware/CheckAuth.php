<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect('inpassing');
        }else{
            if(is_null(Auth::user()->email_verified_at)){
                Auth::logout();
                return redirect('inpassing')->with('msg','akun_tidak_aktif');
            }
            if(!is_null(Auth::user()->deleted_at)){
                Auth::logout();
                return redirect('inpassing')->with('msg','akun_dihapus');
            }
        }
        return $next($request);
    }
}

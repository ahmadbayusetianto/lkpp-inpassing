<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use DB;

class CheckAdminKpp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(Auth::user()->admin_type == "lkpp"){
                return redirect('dashboard');
            }
            $user = DB::table('users')
            ->join('instansis','users.nama_instansi','=','instansis.id','left')
            ->select('users.*','instansis.nama as instansis')
            ->where('users.id',Auth::user()->id)
            ->first();

            if($user->instansis != "")
            {
                $request->session()->put('instansis_user', $user->instansis);
            }else{
                $request->session()->put('instansis_user', '-');
            }
        }
        return $next($request);
    }
}

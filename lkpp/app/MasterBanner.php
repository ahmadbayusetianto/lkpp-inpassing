<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterBanner extends Model
{
     //
    protected $table = 't_banner_master';
}

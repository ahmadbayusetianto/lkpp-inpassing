<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterPesertaLulus extends Model
{
    Protected $table = 'master_peserta_lulus';
}

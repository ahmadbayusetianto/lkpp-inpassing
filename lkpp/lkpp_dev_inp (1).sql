-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2020 at 07:03 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lkpp_dev_inp`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_info`
--

CREATE TABLE `app_info` (
  `id` int(11) NOT NULL,
  `id_info` int(11) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `change_by` varchar(200) DEFAULT NULL,
  `chante_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asesors`
--

CREATE TABLE `asesors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instansi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti_asesor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl` date DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `berkas_prosedurs`
--

CREATE TABLE `berkas_prosedurs` (
  `id` int(10) UNSIGNED NOT NULL,
  `urutan` int(11) DEFAULT NULL,
  `judul_berkas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `berkas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detail_file_inputs`
--

CREATE TABLE `detail_file_inputs` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_detail_input` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_input_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_input_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_input_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detail_inputs`
--

CREATE TABLE `detail_inputs` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_judul_input` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dokumens`
--

CREATE TABLE `dokumens` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_peserta` int(11) DEFAULT NULL,
  `surat_bertugas_pbj` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti_sk_pbj` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ijazah_terakhir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sk_kenaikan_pangkat_terakhir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sk_pengangkatan_jabatan_terakhir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sertifikasi_dasar_pbj` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skp_dua_tahun_terakhir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_ket_tidak_dijatuhi_hukuman` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formulir_kesediaan_mengikuti_inpassing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sk_pengangkatan_cpns` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sk_pengangkatan_pns` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ktp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pas_foto_3_x_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_pernyataan_bersedia_jfppbj` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti_portofolio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_usulan_mengikuti_inpassing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_usulan_peserta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_portofolios`
--

CREATE TABLE `dokumen_portofolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_peserta` int(11) DEFAULT NULL,
  `id_portofolio_a` int(11) DEFAULT NULL,
  `status_portofolio_a` int(11) DEFAULT NULL,
  `id_portofolio_b` int(11) DEFAULT NULL,
  `status_portofolio_b` int(11) DEFAULT NULL,
  `id_portofolio_c` int(11) DEFAULT NULL,
  `status_portofolio_c` int(11) DEFAULT NULL,
  `id_portofolio_d` int(11) DEFAULT NULL,
  `status_portofolio_d` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_portofolio_a_s`
--

CREATE TABLE `dokumen_portofolio_a_s` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_portofolio` int(11) DEFAULT NULL,
  `surat_penyusunan_spesifikasi_teknis_dan_kak` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_penyusunan_spesifikasi_teknis_dan_kak` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_penyusunan_spesifikasi_teknis_dan_kak` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_penyusunan_spesifikasi_teknis_dan_kak` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_identifikasi_reviu_kebutuhan_dan_penetapan_barang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_perumusan_strategi_pengadaan_pemaketan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_perumusan_strategi_pengadaan_pemaketan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_perumusan_strategi_pengadaan_pemaketan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_dokumen_perumusan_strategi_pengadaan_pemaketan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_portofolio_b_s`
--

CREATE TABLE `dokumen_portofolio_b_s` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_portofolio` int(11) DEFAULT NULL,
  `surat_reviu_terhadap_dokumen_persiapan_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_reviu_terhadap_dokumen_persiapan_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_reviu_terhadap_dokumen_persiapan_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_reviu_terhadap_dokumen_persiapan_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_penyusunan_dan_penjelasan_dokumen_pemilihan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_penyusunan_dan_penjelasan_dokumen_pemilihan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_penyusunan_dan_penjelasan_dokumen_pemilihan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_evaluasi_penawaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_evaluasi_penawaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_evaluasi_penawaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_evaluasi_penawaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_penilian_kualifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_penilian_kualifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_penilian_kualifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_penilian_kualifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_pengelolaan_sanggahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_pengelolaan_sanggahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_pengelolaan_sanggahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_pengelolaan_sanggahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_penyusunan_daftar_penyedia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_penyusunan_daftar_penyedia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_penyusunan_daftar_penyedia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_penyusunan_daftar_penyedia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_negosiasi_dalam_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_surat_negosiasi_dalam_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_surat_negosiasi_dalam_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_surat_negosiasi_dalam_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_portofolio_c_s`
--

CREATE TABLE `dokumen_portofolio_c_s` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_portofolio` int(11) DEFAULT NULL,
  `surat_perumusan_kontrak_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_perumusan_kontrak_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_perumusan_kontrak_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_perumusan_kontrak_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_pengendalian_pelaksanaan_kontrak_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_pengendalian_pelaksanaan_kontrak_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_pengendalian_pelaksanaan_kontrak_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_pengendalian_pelaksanaan_kontrak_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_serah_terima_hasil_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_serah_terima_hasil_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_serah_terima_hasil_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_serah_terima_hasil_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_evaluasi_kinerja_penyedia_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_evaluasi_kinerja_penyedia_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_evaluasi_kinerja_penyedia_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_evaluasi_kinerja_penyedia_pbjp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_portofolio_d_s`
--

CREATE TABLE `dokumen_portofolio_d_s` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_portofolio` int(11) DEFAULT NULL,
  `surat_penyusunan_rencana_dan_persiapan_pengadaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_penyusunan_rencana_dan_persiapan_pengadaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_penyusunan_rencana_dan_persiapan_pengadaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_penyusunan_rencana_dan_persiapan_pengadaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_paket_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grup_soal`
--

CREATE TABLE `grup_soal` (
  `id` int(11) NOT NULL,
  `kode_grup` varchar(200) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `change_by` varchar(200) DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status_terpakai` int(11) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hitungs`
--

CREATE TABLE `hitungs` (
  `id` int(11) NOT NULL,
  `id_peserta` int(11) DEFAULT NULL,
  `counter_seri_depan` varchar(225) DEFAULT NULL,
  `counter_seri_belakang` varchar(225) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `import_ujians`
--

CREATE TABLE `import_ujians` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `metode` varchar(45) DEFAULT NULL,
  `hasil_scan_portofolio` varchar(45) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `instansis`
--

CREATE TABLE `instansis` (
  `id` varchar(200) CHARACTER SET utf8 NOT NULL,
  `prp_id` int(11) DEFAULT NULL,
  `kbp_id` int(11) DEFAULT NULL,
  `nama` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `alamat` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `jenis` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `is2014` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `is2015` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jadwals`
--

CREATE TABLE `jadwals` (
  `id` int(10) UNSIGNED NOT NULL,
  `metode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_verifikasi` date DEFAULT NULL,
  `waktu_verifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_tes` date DEFAULT NULL,
  `waktu_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_ujian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_ruang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batas_waktu_input` date NOT NULL,
  `publish_jadwal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_cp_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp_cp_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_cp_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp_cp_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tanggal_ujian` date DEFAULT NULL,
  `tipe_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'regular',
  `hasil_ujian` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_instansis`
--

CREATE TABLE `jadwal_instansis` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin_ppk` int(11) NOT NULL,
  `tanggal_ujian` date DEFAULT NULL,
  `tanggal_verifikasi` date DEFAULT NULL,
  `waktu_verifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `tanggal_tes` date DEFAULT NULL,
  `waktu_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_ujian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `jumlah_ruang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_cp_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp_cp_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_cp_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp_cp_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_permohonan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_permohonan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `di_setujui_oleh` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `batas_waktu_input` date DEFAULT NULL,
  `metode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'tes_tulis',
  `tipe_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'instansi',
  `hasil_ujian` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jawaban_peserta_ujian`
--

CREATE TABLE `jawaban_peserta_ujian` (
  `id` int(11) NOT NULL,
  `kode_peserta` varchar(200) DEFAULT NULL,
  `kode_ujian` varchar(200) DEFAULT NULL,
  `id_soal` int(11) DEFAULT NULL,
  `no_soal` int(11) DEFAULT NULL,
  `jawaban` varchar(200) DEFAULT NULL,
  `nilai` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `judul_inputs`
--

CREATE TABLE `judul_inputs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_komponen` int(1) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `master_peserta`
--

CREATE TABLE `master_peserta` (
  `nip` varchar(200) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `change_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `change_by` varchar(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `instansi` varchar(200) DEFAULT NULL,
  `gelar` varchar(200) DEFAULT NULL,
  `jenis_kelamin` varchar(200) DEFAULT NULL,
  `pangkat_gol` varchar(200) DEFAULT NULL,
  `tempat_lahir` varchar(200) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jabatan` varchar(200) DEFAULT NULL,
  `pendidikan_terakhir` varchar(200) DEFAULT NULL,
  `unit_kerja` varchar(200) DEFAULT NULL,
  `telp_hp` varchar(200) DEFAULT NULL,
  `alamat_rumah` text DEFAULT NULL,
  `alamat_kantor` text DEFAULT NULL,
  `pengalaman_kerja_dibidang_pbj` varchar(200) DEFAULT NULL,
  `pernah_ikut_pelatihan` varchar(200) DEFAULT NULL,
  `jika_ya_brp_x` varchar(200) DEFAULT NULL,
  `lulus_ujian_tingkat` varchar(200) DEFAULT NULL,
  `no_sertifikat` varchar(200) DEFAULT NULL,
  `nilai_ujian` varchar(200) DEFAULT NULL,
  `tgl_ujian` varchar(200) DEFAULT NULL,
  `jamujian` varchar(200) DEFAULT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `tgl_i` varchar(200) DEFAULT NULL,
  `kabupaten` varchar(200) DEFAULT NULL,
  `propinsi` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `no_ktp` varchar(200) DEFAULT NULL,
  `unit_kerja_eselon_2` varchar(200) DEFAULT NULL,
  `departemen_lembaga_daerah_bumn` varchar(200) DEFAULT NULL,
  `telp_rumah` varchar(200) DEFAULT NULL,
  `alamat` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_sertifikat`
--

CREATE TABLE `master_sertifikat` (
  `id` int(11) NOT NULL,
  `nama_deputi` varchar(255) DEFAULT NULL,
  `nip_deputi` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `jabatan_english` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_kontaks`
--

CREATE TABLE `menu_kontaks` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_admin_update` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_prosedurs`
--

CREATE TABLE `menu_prosedurs` (
  `id` int(10) UNSIGNED NOT NULL,
  `urutan` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_admin_update` int(11) DEFAULT NULL,
  `berkas_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `berkas_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `berkas_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_registers`
--

CREATE TABLE `menu_registers` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_admin_update` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_statistiks`
--

CREATE TABLE `menu_statistiks` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_admin_update` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pangkat_gol`
--

CREATE TABLE `pangkat_gol` (
  `pangkat_id` int(11) NOT NULL,
  `pangkat_golongan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pelaksanaan_ujian`
--

CREATE TABLE `pelaksanaan_ujian` (
  `id` int(11) NOT NULL,
  `kode_ujian` varchar(200) DEFAULT NULL,
  `kode_institusi` varchar(200) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `change_by` varchar(200) DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `kapasitas` int(11) DEFAULT 0,
  `param_lulus` varchar(200) DEFAULT 'A',
  `id_jadwal` varchar(200) DEFAULT NULL,
  `status_pelaksanaan` varchar(200) DEFAULT 'BERJALAN',
  `jam_ujian` varchar(200) DEFAULT NULL,
  `lokasi_ujian` varchar(200) DEFAULT NULL,
  `nama_petugas` varchar(200) DEFAULT NULL,
  `nama_daerah` varchar(200) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `catatan` tinytext DEFAULT NULL,
  `jumlah_peserta` varchar(200) DEFAULT NULL,
  `jumlah_peserta_hadir` varchar(200) DEFAULT NULL,
  `jumlah_peserta_tidak_hadir` varchar(200) DEFAULT NULL,
  `jumlah_peserta_lulus` varchar(200) DEFAULT NULL,
  `jumlah_peserta_tidak_lulus` varchar(200) DEFAULT NULL,
  `jumlah_peserta_dis` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengusulan_eformasis`
--

CREATE TABLE `pengusulan_eformasis` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin_ppk` bigint(20) UNSIGNED NOT NULL,
  `pertama` int(11) NOT NULL,
  `muda` int(11) NOT NULL,
  `madya` int(11) NOT NULL,
  `status_pertama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_muda` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_madya` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi_pertama` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi_muda` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi_madya` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokumen_hasil_perhitungan_abk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surat_usulan_rekomendasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_surat_usulan_rekomendasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `perteks`
--

CREATE TABLE `perteks` (
  `id` int(11) NOT NULL,
  `instansi` varchar(255) DEFAULT NULL,
  `status_pertek` varchar(255) DEFAULT NULL,
  `draft` varchar(255) DEFAULT NULL,
  `tanggal_pertek` date DEFAULT NULL,
  `no_pertek` varchar(255) DEFAULT NULL,
  `file_pertek` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pesertas`
--

CREATE TABLE `pesertas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin_ppk` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmt_panggol` date DEFAULT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_instansi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `no_ktp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendidikan_terakhir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `satuan_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi` int(11) DEFAULT NULL,
  `kab_kota` int(11) DEFAULT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_dokumen` int(10) UNSIGNED NOT NULL,
  `id_formasi` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign` int(11) DEFAULT NULL,
  `asesor` int(11) DEFAULT NULL,
  `asesor_2` int(11) DEFAULT NULL,
  `status_inpassing` int(11) NOT NULL DEFAULT 1,
  `verifikasi_berkas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_peserta` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'aktif',
  `no_surat_usulan_peserta` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_ujian_terakhir` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peserta_instansis`
--

CREATE TABLE `peserta_instansis` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `id_peserta` int(11) NOT NULL,
  `id_admin_ppk` int(11) NOT NULL,
  `nama_peserta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_ujian` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metode_ujian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmt_panggol` date DEFAULT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_update` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `no_seri_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ak_kumulatif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peserta_jadwals`
--

CREATE TABLE `peserta_jadwals` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `id_peserta` int(11) NOT NULL,
  `id_admin_ppk` int(11) NOT NULL,
  `id_portofolio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_peserta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metode_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmt_panggol` date DEFAULT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasil_pleno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_update` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `no_ujian` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_seri_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ak_kumulatif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perbaikan_porto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asesor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asesor_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cek_asesor1` int(11) DEFAULT 0,
  `poin_lulus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peserta_perteks`
--

CREATE TABLE `peserta_perteks` (
  `id` int(11) NOT NULL,
  `id_pertek` int(11) DEFAULT 0,
  `id_peserta` int(11) DEFAULT 0,
  `nomor_surat_usulan_peserta` varchar(255) DEFAULT NULL,
  `no_pertek` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `peserta_sphus`
--

CREATE TABLE `peserta_sphus` (
  `id` int(11) NOT NULL,
  `id_sphu` int(11) DEFAULT 0,
  `id_peserta` int(11) DEFAULT 0,
  `nomor_surat_usulan_peserta` varchar(255) DEFAULT NULL,
  `no_sphu` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `peserta_ujian`
--

CREATE TABLE `peserta_ujian` (
  `id` int(11) NOT NULL,
  `kode_ujian` varchar(200) DEFAULT NULL,
  `kode_peserta` varchar(200) DEFAULT NULL,
  `grup_soal` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `change_by` varchar(200) DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `nama` varchar(200) DEFAULT NULL,
  `nip` varchar(200) DEFAULT NULL,
  `mulai_ujian` datetime DEFAULT NULL,
  `selesai_ujian` datetime DEFAULT NULL,
  `status_lulus` varchar(200) DEFAULT 'TIDAK LULUS',
  `nilai` int(11) DEFAULT 0,
  `remote_addr` varchar(200) DEFAULT NULL,
  `ingat` int(11) DEFAULT 0,
  `id_peserta` varchar(200) DEFAULT NULL,
  `status_pelaksanaan` varchar(200) DEFAULT 'BERJALAN',
  `stop_ujian` datetime DEFAULT NULL,
  `random_id` int(11) DEFAULT NULL,
  `alasan_dis` text DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `inpassing_status` int(11) DEFAULT NULL,
  `metode_inpassing` int(11) DEFAULT NULL,
  `unique_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `regencies`
--

CREATE TABLE `regencies` (
  `id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_hasil_verifs`
--

CREATE TABLE `riwayat_hasil_verifs` (
  `id` int(15) UNSIGNED NOT NULL,
  `id_riwayat` int(15) DEFAULT NULL,
  `id_jadwal` int(15) DEFAULT NULL,
  `id_user` int(15) DEFAULT NULL,
  `id_admin` int(15) DEFAULT NULL,
  `id_admin_lkpp` int(15) DEFAULT NULL,
  `perihal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_pembaharuan`
--

CREATE TABLE `riwayat_pembaharuan` (
  `id` int(15) UNSIGNED NOT NULL,
  `id_riwayat` int(15) DEFAULT NULL,
  `id_jadwal` int(15) DEFAULT NULL,
  `id_user` int(15) DEFAULT NULL,
  `id_admin` int(15) DEFAULT NULL,
  `id_admin_lkpp` int(15) DEFAULT NULL,
  `perihal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_detail_file_input` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_users`
--

CREATE TABLE `riwayat_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `id_admin_lkpp` int(11) DEFAULT NULL,
  `id_eformasi` int(11) DEFAULT NULL,
  `perihal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `simpan_berkas_portofolios`
--

CREATE TABLE `simpan_berkas_portofolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_detail_file_input` int(11) NOT NULL,
  `id_detail_input` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_peserta` int(11) DEFAULT NULL,
  `id_jadwal` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `simpan_berkas_portofolios_history`
--

CREATE TABLE `simpan_berkas_portofolios_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_detail_file_input` int(11) NOT NULL,
  `id_detail_input` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_peserta` int(11) DEFAULT NULL,
  `id_jadwal` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sphus`
--

CREATE TABLE `sphus` (
  `id` int(11) NOT NULL,
  `instansi` varchar(255) DEFAULT '',
  `status_sphu` varchar(255) DEFAULT NULL,
  `draft` varchar(255) DEFAULT NULL,
  `no_sphu` varchar(255) DEFAULT NULL,
  `tanggal_sphu` date DEFAULT NULL,
  `file_sphu` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_file_portofolios`
--

CREATE TABLE `status_file_portofolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_peserta` int(11) DEFAULT NULL,
  `id_jadwal` int(11) DEFAULT NULL,
  `id_detail_file_input` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_admin_ppk` int(11) DEFAULT NULL,
  `id_admin_lkpp` int(11) DEFAULT NULL,
  `perbaikan` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status_peserta_inpassings`
--

CREATE TABLE `status_peserta_inpassings` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `surat_usulans`
--

CREATE TABLE `surat_usulans` (
  `id` int(11) NOT NULL,
  `id_admin_ppk` int(11) DEFAULT 0,
  `id_jadwal` int(11) DEFAULT 0,
  `jenis_ujian` varchar(50) DEFAULT NULL,
  `id_peserta` int(11) DEFAULT 0,
  `file` text DEFAULT NULL,
  `no_surat_usulan_peserta` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surat_usulans_int`
--

CREATE TABLE `surat_usulans_int` (
  `id` int(11) NOT NULL,
  `id_admin_ppk` int(11) DEFAULT 0,
  `jenis_ujian` varchar(50) DEFAULT NULL,
  `id_jadwal` int(11) DEFAULT 0,
  `id_peserta` int(11) DEFAULT 0,
  `file` text DEFAULT NULL,
  `no_surat_usulan_peserta` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tabelnilai`
--

CREATE TABLE `tabelnilai` (
  `id` int(11) NOT NULL,
  `no_ujian` int(11) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_soal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grup_soal` int(11) DEFAULT NULL,
  `jml_soal` int(11) DEFAULT NULL,
  `benar_bs` int(11) DEFAULT NULL,
  `salah_bs` int(11) DEFAULT NULL,
  `kosong_bs` int(11) DEFAULT NULL,
  `benar_pg` int(11) DEFAULT NULL,
  `salah_pg` int(11) DEFAULT NULL,
  `kosong_pg` int(11) DEFAULT NULL,
  `benar_sk` int(11) DEFAULT NULL,
  `salah_sk` int(11) DEFAULT NULL,
  `kosong_sk` int(11) DEFAULT NULL,
  `totbetul` int(11) DEFAULT NULL,
  `totsalah` int(11) DEFAULT NULL,
  `skor` int(11) DEFAULT NULL,
  `ket` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `predikat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jml_essai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tglujian` date DEFAULT NULL,
  `jamujian` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctksertfkt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noseri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ceksrtfkt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenjang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_jadwal` int(11) DEFAULT NULL,
  `no_peserta` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `text_runnings`
--

CREATE TABLE `text_runnings` (
  `id` int(10) UNSIGNED NOT NULL,
  `urutan` int(11) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_banner_master`
--

CREATE TABLE `t_banner_master` (
  `id` int(11) NOT NULL,
  `file` text DEFAULT NULL,
  `urutan` int(11) DEFAULT 0,
  `publish` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit_kerjas`
--

CREATE TABLE `unit_kerjas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_satuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_instansi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_ktp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sk_admin_ppk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_admin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `verifikasi_portofolios`
--

CREATE TABLE `verifikasi_portofolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_peserta` int(11) NOT NULL,
  `id_peserta_jadwal` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `nama_peserta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asesor_verifikasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rekomendasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asesor_pleno_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asesor_pleno_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keputusan_pleno` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_post` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_info`
--
ALTER TABLE `app_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asesors`
--
ALTER TABLE `asesors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berkas_prosedurs`
--
ALTER TABLE `berkas_prosedurs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_inputs`
--
ALTER TABLE `detail_inputs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokumens`
--
ALTER TABLE `dokumens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grup_soal`
--
ALTER TABLE `grup_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hitungs`
--
ALTER TABLE `hitungs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `import_ujians`
--
ALTER TABLE `import_ujians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instansis`
--
ALTER TABLE `instansis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama` (`nama`) USING BTREE;

--
-- Indexes for table `jadwals`
--
ALTER TABLE `jadwals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_instansis`
--
ALTER TABLE `jadwal_instansis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jawaban_peserta_ujian`
--
ALTER TABLE `jawaban_peserta_ujian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_peserta` (`kode_peserta`) USING BTREE,
  ADD KEY `kode_ujian` (`kode_ujian`) USING BTREE,
  ADD KEY `id_soal` (`id_soal`) USING BTREE;

--
-- Indexes for table `judul_inputs`
--
ALTER TABLE `judul_inputs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_peserta`
--
ALTER TABLE `master_peserta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_sertifikat`
--
ALTER TABLE `master_sertifikat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_kontaks`
--
ALTER TABLE `menu_kontaks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_registers`
--
ALTER TABLE `menu_registers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_statistiks`
--
ALTER TABLE `menu_statistiks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `pelaksanaan_ujian`
--
ALTER TABLE `pelaksanaan_ujian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengusulan_eformasis`
--
ALTER TABLE `pengusulan_eformasis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengusulan_eformasis_id_admin_ppk_foreign` (`id_admin_ppk`);

--
-- Indexes for table `perteks`
--
ALTER TABLE `perteks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesertas`
--
ALTER TABLE `pesertas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pesertas_id_dokumen_foreign` (`id_dokumen`),
  ADD KEY `pesertas_id_admin_ppk_foreign` (`id_admin_ppk`);

--
-- Indexes for table `peserta_instansis`
--
ALTER TABLE `peserta_instansis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta_jadwals`
--
ALTER TABLE `peserta_jadwals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta_perteks`
--
ALTER TABLE `peserta_perteks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta_sphus`
--
ALTER TABLE `peserta_sphus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta_ujian`
--
ALTER TABLE `peserta_ujian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_ujian` (`kode_ujian`) USING BTREE,
  ADD KEY `kode_peserta` (`kode_peserta`) USING BTREE,
  ADD KEY `remote_addr` (`remote_addr`) USING BTREE,
  ADD KEY `kode_peserta_2` (`kode_peserta`) USING BTREE,
  ADD KEY `password` (`password`) USING BTREE,
  ADD KEY `nip` (`nip`) USING BTREE,
  ADD KEY `remote_addr_2` (`remote_addr`) USING BTREE;

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regencies`
--
ALTER TABLE `regencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `regencies_province_id_index` (`province_id`);

--
-- Indexes for table `riwayat_hasil_verifs`
--
ALTER TABLE `riwayat_hasil_verifs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `riwayat_pembaharuan`
--
ALTER TABLE `riwayat_pembaharuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `riwayat_users`
--
ALTER TABLE `riwayat_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `simpan_berkas_portofolios`
--
ALTER TABLE `simpan_berkas_portofolios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `simpan_berkas_portofolios_history`
--
ALTER TABLE `simpan_berkas_portofolios_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sphus`
--
ALTER TABLE `sphus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_file_portofolios`
--
ALTER TABLE `status_file_portofolios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `status_peserta_inpassings`
--
ALTER TABLE `status_peserta_inpassings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_usulans`
--
ALTER TABLE `surat_usulans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_usulans_int`
--
ALTER TABLE `surat_usulans_int`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `text_runnings`
--
ALTER TABLE `text_runnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_banner_master`
--
ALTER TABLE `t_banner_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_kerjas`
--
ALTER TABLE `unit_kerjas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_info`
--
ALTER TABLE `app_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asesors`
--
ALTER TABLE `asesors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `berkas_prosedurs`
--
ALTER TABLE `berkas_prosedurs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detail_inputs`
--
ALTER TABLE `detail_inputs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dokumens`
--
ALTER TABLE `dokumens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grup_soal`
--
ALTER TABLE `grup_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hitungs`
--
ALTER TABLE `hitungs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `import_ujians`
--
ALTER TABLE `import_ujians`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwals`
--
ALTER TABLE `jadwals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwal_instansis`
--
ALTER TABLE `jadwal_instansis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jawaban_peserta_ujian`
--
ALTER TABLE `jawaban_peserta_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `judul_inputs`
--
ALTER TABLE `judul_inputs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_peserta`
--
ALTER TABLE `master_peserta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_sertifikat`
--
ALTER TABLE `master_sertifikat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_kontaks`
--
ALTER TABLE `menu_kontaks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_registers`
--
ALTER TABLE `menu_registers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_statistiks`
--
ALTER TABLE `menu_statistiks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pelaksanaan_ujian`
--
ALTER TABLE `pelaksanaan_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengusulan_eformasis`
--
ALTER TABLE `pengusulan_eformasis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `perteks`
--
ALTER TABLE `perteks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pesertas`
--
ALTER TABLE `pesertas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peserta_instansis`
--
ALTER TABLE `peserta_instansis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peserta_jadwals`
--
ALTER TABLE `peserta_jadwals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peserta_perteks`
--
ALTER TABLE `peserta_perteks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peserta_sphus`
--
ALTER TABLE `peserta_sphus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peserta_ujian`
--
ALTER TABLE `peserta_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `riwayat_hasil_verifs`
--
ALTER TABLE `riwayat_hasil_verifs`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `riwayat_pembaharuan`
--
ALTER TABLE `riwayat_pembaharuan`
  MODIFY `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `riwayat_users`
--
ALTER TABLE `riwayat_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `simpan_berkas_portofolios`
--
ALTER TABLE `simpan_berkas_portofolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `simpan_berkas_portofolios_history`
--
ALTER TABLE `simpan_berkas_portofolios_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sphus`
--
ALTER TABLE `sphus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_file_portofolios`
--
ALTER TABLE `status_file_portofolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `surat_usulans`
--
ALTER TABLE `surat_usulans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `surat_usulans_int`
--
ALTER TABLE `surat_usulans_int`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `text_runnings`
--
ALTER TABLE `text_runnings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_banner_master`
--
ALTER TABLE `t_banner_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

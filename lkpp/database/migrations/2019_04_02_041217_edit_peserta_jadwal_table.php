<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPesertaJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peserta_jadwals', function(Blueprint $table) {
            $table->string('hasil_pleno')->nullable()->after('metode_ujian');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peserta_jadwals', function($table) {
            $table->dropColumn('hasil_pleno');
          });
    }
}

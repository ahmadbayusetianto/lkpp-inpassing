<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tanggal_verifikasi');
            $table->string('waktu_verifikasi');
            $table->string('tanggal_tes');
            $table->string('waktu_ujian');
            $table->string('lokasi_ujian');
            $table->string('jumlah_ruang');
            $table->string('kapasitas');
            $table->string('jumlah_unit');
            $table->string('batas_waktu_input');
            $table->string('publish_jadwal');
            $table->string('nama_cp_1');
            $table->string('no_telp_cp_1');
            $table->string('nama_cp_2');
            $table->string('no_telp_cp_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwals');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifikasiUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->string('no_ktp')->nullable()->after('nama_instansi');
            $table->string('alamat')->nullable()->after('no_telp');
            $table->string('admin_type')->nullable()->after('sk_admin_ppk');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('no_ktp');
            $table->dropColumn('alamat');
            $table->dropColumn('admin_type');
          });
    }
}

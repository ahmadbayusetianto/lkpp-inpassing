<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_peserta');
            $table->string('surat_bertugas_pbj')->nullable();
            $table->string('bukti_sk_pbj')->nullable();
            $table->string('ijazah_terakhir')->nullable();
            $table->string('sk_kenaikan_pangkat_terakhir')->nullable();
            $table->string('sk_pengangkatan_jabatan_terakhir')->nullable();
            $table->string('sertifikasi_dasar_pbj')->nullable();
            $table->string('skp_dua_tahun_terakhir')->nullable();
            $table->string('surat_ket_tidak_dijatuhi_hukuman')->nullable();
            $table->string('formulir_kesediaan_mengikuti_inpassing')->nullable();
            $table->string('sk_pengangkatan_cpns')->nullable();
            $table->string('sk_pengangkatan_pns')->nullable();
            $table->string('ktp')->nullable();
            $table->string('pas_foto_3_x_4')->nullable();
            $table->string('surat_pernyataan_bersedia_jfppbj')->nullable();
            $table->string('bukti_portofolio')->nullable();
            $table->string('surat_usulan_mengikuti_inpassing')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumens');
    }
}

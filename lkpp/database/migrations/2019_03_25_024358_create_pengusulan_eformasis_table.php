<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengusulanEformasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengusulan_eformasis', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('id_admin_ppk');
            $table->integer('pertama');
            $table->integer('muda');
            $table->integer('madya');
            $table->string('status_pertama')->nullable();
            $table->string('status_muda')->nullable();
            $table->string('status_madya')->nullable();
            $table->string('deskripsi_pertama')->nullable();
            $table->string('deskripsi_muda')->nullable();
            $table->string('deskripsi_madya')->nullable();
            $table->string('dokumen_hasil_perhitungan_abk');
            $table->string('surat_usulan_rekomendasi');
            $table->string('no_surat_usulan_rekomendasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengusulan_eformasis');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifikasiPortofoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verifikasi_portofolios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_peserta');
            $table->integer('id_peserta_jadwal');
            $table->integer('id_jadwal');
            $table->string('nama_peserta')->nullable();
            $table->string('asesor_verifikasi');
            $table->string('rekomendasi')->nullable();
            $table->string('asesor_pleno_1')->nullable();
            $table->string('asesor_pleno_2')->nullable();
            $table->string('keputusan_pleno');
            $table->string('admin_post')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifikasi_portofolios');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelasiPesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pesertas', function (Blueprint $table) {
            $table->integer('id_dokumen')->unsigned()->change();
            $table->foreign('id_dokumen')->references('id')->on('dokumens')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesertas', function(Blueprint $table) {
            $table->dropForeign('templates_id_dokumen_foreign');
            $table->dropIndex('templates_id_dokumen_foreign');
            $table->integer('id_dokumen')->change();
        });
    }
}

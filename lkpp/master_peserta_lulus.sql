-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2020 at 11:18 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lkpp_dev_inp`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_peserta_lulus`
--

CREATE TABLE `master_peserta_lulus` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `jabatan` text DEFAULT NULL,
  `jabatan_english` varchar(255) DEFAULT NULL,
  `posisi_ttd` int(11) DEFAULT NULL,
  `align` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_peserta_lulus`
--

INSERT INTO `master_peserta_lulus` (`id`, `nama`, `nip`, `jabatan`, `jabatan_english`, `posisi_ttd`, `align`, `created_at`, `updated_at`) VALUES
(1, 'Dwi Wahyuni Kartianingsih', NULL, 'Direktur Sertifikasi Profesi, selaku Ketua Lembaga Sertifikasi Profesi LKPP', NULL, 3, 1, '2020-06-05 07:45:45', '2020-06-05 07:58:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_peserta_lulus`
--
ALTER TABLE `master_peserta_lulus`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_peserta_lulus`
--
ALTER TABLE `master_peserta_lulus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
